import RatingStars from "../components/RatingStars"
import { IonAvatar, IonItem } from "@ionic/react"
import "./ReviewComment.scss"

const ReviewComment: React.FC = () => {
  return (
    <IonItem
      lines="none"
      className="ion-no-padding reviewComment ion-align-items-start"
    >
      <IonAvatar slot="start" className="ion-margin-right">
        <img src="/assets/avatar.svg" alt="avatar" />
      </IonAvatar>
      <div>
        <div className="userName">Ginny</div>
        <div className="ratingContainer">
          <RatingStars rating={"4"} width={12} margin={2.2}></RatingStars>
          <span>01 Aug 2021</span>
        </div>
        <div className="comment">
        Easy to locate with clear signage. Overall great experience with efficient and experienced staffs. Not many people or crowed in weekdays and maybe in the morning. Would definitely come back again and maybe could try out other activities like stand up paddle, etc. Highly recommend and make sure to apply sunscreen.
        </div>
      </div>
    </IonItem>
  )
}

export default ReviewComment
