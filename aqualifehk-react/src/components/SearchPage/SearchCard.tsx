import {
  IonCard,
  IonCardContent,
  IonCardHeader,
  IonCardSubtitle,
  IonCardTitle,
  IonIcon,
  IonImg,
  IonLabel,
  IonSkeletonText,
  IonText,
} from "@ionic/react"
import { star } from "ionicons/icons"
import "./SearchCard.scss"
import { DTO } from "shared"
import { useState } from "react"

type SearchCardProps = {
  result: DTO.SearchActivity
}

const SearchCard: React.FC<SearchCardProps> = (props: SearchCardProps) => {
  const activity = props.result
  const [imgLoaded, setImgLoaded] = useState(false)
  const onImgLoaded = () => {
    setImgLoaded(true)
  }
  return (
    <IonCard className="ionCard" button={true} routerLink={`/activity/${props.result.id}`}>
      <IonImg
        src={`/assets/${activity.image_url}`}
        onIonImgDidLoad={() => onImgLoaded()}
      ></IonImg>
      <IonSkeletonText hidden={imgLoaded} />
      <IonCardHeader>
        <IonCardSubtitle>
          {activity.category}．{activity.location}
        </IonCardSubtitle>
        <IonCardTitle>{activity.name}</IonCardTitle>
      </IonCardHeader>

      <IonCardContent>
        <div className="ratingContainer">
          <div className="chip">
            <IonIcon icon={star}></IonIcon>
            <IonText>{activity.rating}</IonText>
          </div>
          <IonLabel>({activity.review_count} Reviews)</IonLabel>
        </div>
      </IonCardContent>
    </IonCard>
  )
}

export default SearchCard
