import { IonButton, IonIcon, IonText } from "@ionic/react"
import { IconContext } from "react-icons"
import { MdChecklist } from "react-icons/md"
import { RiFilterOffLine } from "react-icons/ri"
import { BiSort } from "react-icons/bi"
import { navigateCircleOutline } from "ionicons/icons"

import "./SearchFilters.scss"

const SearchFilters: React.FC = () => {
  return (
    <div className="searchFilter">
      <IonButton fill="clear" className="ion-no-padding">
        <IonIcon icon={navigateCircleOutline} color="primary"></IonIcon>
        <IonText className="ion-text-capitalize buttonText" color="primary">
          Location
        </IonText>
      </IonButton>

      <IonButton fill="clear" className="ion-no-padding">
        <IconContext.Provider
          value={{ size: "22px", color: "var(--ion-color-primary)" }}
        >
          <MdChecklist />
        </IconContext.Provider>
        <IonText className="ion-text-capitalize buttonText" color="primary">
          Categories
        </IonText>
      </IonButton>

      <IonButton fill="clear" className="ion-no-padding">
        <IconContext.Provider
          value={{ size: "20px", color: "var(--ion-color-primary)" }}
        >
          <RiFilterOffLine />
        </IconContext.Provider>
        <IonText className="ion-text-capitalize buttonText" color="primary">
          Filter
        </IonText>
      </IonButton>

      <IonButton fill="clear" className="ion-no-padding">
        <IconContext.Provider
          value={{ size: "20px", color: "var(--ion-color-primary)" }}
        >
          <BiSort />
        </IconContext.Provider>
        <IonText className="ion-text-capitalize buttonText" color="primary">
          Sort
        </IonText>
      </IonButton>
    </div>
  )
}

export default SearchFilters
