import { IonSpinner, IonText } from "@ionic/react"
import { useSelector } from "react-redux"
import { RootState } from "../../redux/state"
import SearchCard from "../SearchPage/SearchCard"
import "./SearchList.scss"

import { formatUnitCount } from "../../helpers/format"

const SearchList: React.FC = () => {
  const searchState = useSelector((state: RootState) => state.searchActivity)

  const isLoading = searchState.isLoading
  const isFirstPage = searchState.nextPage === 1
  const searchResult = searchState.searchResult
  const resultCount = searchResult.length

  return (
    <>
      <div className="searchList" hidden={isLoading && isFirstPage}>
        <div className="resultCount">
          {formatUnitCount(resultCount, "Result")}
        </div>
        {searchResult.map((result) => {
          return <SearchCard key={result.id} result={result}></SearchCard>
        })}
      </div>

      <div
        className={
          isFirstPage ? "spinnerContainer" : "spinnerContainer nextPage"
        }
        hidden={searchState.reachedEnd}
      >
        <IonSpinner color="primary"></IonSpinner>
        <IonText color="primary">Loading...</IonText>
      </div>
    </>
  )
}

export default SearchList
