import {
  IonButton,
  IonIcon,
} from "@ionic/react"

import { dateToDateTimeString } from "../../helpers/dateTimeFormat"
import { formatPackageString } from "../../helpers/format"
import { addCircleOutline } from "ionicons/icons"

import './PackageSection.scss'
import { PackageInfo } from "../../redux/BookingOption/state"

type PackageSectionProps = {
  name: string
  packages: PackageInfo[]
  date: Date
}

const PackageSection: React.FC<PackageSectionProps> = (props) => {
  return (
    <section className="section package"> 
      <div className="title">{props.name}</div>
      <div className="content info">
        <div style={{whiteSpace: 'pre-wrap'}}>{formatPackageString(props.packages)}</div>
        <div>{dateToDateTimeString(props.date, "date")}</div>
      </div>
      <div className="content price">
        HK$ {calculateTotalPrice(props.packages) / 100}
      </div>
      {/* <IonButton fill="outline">
        <IonIcon icon={addCircleOutline} className="ion-margin-end"></IonIcon>
        <div>Enter booking info</div>
      </IonButton> */}
    </section>
  )
}

export default PackageSection

function calculateTotalPrice(packages: PackageInfo[]) {
  return packages.reduce((totalPrice, _pack) => {
    return totalPrice + (_pack.unitPrice * _pack.quantity)
  }, 0)
}


