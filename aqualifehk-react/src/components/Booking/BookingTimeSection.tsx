import { IonChip } from "@ionic/react"

import "./BookingTimeSection.scss"

type Props = {
  id: number,
  name: string,
  selected: boolean
  onSelect: (id: number) => void
  selectable: boolean
}

const BookingTimeSection: React.FC<Props> = (props: Props) => {
  function generateClassName() {
    let baseName = "section"
    if (props.selected) baseName = baseName + " selected"
    if (!props.selectable) baseName = baseName + " disabled"
    return baseName
  }

  return (
    <IonChip color={props.selectable? "primary" : "medium"}
      outline={true} 
      onClick={() => props.onSelect(props.id)} 
      className={generateClassName()} >
      {props.name}
    </IonChip>
  )
}

export default BookingTimeSection
