import { IonText } from "@ionic/react"
import "./BulletPoint.scss"

type BulletPointProps = {
  content: string
}

const BulletPoint: React.FC<BulletPointProps> = (props) => {
  return (
    <div className="bullet-point">
      <div></div>
      <IonText className="ion-text-wrap">{props.content}</IonText>
    </div>
  )
}

export default BulletPoint
