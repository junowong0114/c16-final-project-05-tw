import {
  IonCard,
  IonChip,
  IonCardContent,
  IonCardHeader,
  IonCardSubtitle,
  IonCardTitle,
  IonIcon,
  IonItem,
  IonLabel,
  IonText as div,
  IonTitle,
  IonThumbnail,
  IonImg,
} from "@ionic/react"
import { locationOutline, star } from "ionicons/icons"
import { dateOrTimeFormat, dateToDateTimeString } from "../../helpers/dateTimeFormat"
import { BookingStatus, order } from "shared"
// import image from "../img/activityImg.png"
import styles from "./OrderItem.module.scss"

type OrderProps = {
  order: any
}

const OrderItem: React.FC<OrderProps> = (props: OrderProps) => {
  const link = "/orderDetails/?id=" + props.order.bookingId

  return (
    <IonCard button={true} routerLink={link}>
      <div className={styles.reviewStatusBar}>
        <div className={styles.orderId}>Order id: {props.order.bookingId}</div>
        <div className={styles.orderStatus}>
          {BookingStatus[props.order.status]}
        </div>
      </div>
      <IonCardHeader className={styles.reviewHeader}>
        <IonThumbnail className={styles.reviewImg}>
          <IonImg
            src={`/assets/${props.order.image}`}
          ></IonImg>
        </IonThumbnail>
        <div className={styles.reviewDetails}>
          <IonCardTitle className={styles.reviewTitle}>
            {props.order.activityName}
          </IonCardTitle>
          <div className={styles.reviewGroup}>
            <div className={styles.reviewItem}>
              {dateOrTimeFormat('date', new Date(props.order.date * 1000))}
            </div>
            <div className={styles.reviewItem}>
              {props.order.packages.map((pack: any,i:number) => {
                return <div key = {i}>{pack.name}</div>
              })}
            </div>
            <div className={styles.reviewItem}>{props.order.address}</div>
          </div>
        </div>
      </IonCardHeader>
      <IonCardContent>
        <div className={styles.commentAndPrice}>
          <div className={styles.price}>${(props.order.price/100)}</div>
        </div>
      </IonCardContent>
    </IonCard>
  )
}

export default OrderItem
