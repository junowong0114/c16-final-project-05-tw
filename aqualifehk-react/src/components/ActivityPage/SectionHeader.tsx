import { IonItem } from "@ionic/react"
import "./SectionHeader.scss"

type SectionHeaderProps = {
  content: string
}

const SectionHeader: React.FC<SectionHeaderProps> = (props) => {
  return (
    <IonItem lines="none" className="ion-no-padding sectionHeader">
      <div>{props.content}</div>
    </IonItem>
  )
}

export default SectionHeader
;``
