import { GoogleMap, LoadScript, Marker, useJsApiLoader } from '@react-google-maps/api'
import { useState } from 'react'
import { getGoogleAPIKey } from "../../helpers/api"

const containerStyle = {
  width: '400px',
  height: '400px',
  borderRadius: '5px'
};

const center = {
  lat: 22.364901,
  lng: 114.260644
};

type Props = {
  lat: number
  long: number
}

function ActivityMap() {
  const apiKey = getGoogleAPIKey()
  return (
    <LoadScript
      googleMapsApiKey={apiKey}
    >
      <GoogleMap
        mapContainerStyle={containerStyle}
        center={center}
        zoom={15}
      >
        { /* Child components, such as markers, info windows, etc. */ }
        <>
          <Marker position={center} />
        </>
      </GoogleMap>
    </LoadScript>
  )
}

export default ActivityMap