import { IonChip, IonItem } from "@ionic/react"
import { BsFillPlusSquareFill, BsDashSquareFill } from "react-icons/bs"
import "./PackageRow.scss"

export type PackageRowProp = {
  id: number
  name: string
  quantity: number
  onPlusClick: (id: number) => void
  onMinusClick: (id: number) => void
  fulled: boolean
  hideQuantity?: boolean
}

const PackageRow: React.FC<PackageRowProp> = (props) => {
  return (
    <IonItem lines="none" 
      className="ion-no-padding packageRow" >
      
      <IonChip
        className="packageButton"
        color="secondary"
        outline={true}
        slot="start"
      >
        {props.name}
      </IonChip>
      <div
        slot="end"
        className={props.fulled? "quantityBtn fulled" : "quantityBtn"}
        onClick={props.fulled? () => {} : () => props.onMinusClick(props.id)}
        hidden={props.hideQuantity}
      >
        <BsDashSquareFill />
      </div>
      <span slot="end" className={props.fulled? "quantityDisplay fulled" : "quantityDisplay"} hidden={props.hideQuantity}>
        {props.quantity}
      </span>
      <div
        slot="end"
        className={props.fulled? "quantityBtn fulled" : "quantityBtn"}
        onClick={props.fulled? () => {} : () => props.onPlusClick(props.id)}
        hidden={props.hideQuantity}
      >
        <BsFillPlusSquareFill />
      </div>

    </IonItem>
  )
}

export default PackageRow
