import {
  IonCard,
  IonCheckbox,
  IonIcon,
  IonRippleEffect
} from "@ionic/react"
import NameTag from "./NameTag"
import { useEffect, useState } from "react"
import { DTO } from "shared"
import { dateToDateTimeString } from "../../helpers/dateTimeFormat"
import "./TimeVoteOption.scss"
import { useSelector } from "react-redux"
import { RootState } from "../../redux/state"
import { trashOutline } from "ionicons/icons"

type timeOptionDetail = DTO.EventDetail['times'][number]

type Props = {
  index: number
  info: timeOptionDetail
  onSelect: (id: number, action: "select" | "unselect") => void
  onDelete?: () => void
  isVoting: boolean
}

const TimeVoteOption: React.FC<Props> = (props) => {
  const [isSelected, setIsSelected] = useState<boolean>(false)
  const userId = useSelector((state: RootState) => state.auth.user?.id)

  useEffect(() => {
    if (props.info.voter_ids.some(id => id === userId)) {
      setIsSelected(true)
    }
  }, [])

  function onSelect() {
    const action = isSelected ? "unselect" : "select"
    props.onSelect(props.info.time_vote_id, action)
    setIsSelected(() => !isSelected)
  }

  return (
    <IonCard className={props.isVoting ? "timeVoteOption" : "timeVoteOption create"}>
      <div className="cartInner">
        <div className="d-flex ion-align-items-center">
          <div slot="start" className="timeOptionNumber">{props.index + 1}</div>
          <div className="timeOptionInfo">
            <div className="timeOptionDate">
              {getTimeOptionDate(props.info)}
              <span hidden={!props.isVoting}> ({props.info.vote_count}) votes</span>
            </div>
            <div className="timeOptionTime">{getTimeOptionTime(props.info)}</div>
          </div>
        </div>
        <IonCheckbox
          slot="end"
          onIonChange={() => onSelect()}
          value={isSelected ? 'on' : ''} 
          hidden={!props.isVoting}/>
        <button 
          className="trashBtn ion-activatable ripple-parent"
          hidden={props.isVoting}
          onClick={() => props.onDelete? props.onDelete() : null}  
        >
          <IonIcon icon={trashOutline}></IonIcon>
          <IonRippleEffect></IonRippleEffect>
        </button>
      </div>
      <div className="nameTagContainer" hidden={!props.isVoting}>
        {props.info.voter_names.map((row, i) => {
          if (!row.first_name || !row.last_name) return null
          return (
            <NameTag
              key={props.info.voter_ids[i]}
              firstName={row.first_name}
              lastName={row.last_name}
            />
          )
        })}
      </div>
    </IonCard>
  )
}

export default TimeVoteOption

function getTimeOptionDate(info: timeOptionDetail) {
  return (
    dateToDateTimeString(new Date(info.start_time_unix * 1000), "date")
  )
}

function getTimeOptionTime(info: timeOptionDetail) {
  if (!info.end_time_unix) return "Full day";
  return (
    dateToDateTimeString(new Date(info.start_time_unix * 1000), "time") + " - " +
    dateToDateTimeString(new Date(info.end_time_unix * 1000), "time")
  )
}