import {
  IonCard,
  IonCheckbox,
  IonIcon,
  IonRippleEffect,
} from "@ionic/react"
import { location, trashOutline } from "ionicons/icons"
import { useEffect, useState } from "react"
import { useSelector } from "react-redux"
import { DTO } from "shared"
import { cropText } from "../../helpers/format"
import { RootState } from "../../redux/state"
import "./ActivityVoteOption.scss"
import NameTag from "./NameTag"

type activityOptionDetail = DTO.EventDetail['activities'][number]

type Props = {
  index: number
  info: activityOptionDetail
  onSelect: (id: number, action: "select" | "unselect") => void
  isVoting: boolean
}

const ActivityVoteOption: React.FC<Props> = (props) => {
  const [windowWidth, setWindowWidth] = useState<number>(0)
  const [isSelected, setIsSelected] = useState<boolean>(false)
  const userId = useSelector((state: RootState) => state.auth.user?.id)

  useEffect(() => {
    setWindowWidth(window.innerWidth)

    if (props.info.voter_ids.some(id => id === userId)) {
      setIsSelected(true)
    }
  }, [])

  function onSelect() {
    const action = isSelected ? "unselect" : "select"
    props.onSelect(props.info.activity_vote_id, action)
    setIsSelected(() => !isSelected)
  }

  return (
    <IonCard className={props.isVoting? "activityVoteOption": "activityVoteOption create"}>
      <div className="cartInner">
        <div className="d-flex ion-align-items-center">
          <div slot="start" className="activityOptionNumber">{props.index + 1}</div>
          <div className="activityOptionInfo">
            <div className="activityOptionDate">
              {props.info.activity_name}
              <span hidden={!props.isVoting}> ({props.info.vote_count} votes)</span>
            </div>
            <div className="activityOptionLocation">
              <IonIcon color="primary" icon={location} />
              {cropText(props.info.activity_address, (windowWidth - 162) * 0.14)}
            </div>
          </div>
        </div>
        <IonCheckbox
          slot="end"
          onIonChange={() => onSelect()}
          value={isSelected ? 'on' : ''}
          hidden={!props.isVoting} />
        <button
          className="trashBtn ion-activatable ripple-parent"
          hidden={props.isVoting}>
          <IonIcon icon={trashOutline}></IonIcon>
          <IonRippleEffect></IonRippleEffect>
        </button>
      </div>
      <div className="nameTagContainer">
        {props.info.voter_names.map((row, i) => {
          if (!row.first_name || !row.last_name) return null
          return (
            <NameTag
              key={props.info.voter_ids[i]}
              firstName={row.first_name}
              lastName={row.last_name}
            />
          )
        })}
      </div>
    </IonCard>
  )
}

export default ActivityVoteOption