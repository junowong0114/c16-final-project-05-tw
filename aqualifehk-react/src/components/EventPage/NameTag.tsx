import {
  IonCard,
} from "@ionic/react"
import "./NameTag.scss"

type Props = {
  firstName: string
  lastName: string
}

const NameTag: React.FC<Props> = (props) => {
  return (
    <div className="nameTag">
      {props.firstName + " " + props.lastName}
    </div>
  )
}

export default NameTag