import {
  IonButton,
  IonButtons,
  IonCard,
  IonCardTitle,
  IonIcon,
  IonImg,
  IonThumbnail,
} from "@ionic/react"
import { cropNameString, cropText } from "../../helpers/format"
import { calendarOutline, closeCircle, peopleOutline, personOutline, shareOutline, shareSocial, trashOutline } from "ionicons/icons"
import { DTO } from "shared"
import "./EventCard.scss"
import { useHistory } from "react-router"
import { useDispatch } from "react-redux"
import { setEventList, setSelectedEventId } from "../../redux/eventPage/action"
import { dateOrTimeFormat, dateToDateTimeString } from "../../helpers/dateTimeFormat"
import { useEffect } from "react"

type EventCardProps = {
  ios: boolean
  windowWidth: number,
  eventDetail: DTO.EventDetail,
  isEditing: boolean,
}

const EventCard: React.FC<EventCardProps> = (props) => {
  const ownerName = props.eventDetail.owner_name.first_name + " " + props.eventDetail.owner_name.last_name
  const participantNames = props.eventDetail.participant_names?.reduce((outputStr, row, i) => {
    return outputStr +
      row.first_name + " " + row.last_name +
      ((i + 1) === props.eventDetail.participant_names.length? "" : ", ")
  }, "")
  const history = useHistory()
  const dispatch = useDispatch()

  function onCardClick(e: React.MouseEvent<HTMLIonCardElement, MouseEvent>) {
    const targetTagName = (e.target as HTMLElement).tagName
    dispatch(setSelectedEventId(props.eventDetail.event_id))

    if (targetTagName !== "ION-BUTTONS" && targetTagName !== "ION-BUTTON") {
      history.push(`/eventDetails/${props.eventDetail.event_id}`)
    }
  }

  function onShareBtnClick() {
    history.push('/share_event')
  }

  return (<IonCard onClick={(e) => onCardClick(e)}>
    <div className="d-flex cardInner">
      <IonThumbnail slot="start">
        <IonImg src="/assets/kayak.webp" />
      </IonThumbnail>
      <div className="cardEventInfo">
        <IonCardTitle>{props.eventDetail.event_name? cropText(props.eventDetail.event_name, (props.windowWidth - 162) * 0.11) : "null"}</IonCardTitle>
        <div className="cardInfoRow">
          <IonIcon icon={personOutline} />
          {ownerName}
        </div>
        <div className="cardInfoRow">
          <IonIcon icon={peopleOutline} />
          {participantNames? cropNameString(participantNames, (props.windowWidth - 162) * 0.11) : ""}
        </div>
        <div className="cardInfoRow">
          <IonIcon icon={calendarOutline} />
          {props.eventDetail.unix_confirmed_date? 
            dateToDateTimeString(new Date(props.eventDetail.unix_confirmed_date * 1000), "date") : 
            "TBD"}
        </div>
      </div>

      <IonButtons className="shareBtn">
        <IonButton onClick={() => onShareBtnClick()} >
          <IonIcon icon={props.ios ? shareOutline : shareSocial} />
        </IonButton>
      </IonButtons>

      <IonButtons className="deleteBtn" hidden={!props.isEditing}>
        <IonButton>
          <IonIcon icon={closeCircle} color="danger" />
        </IonButton>  
      </IonButtons>
    </div>
  </IonCard>)
}

export default EventCard