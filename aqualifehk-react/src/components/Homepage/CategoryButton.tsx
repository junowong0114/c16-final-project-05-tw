import { IonRippleEffect } from "@ionic/react"
import { useHistory } from "react-router"
import "./CategoryButton.scss"

type Props = {
  name: string
  onClick: () => void
  isLast: boolean
}

const CategoryButton: React.FC<Props> = (props) => {
  return (
    <div className="categoryButton" onClick={() => props.onClick()}>
      <div className={"button ion-activatable ripple-parent " + (props.isLast? "last" : "")}>
        <img src={`/assets/category_icon/${props.name}.png`} alt={`${props.name} icon`} />
        <IonRippleEffect></IonRippleEffect>
      </div>
      <div className="buttonName">{props.name}</div>
    </div>
  )
}

export default CategoryButton