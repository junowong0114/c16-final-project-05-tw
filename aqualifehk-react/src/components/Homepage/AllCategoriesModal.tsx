import { IonButton, IonButtons, IonContent, IonHeader, IonItem, IonPage, IonTitle, IonToolbar } from "@ionic/react";
import './AllCategoriesModal.scss'

type Props = {
  onDismiss: () => void
}

const AllCategoriesModal: React.FC<Props> = (props: Props) => {
  const categories = [
    'Angling',
    'BananaBoat',
    'Flyboarding',
    'Jet Ski',
    'Kayaking',
    'KiteSurfing',
    'Parasailing',
    'Rafting',
    'Snorkeling',
    'Scuba diving',
    'Surfing',
    'Swimming',
    'Waterskiing',
    'Water polo',
    'Windsurfing',
    'Stand up pedal boarding',
    'Wake boarding',
    'Wake surfing'
  ]

  function categoriesToObj(categories: string[]) {
    let result = [];
    for (let i = 1; i <= categories.length; i++) {
      result.push({
        id: i,
        image: './',
        category: categories[i - 1]
      })
    }
    return result;
  }

  const categoriesArray = categoriesToObj(categories)

  return (
    <IonPage>

      <IonHeader>
        <IonToolbar>
          <IonButtons className='cancel-button' slot="start">
            <IonButton onClick={props.onDismiss}> Back</IonButton>
          </IonButtons>
          <IonTitle>All categories</IonTitle>
        </IonToolbar>
      </IonHeader>

      <IonContent id='categoriesModal' fullscreen>
        {categoriesArray.map((item) => {
          return (
            <CategoriesDisplay
              key={item.id}
              category={item.category}
              onClick={props.onDismiss} />
          )
        })}
      </IonContent>

    </IonPage>
  )
}

export default AllCategoriesModal;

type CategoryProps = {
  image?: string,
  category: string
  onClick: () => void
}
const CategoriesDisplay: React.FC<CategoryProps> = (props: CategoryProps) => {
  return (
    <IonItem routerLink={'/search/' + props.category} onClick={props.onClick}>
      {props.category}
    </IonItem>
  )
}