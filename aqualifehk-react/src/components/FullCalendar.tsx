import { useEffect, useState } from "react"
import Calendar from "react-calendar"
import { DTO } from "shared"

import "./FullCalendar.scss"

type FullCalendarProps = {
  packages: DTO.Package[]
  displayId: number
  idIndexMap: { [k: string]: number }
  maxDate: Date
  onChange: (date: Date) => void
}

const FullCalendar: React.FC<FullCalendarProps> = (props) => {
  const [currentDate, setCurrentDate] = useState(new Date())
  const today = new Date((new Date()).setHours(0, 0, 0, 0))

  useEffect(() => {
    props.onChange(currentDate)
  }, [currentDate])

  function generatePriceTag(date: Date) {
    let basePrice: number
    let weekendPrice: number
    let priceObj: any

    if (props.idIndexMap && props.packages) {
      priceObj = props.packages[props.idIndexMap[props.displayId]].prices[0]
      basePrice = priceObj.base_price / 100
      weekendPrice = priceObj.weekend_price / 100

      const day = date.getDay()
      let isWeekend = day === 0 || day === 6
      if (isWeekend) {
        return weekendPrice
      } else {
        return basePrice
      }
    }
  }

  return (
    <Calendar
      className="calendar"
      locale="en-HK"
      minDetail="month"
      calendarType="US"
      maxDate={props.maxDate}
      minDate={new Date()}
      value={currentDate}
      onChange={setCurrentDate}
      showNeighboringMonth={false}
      tileContent={({ date, view }) => {
        return (
          <div className="tileContent">
            {view === "month" && date >= today && date < props.maxDate ?
              generatePriceTag(date) :
              "-"
            }
          </div>
        )
      }
      }
    />
  )
}

export default FullCalendar

