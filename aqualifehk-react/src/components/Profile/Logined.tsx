import { IonCard, IonItem, useIonAlert } from "@ionic/react";

type LoginProps = {
    onLogout: () => void,
}

const Logined: React.FC<LoginProps> = (props: LoginProps) => {
    const [present] = useIonAlert();


    return (
        <IonCard>
            <IonItem lines='none' button >
                My Account
            </IonItem>
            <IonItem lines='none' button >
                Change Password
            </IonItem>
            <IonItem lines='none' button onClick={() =>
                present({
                    message: 'Are you sure you want to logout now?',
                    buttons: [
                        'Cancel',
                        { text: 'Ok', handler: (d) => props.onLogout() },
                    ]
                })}>
                Logout
            </IonItem>
        </IonCard>
    )
}

export default Logined;