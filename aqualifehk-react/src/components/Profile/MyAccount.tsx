import { IonBackButton, IonButton, IonButtons, IonContent, IonHeader, IonInput, IonItem, IonItemDivider, IonList, IonPage, IonTitle, IonToolbar } from "@ionic/react"
import { useEffect, useState } from "react"
import { useDispatch, useSelector } from "react-redux"
import { getAPIServer } from "../../helpers/api"
import { RootState } from "../../redux/state"
import './MyAccount.scss'

const MyAccount: React.FC = () => {
    const dispatch = useDispatch();
    const [text, setText] = useState('')
    
    useEffect(() => {

    }, [])


    return (
        <IonPage className="my-account">
            <IonHeader translucent={true}>
                <IonToolbar>
                    <IonButtons slot="start">
                        <IonBackButton defaultHref="/" />
                    </IonButtons>
                    <IonTitle>My Info</IonTitle>
                    <IonButtons slot="end">
                        <IonButton disabled>
                            Edit
                        </IonButton>
                    </IonButtons>
                </IonToolbar>
            </IonHeader>

            <IonContent fullscreen color="light">
                <IonList>
                    <IonItem>
                        <IonInput value={text} placeholder="Enter Input" onIonChange={e => setText(e.detail.value!)}></IonInput>
                    </IonItem>
                </IonList>
            </IonContent>
        </IonPage>
    )
}

export default MyAccount;