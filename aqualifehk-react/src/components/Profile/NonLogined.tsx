import { IonCard, IonItem } from "@ionic/react"

const NonLogined: React.FC = () => {
    return (
        <IonCard>
            <IonItem button lines="none" routerLink="/register">
                Register as user
            </IonItem>
            <IonItem button lines="none" routerLink="/login">
                Login
            </IonItem>
        </IonCard>
    )
}

export default NonLogined;