import { IonBackButton, IonButton, IonButtons, IonCard, IonCol, IonContent, IonHeader, IonInput, IonItem, IonLabel, IonList, IonPage, IonRow, IonSelect, IonSelectOption, IonTitle, IonToolbar } from "@ionic/react"
import { FormEvent, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router";
import { registerUser } from "shared";
import { registerErrorClear } from "../../redux/auth/action";
import { registerUserThunk } from "../../redux/auth/thunk";
import { RootState } from "../../redux/state";


const Register: React.FC = () => {
    const error = useSelector((state: RootState) => state.auth.error)
    const dispatch = useDispatch();
    const history = useHistory()
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('')
    const [gender, setGender] = useState('')
    const [phone, setPhone] = useState('')
    const [inputError, setInputError] = useState('')
    const [submitBtn, setSubmitBtn] = useState(false);


    useEffect(() => {
        if (submitBtn && error) {
            setInputError(error);
            setSubmitBtn(false);
        }
        else if (submitBtn && error === undefined) {

            console.log('success',error);
            (document.getElementById("submit-form") as HTMLFormElement).reset();
            setInputError('')
            history.goBack()
        }
    }, [error, submitBtn])

    const onRegister = (event: FormEvent<HTMLFormElement>) => {
        event.preventDefault()
        const userInfo: registerUser = {
            email: email,
            password: password,
            gender: gender,
            phone: phone
        }
        if (!email || !password) {
            return
        }

        if (phone.length !== 0 && phone.length !== 8) {
            setInputError('enter a correct phone number');
            return
        }
        setInputError('')
        dispatch(registerErrorClear());
        dispatch(registerUserThunk(userInfo));
        setSubmitBtn(true)
        // if (error) {
        //     console.log('error')
        //     return
        // } else {
        //     (document.getElementById("submit-form") as HTMLFormElement).reset();
        //     setInputError('')
        //     history.goBack()
        // }
    }

    return (
        <IonPage>
            <IonHeader translucent={true}>
                <IonToolbar>
                    <IonButtons slot="start">
                        <IonBackButton defaultHref="/" />
                    </IonButtons>
                    <IonTitle>Register</IonTitle>
                </IonToolbar>
            </IonHeader>

            <IonContent fullscreen >
                <form onSubmit={onRegister} id="submit-form">
                    <IonItem >
                        <IonLabel position="floating">Email</IonLabel>
                        <IonInput
                            type="email"
                            value={email}
                            onIonChange={(e) => setEmail(e.detail.value!)}
                            clear-input
                            inputmode="email"
                            required
                        />
                    </IonItem>
                    <IonItem>
                        <IonLabel position="floating">Password</IonLabel>
                        <IonInput
                            type="password"
                            value={password}
                            onIonChange={(e) => setPassword(e.detail.value!)}
                            clear-input
                            required
                            minlength={6}
                        />
                    </IonItem>
                    <IonItem lines='none'></IonItem>
                    <IonItem lines='none'></IonItem>
                    <IonItem>
                        <IonLabel>Gender</IonLabel>
                        <IonSelect
                            slot='end'
                            value={gender}
                            placeholder="Select One"
                            onIonChange={e => setGender(e.detail.value)}>
                            <IonSelectOption value="female">Female</IonSelectOption>
                            <IonSelectOption value="male">Male</IonSelectOption>
                            <IonSelectOption value="others">Others</IonSelectOption>
                        </IonSelect>
                    </IonItem>
                    <IonItem >
                        <IonLabel position="floating">Phone number</IonLabel>
                        <IonInput
                            type="tel"
                            value={phone}
                            onIonChange={(e) => setPhone(e.detail.value!)}
                            clear-input
                            size={8}
                        />
                    </IonItem>
                    <IonItem lines='none'>
                        <IonToolbar>
                            <div slot='end'>
                                {(inputError || error) ? (inputError || error) : null}
                            </div>
                        </IonToolbar>
                    </IonItem>
                    <IonToolbar className='login-button'>
                        <IonButton
                            type="submit"
                            expand="full"
                            className="ion-margin-top register-btn">
                            Register
                        </IonButton>
                    </IonToolbar>
                </form>
            </IonContent>



        </IonPage>
    )
}

export default Register;




