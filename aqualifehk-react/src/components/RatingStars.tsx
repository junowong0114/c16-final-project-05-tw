import { IonIcon } from "@ionic/react"
import { star, starOutline } from "ionicons/icons"
import { useEffect, useRef } from "react"
import "./RatingStars.scss"

type RatingStarsProps = {
  rating: string
  width: number
  margin: number
}

const RatingStars: React.FC<RatingStarsProps> = (props) => {
  const filledStarRef = useRef<HTMLDivElement>(null)

  useEffect(() => {
    const filledStarElem = filledStarRef.current
    if (!filledStarElem) return
    const widthOfStar = props.width
    const containerWidth =
      parseFloat(props.rating) * widthOfStar + parseInt(props.rating) * 2.2
    filledStarElem.style.width = containerWidth.toString() + "px"
  }, [props.rating])

  return (
    <div className="ratingStars">
      <div className="outlineStars stars">
        {Array.apply(null, Array(5)).map((foo, i) => {
          return <IonIcon icon={starOutline} color="primary" key={i}></IonIcon>
        })}
      </div>
      <div ref={filledStarRef} className="filledStars stars">
        {Array.apply(null, Array(5)).map((foo, i) => {
          return <IonIcon icon={star} color="primary" key={i}></IonIcon>
        })}
      </div>
    </div>
  )
}

export default RatingStars
