import {
  IonCard,
  IonCardContent,
  IonCheckbox,
  useIonAlert,
  IonButton,
  IonThumbnail,
  IonText,
} from "@ionic/react"
import { DTOBooking } from "../../helpers/DTOformat"
import { dateToDateTimeString } from "../../helpers/dateTimeFormat"
import "./CartCard.scss"
import { useDispatch } from "react-redux"
import { selectItem, unselectItem } from "../../redux/cart/action"
import { CheckboxChangeEventDetail } from "@ionic/core"
import { useHistory } from "react-router-dom"
import { deleteItemThunk } from "../../redux/cart/thunk"
import { errorToString, formatPackageString } from "../../helpers/format"
import { setGeneralMessage, setGeneralShowToast } from "../../redux/general/action"
import { ActivityInfo } from "../../redux/BookingOption/state"
import { getAPIServer } from "../../helpers/api"
import { setBookingId, setBookingOptions } from "../../redux/BookingOption/action"

type CartCardProps = {
  selected: boolean
  item: DTOBooking
}

const CartCard: React.FC<CartCardProps> = (props: CartCardProps) => {
  const [present] = useIonAlert()
  const dispatch = useDispatch()
  const history = useHistory()

  function onCheckboxClick(e: CustomEvent<CheckboxChangeEventDetail<any>>) {
    const checked = e.detail.checked
    if (checked) return dispatch(selectItem(props.item.id))
    return dispatch(unselectItem(props.item.id))
  }

  const onDeleteClick = (
    e: React.MouseEvent<HTMLIonButtonElement, MouseEvent>
  ) => {
    e.preventDefault()
    present({
      header: "Delete Confirmation",
      message: "Are you sure you want to delete this activity?",
      buttons: [
        "Cancel",
        {
          text: "Delete",
          handler: () => dispatch(deleteItemThunk(props.item.id)),
        },
      ],
      // onDidDismiss: () => console.log('did dismiss'),
    })
  }

  const onCardClick = (e: React.MouseEvent<HTMLIonCardElement, MouseEvent>) => {
    const targetTagName = (e.target as HTMLElement).tagName
    if (targetTagName !== "ION-CHECKBOX" && targetTagName !== "ION-BUTTON") {
      history.push(`/activity/${props.item.activityId}`)
    }
  }

  const onEditClick = async () => {
    // TODO: fetch data from API server
    try {
      const token = localStorage.getItem('token')

      let origin
      try {
        origin = getAPIServer()
      } catch (error) {
        console.log(error)
        return
      }

      const res = await fetch(`${origin}/bookingOptions/${props.item.id}`, {
        headers: {
          "Authorization": "Bearer " + token
        }
      })
      
      const result = (await res.json())

      if (result.success) {
        const newBookingOptions: ActivityInfo = {
          activityId: result.item.activityId,
          activityDate: result.item.activityDate * 1000,
          packages: result.item.packages,
          totalPrice: result.item.totalPrice,
          userRequirements: undefined,
          bookingDate: undefined,
        }
      dispatch(setBookingId(props.item.id))
      dispatch(setBookingOptions(newBookingOptions)) 
      history.push(`/book/edit/${result.item.activityId}`)
      }
    } catch (error) {
      dispatch(setGeneralShowToast(true))
      dispatch(setGeneralMessage(errorToString(error)))
      history.goBack()
    }

    return
  }

  return (
    <IonCard button={true} onClick={(e) => onCardClick(e)} className="cartCard">
      <IonCardContent className="ionCard">
        <div className="cardContent">
          <div className="checkboxThumbnail">
            <IonCheckbox
              checked={props.selected}
              onIonChange={(e) => onCheckboxClick(e)}
            ></IonCheckbox>
            <IonThumbnail>
              <img src={`/assets/${props.item.image_urls[0]}`} alt="activity thumbnail" />
            </IonThumbnail>
          </div>
          <div>
            <IonText className="activityTitle">
              {props.item.activityName}
            </IonText>
            <IonText className="activityContent">
              {dateToDateTimeString(props.item.date, "date")}
            </IonText>
            <IonText className="activityContent" style={{whiteSpace: "pre-wrap"}}>
              {formatPackageString(props.item.packages)}
            </IonText>
          </div>
        </div>

        <div className="cardToolbar">
          <IonButton fill="clear" size="small" onClick={() => onEditClick()}>
            Edit
          </IonButton>
          <IonButton
            fill="clear"
            size="small"
            onClick={(e) => onDeleteClick(e)}
          >
            Delete
          </IonButton>
          <div className="price">
            <span>$HKD {props.item.totalPrice / 100}</span>
          </div>
        </div>
      </IonCardContent>
    </IonCard>
  )
}

export default CartCard
