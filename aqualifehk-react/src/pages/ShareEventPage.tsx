import {
  IonBackButton,
  IonButtons,
  IonCard,
  IonContent,
  IonHeader,
  IonPage,
  IonTitle,
  IonToolbar,
} from "@ionic/react"
import { useEffect } from "react"
import { useDispatch, useSelector } from "react-redux"
import { useHistory } from "react-router"
import { setGeneralMessage, setGeneralShowToast } from "../redux/general/action"
import { RootState } from "../redux/state"
import "./ShareEventPage.scss"

const ShareEventPage: React.FC = () => {
  const selectedEventId = useSelector((state: RootState) => state.eventPage.selectedEventId)
  const eventList = useSelector((state: RootState) => state.eventPage.eventList)
  const dispatch = useDispatch()
  const history = useHistory()

  useEffect(() => {
    if (!selectedEventId || !eventList) {
      dispatch(setGeneralMessage('Unknown error: no selected event id / event list information'))
      dispatch(setGeneralShowToast(true))
      history.goBack()
    }
  }, [])

  return (
    <IonPage className="shareEventPage">
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
            <IonBackButton defaultHref="/"></IonBackButton>
          </IonButtons>
          <IonTitle>Share this event</IonTitle>
        </IonToolbar>
      </IonHeader>

      <IonContent fullscreen color="light">
        <IonCard>
          <div className="cardInner">
            <div className="content">
              Copy this invite code:
            </div>
            <div className="inviteCode">
              {eventList?.find(row => row.event_id === selectedEventId)?.invite_code}
            </div>
          </div>
        </IonCard>
      </IonContent>
    </IonPage>

  )
}

export default ShareEventPage