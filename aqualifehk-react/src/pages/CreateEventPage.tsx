import { ToggleChangeEventDetail } from "@ionic/core"
import {
  IonPage,
  IonHeader,
  IonToolbar,
  IonButtons,
  IonBackButton,
  IonTitle,
  IonContent,
  IonInput,
  IonIcon,
  IonRippleEffect,
  IonModal,
  IonToggle,
  IonButton,
  IonItem,
  IonLabel,
  IonDatetime,
  IonItemDivider,
  IonFooter,
  getConfig
} from "@ionic/react"
import { addOutline } from "ionicons/icons"
import { useEffect, useState } from "react"
import { useDispatch } from "react-redux"
import { useHistory } from "react-router"
import { DTO } from "shared"
import ActivityVoteOption from "../components/EventPage/ActivityVoteOption"
import TimeVoteOption from "../components/EventPage/TimeVoteOption"
import SearchCard from "../components/SearchPage/SearchCard"
import { getAPIServer } from "../helpers/api"
import { errorToString } from "../helpers/format"
import { setGeneralMessage, setGeneralShowToast } from "../redux/general/action"
import { ActivityCategory } from "shared"
import "./CreateEventPage.scss"

type TimeOption = {
  id: number
  start_time_unix?: number,
  end_time_unix?: number,
  full_day: boolean
}

type ActOption = {
  id: number
  activity_id: number
}

type createEventDTO = Omit<Omit<Partial<DTO.EventDetail>, 'activities'>, 'times'> & {
  activities: {
    activity_id: number
  }[],
  times: {
    start_time_unix: number,
    end_time_unix?: number,
  }[]
}

type activityOptionDetail = DTO.EventDetail['activities'][number]

const CreateEventPage: React.FC = () => {
  const dispatch = useDispatch()
  const ios = getConfig()?.get('mode') === 'ios'
  const history = useHistory()

  const [eventName, setEventName] = useState<string | undefined | null>()
  const [showTimeModal, setShowTimeModal] = useState<boolean>(false)
  const [showActModal, setShowActModal] = useState<boolean>(false)

  const [nextTimeOption, setNextTimeOption] = useState<TimeOption>({ id: 0, full_day: false })
  const [timeOptions, setTimeOptions] = useState<TimeOption[]>([])
  const [isFullDay, setIsFullDay] = useState<boolean>(false)

  const [nextActId, setNextActId] = useState<number>(1)
  const [actOptions, setActOptions] = useState<ActOption[]>([])

  useEffect(() => {
    console.log(actOptions, timeOptions, nextTimeOption)
  }, [actOptions, timeOptions, nextTimeOption])

  function onPlusClicked(type: "time" | "activity") {
    if (type === "time") {
      setShowTimeModal(true)
    }

    if (type === "activity") {
      setShowActModal(true)
    }
  }

  function onDeleteClick(type: "time" | "activity", id: number) {
    if (type === "time") {
      setTimeOptions(timeOptions.filter(row => row.id !== id))
      return
    }
  }

  // times modal
  function onTimeConfirmClicked() {
    try {
      if (!nextTimeOption) throw new Error()
      if (isFullDay && !nextTimeOption.start_time_unix) throw new Error()
      if (!isFullDay) {
        if (!nextTimeOption.start_time_unix || !nextTimeOption.end_time_unix) throw new Error()
        if (nextTimeOption.start_time_unix >= nextTimeOption.end_time_unix) throw new RangeError()
      }
    } catch (error) {
      let msg
      if (error instanceof RangeError) {
        msg = 'End Time must be later than start time'
      } else {
        msg = 'Please enter all information before proceeding.'
      }
      dispatch(setGeneralMessage(msg))
      dispatch(setGeneralShowToast(true))
      return
    }

    // format time option
    let end_time_unix_processed: number | undefined
    if (isFullDay) {
      end_time_unix_processed = undefined
    } else {
      end_time_unix_processed = getFullTime(nextTimeOption.end_time_unix as number, nextTimeOption.start_time_unix) / 1000
    }

    const newTimeOption: TimeOption = {
      ...nextTimeOption,
      start_time_unix: Math.floor(nextTimeOption.start_time_unix as number / 1000),
      end_time_unix: end_time_unix_processed
    }
    setTimeOptions([...timeOptions, newTimeOption])
    setNextTimeOption({ id: nextTimeOption.id + 1, full_day: false })
    setIsFullDay(false)
    setShowTimeModal(false)
  }

  function onSelectFullDay(e: CustomEvent<ToggleChangeEventDetail>) {
    const isFullDay = e.detail.checked as boolean
    setIsFullDay(isFullDay)
    setNextTimeOption({ ...nextTimeOption, full_day: isFullDay })

  }

  function onBackClicked() {
    setIsFullDay(false)
    setShowTimeModal(false)
    setShowActModal(false)
  }

  // activities modal
  function handleAddAct(act_id: number) {
    const nextActOption: ActOption = {
      id: nextActId,
      activity_id: act_id
    }

    setActOptions([...actOptions, nextActOption])
    setNextActId(nextActId + 1)
    setShowActModal(false)
  }

  function onCreateClick() {
    try {
      if (!eventName) throw new Error('Must input event name')
      if (timeOptions.length === 0) throw new Error('Must select at least one time option')
    } catch (error) {
      dispatch(setGeneralMessage(errorToString(error)))
      dispatch(setGeneralShowToast(true))
    }

    const info: createEventDTO = {
      event_name: eventName as string,
      activities: actOptions.map(row => {
        return {
          activity_id: row.activity_id
        }
      }),
      times: timeOptions.map(row => {
        return {
          start_time_unix: row.start_time_unix as number * 1000,
          end_time_unix: row.end_time_unix as number * 1000,
        }
      })
    }

    console.log(info)

    createEventToServer(info)
      .then(res => {
        if (res.success) {
          dispatch(setGeneralMessage('Create success'))
          dispatch(setGeneralShowToast(true))
          setShowTimeModal(false)
          history.goBack()
        } else {
          dispatch(setGeneralMessage('Unknown error during create event'))
          dispatch(setGeneralShowToast(true))
        }
      })
      .catch(e => {
        dispatch(setGeneralMessage(errorToString(e)))
        dispatch(setGeneralShowToast(true))
      })
  }

  return (
    <IonPage className="createEventPage">
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
            <IonBackButton defaultHref="/"></IonBackButton>
          </IonButtons>
          <IonTitle>Create Event</IonTitle>
          <IonButtons slot="end" hidden={!ios}>
            <IonButton onClick={() => onCreateClick()}>
              Confirm
            </IonButton>
          </IonButtons>
        </IonToolbar>
      </IonHeader>

      <IonContent fullscreen color="light">
        <IonModal isOpen={showTimeModal} cssClass="timeModal">
          <IonHeader>
            <IonToolbar>
              <IonButtons slot="start">
                <IonButton onClick={() => onBackClicked()}>
                  Back
                </IonButton>
              </IonButtons>
              <IonButtons slot="end">
                <IonButton onClick={() => onTimeConfirmClicked()}>
                  Confirm
                </IonButton>
              </IonButtons>
              <IonTitle>Add time option</IonTitle>
            </IonToolbar>
          </IonHeader>

          <IonContent>
            <IonItem>
              <IonLabel>Event Date</IonLabel>
              <IonDatetime
                displayFormat="DD MMM YYYY"
                pickerFormat="DD MMM YYYY"
                min={(new Date()).getFullYear().toString()}
                max={((new Date()).getFullYear() + 10).toString()}
                placeholder="-- --- ----"
                onIonChange={(e) => {
                  const isoDate = e.detail.value
                  if (!isoDate) return
                  setNextTimeOption({ ...nextTimeOption, start_time_unix: (new Date(isoDate).getTime()) })
                }} />
            </IonItem>

            <IonItem>
              <IonLabel>Full Day</IonLabel>
              <IonToggle
                value={isFullDay ? 'on' : ''}
                onIonChange={(e) => onSelectFullDay(e)} />
            </IonItem>

            <IonItemDivider hidden={isFullDay}>
              <IonLabel className="bold">Start</IonLabel>
            </IonItemDivider>

            <IonItem hidden={isFullDay}>
              <IonLabel>Start time</IonLabel>
              <IonDatetime
                displayFormat="HH:mm A"
                minuteValues="0, 15, 30, 45"
                placeholder="--:-- --"
                onIonChange={(e) => {
                  const isoDate = e.detail.value
                  if (!isoDate) return
                  setNextTimeOption({
                    ...nextTimeOption,
                    start_time_unix: getFullTime(
                      (new Date(isoDate)).getTime(),
                      nextTimeOption.start_time_unix
                    )
                  })
                }} />
            </IonItem>

            <IonItemDivider hidden={isFullDay}>
              <IonLabel className="bold">End</IonLabel>
            </IonItemDivider>

            <IonItem hidden={isFullDay}>
              <IonLabel>End time</IonLabel>
              <IonDatetime
                displayFormat="HH:mm A"
                minuteValues="0, 15, 30, 45"
                placeholder="--:-- --"
                onIonChange={(e) => {
                  const isoDate = e.detail.value
                  if (!isoDate) return
                  setNextTimeOption({
                    ...nextTimeOption,
                    end_time_unix: getFullTime(
                      new Date(isoDate).getTime(),
                      nextTimeOption.start_time_unix
                    )
                  })
                }} />
            </IonItem>
          </IonContent>
        </IonModal>

        <IonModal isOpen={showActModal} cssClass="actModal">
          <IonHeader>
            <IonToolbar>
              <IonButtons slot="start">
                <IonButton onClick={() => onBackClicked()}>
                  Back
                </IonButton>
              </IonButtons>
              <IonTitle>Bookmarked</IonTitle>
            </IonToolbar>
          </IonHeader>

          <IonContent fullscreen color="light">
            {mockActData.map((row, i) => {
              return (
                <div key={row.id} onClick={() => handleAddAct(row.id)}>
                  <ActivityVoteOption
                    index={i}
                    info={genActOptionData(row)}
                    onSelect={() => { }}
                    isVoting={false}
                  />
                </div>
              )
            })}
          </IonContent>
        </IonModal>

        <section className="eventName section">
          <h2>Event Name</h2>
          <IonInput
            type="text"
            placeholder="Enter event name"
            onIonChange={(e) => setEventName(e.detail.value)} />
        </section>

        <section className="dateTime section">
          <h2>Date and Time</h2>
          <div className="dateTimeContainer">
            {timeOptions.map((row, i) => {
              return (
                <TimeVoteOption
                  key={row.id}
                  index={i}
                  info={formatTimeRow(row)}
                  onSelect={() => { }}
                  onDelete={() => onDeleteClick("time", row.id)}
                  isVoting={false}
                />
              )
            })}
            <button className="plusBtn ion-activatable ripple-parent" color="transparent"
              onClick={() => onPlusClicked("time")}
            >
              <IonIcon icon={addOutline} />
              <IonRippleEffect></IonRippleEffect>
            </button>
          </div>
        </section>

        <section className="activity section">
          <h2>Activities</h2>
          <div className="dateTimeContainer">
            {actOptions.map((row, i) => {
              return (
                <ActivityVoteOption
                  key={row.activity_id}
                  index={i}
                  info={genActOptionData(row)}
                  onSelect={() => { }}
                  isVoting={false}
                />
              )
            })}
            <button className="plusBtn ion-activatable ripple-parent" color="transparent"
              onClick={() => onPlusClicked("activity")}
            >
              <IonIcon icon={addOutline} />
              <IonRippleEffect></IonRippleEffect>
            </button>
          </div>
        </section>
      </IonContent>

      <IonFooter hidden={ios} translucent style={{ height: '56px' }}>
        <IonItem lines="none">
          <IonButtons style={{ height: '56px', justifyContent: 'center', width: '100%', }}>
            <IonButton color="primary" expand="full" onClick={() => onCreateClick()}>
              Create Event
            </IonButton>
          </IonButtons>
        </IonItem>
      </IonFooter>
    </IonPage>
  )
}

export default CreateEventPage

function getFullTime(time_unix: number, date_unix?: number) {
  if (!date_unix) return time_unix

  const year = (new Date(date_unix)).getFullYear()
  const month = (new Date(date_unix)).getMonth()
  const date = (new Date(date_unix)).getDate()
  const hours = (new Date(time_unix)).getHours()
  const minutes = (new Date(time_unix)).getMinutes()

  return (new Date(year, month, date, hours, minutes, 0).getTime())
}

function formatTimeRow(row: TimeOption): DTO.EventDetail['times'][number] {
  return {
    time_vote_id: 0,
    start_time_unix: row.start_time_unix as number,
    end_time_unix: row.end_time_unix ? row.end_time_unix : null,
    voter_ids: [],
    voter_names: [],
    vote_count: 0,
  }
}

async function createEventToServer(info: createEventDTO) {
  const [origin, token] = useFetchPrecursors()

  const res = await fetch(`${origin}/event`, {
    method: "POST",
    headers: {
      "Authorization": "Bearer " + token,
      "Content-Type": "application/json; charset=utf-8",
    },
    body: JSON.stringify({ eventInfo: info })
  })

  const response = await res.json()
  return response
}

function useFetchPrecursors() {
  let origin = getAPIServer()
  let token = localStorage.getItem('token')

  if (!origin) throw new Error("Backend API domain is not set.")
  if (!token) throw new Error("JWT token is not found in local storage.")

  return [origin, token]
}

function genActOptionData(raw: any): activityOptionDetail {
  const targetData = mockActData.find(row => row.id === raw.id)
  if (!targetData) return ({
    activity_vote_id: 0,
    activity_id: raw.activity_id,
    activity_name: "null",
    activity_address: "null",
    voter_ids: [],
    voter_names: [],
    vote_count: 0,
    thumbnail_urls: [],
  })

  return ({
    activity_vote_id: 0,
    activity_id: raw.activity_id,
    activity_name: targetData.name,
    activity_address: targetData.address,
    voter_ids: [],
    voter_names: [],
    vote_count: 0,
    thumbnail_urls: [],
  })
}

const mockActData = [
  {
    "id": 1,
    "shop_id": 1,
    "name": "Sai Kung Kayak",
    "address": "743 Tai Mong Tsai Rd, Sai Kung",
    "latlong": "22.3894984,114.2734858",
    "category": ActivityCategory.Kayaking,
    "created_at": "2021-11-02T02:14:26.367Z",
    "updated_at": "2021-11-02T02:14:26.367Z",
    "image_url": "Kayak,_Stand_Up_Paddleboard,_Banana_Boat_and_Donut_at_Ola_Beach_Club.webp",
    "rating": 4.2,
    "review_count": "5"
  },
  {
    "id": 2,
    "shop_id": 1,
    "name": "Sai Kung Surfing",
    "address": "743 Tai Mong Tsai Rd, Sai Kung",
    "latlong": "22.3894984,114.2734858",
    "category": ActivityCategory.Surfing,
    "created_at": "2021-11-02T02:14:26.367Z",
    "updated_at": "2021-11-02T02:14:26.367Z",
    "image_url": "Surfing_and_SUP_Lesson_by_Beyond_Surf_Kata_Phuket.webp",
    "rating": 4.3,
    "review_count": "4"
  },
  {
    "id": 3,
    "shop_id": 1,
    "name": "Hong Kong Kiteboarding",
    "address": "Hong Kong Kiteboarding School, Lantau Island",
    "latlong": "22.2211687,113.9173149",
    "category": ActivityCategory["Kite surfing"],
    "created_at": "2021-11-02T02:14:26.367Z",
    "updated_at": "2021-11-02T02:14:26.367Z",
    "image_url": "Water_Sports_Package_at_Tanjung_Benoa.webp",
    "rating": 4.0,
    "review_count": "2"
  },
  {
    "id": 4,
    "shop_id": 1,
    "name": "Hong Kong Surfing",
    "address": "Hong Kong Kiteboarding School, Lantau Island",
    "latlong": "22.2211687,113.9173149",
    "category": ActivityCategory.Surfing,
    "created_at": "2021-11-02T02:14:26.367Z",
    "updated_at": "2021-11-02T02:14:26.367Z",
    "image_url": "Surfing_Experience_by_Flow_House_Bangkok.webp",
    "rating": 3.0,
    "review_count": "2"
  },
  {
    "id": 5,
    "shop_id": 1,
    "name": "Happy Kayak",
    "address": "14 & 14A, Stanley Main St, Stanley",
    "latlong": "22.2173793,114.2108563",
    "category": ActivityCategory.Kayaking,
    "created_at": "2021-11-02T02:14:26.367Z",
    "updated_at": "2021-11-02T02:14:26.367Z",
    "image_url": "kayak.webp",
    "rating": 3.0,
    "review_count": "3"
  },
  {
    "id": 6,
    "shop_id": 1,
    "name": "Angling",
    "address": "Blue Sky Sports Club Tai Mong Tsai Road Sai Kung Hong Kong",
    "latlong": "22.3873406,114.2734807",
    "category": ActivityCategory.Angling,
    "created_at": "2021-11-02T02:14:26.367Z",
    "updated_at": "2021-11-02T02:14:26.367Z",
    "image_url": "Boat_Fishing_Tour_from_Ishigaki_Island.webp",
    "rating": 5.0,
    "review_count": "3"
  },
  {
    "id": 7,
    "shop_id": 1,
    "name": "Swimming",
    "address": "Blue Sky Sports Club Tai Mong Tsai Road Sai Kung Hong Kong",
    "latlong": "22.3873406,114.2734807",
    "category": ActivityCategory.Swimming,
    "created_at": "2021-11-02T02:14:26.367Z",
    "updated_at": "2021-11-02T02:14:26.367Z",
    "image_url": "Akaroa_Swimming_with_Dolphin.webp",
    "rating": 4.0,
    "review_count": "3"
  }
]