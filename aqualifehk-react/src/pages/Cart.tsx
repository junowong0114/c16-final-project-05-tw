// components
import {
  IonBackButton,
  IonButton,
  IonButtons,
  IonCol,
  IonContent,
  IonFooter,
  IonGrid,
  IonHeader,
  IonPage,
  IonRow,
  IonTitle,
  IonToolbar,
} from "@ionic/react"
import CartCard from "../components/Cart/CartCard"

// hooks
import { useDispatch } from "react-redux"
import { useSelector } from "react-redux"
import { useEffect, useState } from "react"

// redux state, action, and thunk
import { RootState } from "../redux/state"
import { selectAllItems, unselectAllItems } from "../redux/cart/action"
import { loadCartThunk } from "../redux/cart/thunk"
import { formatArrayCount } from "../helpers/format"

// style
import styles from "./Cart.module.scss"
import { useHistory } from "react-router"
import { setGeneralMessage, setGeneralShowToast } from "../redux/general/action"

const Cart: React.FC = () => {
  const cartState = useSelector((state: RootState) => state.cart)
  const dispatch = useDispatch()
  const history = useHistory()
  const needsReload = useSelector((state: RootState) => state.general.showGeneralToast)

  const onSelectAllBtnClick = () => {
    const allSelected =
      cartState.items.length === cartState.selectedItemsId.length

    if (allSelected) return dispatch(unselectAllItems)
    return dispatch(selectAllItems)
  }

  const calculateTotalPrice = () => {
    const totalPrice = cartState.items
      .filter((item) => cartState.selectedItemsId.some((id) => id === item.id))
      .reduce((subTotal, item) => subTotal + item.totalPrice, 0)

    return totalPrice
  }

  const onBookNowClicked = () => {
    if (cartState.selectedItemsId.length === 0) {
      dispatch(setGeneralMessage('Selected at least one item before proceeding'))
      dispatch(setGeneralShowToast(true))
      return
    }

    history.push('/confirmBooking')
  }

  useEffect(() => {
    dispatch(loadCartThunk())
  }, [needsReload])

  return (
    <IonPage>
      <IonHeader translucent={true}>
        <IonToolbar>
          <IonButtons slot="start">
            <IonBackButton defaultHref="/" />
          </IonButtons>
          <IonButtons slot="end">
            <IonButton
              color="primary"
              onClick={() => dispatch(onSelectAllBtnClick())}
            >
              {cartState.items.length === cartState.selectedItemsId.length &&
              cartState.items.length
                ? "Unselect All"
                : "Select All"}
            </IonButton>
          </IonButtons>
          <IonTitle>Cart</IonTitle>
        </IonToolbar>
      </IonHeader>

      <IonContent fullscreen color="light">
        {cartState.items.map((item) => {
          return (
            <CartCard
              selected={cartState.selectedItemsId.some((id) => id === item.id)}
              item={item}
              key={item.id}
            ></CartCard>
          )
        })}
      </IonContent>

      <IonFooter>
        <IonToolbar>
          <IonGrid>
            <IonRow>
              <IonCol>
                <div color="medium" className={styles.subTotal}>
                  Subtotal (
                  {formatArrayCount(cartState.selectedItemsId, "item")})
                </div>
                <div className={styles.priceText}>
                  $HKD {calculateTotalPrice() / 100}
                </div>
              </IonCol>
              <IonCol size="auto">
                <IonButton color="secondary" onClick={() => onBookNowClicked()}>Book Now</IonButton>
              </IonCol>
            </IonRow>
          </IonGrid>
        </IonToolbar>
      </IonFooter>
    </IonPage>
  )
}

export default Cart
