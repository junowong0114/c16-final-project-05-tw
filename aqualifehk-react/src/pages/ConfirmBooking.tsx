// components
import {
  IonBackButton,
  IonButton,
  IonButtons,
  IonContent,
  IonHeader,
  IonInput,
  IonItem,
  IonLabel,
  IonList,
  IonPage,
  IonText,
  IonTitle,
  IonToolbar,
} from "@ionic/react"
import PackageSection from "../components/BookingConfirmPage/PackageSection"
import { useEffect, useState } from "react"
import SectionHeader from "../components/ActivityPage/SectionHeader"

// style
import "./ConfirmBooking.scss"
import { ActivityInfo } from "../redux/BookingOption/state"
import { useDispatch, useSelector } from "react-redux"
import { RootState } from "../redux/state"
import { setGeneralMessage, setGeneralShowToast } from "../redux/general/action"
import { useHistory } from "react-router"
import { errorToString } from "../helpers/format"
import { getAPIServer } from "../helpers/api"
import { DTO } from "shared"

type BookingFormData = {
  firstName: string
  lastName: string
  phoneNumber: string
  email: string
}

interface BookingInfo extends ActivityInfo {
  bookingId?: number
}

const ConfirmBooking: React.FC = () => {
  const [formData, setFormData] = useState<BookingFormData>()
  const history = useHistory()
  const from = useSelector((state: RootState) => state.general.from)
  const dispatch = useDispatch()
  const [bookingInfo, setBookingInfo] = useState<BookingInfo[]>([])
  const selectedBookingIds = useSelector((state: RootState) => state.cart.selectedItemsId)
  const cartItems = useSelector((state: RootState) => {
    return (state.cart.items.filter(item => {
      return selectedBookingIds.some(id => id === item.id)
    }))
  })
  const existingInfo = useSelector((state: RootState) => state.booking.activityInfo)

  useEffect(() => {
    // TODO: fetch user info
    fetchUserInfo().then(info => {
      const userData: BookingFormData = {
        firstName: info.firstname,
        lastName: info.lastname,
        email: info.email,
        phoneNumber: info.phone
      }
      setFormData(userData)
    })
    .catch(e => {
      dispatch(setGeneralMessage(errorToString(e)))
      dispatch(setGeneralShowToast(true))
    })

    // TODO: fetch final booking information from API server
    try {
      if (from.match(/\/book\/confirmBooking/)) {
        console.log('from book page')
        if (!existingInfo) throw new Error('Unknown error: no selected info from booking page.')
        
        fetchPrices([existingInfo.activityId]).then(prices => {
          const activityDate = new Date(existingInfo.activityDate)
          const isWeekend = activityDate.getDay() === 0 || activityDate.getDay() === 6

          let totalPrice = 0

          for (let _pack of existingInfo.packages) {
            const priceObj = prices.find(row => row.packageId === _pack.packageId)
            if (!priceObj) continue

            _pack.unitPrice = isWeekend ? priceObj.weekend_price : priceObj.base_price
            totalPrice += _pack.unitPrice * _pack.quantity
          }

          existingInfo.totalPrice = totalPrice
        })

        setBookingInfo([existingInfo])
        return
      }

      if (from.match(/cart/)) {
        console.log('from cart page')
        if (cartItems.length === 0) throw new Error('Unknown error: no selected item from cart.')

        fetchPrices(cartItems.map(item => item.id)).then(prices => {
          let infos = []
          let cartItemsCopy = [...cartItems]

          for (let item of cartItemsCopy) {
            const isWeekend = item.date.getDay() === 0 || item.date.getDay() === 6
            item.packages.map(_pack => {
              const priceObj = prices.find(priceObj => priceObj.packageId === _pack.packageId)
              if (!priceObj) return _pack.unitPrice
              const newUnitPrice = isWeekend? priceObj.weekend_price : priceObj.base_price
              _pack.unitPrice = newUnitPrice
            })

            infos.push({
              activityId: item.activityId,
              activityDate: item.date.getTime(),
              totalPrice: item.totalPrice,
              packages: item.packages.map(_pack => {
                return {
                  packageId: _pack.packageId,
                  timeSectionId: _pack.timeSectionId,
                  quantity: _pack.quantity,
                  unitPrice: _pack.unitPrice,
                  packageName: _pack.packageName,
                  timeSectionName: _pack.timeSectionName
                }
              })
            })
          }
          setBookingInfo(infos)
        })
        return
      }

      // user does not comes from cart or booking page
      dispatch(setGeneralMessage('Unknown error during loading confirm booking page'))
      dispatch(setGeneralShowToast(true))
      history.goBack()
      return
    } catch (error) {
      dispatch(setGeneralMessage(errorToString(error)))
      dispatch(setGeneralShowToast(true))
      history.goBack()
      return
    }
  }, [])

  function onConfirmClick() {
    try {
      if (!formData) throw new Error('Please fill in your contact info')
      if (Object.values(formData).some(row => !row)) throw new Error('Please fill in all contact info')
  
      if (from.match(/cart/)) {
        if (selectedBookingIds.length === 0) throw new Error('No items in selectedBookingIds')
        postBookingByIds(selectedBookingIds).then(res => {
          if(res) {
            dispatch(setGeneralMessage('Booking success'))
            dispatch(setGeneralShowToast(true))
          }
        })
        .catch(e => {
          dispatch(setGeneralMessage(errorToString(e)))
          dispatch(setGeneralShowToast(true))
        })
      }

      if (from.match(/confirmBooking/)) {
        postBookingByInfo(bookingInfo).then(success => {
          if (success) {
            dispatch(setGeneralMessage(errorToString('Booking success')))
            dispatch(setGeneralShowToast(true))
          }
        })
        .catch(e => {
          dispatch(setGeneralMessage(errorToString(e)))
          dispatch(setGeneralShowToast(true))
          history.push('/tabs/home')
        })
      }
      
      history.push('tabs/order')
      return
    } catch (error) {
      dispatch(setGeneralMessage(errorToString(error)))
      dispatch(setGeneralShowToast(true))
    }
  }

  return (
    <IonPage className="confirmBooking">
      <IonHeader translucent={true}>
        <IonToolbar>
          <IonButtons slot="start">
            <IonBackButton defaultHref="/" />
          </IonButtons>
          <IonTitle>Confirm Booking</IonTitle>
        </IonToolbar>
      </IonHeader>

      <IonContent fullscreen color="light">
        {bookingInfo.map((booking, i) => {
          return (
            <PackageSection
              key={i}
              name={booking.activityName? booking.activityName : "Kayak in Sai Kung"}
              packages={booking.packages}
              date={new Date(booking.activityDate)} />
          )
        })}

        <section className="section form">
          <SectionHeader content="Contact info"></SectionHeader>
          <IonList>
            <IonItem slot="start" className="noMargin">
              <IonLabel position="stacked">First name</IonLabel>
              <IonInput value={formData?.firstName} name="firstName" required></IonInput>
            </IonItem>
            <IonItem slot="start" className="noMargin">
              <IonLabel position="stacked">Last name</IonLabel>
              <IonInput value={formData?.lastName} name="lastName" required></IonInput>
            </IonItem>
            <IonItem slot="start" className="noMargin">
              <IonLabel position="stacked">Phone number</IonLabel>
              <IonInput value={formData?.phoneNumber} name="phoneNumber" required type="tel"></IonInput>
            </IonItem>
            <IonItem slot="start" className="noMargin">
              <IonLabel position="stacked">Email address</IonLabel>
              <IonInput value={formData?.email} name="email" required type="email"></IonInput>
            </IonItem>
          </IonList>
        </section>

        <section className="section payment">
          <SectionHeader content="Payment summary"></SectionHeader>
          <IonList>
            <IonItem className="noMargin" lines="none">
              <IonText>Total</IonText>
              <IonText slot="end" color="secondary">HK$ {bookingInfo.reduce((total, booking) => (total + booking.totalPrice), 0) / 100}</IonText>
            </IonItem>
          </IonList>
          <IonButton color="secondary" size="default" onClick={() => onConfirmClick()}>Confirm</IonButton>
        </section>
      </IonContent>
    </IonPage>
  )
}

export default ConfirmBooking

async function postBookingByInfo(bookingInfos: BookingInfo[]) {
  const [origin, token] = useFetchPrecursors()
  
  const bookRequests = []
  console.log(bookingInfos)
  for (let info of bookingInfos) {
    const query = fetch(`${origin}/booking`, {
      method: "POST",
      headers: {
        "Authorization": "Bearer " + token,
        "Content-Type": "application/json; charset=utf-8",
      },
      body: JSON.stringify(info)
    })
    bookRequests.push(query)
  }

  const resList = await Promise.all(bookRequests)
  const resJSONParseList = []
  for (let res of resList) {
    const parseAction = res.json()
    resJSONParseList.push(parseAction)
  }

  const results = await Promise.all(resJSONParseList)
  for (let result of results) {
    console.log(result)
    if (!result.success) throw new Error(result.error)
  }

  return true
}

async function postBookingByIds(bookingIds: number[]) {
  const [origin, token] = useFetchPrecursors()

  const bookRequests = []
  for (let id of bookingIds) {
    const query = fetch(`${origin}/bookingFromCart`, {
      method: "POST",
      headers: {
        "Authorization": "Bearer " + token,
        "Content-Type": "application/json; charset=utf-8",
      },
      body: JSON.stringify({bookingId: id})
    })
    bookRequests.push(query)
  }

  const resList = await Promise.all(bookRequests)
  const resJSONParseList = []
  for (let res of resList) {
    const parseAction = res.json()
    resJSONParseList.push(parseAction)
  }

  const results = await Promise.all(resJSONParseList)
  for (let result of results) {
    if (!result.success) throw new Error(`Booking failed`)
  }

  return true
}

async function fetchPrices(activityIds: number[]) {
  let queries = []
  for (let activityId of activityIds) {
    queries.push(fetchActivityInfo(activityId))
  }
  const activities = await Promise.all(queries)
  if (activities.some(row => row === undefined)) {
    throw new Error('Empty query result')
  }

  let prices: {
    packageId: number;
    base_price: number;
    weekend_price: number;
    unix_effective_date: number;
    unix_expiration_date: number | null;
  }[] = []

  for (let activity of activities) {
    (activity as DTO.Activity).packages.map(_pack => {
      return _pack.prices.map(priceObj => {
        prices.push({ ...priceObj, packageId: _pack.id })
      })
    })
  }
  return prices
}

async function fetchActivityInfo(activityId: number): Promise<DTO.Activity | undefined> {
  const [origin, token] = useFetchPrecursors()

  let res
  let response

  res = await fetch(`${origin}/activityInfo/${activityId}`, {
    headers: {
      "Authorization": "Bearer " + token
    }
  })
  response = await res.json()

  if (response.success) {
    return response.result
  } else {
    throw new Error(`Error during fetching ${origin}/activityInfo/${activityId}: ${response.error}`)
  }
}

function useFetchPrecursors() {
  let origin = getAPIServer()
  let token = localStorage.getItem('token')

  if (!origin) throw new Error("Backend API domain is not set.")
  if (!token) throw new Error("JWT token is not found in local storage.")

  return [origin, token]
}

async function fetchUserInfo() {
  const [origin, token] = useFetchPrecursors()

  const res = await fetch(`${origin}/userInfo`, {
    headers: {
      "Authorization": "Bearer " + token
    }
  })
  const response = await res.json()

  if (response.success) {
    return response.result
  } else {
    throw new Error(`Error during fetching ${origin}/userInfo: ${response.error}`)
  }
}