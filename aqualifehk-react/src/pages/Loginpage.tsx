import {
  IonBackButton,
  IonButton,
  IonButtons,
  IonCol,
  IonContent,
  IonHeader,
  IonInput,
  IonItem,
  IonLabel,
  IonPage,
  IonRow,
  IonTitle,
  IonToolbar,
} from "@ionic/react"
import { useEffect, useState } from "react"
import { useDispatch, useSelector } from "react-redux"
import { useHistory } from "react-router"
import { loginWithPasswordThunk } from "../redux/auth/thunk"
import { RootState } from "../redux/state"
import { setGeneralMessage }from '../redux/general/action'

const LoginPage: React.FC = () => {
  const [email, setEmail] = useState("")
  const [password, setPassword] = useState("")
  const user = useSelector((state: RootState) => state.auth.user)
  const dispatch = useDispatch()
  const history = useHistory();
  const [loginError,setLoginError] = useState('')


  useEffect(()=>{
    if(user?.email){
      dispatch(setGeneralMessage('Login success'))
      history.push("/tabs/profile");
    }else if ( user === undefined){
      dispatch(setGeneralMessage('Invalid email or password'))
    }
  },[user])


  function LoginWithPassword() {
    if(!email){
      return
    }
    if(!password){
      return
    }
    dispatch(loginWithPasswordThunk(email, password))
  }



  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start" >
            <IonBackButton defaultHref="/" />
          </IonButtons>
          <IonTitle>Login</IonTitle>
        </IonToolbar>
      </IonHeader>

      <IonContent>
        <IonItem >
          <IonLabel position="floating">Email</IonLabel>
          <IonInput
            required
            type="email"
            value={email}
            onIonChange={(e) => setEmail(e.detail.value!)}
          />
        </IonItem>
        <IonItem>
          <IonLabel position="floating">Password</IonLabel>
          <IonInput
            required
            type="password"
            value={password}
            onIonChange={(e) => setPassword(e.detail.value!)}
          />
        </IonItem>

        <IonToolbar className='login-button'>
          <IonButton
            expand="full"
            className="ion-margin-top"
            onClick={LoginWithPassword}>
            Login
          </IonButton>
          <div>{loginError? loginError : null }</div>
          <IonRow>
            <IonCol size='6'></IonCol>
            <IonCol size='6'>
              <IonButtons>
                <IonButton routerLink="/register" >
                  Register an account
                </IonButton>

              </IonButtons>
            </IonCol>

          </IonRow>

        </IonToolbar>




        {/* todo  remove after finished
        <div>user.id: {user?.id}</div> */}
      </IonContent>
    </IonPage >
  )
}

export default LoginPage
