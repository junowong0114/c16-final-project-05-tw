import React, { useState } from "react"

// components
import {
  IonContent,
  IonHeader,
  IonPage,
  IonSearchbar,
  IonToolbar,
  IonIcon,
  IonButton,
  IonButtons,
  IonModal,
} from "@ionic/react"
import { cartOutline } from "ionicons/icons"
import CategoryButton from "../components/Homepage/CategoryButton"

// style
import "./Homepage.scss"
import "./homePageIonCard.scss"

// react-redux state/hooks
import { Link, useHistory } from "react-router-dom"
import AllCategoriesModal from "../components/Homepage/AllCategoriesModal"
import { useSelector } from "react-redux"
import { RootState } from "../redux/state"
import SearchCard from "../components/SearchPage/SearchCard"


const Homepage: React.FC = () => {
  const history = useHistory()
  const recommendations = useSelector((state: RootState) => state.homePage.recommendation);
  const whatsNew = useSelector((state: RootState) => state.homePage.whatsNew)
  const [showCategoriesModal, setShowCategoriesModal] = useState<boolean>(false)

  return (
    <IonPage className="homePage">
      <IonHeader>
        <IonToolbar color="primary" className="ionToolBar">
          <Link to="/search">
            <IonSearchbar
              placeholder="Activities, shops, location"
              className="ionSearchBar"
            />
          </Link>
          <IonButtons slot="end">
            <IonButton routerLink="/cart" className="cartBtn ion-no-padding">
              <IonIcon icon={cartOutline} className="cartIcon" />
            </IonButton>
          </IonButtons>
        </IonToolbar>
      </IonHeader>

      <IonContent fullscreen color="light" className="homepage-content">
        <IonModal
          animated={false}
          isOpen={showCategoriesModal}
        >
          <AllCategoriesModal onDismiss={() => setShowCategoriesModal(false)} />
        </IonModal>

        <section className="section categories">
          <h2>Categories</h2>
          <div className="iconContainer">
            {iconNames.map((name, i) => {
              return (<CategoryButton
                key={i}
                name={name}
                isLast={i === iconNames.length - 1}
                onClick={i === iconNames.length - 1 ?
                  () => setShowCategoriesModal(true) :
                  () => history.push(`/search/${name}`)}
              />)
            })}
          </div>
        </section>

        <section className='section recommendation'>
          <h2>Recommendations</h2>
          <div className='scrolls'>
            {recommendations.map((item) => {
              return (
                <SearchCard result={item} key={item.id} />
              )
            })}
          </div>
        </section>

        <section className='section whatsNew'>
          <h2>What's new</h2>
          <div className='scrolls'>
            {whatsNew.map((item) => {
              return (
                <SearchCard result={item} key={item.id} />
              )
            })}
          </div>
        </section>
      </IonContent>
    </IonPage>
  )
}

export default Homepage

const iconNames = [
  'kayaking',
  'snorkeling',
  'surfing',
  'banana boat',
  'water skiing',
  'jet skiing',
  'windsurfing',
  'view all',
]
