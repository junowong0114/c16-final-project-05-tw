import React from "react"
import {
  IonCard,
  IonContent,
  IonHeader,
  IonItem,
  IonPage,
} from "@ionic/react"
import { useDispatch, useSelector } from "react-redux"
import { getNonRegisteredTokenThunk } from "../redux/auth/thunk"
import { RootState } from "../redux/state"
import './Profile.scss'
import { Link } from "react-router-dom"
import Logined from "../components/Profile/Logined"
import NonLogined from "../components/Profile/NonLogined"
import { getAPIServer } from "../helpers/api"

const Profile: React.FC = () => {
  const dispatch = useDispatch()
  const user = useSelector((state: RootState) => state.auth.user)

  const userEmail = user?.email
  const userIcon = user?.email?.charAt(0).toUpperCase();

  const isProduction = getAPIServer().match(/16.162.219.170/)

  function Logout() {
    dispatch(getNonRegisteredTokenThunk())
  }

  return (
    <IonPage>
      <IonContent>
        <IonHeader className='profile-header' >
          <div className='personal'>
            <div className='no-image'>
              {userEmail ? userIcon : 'U'}
            </div>
            <div className='username'>
              {userEmail ? userEmail :
                (
                  <Link to='/login'>
                    Login or Sign up
                  </Link>
                )}
            </div>
          </div>
        </IonHeader>

        <IonCard >
          <IonItem lines='none' routerLink="/cart">
            My Cart
          </IonItem>
          <IonItem lines='none' className='history' disabled button>
            History
          </IonItem>
        </IonCard>
        {!userEmail ? <NonLogined /> : <Logined onLogout={Logout} />}

        <div className="ion-padding" style={{ textAlign: 'end' }}>
          {isProduction? "beta" : "alpha"}
        </div>
      </IonContent>
    </IonPage>
  )
}

export default Profile


