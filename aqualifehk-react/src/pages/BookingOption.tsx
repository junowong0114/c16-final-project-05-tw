// components
import {
  IonBackButton,
  IonButton,
  IonButtons,
  IonCard,
  IonContent,
  IonFooter,
  IonHeader,
  IonItem,
  IonList,
  IonLoading,
  IonPage,
  IonSelect,
  IonSelectOption,
  IonText,
  IonTitle,
  IonToolbar,
  useIonToast,
} from "@ionic/react"
import FullCalender from "../components/FullCalendar"
import SectionHeader from '../components/ActivityPage/SectionHeader'
import BookingTimeSection from "../components/Booking/BookingTimeSection"

// styles
import styles from "./Cart.module.scss"
import "../theme/calendar.scss"
import "./BookingOption.scss"

// helpers and hooks
import { getAPIServer } from "../helpers/api"
import { useHistory, useParams } from "react-router"
import { useEffect, useState } from "react"
import { useDispatch, useSelector } from "react-redux"
import { RootState } from "../redux/state"
import { DTO } from "shared"
import PackageRow from "../components/ActivityPage/PackageRow"
import { dateToDateTimeString } from "../helpers/dateTimeFormat"
import { setBookingOptions } from "../redux/BookingOption/action"
import { ActivityInfo, PackageInfo } from "../redux/BookingOption/state"
import { setGeneralMessage, setGeneralShowToast } from "../redux/general/action"
import { errorToString } from "../helpers/format"
import { loadCartThunk } from "../redux/cart/thunk"

type BookingPageParams = {
  activityId: string
  to: "cart" | "confirmBooking" | "edit"
}

const emptyPackages: DTO.Package[] = [{
  id: 0,
  name: "invalid",
  inclusion: "",
  eligibility: "",
  capacity: 0,
  time_sections: [{
    id: 0,
    name: "invalid"
  }],
  prices: [{
    base_price: 0,
    weekend_price: 0,
    unix_effective_date: 0,
    unix_expiration_date: 0,
  }]
}]

const BookingOption: React.FC = () => {
  const dispatch = useDispatch();
  const { to, activityId } = useParams<BookingPageParams>()
  const [isLoading, setIsLoading] = useState<boolean>(false)
  const [selectedDate, setSelectedDate] = useState(new Date())
  const selectedDateRedux = useSelector((state: RootState) => state.booking.activityInfo?.activityDate)
  const maxDate = new Date((new Date()).getTime() + 1000 * 3600 * 24 * 60)
  const from = useSelector((state: RootState) => state.general.from)
  const [presentToast, dismissToast] = useIonToast()
  const history = useHistory()
  const bookingId = useSelector((state: RootState) => state.booking.bookingId)

  const [info, setInfo] = useState<DTO.Activity>()
  const [packages, setPackages] = useState<DTO.Package[]>(emptyPackages)
  const [idIndexMap, setIdIndexMap] = useState<any>()
  const [displayId, setDisplayId] = useState<number>(0)
  const [selectedSectionIds, setSelectedSectionIds] = useState<(number | null)[]>([])
  const selectedSectionIdRedux = useSelector((state: RootState) => state.booking.activityInfo?.packages.map(_pack => _pack.timeSectionId))
  const [quantities, setQuantities] = useState<number[]>([])
  const quantitiesRedux = useSelector((state: RootState) => state.booking.activityInfo?.packages.map(_pack => _pack.quantity))
  const [availabilities, setAvailabilities] = useState<(Record<string, number> | null)[]>([])

  // load activity info on init
  useEffect(() => {
    setIsLoading(true)

    if (activityId) {
      getActivityInfo(activityId).then(res => {
        if (res && res.packages.length > 0) {
          // TODO: remove after finish developing booking page
          // console.log(res)

          setInfo(res)
          setPackages(res.packages)
          setDisplayId(res.packages[0].id)

          const idIndexMap = Object.fromEntries(res.packages.map((_pack, i) => {
            return [_pack.id, i]
          }))
          setIdIndexMap(idIndexMap)

          setSelectedSectionIds(res.packages.map(_pack => {
            return null
          }))

          setQuantities(res.packages.map(_pack => {
            return 0
          }))

          return res
        } else {
          console.log(`error during fetch /activityInfo/${activityId}`)
          return
        }
      }).then((res) => {
        if (!res) return

        const availabilityQueries = []
        for (let _pack of res.packages) {
          availabilityQueries.push(getAvailability(_pack.id))
        }
        Promise.all(availabilityQueries)
          .then(res => {
            if (res.some(row => row === undefined)) {
              throw new Error('Error during fetching availability')
            }

            const availabilities = res.map(row => {
              if (!row) return null
              const target = row.availability.find(row => {
                return row.remaining
              })
              if (!target) return null
              return target.remaining
            })

            setAvailabilities(availabilities)

            if (to === 'edit') {
              // TODO: set displayed state
              if (selectedDateRedux) setSelectedDate(new Date(selectedDateRedux))
              if (selectedSectionIdRedux) setSelectedSectionIds(selectedSectionIdRedux)
              if (quantitiesRedux) setQuantities(quantitiesRedux)
              setIsLoading(false)
              return
            }

            setIsLoading(false)
          })
        .catch(e => {
          dispatch(setGeneralMessage(errorToString(e)))
          dispatch(setGeneralShowToast(true))
          history.goBack()
        })
      })
    }
  }, [activityId])

  useEffect(() => {
    if (!packages || !idIndexMap) setIsLoading(true)
  }, [])

  useEffect(() => {
    if (!selectedSectionIds) return
    for (let packIndex in selectedSectionIds) {
      const selectedSectionId = selectedSectionIds[packIndex]
      if (!selectedSectionId) continue
      let remaining = generateAvailability(parseInt(packIndex), selectedSectionId)
      if (remaining < quantities[packIndex] && typeof remaining === "number") {
        const newState = [...quantities]
        newState[packIndex] = remaining
        setQuantities(newState)
      }
    }
  }, [selectedSectionIds])

  useEffect(() => {
    let availabilityQueries = []
    for (let _pack of packages) {
      const id = _pack.id
      availabilityQueries.push(getAvailability(id, selectedDate).catch(e => {
        dispatch(setGeneralMessage(errorToString(e)))
        dispatch(setGeneralShowToast(true))
        history.goBack()
      }))
    }

    Promise.all(availabilityQueries)
      .then(res => {
        const newState = res.map(row => {
          if (!row) return null
          const target = row.availability.find(row => {
            return row.remaining
          })
          if (!target) return null
          return target.remaining
        })

        setAvailabilities(newState)
      })
      .catch(e => {
        dispatch(setGeneralMessage(errorToString(e)))
        dispatch(setGeneralShowToast(true))
        history.goBack()
      })

  }, [selectedDate])

  useEffect(() => {
    // console.log(availabilities)
  }, [availabilities])

  async function getActivityInfo(activityId: string) {
    // get API ip/domain
    let origin
    try {
      origin = getAPIServer()
    } catch (error) {
      console.log(error)
      return
    }

    // fetch data
    try {
      const res = await fetch(`${origin}/activityInfo/${activityId}`);
      if (res.status === 200) {
        const result: DTO.Activity = (await res.json()).result
        return result
      }
    } catch (error) {
      console.log(error)
    }

    return
  }

  async function getAvailability(
    package_id: number,
    start_date: Date = new Date())
    : Promise<DTO.Availability | undefined> {

    if (!package_id) return

    const start_date_unix = start_date.getTime();
    const quantity = 1;

    let origin
    origin = getAPIServer()

    const res = await fetch(`${origin}/packageAvailability/?package_id=${package_id}&start_date_unix=${start_date_unix}&quantity=${quantity}`)
    const result = (await res.json()).result
    if (!result.availability || result.availability.length === 0) {
      throw new Error('No availability found')
    }

    return result
  }

  function onSelectTime(packageIndex: number, sectionId: number) {
    if (!selectedSectionIds) return

    const newState = [...selectedSectionIds]
    if (newState[packageIndex] !== sectionId) {
      newState[packageIndex] = sectionId
      if (quantities[packageIndex] === 0) {
        const newQState = [...quantities]
        newQState[packageIndex] = 1
        setQuantities(newQState)
      }
    } else {
      // de-select
      newState[packageIndex] = null
      if (quantities[packageIndex] !== 0) {
        const newQState = [...quantities]
        newQState[packageIndex] = 0
        setQuantities(newQState)
      }
    }

    setSelectedSectionIds(newState)
  }

  function onPlusMinusClick(packageIndex: number, action: "plus" | "minus") {
    const newState = [...quantities]

    if (action === "minus") {
      if (newState[packageIndex] === 0) return

      newState[packageIndex] = newState[packageIndex] - 1
      setQuantities(newState)

      if (newState[packageIndex] === 0) {
        const newSState = [...selectedSectionIds]
        newSState[packageIndex] = null
        setSelectedSectionIds(newSState)
      }
    } else {
      let sectionId = selectedSectionIds[packageIndex]
      if (!sectionId) return
      let remaining = generateAvailability(packageIndex, sectionId)
      if (quantities[packageIndex] === remaining) return

      newState[packageIndex] = newState[packageIndex] + 1
      setQuantities(newState)
    }
  }

  function generatePriceTag(date: Date, packageId: number) {
    let basePrice: number
    let weekendPrice: number
    let priceObjArray: any
    let priceObj: any

    if (idIndexMap && packages) {
      priceObjArray = packages[idIndexMap[packageId]].prices
      priceObj = priceObjArray[0]
      basePrice = priceObj.base_price
      weekendPrice = priceObj.weekend_price

      const day = date.getDay()
      let isWeekend = day === 0 || day === 6
      if (isWeekend) {
        return weekendPrice
      } else {
        return basePrice
      }
    }

    return 0
  }

  function generateAvailability(packageIndex: number, sectionId?: number) {
    if (!availabilities) return "-"

    let packageAvailability = availabilities[packageIndex]

    let querySectionId
    if (!sectionId) {
      querySectionId = selectedSectionIds[packageIndex]
    } else {
      querySectionId = sectionId
    }

    if (!packageAvailability || querySectionId === null) return "-"

    return packageAvailability[querySectionId]
  }

  function calculateSubTotal() {
    let prices = 0

    if (!quantities) return 0
    if (!selectedDate) return 0
    for (let index in quantities) {
      const unitPrice = generatePriceTag(selectedDate, packages[parseInt(index)].id)
      let quantity = quantities[index]
      if (!quantity) continue
      prices += unitPrice * quantities[index]
    }

    return prices
  }

  async function onConfirmClicked() {
    if (!info || !info.id) return

    const confirmedPackages: PackageInfo[] = []
    for (let index in selectedSectionIds) {
      const packageId = packages[index].id

      const timeSectionId = selectedSectionIds[index]
      if (!timeSectionId) continue

      let timeSectionName = ""
      for (let _sect of packages[index].time_sections) {
        if (_sect.id === timeSectionId) {
          timeSectionName = _sect.name
        }
      }

      const quantity = quantities[index]
      if (quantity === 0) return

      const unitPrice = generatePriceTag(selectedDate, packageId)
      if (!unitPrice || typeof unitPrice !== "number") return

      confirmedPackages.push({
        packageId,
        timeSectionId,
        timeSectionName,
        quantity,
        unitPrice,
        packageName: packages[idIndexMap[packageId]].name
      })
    }

    if (confirmedPackages.length === 0) {
      presentToast({
        message: "Please choose a package before proceeding.",
        duration: 1500,
        position: "middle",
        cssClass: "noPackageWarningToast"
      })
      return
    }

    const totalPrice = calculateSubTotal()

    const activityInfo: ActivityInfo = {
      activityId: info.id,
      activityName: info.activity_name,
      activityDate: selectedDate.getTime(),
      bookingDate: (new Date()).getTime(),
      packages: confirmedPackages,
      userRequirements: "",
      totalPrice,
    }

    dispatch(setBookingOptions(activityInfo))

    // differentiate action for add cart / direct booking / edit cart 
    if (to === "confirmBooking") {
      history.push(`/confirmBooking`)
    } else if (to === "cart") {
      try {
        const result = await postCart(activityInfo)
        if (result.success) {
          dispatch(setGeneralMessage("Successfully added to cart"))
          dispatch(setGeneralShowToast(true))
        }
      } catch (error) {
        dispatch(setGeneralMessage(errorToString(error)))
        dispatch(setGeneralShowToast(true))
      }

      history.goBack()
    } else if (to === "edit") {
      // TODO: edit cart
      let origin
      origin = getAPIServer()
      let token = localStorage.getItem('token')

      try {
        if (!bookingId) throw new Error('Booking Id not found in redux state')

        const res = await fetch(`${origin}/cart`, {
          method: "PUT",
          headers: {
            "Authorization": "Bearer " + token,
            "Content-Type": "application/json; charset=utf-8",
          },
          body: JSON.stringify({
            ...activityInfo,
            bookingId
          })
        })
        const result = await res.json()

        if (!result.success) throw new Error('Unknown error during cart update request.')
      } catch (error) {
        dispatch(setGeneralMessage(errorToString(error)))
        dispatch(setGeneralShowToast(true))
      }

      dispatch(loadCartThunk())
      dispatch(setGeneralMessage("Edit success"))
      dispatch(setGeneralShowToast(true))
      history.goBack()
    }
  }

  async function postCart(activityInfo: ActivityInfo) {
    // get API ip/domain
    let origin
    origin = getAPIServer()

    let token = localStorage.getItem('token')

    const res = await fetch(`${origin}/cart`, {
      method: "POST",
      headers: {
        "Authorization": "Bearer " + token,
        "Content-Type": "application/json; charset=utf-8",
      },
      body: JSON.stringify(activityInfo),
    })

    const result = (await res.json())
    return result
  }

  function generateNextBtnText(to: string) {
    switch (to) {
      case "cart":
        return "Add to cart"

      case "confirmBooking":
        return "Next"

      case "edit":
        return "Confirm"

      default:
        return "error"
    }
  }

  return (
    <IonPage className="bookPage">
      <IonHeader translucent={true}>
        <IonToolbar>
          <IonButtons slot="start">
            <IonBackButton defaultHref="/" />
          </IonButtons>
          <IonTitle>{to === "edit" ? "Edit Booking" : "Booking Options"}</IonTitle>
        </IonToolbar>
      </IonHeader>

      <IonContent fullscreen color="light">
        <IonLoading isOpen={isLoading} message='Loading Booking' backdropDismiss />

        <div className="mainContent" hidden={isLoading}>
          <IonCard>
            <IonList>
              <SectionHeader content="Select a date" />
              <IonList className="packageSelector">
                <IonItem lines="none" className="ion-no-padding">
                  <div slot="start">Select package: </div>
                  <IonSelect
                    okText="Select"
                    value={displayId}
                    placeholder={packages[0].name}
                    onIonChange={(e) => setDisplayId(e.detail.value)}
                    className="ion-no-margin ion-no-padding"
                    color="primary"
                    slot="end"
                  >
                    {packages.map(_pack => {
                      return (
                        <IonSelectOption key={_pack.id} value={_pack.id}>
                          {_pack.name}
                        </IonSelectOption>
                      )
                    })
                    }
                  </IonSelect>
                </IonItem>
              </IonList>

              <IonText className="date-selected">
                Date selected: {dateToDateTimeString(selectedDate, "date")}
              </IonText>

              <FullCalender
                packages={packages}
                displayId={displayId}
                idIndexMap={idIndexMap}
                maxDate={maxDate}
                onChange={(newDate) => setSelectedDate(newDate)}
              />
            </IonList>
          </IonCard>

          <IonCard>
            <SectionHeader content="Select a section" />
            {packages.map((_pack, _pack_i) => {
              return (
                <div key={_pack.id} className="sectionSelectorContainer">
                  <div className="sectionSelectorTitle">{_pack.name}</div>
                  <div className="sectionSelector">
                    {idIndexMap && selectedSectionIds ? packages[idIndexMap[_pack.id]].time_sections.map((section, i) => {
                      return (
                        <BookingTimeSection
                          key={section.id}
                          id={section.id}
                          name={section.name}
                          selectable={generateAvailability(_pack_i, section.id) === 0 ? false : true}
                          selected={selectedSectionIds.some(id => id === section.id)}
                          onSelect={(id) => onSelectTime(idIndexMap[_pack.id], id)} />
                      )
                    }) : <div>unknown error</div>}
                  </div>
                </div>
              )
            })}
          </IonCard>

          <IonCard>
            <SectionHeader content="Select quantity" />
            <IonList>
              {packages.map((_pack, i) => {
                return (
                  <div className="quantitySelector" key={_pack.id}>
                    <PackageRow
                      id={_pack.id}
                      name={_pack.name}
                      quantity={quantities[i]}
                      onPlusClick={() => onPlusMinusClick(i, "plus")}
                      onMinusClick={() => onPlusMinusClick(i, "minus")}
                      fulled={generateAvailability(i) === 0 || selectedSectionIds[i] === null ? true : false} />
                    <div className="price">Unit price: HK$ {generatePriceTag(selectedDate, _pack.id) / 100}</div>
                    <div className="price">Remaining: {generateAvailability(i)}</div>
                  </div>
                )
              })}
            </IonList>
          </IonCard>
        </div>
      </IonContent>

      <IonFooter>
        <IonToolbar>
          <IonItem lines="none">
            <IonText slot="start" className="subtotal">SubTotal:</IonText>
            <div className={styles.priceText} slot="end">$HKD {calculateSubTotal() / 100}</div>
          </IonItem>
          <IonButton color='secondary'
            className='nextBtn'
            onClick={() => onConfirmClicked()}>
            {generateNextBtnText(to)}
          </IonButton>
        </IonToolbar>
      </IonFooter>
    </IonPage>
  )
}

export default BookingOption

export type Package = {
  id: number,
  name: string,
  inclusion?: string,
  eligibility?: string,
  capacity: number,
  time_sections: TimeSection[],
  prices: PricePackage[]
}

export type TimeSection = {
  id: number,
  name: string
}

export type PricePackage = {
  base_price: number,
  weekend_price: number,
  unix_effective_date: string,
  unix_expiration_date?: string,
}
