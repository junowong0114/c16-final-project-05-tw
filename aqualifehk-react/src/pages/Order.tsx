import {
  IonButton,
  IonButtons,
  IonContent,
  IonHeader,
  IonIcon,
  IonPage,
  IonTitle,
  IonToolbar,
} from "@ionic/react"
import { cartOutline, handRightOutline } from "ionicons/icons"
import { useEffect, useState } from "react"
import { useSelector, useDispatch } from "react-redux"
import OrderItem from "../components/Order/OrderItem"
import { loadOrderThunk } from "../redux/order/thunk"
import { RootState } from "../redux/state"

import "./Order.scss"

const Order: React.FC = () => {
  const dispatch = useDispatch()


  const orderStatus = useSelector((state: RootState) => state.order)
  const from = useSelector((state: RootState) => state.general.from)

  useEffect(() => {
    dispatch(loadOrderThunk())
  }, [from])

  const ordersRender = () => {
    if (orderStatus.orders.length < 1) {
      return (
        <div className="noneOrdersContainer">
          <IonIcon icon={handRightOutline} className="noneOrdersIcon"></IonIcon>
          <div className="noneOrderText">
            You have no orders! Go to Add some!
          </div>
          <IonButtons className="noneOrderBtn">
            <IonButton fill="solid" color="secondary" routerLink="/tabs/home">
              Explore
            </IonButton>
          </IonButtons>
        </div>
      )
    }

    const reversedOrders = [...orderStatus.orders].reverse()
    return reversedOrders.map((item) => {
      
      return <OrderItem key={item.bookingId} order={item}></OrderItem>
    })
  }

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Orders</IonTitle>
          <IonButtons slot="end" className="buttons">
            <IonButton href="cart" className="cartBtn">
              <IonIcon icon={cartOutline} className="cartIcon" />
            </IonButton>
          </IonButtons>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>{ordersRender()}</IonContent>
    </IonPage>
  )
}

export default Order
