import {
  getConfig,
  IonBackButton,
  IonButton,
  IonButtons,
  IonContent,
  IonFooter,
  IonHeader,
  IonIcon,
  IonInput,
  IonItem,
  IonLoading,
  IonPage,
  IonToolbar,
  useIonActionSheet,
  useIonAlert,
} from "@ionic/react"
import TimeVoteOption from "../components/EventPage/TimeVoteOption"
import ActivityVoteOption from "../components/EventPage/ActivityVoteOption"
import { ellipsisVertical } from "ionicons/icons"
import { useEffect, useState } from "react"
import { RootState } from "../redux/state"
import { DTO } from "shared"
import "./EventDetailPage.scss"
import { useDispatch, useSelector } from "react-redux"
import { getAPIServer } from "../helpers/api"
import { setGeneralMessage, setGeneralShowToast } from "../redux/general/action"
import { errorToString } from "../helpers/format"
import { useHistory } from "react-router"

const EventDetailPage: React.FC = () => {
  const ios = getConfig()?.get('mode') === 'ios'
  const dispatch = useDispatch()
  const history = useHistory()
  const user = useSelector((state: RootState) => state.auth.user)
  const selectedEventId = useSelector((state: RootState) => state.eventPage.selectedEventId)
  const eventList = useSelector((state: RootState) => state.eventPage.eventList)
  const [eventInfo, setEventInfo] = useState<DTO.EventDetail>()
  const [isLoading, setIsLoading] = useState(false)
  const [userFullName, setUserFullName] = useState<string | undefined>()
  const isOwner = user?.id === eventInfo?.owner_id
  const isLoggedIn = user?.email ? true : false
  const [presentActionSheet] = useIonActionSheet();
  const [presentAlert] = useIonAlert();

  const [selectedTimeIds, setSelectedTimeIds] = useState<number[]>([])
  const [selectedActIds, setSelectedActIds] = useState<number[]>([])

  useEffect(() => {
    setIsLoading(true)
    fetchUserInfo()
      .then(res => {
        setUserFullName(res.firstname + " " + res.lastname)
      })
      .catch(e => {
        dispatch(setGeneralMessage(errorToString(e)))
        dispatch(setGeneralShowToast(true))
        history.goBack()
      })

    if (!selectedEventId || !eventList) return
    setEventInfo(eventList.find(row => row.event_id === selectedEventId))
  }, [])

  useEffect(() => {
    if (eventInfo && userFullName) setIsLoading(false)
  }, [eventInfo, userFullName])

  function onActOptionClicked(selectedId: number, action: "select" | "unselect") {
    if (action === "select") {
      setSelectedActIds([
        ...selectedActIds,
        selectedId
      ])
    } else {
      setSelectedActIds(selectedActIds.filter(id => id !== selectedId))
    }

    return
  }

  function onTimeOptionClicked(selectedId: number, action: "select" | "unselect") {
    if (action === "select") {
      setSelectedTimeIds([
        ...selectedTimeIds,
        selectedId
      ])
    } else {
      setSelectedTimeIds(selectedTimeIds.filter(id => id !== selectedId))
    }

    return
  }

  function onDeleteClick() {
    const eventId = eventInfo?.event_id
    if (!eventId) {
      dispatch(setGeneralMessage('Delete failed: event id is not found'))
      dispatch(setGeneralShowToast(true))
      return
    }

    deleteEvent(eventId)
      .then(res => {
        if (res.success) {
          dispatch(setGeneralMessage('Delete success'))
          dispatch(setGeneralShowToast(true))
          history.goBack()
          return
        } else {
          throw new Error(res.error)
        }
      })
      .catch(e => {
        dispatch(setGeneralMessage(errorToString(e)))
        dispatch(setGeneralShowToast(true))
      })
  }

  function onVoteClick() {
    const eventId = eventInfo?.event_id
    if (!eventId) {
      dispatch(setGeneralMessage('Failed to vote, event id not found.'))
      dispatch(setGeneralShowToast(true))
      return
    }

    if (selectedActIds.length === 0) {
      dispatch(setGeneralMessage('Must choose at least one activity'))
      dispatch(setGeneralShowToast(true))
      return
    }
    if (selectedTimeIds.length === 0) {
      dispatch(setGeneralMessage('Must choose at least one time option'))
      dispatch(setGeneralShowToast(true))
      return
    }

    postVotes(selectedActIds, selectedTimeIds, eventId)
    .then(res => {
      if (res) {
        dispatch(setGeneralMessage('Vote success'))
        dispatch(setGeneralShowToast(true))
        history.goBack()
        return
      }
    })
    .catch(e => {
      dispatch(setGeneralMessage(errorToString(e)))
      dispatch(setGeneralShowToast(true))
    })
  }

  return (
    <IonPage className="eventDetailPage">
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
            <IonBackButton defaultHref="/" />
          </IonButtons>
          <IonButtons slot="end">
            <IonButton hidden={!ios} onClick={() => onVoteClick()}>
              Vote
            </IonButton>
            <IonButton
              hidden={!isOwner}
              onClick={() => {
                presentActionSheet({
                  buttons: [{
                    text: 'Delete event',
                    role: 'destructive',
                    handler: () => presentAlert({
                      header: 'Are you sure you want to delete the event?',
                      buttons: [
                        'Cancel',
                        { text: 'Ok', handler: () => onDeleteClick() },
                      ],
                    }),
                  },
                  {
                    text: 'Edit event',
                  },
                  {
                    text: 'Confirm event',
                  },
                  {
                    text: 'Cancel',
                    role: 'cancel',
                  }],
                  header: 'Event action',
                  keyboardClose: true,
                })
              }}>
              <IonIcon color="primary" icon={ellipsisVertical} />
            </IonButton>
          </IonButtons>
        </IonToolbar>
      </IonHeader>

      <IonContent color="light">
        <IonLoading isOpen={isLoading} />
        <div hidden={isLoading}>
          <h1 className="section">{eventInfo?.event_name}</h1>

          <section className="nameSection section">
            <h2>{isLoggedIn ? `Hi, ${userFullName}` : "Input Your Name"}</h2>
            <div className="nameInputWrapper" hidden={isLoggedIn}>
              <IonInput />
              <div>or</div>
              <IonButton onClick={() => console.log('Login')}>Login</IonButton>
            </div>
          </section>

          <section className="timeSection section">
            <h2>Vote a time</h2>
            {eventInfo?.times.map((row, i) => {
              return (
                <TimeVoteOption
                  key={row.time_vote_id}
                  info={row}
                  index={i}
                  onSelect={(id, action) => onTimeOptionClicked(id, action)}
                  isVoting
                />
              )
            })}
          </section>

          <section className="timeSection section">
            <h2>Vote an activity</h2>
            {eventInfo?.activities.map((row, i) => {
              return (
                <ActivityVoteOption
                  key={row.activity_vote_id}
                  info={row}
                  index={i}
                  onSelect={(id, action) => onActOptionClicked(id, action)}
                  isVoting
                />
              )
            })}
          </section>
        </div>
      </IonContent>

      <IonFooter hidden={ios} translucent style={{ height: '56px' }}>
        <IonItem lines="none">
          <IonButtons style={{ height: '56px', justifyContent: 'center', width: '100%', }}>
            <IonButton color="primary" expand="full" onClick={() => onVoteClick()}>
              Confirm vote
            </IonButton>
          </IonButtons>
        </IonItem>
      </IonFooter>
    </IonPage>
  )
}

export default EventDetailPage

async function postVotes(eventActivityIds: number[], eventTimesIds: number[], eventId: number) {
  const [origin, token] = useFetchPrecursors()

  const insertActivityVoteQuery = fetch(`${origin}/event/activity_vote`, {
    method: "POST",
    headers: {
      "Authorization": "Bearer " + token,
      "Content-Type": "application/json; charset=utf-8"
    },
    body: JSON.stringify({ eventId, eventActivityIds })
  })

  const insertTimeVoteQuery = fetch(`${origin}/event/time_vote`, {
    method: "POST",
    headers: {
      "Authorization": "Bearer " + token,
      "Content-Type": "application/json; charset=utf-8"
    },
    body: JSON.stringify({ eventId, eventTimesIds })
  })

  const [actSuccessRes, timeSuccessRes] = await Promise.all([insertActivityVoteQuery, insertTimeVoteQuery])

  let act
  let time
  try {
    act = await actSuccessRes.json()
    time = await timeSuccessRes.json()
  } catch (error) {
    throw new Error('Failed during parse json from server response')
  }

  if (act.success && time.success) return true
  if (!act.success) throw new Error(act.error)
  if (!time.success) throw new Error(time.error)

  return
}

function useFetchPrecursors() {
  let origin = getAPIServer()
  let token = localStorage.getItem('token')

  if (!origin) throw new Error("Backend API domain is not set.")
  if (!token) throw new Error("JWT token is not found in local storage.")

  return [origin, token]
}

async function fetchUserInfo() {
  const [origin, token] = useFetchPrecursors()

  const res = await fetch(`${origin}/userInfo`, {
    headers: {
      "Authorization": "Bearer " + token
    }
  })
  const response = await res.json()

  if (response.success) {
    return response.result
  } else {
    throw new Error(`Error during fetching ${origin}/userInfo: ${response.error}`)
  }
}

async function deleteEvent(eventId: number) {
  const [origin, token] = useFetchPrecursors()

  const res = await fetch(`${origin}/event`, {
    method: "DELETE",
    headers: {
      "Authorization": "Bearer " + token,
      "Content-Type": "application/json; charset=utf-8"
    },
    body: JSON.stringify({ eventId })
  })
  const response = await res.json()
  return response
}