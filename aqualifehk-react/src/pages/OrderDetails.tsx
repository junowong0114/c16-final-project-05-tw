import {
  IonBackButton,
  IonButton,
  IonButtons,
  IonContent,
  IonHeader,
  IonIcon,
  IonImg,
  IonLoading,
  IonPage,
  IonThumbnail,
  IonToolbar,
} from "@ionic/react"
import { chevronBack } from "ionicons/icons"
import { useEffect, useState } from "react"
import { useSelector, useDispatch } from "react-redux"
import { useLocation } from "react-router-dom"
import { BookingStatus } from "shared"
import { dateOrTimeFormat } from "../helpers/dateTimeFormat"
import { loadOrderDetailsThunk } from "../redux/orderDetails/thunk"
import { RootState } from "../redux/state"
import styles from "./OrderDetails.module.scss"

const OrderDetails: React.FC = () => {
  let query = new URLSearchParams(useLocation().search).get("id")
  const orderDetailsStatus = useSelector((state: RootState) => state.orderDetails)
  const from = useSelector((state: RootState) => state.general.from)
  const [isLoading, setIsLoading] = useState(false)
  const dispatch = useDispatch()

  // refresh page when coming from other pages
  useEffect(() => {
    if (from === '/tabs/order' && query) {
      setIsLoading(true)
      dispatch(loadOrderDetailsThunk(query))
    }
  }, [from])

  // load data from API server on mount
  useEffect(() => {
    setIsLoading(true)
    dispatch(loadOrderDetailsThunk(query))
  }, [])

  // check incoming data on receive data
  useEffect(() => {
    if (orderDetailsStatus) setIsLoading(false)
  }, [orderDetailsStatus])

  const status = orderDetailsStatus.orderDetails?.status
  const price = orderDetailsStatus.orderDetails?.price
  const activityName = orderDetailsStatus.orderDetails?.activityName

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
            <IonBackButton defaultHref={"/"}></IonBackButton>
          </IonButtons>
        </IonToolbar>
      </IonHeader>

      <IonContent>
        <IonLoading isOpen={isLoading} backdropDismiss></IonLoading>

        <div className={styles.bookingInfo} hidden={isLoading}>
          <div className={styles.bookingStatus}>
            Booking {status ? BookingStatus[status] : "null"}
          </div>
          <div className={styles.bookingPrice}>
            HK$ {price ? price / 100 : "null"}
          </div>
        </div>

        <div className={styles.bookingContainer} hidden={isLoading}>
          <div className={styles.bookingDetails}>
            <div className={styles.bookingTitle}>Booking Details</div>
            <div className={styles.bookingActivity}>
              {activityName ? activityName : "null"}
              <IonThumbnail className={styles.bookingActivityIcon}>
                <IonImg
                  src={`/assets/${orderDetailsStatus.orderDetails ? orderDetailsStatus.orderDetails.image : null}`}
                ></IonImg>
              </IonThumbnail>
            </div>
            <div className={styles.packageDetailsContainer}>
              <div className={styles.packageDetailsTitle}>Package Details</div>
              <div className={styles.packageDetails}>
                {orderDetailsStatus.orderDetails.packageNames.map((packageName, i) => {
                  const timeSections = orderDetailsStatus.orderDetails.timeSections
                  let timeSectionName: string | undefined = timeSections.length ? timeSections[i] : undefined

                  const quantities = orderDetailsStatus.orderDetails.quantity
                  let quantity: number | undefined = quantities.length ? quantities[i] : undefined

                  return (<div className={styles.orderDetailsSubtitle} key={i}>
                    {formatPackageData(packageName, timeSectionName, quantity)}
                  </div>)
                })}
                <div className={styles.orderDetailsSubtitle}>
                  Participation Date:{" "}
                  {
                    orderDetailsStatus.orderDetails?.dateTime ?
                      formatDateTimeOrderDetails(orderDetailsStatus.orderDetails.dateTime) :
                      "null"
                  }
                </div>
                <div className={styles.orderDetailsSubtitle}>
                  {/* {orderDetailsStatus.orderDetails.timeSection} */}
                </div>
              </div>
            </div>
            <div className={styles.bookingNumbers}>
              <div className={styles.orderDetailsSubtitle}>
                Booking number: {orderDetailsStatus.orderDetails?.bookingId ? orderDetailsStatus.orderDetails.bookingId : "null"}
              </div>
              <div className={styles.orderDetailsSubtitle}>
                Date Booked:{" "}
                {
                  orderDetailsStatus.orderDetails.dateOfBook ?
                    formatDateTimeOrderDetails(orderDetailsStatus.orderDetails.dateOfBook) :
                    "null"
                }
              </div>
            </div>
          </div>
          <div className={styles.bookingDetails} hidden={!orderDetailsStatus.shopDetails}>
            <div className={styles.bookingTitle}>Contacts</div>
            <div className={styles.bookingContact}>
              <div className={styles.orderDetailsSubtitle}>
                Shop Name: {orderDetailsStatus.shopDetails ? orderDetailsStatus.shopDetails.name : null}
              </div>
              <div className={styles.orderDetailsSubtitle}>
                Phone number: {orderDetailsStatus.shopDetails ? orderDetailsStatus.shopDetails.phone : null}
              </div>
              <div className={styles.orderDetailsSubtitle}>
                Email: {orderDetailsStatus.shopDetails ? orderDetailsStatus.shopDetails.email : null}
              </div>
            </div>
          </div>
        </div>
      </IonContent>
    </IonPage>
  )
}

export default OrderDetails

function formatDateTimeOrderDetails(unix_time: number | undefined) {
  if (!unix_time) return "null"

  return dateOrTimeFormat('date', new Date(unix_time * 1000))
}

function formatPackageData(packageName: string, timeSectionName?: string, quantity?: number) {
  if (!timeSectionName || !quantity) {
    return "null"
  }

  return (
    packageName + ' (' +
    timeSectionName + ') x ' +
    quantity.toString()
  )
}