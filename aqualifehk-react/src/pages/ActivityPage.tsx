import { ScrollDetail } from "@ionic/core"
import {
  IonButton,
  IonButtons,
  IonContent,
  IonFooter,
  IonIcon,
  IonImg,
  IonItem,
  IonLabel,
  IonList,
  IonPage,
  IonText,
  IonToolbar,
  IonLoading,
  IonRippleEffect,
} from "@ionic/react"
import {
  bookmarksOutline,
  chevronBack,
  cartOutline,
  star,
  chevronForwardOutline,
  calendarNumber,
} from "ionicons/icons"
import BulletPoint from "../components/BulletPoint"
import SectionHeader from "../components/ActivityPage/SectionHeader"
import PackageRow from "../components/ActivityPage/PackageRow"
import RatingStars from "../components/RatingStars"
import ReviewComment from "../components/ReviewComment"
import ActivityMap from "../components/ActivityPage/ActivityMap"
import { useEffect, useRef, useState } from "react"
import { useDispatch, useSelector } from "react-redux"
import { useHistory, useParams } from "react-router"
import { loadActivityThunk } from "../redux/activityPage/thunk"
import { RootState } from "../redux/state"
import { formatUnitCount } from "../helpers/format"
import "./ActivityPage.scss"
import { setIsLoadingImgActivityPage } from "../redux/activityPage/action"
import { dateToDateTimeString } from "../helpers/dateTimeFormat"
import { Link } from "react-router-dom"

type ActivityPageParams = {
  activityId: string
}

const ActivityPage: React.FC = () => {
  const { activityId } = useParams<ActivityPageParams>()
  const toolbarRef = useRef<HTMLIonItemElement>(null)
  const contentRef = useRef<HTMLIonContentElement>(null)
  const dispatch = useDispatch()
  const info = useSelector(
    (state: RootState) => state.activityPage.activityInfo
  )
  const isLoading = useSelector(
    (state: RootState) => state.activityPage.isLoading
  )
  const [selectedPackage, setSelectedPackage] = useState<any>({})
  const history = useHistory()
  const image = info.activity_images[0]
  // on component will mount
  useEffect(() => {
    // get scroll element
    let scrollElement: HTMLElement
    if (!contentRef.current) return
    contentRef.current.getScrollElement().then((res) => {
      scrollElement = res
      const scrollTop = scrollElement.scrollTop

      // update scrollbar transparency according to scroll top position
      if (!toolbarRef.current) return
      toolbarRef.current.style.backgroundColor = `rgba(255, 255, 255, ${scrollTop / 100
        })`
    })

    // download activity information
    dispatch(loadActivityThunk(activityId))
    dispatch(setIsLoadingImgActivityPage())
  }, [])

  useEffect(() => {
    if (Object.keys(selectedPackage).length === 0) {
      setSelectedPackage(
        Object.fromEntries(
          info.packages.map((_package) => {
            return [_package.id, 0]
          })
        )
      )
    }
  }, [info])

  function onIonScroll(e: CustomEvent<ScrollDetail>) {
    let scrollElement: HTMLElement
      ; (e.target as HTMLIonContentElement).getScrollElement().then((res) => {
        scrollElement = res
        const scrollTop = scrollElement.scrollTop

        if (!toolbarRef.current) return
        toolbarRef.current.style.backgroundColor = `rgba(255, 255, 255, ${scrollTop / 100
          })`

        if (scrollTop > 50) {
          document
            .querySelectorAll("ion-toolbar.topBar ion-button")
            .forEach((btn) => {
              btn.classList.remove("ion-color-light")
              btn.classList.add("ion-color-secondary")
            })

          document
            .querySelector("ion-toolbar.topBar")
            ?.classList.add("bottom-shadow")
        }

        if (scrollTop < 50) {
          document
            .querySelectorAll("ion-toolbar.topBar ion-button")
            .forEach((btn) => {
              btn.classList.remove("ion-color-secondary")
              btn.classList.add("ion-color-light")
            })

          document
            .querySelector("ion-toolbar.topBar")
            ?.classList.remove("bottom-shadow")
        }
      })
  }

  function onQtyBtnClick(id: number, isPlus: boolean) {
    setSelectedPackage((selectedPackage: any) => {
      const newState = { ...selectedPackage }
      if (isPlus) {
        newState[id] += 1
      } else {
        if (newState[id] > 0) newState[id] -= 1
      }
      return newState
    })
  }

  return (
    <IonPage className="activityPage">
      <IonLoading isOpen={isLoading} backdropDismiss keyboardClose translucent message='Loading Activity' />

      <IonContent
        fullscreen
        ref={contentRef}
        scrollEvents={true}
        onIonScroll={(e) => onIonScroll(e)}
      >
        <IonToolbar className="topBar" ref={toolbarRef} color="transparent">
          <IonButtons slot="start">
            <IonButton routerDirection="back" color="light" onClick={() => history.goBack()}>
              <IonIcon icon={chevronBack}></IonIcon>
            </IonButton>
          </IonButtons>
          <IonButtons slot="end">
            <IonButton color="light">
              <IonIcon className="infoIcon" icon={bookmarksOutline}></IonIcon>
            </IonButton>
            <IonButton routerLink="/cart" color="light">
              <IonIcon className="infoIcon" icon={cartOutline}></IonIcon>
            </IonButton>
          </IonButtons>
        </IonToolbar>

        <div className="carousel" hidden={isLoading}>
          <IonImg src={`/assets/${image}`} alt="activity image"></IonImg>
        </div>

        <div hidden={isLoading} className="infoHero">
          <section className="first-child infoSection">
            <IonList>
              <IonItem className="ion-no-padding">
                <IonLabel>
                  <h1>{info.activity_name}</h1>
                  <div className="ratingContainer">
                    <div className="chip">
                      <IonIcon icon={star}></IonIcon>
                      <IonText>{info.rating}</IonText>
                    </div>
                    <IonLabel>
                      ({formatUnitCount(info.review_count, "Review")})
                    </IonLabel>
                  </div>
                </IonLabel>
              </IonItem>
              <IonItem className="ion-no-padding" lines="none">
                <IonLabel className="sellingPointsContainer">
                  {/* TODO: remove hard coded data */}
                  <BulletPoint content="Discover Sai Kung from a different angle by renting a kayak and an exciting kayak ride" />
                  <BulletPoint content="Get up close to local sea birds and wildlife as you paddle along the serene water ways" />
                  <BulletPoint content="Tighten your bond with your friends and family in this 2-hour kayak ride" />
                  <BulletPoint content="Choose from single kayak or double kayak options for your adventure" />
                </IonLabel>
              </IonItem>
            </IonList>
          </section>

          <Link to={`/book/confirmBooking/${info.id}`} className="infoSection">
            <section className="availability ion-activatable ripple-parent infoSection">
              <IonList>
                <IonItem className="ion-no-padding" lines="none">
                  <IonIcon icon={calendarNumber} color="primary"></IonIcon>
                  <div>
                    <IonText className="bold">Check availability</IonText>
                    <IonText>
                      Available from {dateToDateTimeString(new Date(), "date")}
                    </IonText>
                  </div>
                  <IonIcon
                    slot="end"
                    icon={chevronForwardOutline}
                    className="activityInfoCheckDateArrow"
                  ></IonIcon>
                </IonItem>
              </IonList>
              <IonRippleEffect></IonRippleEffect>
            </section>
          </Link>

          <section className="infoSection packages">
            <IonList>
              <SectionHeader content="Package options" />
              {info.packages.map((_package) => {
                return (
                  <PackageRow
                    key={_package.id}
                    id={_package.id}
                    name={_package.name}
                    quantity={selectedPackage[_package.id]}
                    onMinusClick={(id: number) => onQtyBtnClick(id, false)}
                    onPlusClick={(id: number) => onQtyBtnClick(id, true)}
                    fulled={false}
                    hideQuantity={true}
                  />
                )
              })}
            </IonList>
          </section>

          <section className="availability ion-activatable ripple-parent infoSection">
            <IonList>
              <IonItem className="ion-no-padding" lines="none">
                <SectionHeader content="Location" />
              </IonItem>
              <IonItem className="ion-no-padding" lines="none">
                {/* <IonSkeletonText className="skeletonMap"></IonSkeletonText> */}
                <ActivityMap />
              </IonItem>
            </IonList>
          </section>

          <section className="ion-activatable ripple-parent infoSection">
            <IonList>
              <IonItem className="ion-no-padding" lines="none">
                <SectionHeader content="Reviews" />
              </IonItem>
              <IonItem className="ion-no-padding reviewTitle" lines="none">
                <div slot="start" className="rating" color="primary">
                  {info.rating}
                </div>
                <div slot="start">
                  <RatingStars
                    rating={info.rating}
                    width={17}
                    margin={2.2}
                  ></RatingStars>
                  <div className="reviewsCount">
                    {formatUnitCount(info.review_count, "Review")}
                  </div>
                </div>
              </IonItem>
              <ReviewComment></ReviewComment>
            </IonList>
            <IonRippleEffect></IonRippleEffect>
          </section>

        </div>
      </IonContent>

      <IonFooter hidden={isLoading}>
        <IonToolbar className="ion-no-padding">
          <div>
            <IonButton color="primary" size="default" routerLink={`/book/cart/${info.id}`}>Add to cart</IonButton>
            <IonButton color="secondary" size="default" routerLink={`/book/confirmBooking/${info.id}`}>Book Now</IonButton>
          </div>
        </IonToolbar>
      </IonFooter>
    </IonPage>
  )
}

export default ActivityPage
