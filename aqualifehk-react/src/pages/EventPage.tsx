import {
  getConfig,
  IonActionSheet,
  IonAlert,
  IonButton,
  IonButtons,
  IonContent,
  IonFab,
  IonFabButton,
  IonHeader,
  IonIcon,
  IonPage,
  IonRippleEffect,
  IonSelect,
  IonSelectOption,
  IonTitle,
  IonToolbar,
} from "@ionic/react"
import EventCard from "../components/EventPage/EventCard"
import { add, addOutline, cartOutline, createOutline } from "ionicons/icons"
import { useEffect, useRef, useState } from "react"
import { DTO } from "shared"
import "./EventPage.scss"
import { getAPIServer } from "../helpers/api"
import { useDispatch, useSelector } from "react-redux"
import { setGeneralMessage, setGeneralShowToast } from "../redux/general/action"
import { errorToString } from "../helpers/format"
import { setEventList } from "../redux/eventPage/action"
import { RootState } from "../redux/state"
import { useHistory } from "react-router-dom"

enum displayStatusEnum {
  "All" = 0,
  "Planning",
  "Confirmed",
  "Ended"
}

const EventPage: React.FC = () => {
  const ios = getConfig()?.get('mode') === 'ios'
  const dispatch = useDispatch();
  const [displayStatus, setDisplayStatus] = useState<keyof typeof displayStatusEnum>("All")
  const [windowWidth, setWindowWidth] = useState<number>(0)
  const [eventDetails, setEventDetails] = useState<DTO.EventDetail[]>([])
  const [isEditing, setIsEditing] = useState(false)
  const [showActionSheet, setShowActionSheet] = useState<boolean>(false)
  const [showNewEventAlert, setShowNewEventAlert] = useState<boolean>(false)
  const inviteCodeBoxRef = useRef<HTMLIonAlertElement>(null)
  const [eventListLocal, setEventListLocal] = useState<DTO.EventDetail[]>([])
  const userId = useSelector((state: RootState) => state.auth.user?.id)
  const from = useSelector((state: RootState) => state.general.from)
  const history = useHistory()

  useEffect(() => {
    setWindowWidth(window.innerWidth)

    // TODO: call API server for real data
    loadEvents()
      .then(res => {
        setEventDetails(res)
        dispatch(setEventList(res))
      })
      .catch(error => {
        dispatch(setGeneralMessage(errorToString(error)))
        dispatch(setGeneralShowToast(true))
      })
  }, [from])

  function onJoinEvent() {
    const inviteCode = (inviteCodeBoxRef.current?.querySelector('input'))?.value

    if (!inviteCode || inviteCode.length === 0) {
      dispatch(setGeneralMessage('Invite Code cannot be empty!'))
      dispatch(setGeneralShowToast(true))
      return
    }

    const [origin, token] = useFetchPrecursors()
    fetch(`${origin}/event/insert_participant`, {
      method: "PUT",
      headers: {
        "Authorization": "Bearer " + token,
        "Content-Type": "application/json; charset=utf-8",
      },
      body: JSON.stringify({ inviteCode: inviteCode })
    })
      .then(res => {
        return res.json()
      })
      .then(res => {
        if (res.success) {
          dispatch(setGeneralMessage('You are invited to the party!'))
          dispatch(setGeneralShowToast(true))

          return loadEvents()
        } else {
          throw new Error(res.error)
        }
      })
      .then(res => {
        setEventDetails(res)
        dispatch(setEventList(res))
        setEventListLocal(res)
      })
      .catch(e => {
        dispatch(setGeneralMessage(errorToString(e)))
        dispatch(setGeneralShowToast(true))
      })
  }

  function onAddClick() {
    if (!userId) {
      dispatch(setGeneralMessage('Error: No user id is found in local storage'))
      dispatch(setGeneralShowToast(true))
      return
    }
    history.push('/create_event')
  }

  return (
    <IonPage className="eventPage">
      <IonHeader>
        <IonToolbar>
          <IonTitle>Events</IonTitle>
          <IonButtons slot="end">
            <IonButton onClick={() => setIsEditing(!isEditing)} >
              <IonIcon icon={createOutline} />
            </IonButton>
            <IonButton hidden={!ios} onClick={() => setShowActionSheet(true)}>
              <IonIcon icon={addOutline} />
            </IonButton>
          </IonButtons>
        </IonToolbar>
      </IonHeader>

      <IonContent fullscreen color="light">
        <IonActionSheet
          isOpen={showActionSheet}
          header="Event action"
          buttons={[{
            text: "Invite code",
            handler: () => setShowNewEventAlert(true)
          },
          {
            text: "Create event",
            handler: () => onAddClick(),
          },
          {
            text: "Cancel",
            role: "cancel"
          }]}
          onDidDismiss={() => setShowActionSheet(false)}
        />

        <IonAlert
          ref={inviteCodeBoxRef}
          isOpen={showNewEventAlert}
          onWillDismiss={() => setShowNewEventAlert(false)}
          header='Input the invite code'
          inputs={[
            {
              name: 'inviteCode',
              type: 'text',
              label: 'Invite Code',
            }
          ]}
          buttons={[
            {
              text: 'Cancel',
              role: 'cancel',
              handler: () => {
                console.log('Confirm Cancel');
              }
            },
            {
              text: 'Ok',
              handler: () => onJoinEvent(),
            }
          ]}
        />

        <IonFab
          vertical="bottom"
          horizontal="end"
          slot="fixed"
          style={{ bottom: '30px', right: '20px' }}
          hidden={ios}>
          <IonFabButton onClick={() => setShowActionSheet(true)}>
            <IonIcon icon={add} />
          </IonFabButton>
        </IonFab>

        <section className="d-flex ion-justify-content-between selectorContainer">
          <IonSelect value={displayStatusEnum[0]} okText="Select" cancelText="Dismiss" color="dark">
            <IonSelectOption value={displayStatusEnum[0]}>{displayStatusEnum[0]}</IonSelectOption>
            <IonSelectOption value={displayStatusEnum[1]}>{displayStatusEnum[1]}</IonSelectOption>
            <IonSelectOption value={displayStatusEnum[2]}>{displayStatusEnum[2]}</IonSelectOption>
            <IonSelectOption value={displayStatusEnum[3]}>{displayStatusEnum[3]}</IonSelectOption>
          </IonSelect>
          <IonButtons>
            <IonButton routerLink="/cart">
              <IonIcon icon={cartOutline} />
            </IonButton>
          </IonButtons>
        </section>

        <div className="noEventContainer" hidden={eventDetails.length !== 0}>
          <div>
            <button
              onClick={() => onAddClick()}
              className="ion-activatable ripple-parent" >
              Create
              <IonRippleEffect></IonRippleEffect>
            </button>
            an event, or
          </div>
          <div>
            <button
              onClick={() => setShowNewEventAlert(true)}
              className="ion-activatable ripple-parent">
              Join
              <IonRippleEffect></IonRippleEffect>
            </button>
            an event via invite code
          </div>
        </div>

        <section className="eventCards">
          {eventDetails.map(detail => {
            return <EventCard
              key={detail.event_id}
              windowWidth={windowWidth}
              eventDetail={detail}
              ios={ios}
              isEditing={isEditing} />
          })}
        </section>
      </IonContent>
    </IonPage>
  )
}

export default EventPage

async function addEvent(ownerId: number, eventInfo?: any) {
  // mockData
  const mockInfo = {
    event_name: "Kayak with Tecky c16 good friends",
    owner_id: ownerId,
    activities: [{
      activity_id: 1,
    },
    {
      activity_id: 2,
    }],
    times: [{
      start_time_unix: (new Date()).setHours(8, 0, 0, 0),
      end_time_unix: (new Date()).setHours(15, 0, 0, 0)
    },
    {
      start_time_unix: (new Date()).setDate(29),
    }]
  }

  const [origin, token] = useFetchPrecursors()
  const res = await (await fetch(`${origin}/event`, {
    method: "POST",
    headers: {
      "Authorization": "Bearer " + token,
      "Content-Type": "application/json; charset=utf-8"
    },
    body: JSON.stringify({
      eventInfo: mockInfo
    })
  })).json()

  return res
}

async function loadEvents(): Promise<DTO.EventDetail[]> {
  const [origin, token] = useFetchPrecursors()
  const res = await (await fetch(`${origin}/event`, {
    headers: {
      "Authorization": "Bearer " + token
    }
  })).json()

  if (!res.success) throw new Error(`${res.error}`)
  return res.result
}

function useFetchPrecursors() {
  let origin = getAPIServer()
  let token = localStorage.getItem('token')

  if (!origin) throw new Error("Backend API domain is not set.")
  if (!token) throw new Error("JWT token is not found in local storage.")

  return [origin, token]
}