import { IonContent, IonImg, IonPage } from "@ionic/react"

export default function Landing() {
  return (
    <IonPage>
      <IonContent>
        <IonImg src="../../public/logo/logo_transparent.png"></IonImg>
      </IonContent>
    </IonPage>
  )
}
