import { useEffect, useRef, useState } from "react"

import {
  IonBackButton,
  IonButton,
  IonContent,
  IonHeader,
  IonIcon,
  IonPage,
  IonSearchbar,
  IonToolbar,
} from "@ionic/react"
import { closeOutline, mapOutline, listOutline } from "ionicons/icons"

import SearchList from "../components/SearchPage/SearchList"
import SearchMap from "../components/SearchPage/SearchMap"
import SearchFilters from "../components/SearchPage/SearchFilters"

import "./SearchPage.scss"
import { useDispatch, useSelector } from "react-redux"
import { SearchbarChangeEventDetail } from "@ionic/core"
import { searchActivityThunk } from "../redux/searchActivity/thunk"
import { RootState } from "../redux/state"
import { clearSearchActivityState } from "../redux/searchActivity/action"
import React from "react"
import { useParams } from "react-router"
import { setGeneralMessage, setGeneralShowToast } from "../redux/general/action"

type SearchParams = {
  keyword: string
}

const SearchPage: React.FC = () => {
  const dispatch = useDispatch()
  const { keyword } = useParams<SearchParams>()
  const searchState = useSelector((state: RootState) => state.searchActivity)
  const nextPage = searchState.nextPage
  const nextQueryId = searchState.nextQueryId

  // on component will mount
  useEffect(() => {
    return () => {
      dispatch(clearSearchActivityState())
    }
  }, [])

  // search from front page by pressing category icons
  useEffect(() => {
    if (keyword) {
      if (searchBarRef.current) searchBarRef.current.value = keyword
      dispatch(searchActivityThunk(keyword, nextQueryId, 1))
    }
  }, [keyword])

  // show map statt
  const [showMap, setShowMap] = useState(false)
  function onMapBtnClick() {
    setShowMap(!showMap)
  }

  // dispatch search thunk on search bar change
  function onSearchBarChange(e: CustomEvent<SearchbarChangeEventDetail>) {
    const searchText = e.detail.value
    if (searchText) {
      dispatch(clearSearchActivityState())
      dispatch(searchActivityThunk(searchText, nextQueryId, 1))
    }
  }

  // infinite scroll
  const searchBarRef = useRef<HTMLIonSearchbarElement>(null)
  const contentRef = useRef<HTMLIonContentElement>(null)
  let scrollElement: HTMLElement

  useEffect(() => {
    if (contentRef.current && contentRef.current.getScrollElement) {
      contentRef.current.getScrollElement().then((res) => {
        scrollElement = res
      })
    }
  }, [searchState, showMap])

  function onScrollEnd(e: any) {
    if (showMap) return
    if (!scrollElement || searchState.reachedEnd) return
    const scrollTop = scrollElement.scrollTop
    const scrollHeight = scrollElement.scrollHeight
    const clientHeight = scrollElement.clientHeight
    const bottomReached = scrollTop + clientHeight * 1.5 >= scrollHeight

    if (bottomReached) {
      const searchBar = searchBarRef.current
      let searchText: string
      if (searchBar?.value) {
        searchText = searchBar.value
        dispatch(searchActivityThunk(searchText, nextQueryId, nextPage))
      }
    }
  }

  // alert user with 0 length search result
  useEffect(() => {
    if (searchState.nextQueryId === 0) return
    if (searchState.reachedEnd === true) {
      if (searchState.searchResult.length === 0) {
        dispatch(setGeneralMessage('No matching result'))
        dispatch(setGeneralShowToast(true))
        return
      } else {
        dispatch(setGeneralMessage('End of search result'))
        dispatch(setGeneralShowToast(true))
        return
      }
    }
  }, [searchState.reachedEnd])

  return (
    <IonPage className="searchPage">
      <IonHeader>
        <IonToolbar className="ion-no-padding">
          <div className="searchBarContainer">
            <IonBackButton
              defaultHref="/"
              icon={closeOutline}
              text=""
              className="ionBackButton"
              color="primary"
            />
            <IonSearchbar
              onIonChange={(e) => onSearchBarChange(e)}
              debounce={1000}
              className="ionSearchBar"
              ref={searchBarRef}
            />
            <IonButton
              color="primary"
              fill="clear"
              size="small"
              onClick={() => onMapBtnClick()}
            >
              <IonIcon icon={!showMap ? mapOutline : listOutline} />
            </IonButton>
          </div>
        </IonToolbar>
      </IonHeader>

      <IonContent
        color="light"
        scrollEvents={true}
        onIonScrollEnd={(e) => onScrollEnd(e)}
        ref={contentRef}
      >
        <SearchFilters />

        {showMap ? <SearchMap /> : <SearchList />}
      </IonContent>
    </IonPage>
  )
}

export default SearchPage
