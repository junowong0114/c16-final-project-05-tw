import { DTO } from "shared"

export function clearSearchActivityState() {
  return {
    type: "@searchActivity/clear" as const,
  }
}

export function setSearchActivityLoading(queryId: number) {
  return {
    type: "@searchActivity/setLoading" as const,
    queryId,
  }
}

export function searchActivitySuccess(
  queryId: number,
  searchResult: DTO.SearchActivity[]
) {
  return {
    type: "@searchActivity/searchSuccess" as const,
    queryId,
    searchResult,
  }
}

export function searchActivityFailed(error: string) {
  return {
    type: "@searchActivity/searchFailed" as const,
    error,
  }
}

export function setSearchActivityReachedEnd(reachedEnd: boolean) {
  return {
    type: "@searchActivity/setReachedEnd" as const,
    reachedEnd
  }
}

export type SearchActivityAction =
  | ReturnType<typeof clearSearchActivityState>
  | ReturnType<typeof setSearchActivityLoading>
  | ReturnType<typeof searchActivitySuccess>
  | ReturnType<typeof searchActivityFailed>
  | ReturnType<typeof setSearchActivityReachedEnd>
