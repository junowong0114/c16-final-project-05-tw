import { DTO } from "shared"

export type SearchActivityState = {
  searchResult: DTO.SearchActivity[]
  nextQueryId: number
  nextPage: number
  isLoading: boolean
  reachedEnd: boolean
  error?: string
}

export const initialState: SearchActivityState = {
  searchResult: [],
  nextQueryId: 0,
  nextPage: 1,
  isLoading: false,
  reachedEnd: true,
}
