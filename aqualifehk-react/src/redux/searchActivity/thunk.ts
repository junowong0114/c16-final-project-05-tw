import { DTO } from "shared"
import { Dispatch } from "redux"
import { SearchActivityAction, searchActivityFailed, setSearchActivityLoading, setSearchActivityReachedEnd } from "./action"
import { searchActivitySuccess } from "./action"
import { getAPIServer } from "../../helpers/api"
import { errorToString } from "../../helpers/format"

export function searchActivityThunk(
  searchText: string,
  queryId: number,
  page: number
) {
  return async (dispatch: Dispatch<SearchActivityAction>) => {
    // do nothing if searchText is empty
    if (searchText === "") return

    dispatch(setSearchActivityLoading(queryId))
    dispatch(setSearchActivityReachedEnd(false))
    const itemsPerQuery = 10

    let APIServer
    try {
      APIServer = getAPIServer()
    } catch (error) {
      dispatch(searchActivityFailed(errorToString(error)))
      console.log(error)
    }

    let result;
    try {
      const res = await fetch(`${APIServer}/search/?keyword=${searchText}&page=${page}`)
      result = await res.json()
    } catch (error) {
      console.error("GET /search/all network error:", error)
      dispatch(searchActivityFailed(errorToString(error)))
    }

    if (!result.success) {
      console.error("GET /search/all server error:", result.error)
      dispatch(searchActivityFailed(errorToString(result.error)))
    }

    const activities: DTO.SearchActivity[] = result.activities;

    dispatch(searchActivitySuccess(queryId, activities))
  }
}
