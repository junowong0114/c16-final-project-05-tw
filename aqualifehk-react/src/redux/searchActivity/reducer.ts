import { SearchActivityState, initialState } from "./state"
import { SearchActivityAction } from "./action"

export const searchActivityReducer = (
  state: SearchActivityState = initialState,
  action: SearchActivityAction
): SearchActivityState => {
  switch (action.type) {
    case "@searchActivity/clear": {
      return {
        ...state,
        searchResult: [],
        isLoading: false,
        reachedEnd: true,
        nextPage: 1,
        error: undefined,
      }
    }

    case "@searchActivity/setLoading": {
      return {
        ...state,
        isLoading: true,
        nextQueryId: action.queryId + 1,
      }
    }

    case "@searchActivity/searchSuccess": {
      if (!isLatestQuery(action.queryId, state.nextQueryId)) return state

      if (action.searchResult.length === 0) {
        return {
          ...state,
          reachedEnd: true,
          isLoading: false,
          nextPage: state.nextPage + 1,
        }
      }

      if (isLoadingFirstPage(state.nextPage)) {
        return {
          ...state,
          searchResult: action.searchResult,
          isLoading: false,
          nextPage: state.nextPage + 1,
        }
      } else {
        return {
          ...state,
          searchResult: [...state.searchResult, ...action.searchResult],
          isLoading: false,
          nextPage: state.nextPage + 1,
        }
      }
    }

    case "@searchActivity/searchFailed": {
      return {
        ...state,
        searchResult: [],
        isLoading: false,
        error: action.error,
      }
    }

    case "@searchActivity/setReachedEnd": {
      return {
        ...state,
        reachedEnd: action.reachedEnd
      }
    }

    default: {
      return state
    }
  }
}

function isLatestQuery(queryId: number, nextQueryId: number) {
  return queryId + 1 === nextQueryId
}

function isLoadingFirstPage(page: number) {
  return page === 1
}
