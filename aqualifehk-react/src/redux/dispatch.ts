import type { ThunkDispatch } from "redux-thunk"
import type { RootState } from "./state"
import type { RootAction } from "./action"

export type RootDispatch = ThunkDispatch<RootState, unknown, RootAction>
