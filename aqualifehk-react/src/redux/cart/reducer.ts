import { CartState, initialState } from "./state"
import { CartAction } from "./action"

export const cartReducer = (
  state: CartState = initialState,
  action: CartAction
): CartState => {
  switch (action.type) {
    case "@Cart/loadCart": {
      return {
        ...state,
        items: action.items,
      }
    }

    case "@Cart/loadCartFailed": {
      return {
        ...state,
        error: action.error,
      }
    }

    case "@Cart/selectItem": {
      const alreadySelected = state.selectedItemsId.some(
        (id) => id === action.id
      )

      return {
        ...state,
        selectedItemsId: alreadySelected
          ? state.selectedItemsId
          : [...state.selectedItemsId, action.id],
      }
    }

    case "@Cart/unselectItem": {
      return {
        ...state,
        selectedItemsId: state.selectedItemsId.filter((id) => id !== action.id),
      }
    }

    case "@Cart/selectAllItems": {
      return {
        ...state,
        selectedItemsId: state.items.map((item) => item.id),
      }
    }

    case "@Cart/unselectAllItems": {
      return {
        ...state,
        selectedItemsId: [],
      }
    }

    case "@Cart/deleteItemSuccess": {
      return {
        ...state,
        items: state.items.filter((item) => item.id !== action.id),
        selectedItemsId: state.selectedItemsId.filter((id) => id !== action.id),
      }
    }

    case "@Cart/deleteFailed": {
      return {
        ...state,
        error: action.error,
      }
    }

    default:
      return state
  }
}
