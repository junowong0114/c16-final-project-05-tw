import { DTOBooking } from "../../helpers/DTOformat"

export type CartState = {
  items: DTOBooking[]
  selectedItemsId: number[]
  error?: string
}

export const initialState: CartState = {
  items: [],
  selectedItemsId: [],
}
