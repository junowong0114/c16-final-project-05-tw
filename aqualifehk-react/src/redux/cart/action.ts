import { DTOBooking } from "../../helpers/DTOformat"

export function loadCart(items: DTOBooking[]) {
  return {
    type: "@Cart/loadCart" as const,
    items,
  }
}

export function loadCartFailed(error: string) {
  return {
    type: "@Cart/loadCartFailed" as const,
    error,
  }
}

export function selectItem(id: number) {
  return {
    type: "@Cart/selectItem" as const,
    id,
  }
}

export function unselectItem(id: number) {
  return {
    type: "@Cart/unselectItem" as const,
    id,
  }
}

export function selectAllItems() {
  return {
    type: "@Cart/selectAllItems" as const,
  }
}

export function unselectAllItems() {
  return {
    type: "@Cart/unselectAllItems" as const,
  }
}

export function deleteItemSuccess(id: number) {
  return {
    type: "@Cart/deleteItemSuccess" as const,
    id,
  }
}

export function deleteItemFailed(error: string) {
  return {
    type: "@Cart/deleteFailed" as const,
    error,
  }
}

export type CartAction =
  | ReturnType<typeof loadCart>
  | ReturnType<typeof loadCartFailed>
  | ReturnType<typeof selectItem>
  | ReturnType<typeof unselectItem>
  | ReturnType<typeof selectAllItems>
  | ReturnType<typeof unselectAllItems>
  | ReturnType<typeof deleteItemSuccess>
  | ReturnType<typeof deleteItemFailed>
