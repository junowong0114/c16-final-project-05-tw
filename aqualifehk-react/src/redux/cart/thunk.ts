import { Dispatch } from "redux"
import {
  CartAction,
  deleteItemSuccess,
  deleteItemFailed,
  loadCart,
  loadCartFailed,
} from "./action"
import { DTO } from "../type"
import { getAPIServer } from "../../helpers/api"
import { RootState } from "../state"
import { errorToString } from "../../helpers/format"
import { formatDTOBooking, DTOBooking } from "../../helpers/DTOformat"

export function loadCartThunk() {
  return async (dispatch: Dispatch<CartAction>, getState: () => RootState) => {
    // JWT guard
    const token = getState().auth.token
    if (!token) {
      // TODO: give user a new token if token is not found
      dispatch(
        loadCartFailed("Error during loading cart: user token not found.")
      )
      return
    }

    let APIServer
    try {
      APIServer = getAPIServer()
    } catch (error) {
      dispatch(loadCartFailed(errorToString(error)))
      return
    }

    let result: any
    try {
      const res = await fetch(`${APIServer}/cart`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      result = await res.json()
    } catch (error) {
      console.error("GET /cart/all network error:", error)
      dispatch(loadCartFailed(errorToString(error)))
      return
    }

    if (!result.success) {
      console.error("GET /cart/all server error:", result.error)
      dispatch(loadCartFailed(result.error))
      return
    }

    const items: DTO.Booking[] = result.items
    const returnItems: DTOBooking[] = formatDTOBooking(items)

    dispatch(loadCart(returnItems))
  }
}

export function deleteItemThunk(id: number) {
  return async (dispatch: Dispatch<CartAction>, getState: () => RootState) => {
    // TODO: call backend API to remove item from cart
    // JWT guard
    const token = getState().auth.token

    if (!token) {
      // TODO: give user a new token if token is not found
      dispatch(loadCartFailed("Error during loading cart: user id not found."))
      return
    }

    let APIServer
    try {
      APIServer = getAPIServer()
    } catch (error) {
      dispatch(loadCartFailed(errorToString(error)))
      return
    }

    try {
      await fetch(`${APIServer}/cart/${id}`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
        method: "DELETE",
      })
    } catch (error) {
      dispatch(deleteItemFailed(errorToString(error)))
    }

    dispatch(deleteItemSuccess(id))
  }
}
