import { AuthAction } from "./auth/action"
import { CartAction } from "./cart/action"
import { OrderAction } from "./order/action"
import { SearchActivityAction } from "./searchActivity/action"
import { GeneralAction } from "./general/action"
import { OrderDetailAction } from "./orderDetails/action"
import { ActivityPageAction } from "./activityPage/action"
import { BookingAction } from "./BookingOption/action"
import { EventPageAction } from "./eventPage/action"

export type RootAction =
  | AuthAction
  | CartAction
  | OrderAction
  | SearchActivityAction
  | GeneralAction
  | OrderDetailAction
  | ActivityPageAction
  | BookingAction
  | EventPageAction
