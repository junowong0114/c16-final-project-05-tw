import { OrderAction } from "./action"
import { OrderState, loadOrderInitialState } from "./state"

export const orderReducer = (
  state: OrderState = loadOrderInitialState(),
  action: OrderAction
): OrderState => {
  switch (action.type) {
    case "loadOrder": {
      return {
        ...state,
        orders: action.orders,
      }
    }
    case "loadOrderFailed": {
      return {
        ...state,
        error: action.error,
      }
    }
    default:
      return state
  }
}
