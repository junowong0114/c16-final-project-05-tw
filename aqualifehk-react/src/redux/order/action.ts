import { order } from "shared"

export function loadOrder(orders: order[]) {
  return {
    type: "loadOrder" as const,
    orders,
  }
}

export function loadOrderFailed(error: string) {
  return {
    type: "loadOrderFailed" as const,
    error,
  }
}

export type OrderAction =
  | ReturnType<typeof loadOrder>
  | ReturnType<typeof loadOrderFailed>
