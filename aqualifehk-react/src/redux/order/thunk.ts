import { loadOrder, loadOrderFailed, OrderAction } from "./action"
import { Dispatch } from "redux"
import { DTO } from "../type"
import { getAPIServer } from "../../helpers/api"
import { errorToString } from "../../helpers/format"
import { RootState } from "../state"
import { orderFormat } from "../../helpers/orderFormat"

export function loadOrderThunk() {
  return async (dispatch: Dispatch<OrderAction>, getState: () => RootState) => {

    // get token and APISever ip/domain
    let token: string | undefined
    let APIServer: string
    try {
      token = getState().auth.token
      if (!token) throw new Error("Error during loading order: user id not found.")
      APIServer = getAPIServer()
    } catch (error) {
      dispatch(loadOrderFailed(errorToString(error)))
      return
    }

    let result: any
    let items: DTO.Booking[]
    try {
      // fetch API server
      const res = await fetch(`${APIServer}/order`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      result = await res.json()
      if (!result.success) throw new Error('')

      // get 
      items = result.items
      if (items.some(row => !row)) throw new Error('Corrupted data from server')

    } catch (error) {
      console.error("GET /order/all network error:", error)
      dispatch(loadOrderFailed(errorToString(error)))
      return
    }

    const returnItems: ReturnType<typeof orderFormat> = orderFormat(items)
    dispatch(loadOrder(returnItems))
  }
}
