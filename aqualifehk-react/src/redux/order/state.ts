import { order } from "../type"

export type OrderState = {
  orders: order[]
  error?: string
}

export const loadOrderInitialState = (): OrderState => {
  return {
    orders: [],
  }
}
