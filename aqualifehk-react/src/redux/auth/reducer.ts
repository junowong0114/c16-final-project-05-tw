import { initialState, AuthState } from "./state"
import { AuthAction } from "./action"

export const authReducer = (
  state: AuthState = initialState,
  action: AuthAction
): AuthState => {
  switch (action.type) {
    case "@Auth/loadingLogin":
      return { loading: true }

    case "@Auth/loginSuccess":
      return { loading: false, user: action.payload, token: action.token }

    case "@Auth/loginFailed":
      return { loading: false, error: action.reason }

    case "@Auth/logout":
      return initialState

    case "@Auth/loadToken":
      return {
        ...state,
        token: action.token,
      }

    case "@Auth/assignNonRegisteredToken":
      return {
        loading: false,
        user: action.payload,
        token: action.token,
      }

    case "@Auth/registerFail":
      return {
        ...state,
        error: action.error,
      }

    case "@Auth/registerErrorClear":
      return {
        ...state,
        error: undefined
      }

    default:
      return state
  }
}
