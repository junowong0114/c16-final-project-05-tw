import { getAPIServer } from "../../helpers/api"
import { errorToString } from "../../helpers/format"
import { RootDispatch } from "../dispatch"
import { JWTPayload } from "../type"
import {
  assignNonRegisteredToken,
  loadingLogin,
  loginFailed,
  loginSuccess,
  logout,
  registerFail,
} from "./action"
import jwtDecode from "jwt-decode"
import { RootState } from "../state"
import { registerUser } from "shared"
import { setGeneralMessage, setGeneralShowToast } from "../general/action"

export function loginWithPasswordThunk(email: string, password: string) {
  return async (dispatch: RootDispatch) => {
    // get REACT_APP_API_SERVER
    let origin
    try {
      origin = getAPIServer()
    } catch (error) {
      dispatch(loginFailed(errorToString(error)))
      return
    }

    // set state as loading: true
    dispatch(loadingLogin())

    // get json with token
    let json: any
    try {
      const res = await fetch(`${origin}/loginPassword`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ email, password }),
      })
      json = await res.json()
    } catch (error) {
      // network error
      console.error("failed to call login with password API:", error)
      dispatch(loginFailed(errorToString(error)))
      return
    }

    // server side error
    if (json.error) {
      console.error("login with password response error:", json.error)
      dispatch(loginFailed(json.error))
      return
    }

    // retrieve token
    const token: string = json.token
    dispatch(handleTokenThunk(token))
  }
}

function handleTokenThunk(token: string) {
  return (dispatch: RootDispatch) => {
    localStorage.setItem("token", token)
    try {
      const payload = jwtDecode<JWTPayload>(token)
      dispatch(loginSuccess(token, payload))
      return
    } catch (error) {
      console.error("failed to decode JWT token:", error)
      dispatch(loginFailed(errorToString(error)))
    }
  }
}

export function checkTokenThunk() {
  return (dispatch: RootDispatch) => {
    const token = localStorage.getItem("token")
    if (!token) {
      // dispatch(logout())
      dispatch(getNonRegisteredTokenThunk())
      return
    }
    dispatch(handleTokenThunk(token))
  }
}

export function getNonRegisteredTokenThunk() {
  return async (dispatch: RootDispatch) => {
    let origin
    try {
      origin = getAPIServer()
    } catch (error) {
      dispatch(loginFailed(errorToString(error)))
      return
    }

    let json
    try {
      const res = await fetch(`${origin}/getNonRegisteredToken`)
      json = await res.json()
    } catch (error) {
      // network error
      console.log(error)
      return
    }
    if (json.error) {
      console.error("get non registered token error", json.error)
      return
    }

    const token: string = json.token
    localStorage.setItem("token", token)
    const payload = jwtDecode<JWTPayload>(token)
    dispatch(assignNonRegisteredToken(token, payload))
    dispatch(setGeneralMessage('Logout Success'))
    dispatch(setGeneralShowToast(true))
  }
}


export function getUserInfo() {
  return async (dispatch: RootDispatch, getState: () => RootState) => {
    const token = getState().auth.token


    let APIServer
    try {
      APIServer = getAPIServer()
    } catch (error) {
      console.log(error)
      return
    }

    let result: any

  }
}


export function registerUserThunk(user: registerUser) {
  return async (dispatch: RootDispatch, getState: () => RootState) => {
    const token = getState().auth.token
    if (!token) {
      dispatch(registerFail("Error during loading cart: user token not found."))
      return
    }

    let APIServer
    try {
      APIServer = getAPIServer()
    } catch (error) {
      dispatch(registerFail(errorToString(error)))
      console.log(error)
      return
    }

    let result
    try {
      const res = await fetch(APIServer + '/registerUser', {
        method: "POST",
        headers: {
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json; charset=utf-8",
        },
        body: JSON.stringify(user)
      })
      result = await res.json()
    } catch (error) {
      console.log('get register error: ', error)
      dispatch(registerFail(errorToString(error)))
      return
    }
    // to do remove it
    console.log(result);
    if(!result.success){
      dispatch(registerFail(errorToString(result.error)))
      return
    }

    const newToken: string = result.token
    dispatch(handleTokenThunk(newToken));
  }
}