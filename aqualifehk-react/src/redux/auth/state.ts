import { JWTPayload } from "../type"

export type AuthState = {
  user?: JWTPayload
  loading: boolean
  error?: string
  token?: string
}

export const initialState: AuthState = {
  user: undefined,
  loading: false,
  token: undefined,
  error: undefined,
}
