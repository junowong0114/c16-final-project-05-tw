import { JWTPayload } from "../type"

export function loginSuccess(token: string, payload: JWTPayload) {
  return {
    type: "@Auth/loginSuccess" as const,
    token,
    payload,
  }
}

export function loginFailed(reason: string) {
  return {
    type: "@Auth/loginFailed" as const,
    reason,
  }
}

export function logout() {
  return {
    type: "@Auth/logout" as const,
  }
}

export function loadingLogin() {
  return {
    type: "@Auth/loadingLogin" as const,
  }
}

export function loadToken(token: string) {
  return {
    type: "@Auth/loadToken" as const,
    token,
  }
}

export function registerFail(error: string) {
  return {
    type: "@Auth/registerFail" as const,
    error,
  }
}

export function registerErrorClear() {
  return {
    type: "@Auth/registerErrorClear" as const,
  }
}

export function assignNonRegisteredToken(token: string, payload: JWTPayload) {
  return {
    type: "@Auth/assignNonRegisteredToken" as const,
    token,
    payload,
  }
}

export type AuthAction =
  | ReturnType<typeof loginSuccess>
  | ReturnType<typeof loginFailed>
  | ReturnType<typeof logout>
  | ReturnType<typeof loadingLogin>
  | ReturnType<typeof loadToken>
  | ReturnType<typeof assignNonRegisteredToken>
  | ReturnType<typeof registerFail>
  | ReturnType<typeof registerErrorClear>