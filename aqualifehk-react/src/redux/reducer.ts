import { orderDetailsReducer } from "./orderDetails/reducer"
import { RootState } from "./state"
import { RootAction } from "./action"
import { combineReducers } from "redux"
import { authReducer } from "./auth/reducer"
import { cartReducer } from "./cart/reducer"
import { orderReducer } from "./order/reducer"
import { searchActivityReducer } from "./searchActivity/reducer"
import { generalReducer } from "./general/reducer"
import { activityPageReducer } from "./activityPage/reducer"
import { bookingReducer } from "./BookingOption/reducer"
import { homePageReducer } from "./homepage/reducer"
import { eventPageReducer } from "./eventPage/reducer"

export const rootReducer: (
  state: RootState | undefined,
  action: RootAction
) => RootState = combineReducers<RootState>({
  auth: authReducer,
  cart: cartReducer,
  order: orderReducer,
  searchActivity: searchActivityReducer,
  orderDetails: orderDetailsReducer,
  general: generalReducer,
  activityPage: activityPageReducer,
  booking: bookingReducer,
  homePage: homePageReducer,
  eventPage: eventPageReducer,
})
