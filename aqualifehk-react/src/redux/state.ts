import { AuthState } from "./auth/state"
import { CartState } from "./cart/state"
import { OrderState } from "./order/state"
import { SearchActivityState } from "./searchActivity/state"
import { GeneralState } from "./general/state"
import { OrderDetailsState } from "./orderDetails/state"
import { ActivityPageState } from "./activityPage/state"
import { BookingState } from "./BookingOption/state"
import { HomePageState } from "./homepage/state"
import { EventPageState } from "./eventPage/state"

export type RootState = {
  auth: AuthState
  cart: CartState
  order: OrderState
  searchActivity: SearchActivityState
  orderDetails: OrderDetailsState
  general: GeneralState
  activityPage: ActivityPageState
  booking: BookingState
  homePage: HomePageState
  eventPage: EventPageState
}
