import { createStore, applyMiddleware, compose } from "redux"
import { rootReducer } from "./reducer"
import { createLogger } from "redux-logger"
import thunk from "redux-thunk"

declare global {
  /* tslint:disable:interface-name */
  interface Window {
    __REDUX_DEVTOOLS_EXTENSION__: any
    __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: any
  }
}

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

const logger = createLogger({
  collapsed: true,
})

const rootEnhancer = composeEnhancers(
  applyMiddleware(thunk),
  // applyMiddleware(logger),
)

const store = createStore(rootReducer, rootEnhancer)

export default store
