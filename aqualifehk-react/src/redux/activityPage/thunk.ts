import { Dispatch } from "redux"
import {
  ActivityPageAction,
  loadActivityFailed,
  loadActivitySuccess,
  setIsLoadingActivityPage,
} from "./action"
import { RootState } from "../state"
import { getAPIServer } from "../../helpers/api"
import { errorToString } from "../../helpers/format"
import { DTO } from "shared"
import "../type"
import { formatDTOActivity } from "../../helpers/DTOformat"

export function loadActivityThunk(idParam: string) {
  return async (
    dispatch: Dispatch<ActivityPageAction>,
    getState: () => RootState
  ) => {
    // check idParam
    if (!idParam.match(/^\d+$/)) {
      return dispatch(
        loadActivityFailed(
          "Error during loading activity page (/activity/:id): id param must be a number"
        )
      )
    }
    const id = parseInt(idParam)

    // get token
    const token = getState().auth.token
    if (!token) {
      // TODO: give user a new token if token is not found
      dispatch(
        loadActivityFailed(
          "Error during loading activity page: JWT token not found"
        )
      )
      return
    }

    let APIServer
    try {
      APIServer = getAPIServer()
    } catch (error) {
      dispatch(loadActivityFailed(errorToString(error)))
      return
    }

    // set loading
    dispatch(setIsLoadingActivityPage())

    // TODO: fetch API server for activity info
    let result: DTO.Activity
    try {
      const res = await fetch(`${APIServer}/activityInfo/${id}`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      const json = await res.json()
      if (!json.success) {
        dispatch(
          loadActivityFailed(errorToString(`Server error: ${json.error}`))
        )
        return
      }
      result = json.result as DTO.Activity
    } catch (error) {
      dispatch(loadActivityFailed(errorToString(error)))
      return
    }

    return dispatch(loadActivitySuccess(formatDTOActivity(result)))
  }
}
