import { DTOActivity } from "../../helpers/DTOformat"

export function loadActivitySuccess(result: DTOActivity) {
  return {
    type: "@ActivityPage/loadSuccess" as const,
    result,
  }
}

export function loadActivityFailed(error: string) {
  return {
    type: "@ActivityPage/loadFailed" as const,
    error,
  }
}

export function setIsLoadingActivityPage() {
  return {
    type: "@ActivityPage/setIsLoading" as const,
  }
}

export function setIsLoadingImgActivityPage() {
  return {
    type: "@ActivityPage/setIsLoadingImg" as const,
  }
}

export type ActivityPageAction =
  | ReturnType<typeof loadActivitySuccess>
  | ReturnType<typeof loadActivityFailed>
  | ReturnType<typeof setIsLoadingActivityPage>
  | ReturnType<typeof setIsLoadingImgActivityPage>
