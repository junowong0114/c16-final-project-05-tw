import { ActivityCategory } from "shared"
import { DTOActivity } from "../../helpers/DTOformat"

export type ActivityPageState = {
  activityInfo: DTOActivity
  isLoading: boolean
  isLoadingImg: boolean
  error?: string
}

export const initialState: ActivityPageState = {
  activityInfo: {
    id: 0,
    activity_name: "",
    category_id: ActivityCategory.invalid,
    rating: "0",
    review_count: 0,
    descriptions: [],
    activity_images: [],
    address: "",
    packages: [],
    latlong: "",
    shop_id: 0,
  },
  isLoading: true,
  isLoadingImg: true,
}
