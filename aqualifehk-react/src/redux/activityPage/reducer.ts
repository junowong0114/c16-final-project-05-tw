import { ActivityPageAction } from "./action"
import { ActivityPageState, initialState } from "./state"

export const activityPageReducer = (
  state: ActivityPageState = initialState,
  action: ActivityPageAction
): ActivityPageState => {
  switch (action.type) {
    case "@ActivityPage/loadSuccess":
      return {
        ...state,
        activityInfo: action.result,
        isLoading: false,
        error: undefined,
      }

    case "@ActivityPage/loadFailed":
      return {
        ...state,
        activityInfo: state.activityInfo,
        isLoading: false,
        error: action.error,
      }

    case "@ActivityPage/setIsLoading":
      return {
        ...state,
        isLoading: true,
      }

    case "@ActivityPage/setIsLoadingImg":
      return {
        ...state,
        isLoadingImg: true,
      }

    default:
      return state
  }
}
