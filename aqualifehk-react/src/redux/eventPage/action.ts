import { DTO } from "shared"

export function setEventList(eventList: DTO.EventDetail[]) {
  return {
    type: "@eventPage/setEventList" as const,
    eventList,
  }
}

export function setSelectedEventId(selectedEventId: number) {
  return {
    type: "@eventPage/setSelectedEventId" as const,
    selectedEventId,
  }
}

export type EventPageAction = 
  | ReturnType<typeof setEventList>
  | ReturnType<typeof setSelectedEventId>
