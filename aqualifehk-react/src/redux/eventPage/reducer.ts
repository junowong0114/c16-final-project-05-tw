import { EventPageState, initialState } from "./state";
import { EventPageAction } from "./action";

export const eventPageReducer = (
  state: EventPageState = initialState,
  action: EventPageAction
): EventPageState => {
  switch (action.type) {
    case "@eventPage/setEventList": {
      return {
        ...state,
        eventList: action.eventList,
      }
    }

    case "@eventPage/setSelectedEventId": {
      return {
        ...state,
        selectedEventId: action.selectedEventId,
      }
    }

    default: {
      return state
    }
  }
}