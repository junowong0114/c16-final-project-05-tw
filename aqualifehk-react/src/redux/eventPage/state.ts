import {DTO} from "shared"

export type EventPageState = {
  eventList?: DTO.EventDetail[],
  selectedEventId?: number,
}

export const initialState: EventPageState = {}