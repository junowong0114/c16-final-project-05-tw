export type HomePageState = {
    recommendation: any[]
    whatsNew: any[]
    isLoading: boolean
    error?: string
}

export const initialState: HomePageState = {
    recommendation: [],
    isLoading: true,
    whatsNew: []
}
