import { HomePageAction } from "./action";
import { HomePageState, initialState } from "./state";

export const homePageReducer = (
    state: HomePageState = initialState,
    action: HomePageAction
): HomePageState => {
    switch (action.type) {
        case 'loadHomePage':
            return {
                ...state,
                recommendation: action.homepageInfo.recommendations,
                whatsNew: action.homepageInfo.whatsNew,
                isLoading:false,
            }

        case 'loadHomepageInfoFailed':
            return {
                ...state,
                isLoading:false,
                error: action.error
            }
            
        default:
            return state;
    }
}