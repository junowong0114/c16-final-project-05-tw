import { Dispatch } from "redux";
import { DTO } from "shared";
import { getAPIServer } from "../../helpers/api";
import { errorToString } from "../../helpers/format";
import { HomePageAction, loadHomePage, loadHomepageInfoFailed } from "./action";

export function loadHomePageThunk() {
    return async (dispatch: Dispatch<HomePageAction>) => {
        let APIServer
        try {
            APIServer = getAPIServer()
        } catch (error) {
            dispatch(loadHomepageInfoFailed(errorToString(error)))
            return
        }

        let result: any
        try {
            const res = await fetch(`${APIServer}/homepageInfo`)
            result = await res.json();
        } catch (error) {
            console.log(error)
            dispatch(loadHomepageInfoFailed(errorToString(error)));
            return
        }

        if (!result.success) {
            console.log('get homepage info error: ', result.error);
            dispatch(loadHomepageInfoFailed(errorToString(errorToString)));
            return
        }

        const homePageInfo:DTO.HomepageInfo = result.homepageInfo
        dispatch(loadHomePage(homePageInfo));
    }
}