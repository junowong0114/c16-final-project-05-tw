import { DTO } from "shared"

export function loadHomePage(homepageInfo:DTO.HomepageInfo){
    return{
        type:"loadHomePage" as const,
        homepageInfo,
    }
}

export function loadHomepageInfoFailed(error:string){
    return {
        type:"loadHomepageInfoFailed" as const,
        error
    }
}


export type HomePageAction = 
    | ReturnType<typeof loadHomePage>
    | ReturnType<typeof loadHomepageInfoFailed>