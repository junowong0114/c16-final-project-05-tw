import { ActivityInfo } from "./state"

export function setBookingOptions(activityInfo: ActivityInfo) {
  return {
    type: "@bookingOptions/confirmOptions" as const,
    activityInfo,
  }
}

export function setBookingId(bookingId: number | undefined) {
  return {
    type: "@bookingOptions/setBookingId" as const,
    bookingId
  }
}

export type BookingAction = 
  | ReturnType<typeof setBookingOptions>
  | ReturnType<typeof setBookingId>
