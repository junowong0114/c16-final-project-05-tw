import { BookingAction } from "./action"
import { BookingState, initialState } from "./state"

export const bookingReducer = (
  state: BookingState = initialState,
  action: BookingAction
): BookingState => {
  switch (action.type) {
    case "@bookingOptions/confirmOptions": {
      return {
        ...state,
        activityInfo: action.activityInfo,
      }
    }

    case "@bookingOptions/setBookingId": {
      return {
        ...state,
        bookingId: action.bookingId
      }
    }

    default:
      return state
  }
}
