export type BookingState = {
  activityInfo?: ActivityInfo,
  bookingId?: number
}

export const initialState: BookingState = {
  activityInfo: undefined,
}

export type ActivityInfo = {
  activityId: number,
  activityDate: number,
  activityName?: string,
  bookingDate?: number,
  userRequirements?: string,
  packages: PackageInfo[],
  totalPrice: number,
}

export type PackageInfo = {
  packageId: number,
  timeSectionId: number,
  timeSectionName: string
  quantity: number,
  unitPrice: number,
  packageName: string,
}