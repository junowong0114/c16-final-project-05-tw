import { orderDetail } from "shared"

export function loadOrderDetail(orderDetails: orderDetail) {
  return {
    type: "loadOrderDetail" as const,
    orderDetails,
  }
}

export function loadOrderDetailFailed(error: string) {
  return {
    type: "loadOrderDetailFailed" as const,
    error,
  }
}

export function loadShopDetail(shopDetails: any) {
  return {
    type: "loadShopDetail" as const,
    shopDetails,
  }
}

export function loadShopDetailFailed(error: string) {
  return {
    type: "loadShopDetailFailed" as const,
    error,
  }
}

export type OrderDetailAction =
  | ReturnType<typeof loadOrderDetail>
  | ReturnType<typeof loadOrderDetailFailed>
  | ReturnType<typeof loadShopDetail>
  | ReturnType<typeof loadShopDetailFailed>
