import { BookingStatus, orderDetail } from "../type"

export type OrderDetailsState = {
  orderDetails: orderDetail
  shopDetails:any
  error?: string
}

export const loadOrderDetailsInitialState = (): OrderDetailsState => {
  return {
    orderDetails: {
      activityName: "",
      status: BookingStatus.confirmed,
      price: 1,
      packageNames: [],
      dateTime: new Date().getTime(),
      timeSections: [],
      bookingId: 1,
      dateOfBook: new Date().getTime(),
      bookingRequirement: "",
      quantity:[1],
      image:''
    },
    shopDetails:{}
  }
}
