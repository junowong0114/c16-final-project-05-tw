import { OrderDetailAction } from "./action"
import { OrderDetailsState, loadOrderDetailsInitialState } from "./state"

export const orderDetailsReducer = (
  state: OrderDetailsState = loadOrderDetailsInitialState(),
  action: OrderDetailAction
): OrderDetailsState => {
  switch (action.type) {
    case "loadOrderDetail": {
      return {
        orderDetails: action.orderDetails,
        shopDetails: state.shopDetails
      }
    }
    case "loadOrderDetailFailed": {
      return {
        ...state,
        error: action.error,
      }
    }
    case "loadShopDetail": {
      return {
        orderDetails: state.orderDetails,
        shopDetails: action.shopDetails
      }
    }
    case "loadShopDetailFailed": {
      return {
        ...state,
        error: action.error,
      }
    }
    default:
      return state
  }
}
