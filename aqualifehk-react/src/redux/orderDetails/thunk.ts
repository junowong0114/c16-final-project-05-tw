import { Dispatch } from "react"
import { DTO } from "shared"
import { getAPIServer } from "../../helpers/api"
import { errorToString } from "../../helpers/format"
import { orderDetailsFormat } from "../../helpers/orderDetailsFormat"
import { RootState } from "../state"
import {
  OrderDetailAction,
  loadOrderDetailFailed,
  loadOrderDetail,
  loadShopDetail,
  loadShopDetailFailed,
} from "./action"

export function loadOrderDetailsThunk(bookingId: string | null) {
  return async (
    dispatch: Dispatch<OrderDetailAction>,
    getState: () => RootState
  ) => {
    // get token and APISever ip/domain
    let token: string | undefined
    let APIServer: string
    try {
      token = getState().auth.token
      if (!token) throw new Error("Error during loading order: user id not found.")
      APIServer = getAPIServer()
    } catch (error) {
      dispatch(loadOrderDetailFailed(errorToString(error)))
      return
    }

    let result: any
    let id

    try {
      if (!bookingId) throw new Error('No booking id found.')
      id = parseInt(bookingId)
    } catch (error) {
      dispatch(loadOrderDetailFailed(errorToString(error)))
      return
    }

    try {
      const res = await fetch(`${APIServer}/orderDetails/${id}`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      result = await res.json()

      if (!result.success) throw new Error()
    } catch (error) {
      console.error("GET /order/all network error:", error)
      dispatch(loadOrderDetailFailed(errorToString(error)))
      dispatch(loadShopDetailFailed(result.error))
      return
    }

    const items = result.items
    const shop = result.shop
    const returnItems: ReturnType<typeof orderDetailsFormat> = orderDetailsFormat(items)
    dispatch(loadOrderDetail(returnItems[0]))
    dispatch(loadShopDetail(shop[0]))
  }
}
