import { GeneralState, initialState } from "./state"
import { GeneralAction } from "./action"

export function generalReducer(
  state: GeneralState = initialState,
  action: GeneralAction
): GeneralState {
  switch (action.type) {
    case "@general/navigateToNextPath": {
      return {
        ...state,
        from: state.to,
        to: action.to,
      }
    }

    case "@general/setShowToast": {
      return {
        ...state,
        showGeneralToast: action.showGeneralToast,
      }
    }

    case "@general/setMessage": {
      return {
        ...state,
        message: action.message,
      }
    }

    default:
      return state
  }
}
