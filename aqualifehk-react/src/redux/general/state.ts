export type GeneralState = {
  from: string
  to: string
  showGeneralToast: boolean,
  message?: string
}

export const initialState: GeneralState = {
  from: "",
  to: "",
  showGeneralToast: false,
  message: undefined
}
