export function navigateToNextPath(to: string) {
  return {
    type: "@general/navigateToNextPath" as const,
    to,
  }
}

export function setGeneralShowToast(bool: boolean) {
  return {
    type: "@general/setShowToast" as const,
    showGeneralToast: bool,
  }
}

export function setGeneralMessage(message: string | undefined) {
  return {
    type: "@general/setMessage" as const,
    message,
  }
}

export type GeneralAction = 
  | ReturnType<typeof navigateToNextPath>
  | ReturnType<typeof setGeneralShowToast>
  | ReturnType<typeof setGeneralMessage>
