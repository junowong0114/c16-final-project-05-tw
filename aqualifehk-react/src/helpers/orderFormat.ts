import { order } from "shared"

export function orderFormat(rows: any): order[] {
  const mapped = rows.map((row:any) => {
    return {
      bookingId: row.id,
      activityName: row.activityName,
      date: row.unix_time,
      price: row.totalPrice ,
      address: row.address,
      packages: row.packages,
      status: row.status,
      location: "",
      image:row.image
    }
  })

  return [...mapped]
}
