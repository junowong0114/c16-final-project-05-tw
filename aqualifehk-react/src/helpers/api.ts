export function getAPIServer() {
  if (window.location.origin.match(/:8100/)) {
    return 'http://127.0.0.1:8080'
  }
  return 'http://16.162.219.170'
  // const { REACT_APP_API_SERVER } = process.env
  // if (!REACT_APP_API_SERVER) {
  //   console.error("missing REACT_APP_API_SERVER in env")
  //   throw new Error("missing REACT_APP_API_SERVER in env")
  // }
  // return REACT_APP_API_SERVER
}

export function getGoogleAPIKey() {
  const { REACT_APP_GOOGLE_API_KEY } = process.env
  if (!REACT_APP_GOOGLE_API_KEY) {
    console.error("missing GOOGLE_API_KEY in env")
    throw new Error("missing GOOGLE_API_KEY in env")
  }
  return REACT_APP_GOOGLE_API_KEY
}