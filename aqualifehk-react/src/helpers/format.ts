import { DTO } from "shared"
import { PackageInfo } from "../redux/BookingOption/state"

export function formatUnitCount(count: number, unit: string) {
  if (count > 1) {
    if (unit.match(/y$/)) {
      unit = unit.slice(0, -1) + "ies"
    } else {
      unit += "s"
    }
  }
  return count + " " + unit
}

export function formatArrayCount(array: any[], unit: string) {
  return formatUnitCount(array.length, unit)
}

export function errorToString(error: unknown): string {
  return (error as Error).toString()
}

export function formatFloat(num: number, digits: number) {
  return num.toFixed(digits)
}

function joinNameQuantity(name: string, quantity: number) {
  return name + " x " + quantity
}

export function formatPackageString(packages: PackageInfo[]) {
  return packages.reduce((output, _package, index) => {
    return (
      output +
      _package.timeSectionName + ': ' +
      joinNameQuantity(_package.packageName, _package.quantity)
      + (index === packages.length? '' : '\n')
    )
  }, "")
}

// names must be separated by ': ', eg: 'Juno Wong, Jason Sew, Happy Sew'
export function cropNameString(names:string, maxLength: number) {
  if (names.length <= maxLength) return names

  const nameSubStr = names.substr(0, maxLength - 4)
  const croppedText = nameSubStr.match(/(.+),/)
  if (!croppedText) return nameSubStr

  const numberOfPplLeft = names.split(', ').length - croppedText[1].split(', ').length
  return croppedText[1] + ` ...(+${numberOfPplLeft} more)`
}

export function cropText(text: string, maxLength: number) {
  if (!text) return
  if (text.length <= maxLength) return text

  return text.substr(0, maxLength - 4) + ' ...'
}
 