import { orderDetail } from "shared"

export function orderDetailsFormat(rows: any): orderDetail[] {
  const mapped = rows.map((row: any) => {
    const packagesArray: any = []
    row.packages.map((eachPackage: any) => {
      packagesArray.push(eachPackage.name)
    })
    const timeSectionArray: any = []
    row.packages.map((eachPackage: any) => {
      timeSectionArray.push(eachPackage.timeSection)
    })
    const quantityArray: any = []
    row.packages.map((eachPackage: any) => {
      quantityArray.push(eachPackage.quantity)
    })
    return {
      activityName: row.activityName,
      status: row.status,
      price: row.totalPrice,
      packageNames: packagesArray,
      dateTime: row.unix_time,
      timeSections: timeSectionArray,
      bookingId: row.id,
      dateOfBook: row.unix_time,
      bookingRequirement: row.userRequirement,
      quantity: quantityArray,
      image: row.image
    }
  })

  return mapped
}
