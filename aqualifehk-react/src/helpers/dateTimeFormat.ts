// option === undefined: 10-30-2021 12:00 AM
// option === "date": 10-30-2021
// option === "time": 12:00 AM
export function dateToDateTimeString(date: Date, option?: "date" | "time") {
  const formatter = new DateTimeFormatter(option)
  const formattedString = formatter.format(date)

  if (!option) return formatDateTimeString(formattedString)
  if (option === "date") return formatDateString(formattedString)
  return formattedString
}

function formatDateString(dateString: string) {
  return dateString.replaceAll(/\//g, "-")
}

function formatDateTimeString(dateTimeString: string) {
  return dateTimeString.replace(/,/, "").replaceAll(/\//g, "-")
}

class DateTimeFormatter {
  private option: Intl.DateTimeFormatOptions = {
    hour12: true,
    timeZone: "Asia/Hong_Kong",
  }

  private formatter: Intl.DateTimeFormat

  constructor(option?: "date" | "time") {
    switch (option) {
      case "date":
        this.option = {
          ...this.option,
          day: "numeric",
          month: "short",
          year: "numeric",
        }
        break

      case "time":
        this.option = {
          ...this.option,
          hour: "2-digit",
          minute: "2-digit",
        }
        break

      default:
        this.option = {
          ...this.option,
          day: "numeric",
          month: "short",
          year: "numeric",
          hour: "2-digit",
          minute: "2-digit",
        }
        break
    }

    this.formatter = new Intl.DateTimeFormat("en-HK", this.option)
  }

  public format(date: Date) {
    return this.formatter.format(date)
  }
}


export function dateOrTimeFormat(option: "date" | "time", date: Date){
  let returnValue:string;
  if(option === "date"){
    return returnValue = date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear()
  }else{
    return returnValue = date.getHours() + ':' + date.getMinutes()
  }
}