import { DTO } from "shared"
import { formatFloat } from "./format"

export function formatDTOBooking(rows: DTO.Booking[]): DTOBooking[] {
  return rows.map((row) => {
    return {
      ...row,
      date: new Date(row.unix_time * 1000),
      totalPrice: row.totalPrice,
    }
  })
}

export interface DTOBooking
  extends Omit<DTO.Booking, "id" | "userId" | "totalPrice"> {
  id: number
  date: Date
  userId: number
  totalPrice: number
}

export function formatDTOActivity(row: DTO.Activity): DTOActivity {
  return {
    ...row,
    id: row.id? row.id : 0,
    rating: formatFloat(parseFloat(row.rating), 1),
  }
}

export interface DTOActivity
  extends Omit<DTO.Activity, "id" | "review_count" | "shop_id"> {
  id: number
  review_count: number
  shop_id: number
}
