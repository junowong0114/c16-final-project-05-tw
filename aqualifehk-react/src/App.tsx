import {
  IonApp,
  IonIcon,
  IonLabel,
  IonLoading,
  IonRouterOutlet,
  IonTabBar,
  IonTabButton,
  IonTabs,
  useIonToast,
} from "@ionic/react"
import { IonReactRouter } from "@ionic/react-router"
/* Core CSS required for Ionic components to work properly */
import "@ionic/react/css/core.css"
import "@ionic/react/css/display.css"
import "@ionic/react/css/flex-utils.css"
import "@ionic/react/css/float-elements.css"
/* Basic CSS for apps built with Ionic */
import "@ionic/react/css/normalize.css"
/* Optional CSS utils that can be commented out */
import "@ionic/react/css/padding.css"
import "@ionic/react/css/structure.css"
import "@ionic/react/css/text-alignment.css"
import "@ionic/react/css/text-transformation.css"
import "@ionic/react/css/typography.css"
import {
  bookmarksOutline,
  homeOutline,
  listOutline,
  personOutline,
  receiptOutline,
} from "ionicons/icons"
import { Redirect, Route, useLocation } from "react-router-dom"
/* Pages */
import Bookmark from "./pages/Bookmark"
import Cart from "./pages/Cart"
import EventPage from "./pages/EventPage"
import Homepage from "./pages/Homepage"
import Order from "./pages/Order"
import Profile from "./pages/Profile"
import OrderDetails from "./pages/OrderDetails"

/* Theme variables */
import "./App.scss"
import "./theme/variables.scss"
import "./theme/_common.scss"

import { useDispatch, useSelector } from "react-redux"
import { useEffect, useRef, useState } from "react"
import { checkTokenThunk } from "./redux/auth/thunk"
import { navigateToNextPath, setGeneralShowToast, setGeneralMessage } from "./redux/general/action"

import SearchPage from "./pages/SearchPage"
import LoginPage from "./pages/Loginpage"
import ActivityPage from "./pages/ActivityPage"
import BookingOption from "./pages/BookingOption"
import ConfirmBooking from "./pages/ConfirmBooking"
import EventDetailPage from "./pages/EventDetailPage"
import ShareEventPage from "./pages/ShareEventPage"
import CreateEventPage from "./pages/CreateEventPage"
import { RootState } from "./redux/state"
import { loadHomePageThunk } from "./redux/homepage/thunks"
import MyAccount from "./components/Profile/MyAccount"
import Register from "./components/Profile/Register"
import { getGoogleAPIKey } from "./helpers/api"
import { useJsApiLoader } from "@react-google-maps/api"


const App: React.FC = () => {
  const dispatch = useDispatch()
  const routerRef = useRef<IonReactRouter>(null)
  const loadingUser = useSelector((state: RootState) => state.auth.loading);
  const loadingHomepageInfo = useSelector((state: RootState) => state.homePage.isLoading);
  const [isLoading, setIsLoading] = useState(loadingUser || loadingHomepageInfo)

  useEffect(() => {
    setIsLoading(true)
    dispatch(loadHomePageThunk());
    dispatch(checkTokenThunk());
  }, [])

  useEffect(() => {
    setIsLoading(loadingUser || loadingHomepageInfo)
  }, [loadingHomepageInfo, loadingUser])

  return (
    <IonApp>
      <IonReactRouter ref={routerRef}>
        {isLoading ?
          <IonLoading isOpen={isLoading} message='Loading' /> : <PageApp />}
      </IonReactRouter>
    </IonApp>
  )
}

const PageApp: React.FC = () => {
  const dispatch = useDispatch()
  const location = useLocation()
  const showGeneralToast = useSelector((state: RootState) => state.general.showGeneralToast)
  const message = useSelector((state: RootState) => state.general.message)
  const error = useSelector((state: RootState) => state.general.message)
  const [presentGeneralToast] = useIonToast();

  useEffect(() => {
    dispatch(navigateToNextPath(location.pathname))
  }, [location])

  useEffect(() => {
    if (showGeneralToast) {
      presentGeneralToast({
        message: message,
        duration: 1500,
        position: "bottom",
        cssClass: "generalToast",
        onWillDismiss: () => dispatch(setGeneralShowToast(false))
      })
    }
  }, [showGeneralToast])

  useEffect(() => {
    if (error) {
      presentGeneralToast({
        message: error,
        duration: 1500,
        position: "bottom",
        cssClass: "generalToast",
        onWillDismiss: () => dispatch(setGeneralMessage(undefined))
      })
    }
  }, [error])

  return (
    <IonRouterOutlet>
      <Route path="/book/:to/:activityId" component={BookingOption} />
      <Route path="/confirmBooking" component={ConfirmBooking} />
      <Route path="/tabs">
        {AppTabs()}
      </Route>
      <Route path="/search/:keyword" component={SearchPage} />
      <Route path="/search" exact component={SearchPage} />
      <Route path="/cart" component={Cart} />
      <Route path="/login" component={LoginPage} />
      <Route path="/myAccount" component={MyAccount} />
      <Route path="/register" component={Register} />
      <Route path="/activity/:activityId" component={ActivityPage} />
      <Route path="/orderDetails" component={OrderDetails} />
      <Route path="/eventDetails/:eventId" component={EventDetailPage} />
      <Route path="/share_event" component={ShareEventPage} />
      <Route path="/create_event" component={CreateEventPage} />
      <Route path="/" exact>
        <Redirect to="/tabs/home"></Redirect>
      </Route>
    </IonRouterOutlet>
  )
}

const AppTabs = () => (
  <IonTabs className="tabs">
    <IonRouterOutlet>
      <Route path="/tabs/home" component={Homepage} />
      <Route path="/tabs/events" component={EventPage} />
      <Route path="/tabs/order" component={Order} />
      <Route path="/tabs/bookmark" component={Bookmark} />
      <Route path="/tabs/profile" component={Profile} />
    </IonRouterOutlet>

    <IonTabBar slot="bottom" color="primary">
      <IonTabButton tab="homepage" href="/tabs/home">
        <IonIcon icon={homeOutline} />
        <IonLabel>Home</IonLabel>
      </IonTabButton>

      <IonTabButton tab="events" href="/tabs/events">
        <IonIcon icon={listOutline} />
        <IonLabel>Events</IonLabel>
      </IonTabButton>

      <IonTabButton tab="order" href="/tabs/order">
        <IonIcon icon={receiptOutline} />
        <IonLabel>Orders</IonLabel>
      </IonTabButton>

      {/* <IonTabButton tab="bookmark" href="/tabs/bookmark">
        <IonIcon icon={bookmarksOutline} />
        <IonLabel>Bookmark</IonLabel>
      </IonTabButton> */}

      <IonTabButton tab="profile" href="/tabs/profile">
        <IonIcon icon={personOutline} />
        <IonLabel>Profile</IonLabel>
      </IonTabButton>
    </IonTabBar>
  </IonTabs>
)

export default App
