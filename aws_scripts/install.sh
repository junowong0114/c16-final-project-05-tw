#!/bin/bash
set -e
set -o pipefail
set -x

sudo apt update

sudo apt install --yes docker.io docker-compose
sudo usermod -aG docker ubuntu

sudo apt install --yes nginx
sudo service nginx start

sudo apt install --yes zstd
sudo apt install --yes python3-certbot-nginx