#!/bin/bash
set -e
set -o pipefail
set -x

scp aqualifehk:/etc/nginx/sites-available/default aws_scripts/etc/nginx/sites-available/default