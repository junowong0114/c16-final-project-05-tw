#!/bin/bash
set -e
set -o pipefail
set -x

scp aws_scripts/install.sh aqualifehk:~/install.sh
ssh aqualifehk "~/install.sh"