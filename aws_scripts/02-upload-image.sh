#!/bin/bash
set -e
set -o pipefail
set -x

docker images | grep aqualifehk-web-image
docker save aqualifehk-web-image | zstd | ssh aqualifehk "unzstd | docker load"