#!/bin/bash
set -e
set -o pipefail
set -x

ssh aqualifehk "sudo certbot --nginx"
ssh aqualifehk "sudo nginx -t"
ssh aqualifehk "sudo service nginx restart"
