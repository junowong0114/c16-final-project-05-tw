#!/bin/bash
set -e
set -o pipefail
set -x

scp aws_scripts/etc/nginx/sites-available/default aqualifehk:~/default
ssh aqualifehk "sudo chown root:root default"
ssh aqualifehk "sudo mv default /etc/nginx/sites-available/default"
ssh aqualifehk "sudo nginx -t"
ssh aqualifehk "sudo service nginx restart"