import express, { Request, Response } from 'express';
import dotenv from 'dotenv';
import cors from 'cors';
import { print } from 'listening-on';
import {  knex } from './db'
import { UserService } from './services/userService';
import { CartService } from './services/cartService';
import { UserController } from './controllers/userController';
import { CartController } from './controllers/cartController';
import { BookingService } from './services/bookingService';
import { BookingController } from './controllers/bookingController';
import { OrderService } from './services/orderService';
import { OrderController } from './controllers/orderController';
import { OrderDetailsService } from './services/orderDetailService';
import { OrderDetailsController } from './controllers/orderDetailsController';
import { SearchService } from './services/searchService';
import { SearchController } from './controllers/searchController';
import { BookmarksService } from './services/bookmarksService';
import { BookmarksController } from './controllers/bookmarksController';
import { EventController } from './controllers/eventController';
import { EventService } from './services/eventService';



// dotenv config
dotenv.config();

// express set up
const app = express();
app.use(cors());
app.use(express.json())
app.use(express.urlencoded({ extended: false }))



// services and controllers
export const userService = new UserService(knex);
export const cartService = new CartService(knex);
export const bookingService = new BookingService (knex);
export const orderService = new OrderService(knex);
export const orderDetailsService = new OrderDetailsService(knex);
export const searchService = new SearchService(knex);
export const bookmarksService = new BookmarksService(knex);
export const eventService = new EventService(knex);

export const userController = new UserController(userService);
export const cartController = new CartController(cartService);
export const orderController = new OrderController(orderService);
export const orderDetailsController = new OrderDetailsController(orderDetailsService);
export const bookingController = new BookingController(bookingService);
export const searchController = new SearchController(searchService);
export const bookmarksController = new BookmarksController(bookmarksService);
export const eventController = new EventController(eventService);

// routing
import { routes } from './routes';



app.use('/', routes);
app.get('/', (req: Request, res: Response) => {
    res.end("Welcome to AquaLifeHK API server, please checkout our app.");
});

// initialize server
const PORT = process.env.PORT;
if (!PORT) {
  throw new Error('Missing PORT in env');
}
let port = parseInt(PORT);

app.listen(port, () => {
    print(port);
});