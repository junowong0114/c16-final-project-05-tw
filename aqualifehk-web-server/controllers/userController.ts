import { Request, Response } from "express";
import { JWTPayload, registerUser } from "shared";
import { errorToString } from "../helpers/format";
import { UserService } from "../services/userService";

export class UserController {
  constructor(private userService: UserService) { }

  createTokenForNonRegistered = async (req: Request, res: Response) => {
    let token = await this.userService.createTokenForNonRegistered();
    try {
      res.json({
        success: true,
        token
      })
    } catch (error) {
      res.status(400).json({
        success: false,
        error: (error as Error).toString()
      })
    }
  }

  createTokenWithPassword = async (req: Request, res: Response) => {
    let { email, password } = req.body;
    if (!email || !password) {
      res.status(400).json({
        success: false,
        error: 'missing username or password in req.body'
      })
      return;
    }

    try {
      let token = await this.userService.createTokenWithPassword(email, password);
      res.json({
        success: true,
        token
      });
    } catch (error) {
      res.status(400).json({
        success: false,
        error: (error as Error).toString(),
      });
    }
  }

  getUserInfo = async (req: Request, res: Response) => {
    let payload = req.jwtPayload as JWTPayload

    try {
      const info = await this.userService.getUserInfo(payload.id)
      res.json({
        success: true,
        result: info,
      })
    } catch (error) {
      res.status(500).json({
        success: false,
        error: errorToString(error)
      })
    }
  }

  registerUser = async (req: Request, res: Response) => {
    const user: registerUser = req.body;
    const payload = req.jwtPayload as JWTPayload;

    if (!user.email || !user.password) {
      res.status(400).json({
        success: false,
        error: 'missing email or password in req.body'
      })
      return
    }

    try {
      let result = await this.userService.registerUser(user, payload.id)

      res.json(result)
    } catch (error) {
      res.status(400).json({
        success: false,
        error: (error as Error).toString()
      })
    }
  }
}