import { Request, Response } from 'express';
import { JWTPayload } from 'shared';
import { errorToString, jsUnixToPostgresUnix } from '../helpers/format';
import { BookingService } from '../services/bookingService';

export class BookingController {
  constructor(private bookingService: BookingService) { };

  getActivityInfo = async (req: Request, res: Response) => {
    const activity_id = req.params.id

    if (!activity_id.match(/^\d+$/)) {
      res.status(400).json({
        success: false,
        error: 'activity_id must be a number'
      })
      return
    }

    let result
    try {
      result = await this.bookingService.getActivityInfo(parseInt(activity_id))
    } catch (error) {
      res.status(400).json({
        success: false,
        error: errorToString(error)
      })
      return
    }

    if (!result || !(Object.keys(result).length === 10) || !Object.keys(result).some(key => key === "packages")) {
      res.status(400).json({
        success: false,
        error: `Query of activity id ${activity_id} returns empty result`
      })
      return
    }

    res.json({
      success: true,
      result: result
    })
  }

  getPackageAvailability = async (req: Request, res: Response) => {
    let package_id = req.query.package_id as string
    let quantityQ = req.query.quantity as string
    let js_start_date_unix = req.query.start_date_unix as string

    if (!package_id || !js_start_date_unix || !quantityQ) {
      res.status(400).json({
        success: false,
        error: 'Missing query string(s): /getAvailability/?package_id?start_date_unix?quantity'
      })
      return
    }

    try {
      if (!package_id.match(/^\d+$/)) {
        res.status(400).json({
          success: false,
          error: 'package_id must be a number'
        })
        return
      }
    } catch (error) {
      res.status(400).json({
        success: false,
        error: 'package_id must be a number'
      })
      return
    }

    try {
      if (!quantityQ.match(/^\d+$/)) {
        res.status(400).json({
          success: false,
          error: 'quantity must be a number'
        })
        return
      }
    } catch (error) {
      res.status(400).json({
        success: false,
        error: 'quantity must be a number'
      })
      return
    }

    let startDate = new Date(parseInt(js_start_date_unix))
    if (!startDate.getTime()) {
      res.status(400).json({
        success: false,
        error: 'Wrong date format: start_date_unix must be a unix date string'
      })
      return
    }
    if (startDate < new Date('2021-10-01')) {
      res.status(400).json({
        success: false,
        error: 'start_date_unix must not be earlier than 2021-10-01'
      })
      return
    }

    let packageId = parseInt(package_id)
    let quantity = parseInt(quantityQ)
    let psql_start_date_unix = jsUnixToPostgresUnix(parseInt(js_start_date_unix))

    let result
    try {
      result = await this.bookingService.getPackageAvailability(packageId, psql_start_date_unix, quantity);

    } catch (error) {
      res.status(400).json({
        success: false,
        error: errorToString(error)
      })
      return
    }
    res.json({
      success: true,
      result: {
        package_id: packageId,
        availability: result,
      }
    });
  }

  postBooking = async (req: Request, res: Response) => {
    const payload = req.jwtPayload as JWTPayload
    const userId = payload.id

    const bookingDate = (new Date()).getTime()

    try {
      await this.bookingService.postBooking(userId, 
        req.body.activityId, 
        req.body.activityDate, 
        bookingDate, 
        req.body.userRequirements, 
        req.body.packages)
    } catch (error) {
      res.status(400).json({
        success: false,
        error: errorToString(error)
      })
      return
    }

    res.json({
      success: true
    })

  }

  postBookingFromCart = async (req: Request, res: Response) => {
    try {
      const returnValue = await this.bookingService.postBookingFromCart(req.body.bookingId, req.body.userRequirements , new Date().getTime())
      if(returnValue !== undefined){
        throw Error(returnValue)
      }
    } catch (error) {
      res.status(400).json({
        success: false,
        error: errorToString(error)
      })
      return
    }

    res.json({
      success: true
    })

  }
}