import { Request, Response } from "express";
import { DTO, JWTPayload } from "shared";
import { errorToString } from "../helpers/format";
import { CartService } from "../services/cartService";

export class CartController {
  constructor(private cartService: CartService) { };

  loadCart = async (req: Request, res: Response) => {
    const payload = req.jwtPayload as JWTPayload
    const userId = payload.id

    try {
      const items: DTO.Booking[] = await this.cartService.loadCart(userId);
      res.json({
        success: true,
        items
      });
      return;
    } catch (error) {
      res.status(400).json({
        success: false,
        error: errorToString(error),
      });
    }
    return;
  }

  deleteItem = async (req: Request, res: Response) => {
    const payload = req.jwtPayload as JWTPayload
    const userId = payload.id

    const idString = req.params.id
    if (!idString.match(/^\d+$/)) {
      res.status(400).json({
        success: false,
        error: 'ID is missing from params. Please send put request in format /cart/delete/:id, where id is a number'
      })
      return
    }

    const itemId = parseInt(idString)
    try {
      await this.cartService.deleteItem(userId, itemId)
      res.json({
        success: true,
      })
      return
    } catch (error) {
      res.status(400).json({
        success: false,
        error: errorToString(error),
      });
      return
    }
  }

  addItem = async (req: Request, res: Response) => {
    const payload = req.jwtPayload as JWTPayload
    const userId = payload.id

    try {
      await this.cartService.postCart(userId, 
        req.body.activityId, 
        req.body.activityDate, 
        new Date().getTime(), 
        req.body.totalPrice,
        req.body.packages)
        
      res.json({
        success: true,
      })
      return
    } catch (error) {
      res.status(400).json({
        success: false,
        error: errorToString(error),
      });
      return
    }
  }

  getSingleItem = async(req:Request, res:Response)=>{
    const payload = req.jwtPayload as JWTPayload
    const userId = payload.id
    try{
      const item = await this.cartService.getSingleCart(userId, parseInt(req.params.bookingId))
      res.json({
        success: true,
        item: item
      })
      return
    }catch (error) {
      res.status(400).json({
        success: false,
        error: errorToString(error),
      });
      return
    }
  }

  updateCartItem = async(req:Request, res:Response)=>{
    const payload = req.jwtPayload as JWTPayload
    const userId = payload.id

    try{
      await this.cartService.updateCart(userId, req.body.bookingId, req.body.activityDate, req.body.packages)
      res.json({
        success: true
      })
      return
    }catch (error) {
      res.status(400).json({
        success: false,
        error: errorToString(error),
      });
      return
    }
  }
}