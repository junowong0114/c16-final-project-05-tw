import { Request, Response } from 'express';
import { JWTPayload } from 'shared';
import { BookmarksService } from '../services/bookmarksService'
import { errorToString } from '../helpers/format';


export class BookmarksController {
    constructor(private bookmarksService: BookmarksService) { };

    addBookmark = async (req: Request, res: Response) => {
        const payload = req.jwtPayload as JWTPayload
        const userId = payload.id

        try {
            await this.bookmarksService.addBookmark(userId, req.body.activityId)
        } catch (error) {
            res.status(400).json({
                success: false,
                error: errorToString(error)
            })
            return
        }

        res.json({
            success: true
        })
    }

    deleteBookmark = async (req: Request, res: Response) => {
        const payload = req.jwtPayload as JWTPayload
        const userId = payload.id

        try {
            await this.bookmarksService.deleteBookmark(userId, req.body.bookmarkId)
        } catch (error) {
            res.status(400).json({
                success: false,
                error: errorToString(error)
            })
            return
        }

        res.json({
            success: true
        })
    }

    getBookmark = async (req: Request, res: Response) => {
        const payload = req.jwtPayload as JWTPayload
        const userId = payload.id

        try {
            const bookmarks = await this.bookmarksService.getBookmark(userId)
            res.json({
                success: true,
                bookmarks: bookmarks
            })
        } catch (error) {
            res.status(400).json({
                success: false,
                error: errorToString(error)
            })
            return
        }
    }
}