import { Request, Response } from 'express'
import { DTO, JWTPayload } from 'shared'
import { errorToString } from '../helpers/format'
import { EventService } from '../services/eventService'

export class EventController {
  constructor(private eventService: EventService) {}

  getEvent = async(req: Request, res: Response) => {
    let payload: JWTPayload
    let userId: number
    try {
      payload = req.jwtPayload as JWTPayload
      if (!payload) throw new Error('JWTPayload not found in request')
  
      userId = payload.id
      if (!userId) throw new Error('User Id not found in request')
    } catch (e) {
      res.status(400).json({
        success: false,
        error: errorToString(e)
      })
      return
    }
  
    try {
      const result = await this.eventService.getEvent(userId)
      res.json({
        success: true,
        result
      })
      return
    } catch (e) {
      res.status(500).json({
        success: false,
        error: errorToString(e)
      })
      return
    }
  }

  createEvent = async(req: Request, res: Response) => {
    let payload: JWTPayload
    let userId: number
    let eventInfo: DTO.EventDetail
    try {
      payload = req.jwtPayload as JWTPayload
      if (!payload) throw new Error('JWTPayload not found in request')

      userId = payload.id
      if (!userId) throw new Error('User Id not found in request')

      eventInfo = req.body.eventInfo
      if (!eventInfo) throw new Error('Event info not found in request')
    } catch (error) {
      res.status(400).json({
        success: false,
        error: errorToString(error)
      })
      return
    }

    try {
      await this.eventService.createEvent(eventInfo, userId)
      res.json({
        success: true
      })
      return
    } catch (error) {
      res.status(500).json({
        success: false,
        error: errorToString(error)
      })
      return
    }
  }

  deleteEvent = async(req: Request, res: Response) => {
    let payload: JWTPayload
    let userId: number
    let eventId: number
    try {
      payload = req.jwtPayload as JWTPayload
      if (!payload) throw new Error('JWTPayload not found in request')

      userId = payload.id
      if (!userId) throw new Error('User id not found in request')

      eventId = req.body.eventId
      if (!eventId) throw new Error('Event id not found in request')
    } catch (error) {
      res.status(400).json({
        success: false,
        error: errorToString(error)
      })
      return
    }

    try {
      await this.eventService.deleteEvent(eventId, userId)
      res.json({
        success: true
      })
      return
    } catch (error) {
      res.status(500).json({
        success: false,
        error: errorToString(error)
      })
      return
    }
  }

  insertParticipant = async(req: Request, res: Response) => {
    let payload: JWTPayload
    let userId: number
    let inviteCode : string
    try {
      payload = req.jwtPayload as JWTPayload
      if (!payload) throw new Error('JWTPayload not found in request')

      userId = payload.id
      if (!userId) throw new Error('User Id not found in request')

      inviteCode = req.body.inviteCode
      if (!inviteCode) throw new Error('Invite code not found in request')
    } catch (error) {
      res.status(400).json({
        success: false,
        error: errorToString(error)
      })
      return
    }

    try {
      await this.eventService.insertParticipant(userId, inviteCode)
      res.json({
        success: true,
      })
      return
    } catch (e) {
      res.status(500).json({
        success: false,
        error: errorToString(e)
      })
      return
    }
  }

  insertActivityVote = async(req: Request, res: Response) => {
    let payload: JWTPayload
    let userId: number
    let eventId: number
    let eventActivityIds: number[]
    try {
      payload = req.jwtPayload as JWTPayload
      if (!payload) throw new Error('JWTPayload not found in request')

      userId = payload.id
      if (!userId) throw new Error('User Id not found in request')

      eventId = req.body.eventId
      if (!eventId) throw new Error('Event id not found in request')

      eventActivityIds = req.body.eventActivityIds
      if (!eventActivityIds) throw new Error('Vote content not found in request')
    } catch (error) {
      res.status(400).json({
        success: false,
        error: errorToString(error)
      })
      return
    }
    
    try {
      await this.eventService.insertActivityVote(eventId, eventActivityIds, userId)
      res.json({
        success: true
      })
    } catch (error) {
      res.status(500).json({
        success: false,
        error: errorToString(error)
      })
      return
    }
  }

  insertTimeVote = async(req: Request, res: Response) => {
    let payload: JWTPayload
    let userId: number
    let eventId: number
    let eventTimesIds: number[]
    try {
      payload = req.jwtPayload as JWTPayload
      if (!payload) throw new Error('JWTPayload not found in request')

      userId = payload.id
      if (!userId) throw new Error('User Id not found in request')

      eventId = req.body.eventId
      if (!eventId) throw new Error('Event id not found in request')

      eventTimesIds = req.body.eventTimesIds
      if (!eventTimesIds) throw new Error('Vote content not found in request')
    } catch (error) {
      res.status(400).json({
        success: false,
        error: errorToString(error)
      })
      return
    }

    try {
      await this.eventService.insertTimeVote(eventId, eventTimesIds, userId)
      res.json({
        success: true
      })
    } catch (error) {
      res.status(500).json({
        success: false,
        error: errorToString(error)
      })
      return
    }
  }

}