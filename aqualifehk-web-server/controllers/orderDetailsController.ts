import { Request, Response } from "express";
import { JWTPayload } from "shared";
import { errorToString } from "../helpers/format";
import { OrderDetailsService } from "../services/orderDetailService";

export class OrderDetailsController {
    constructor(private orderDetailsService: OrderDetailsService) {};
  
    loadOrderDetails = async (req: Request, res: Response) => {
      const payload = req.jwtPayload as JWTPayload
      const userId = payload.id
  
      const bookingId = parseInt(req.params.id)
      try {
        const items: any = await this.orderDetailsService.loadOrder(userId, bookingId);
        const shop: any = await this.orderDetailsService.loadOrderShop(userId, bookingId);
        res.json({
          success: true,
          items,
          shop
        });
        return;
      } catch (error) {
        res.status(400).json({
          success: false,
          error: errorToString(error),
        });
      }
      return;
    }

  }