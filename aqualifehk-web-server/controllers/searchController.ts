import { SearchService } from "../services/searchService";
import { Request, Response } from "express"
import { errorToString } from "../helpers/format";
import { DTO } from 'shared'


export class SearchController {
    constructor(private searchService: SearchService) { };

    getSearch = async (req: Request, res: Response) => {
        const keyword = req.query.keyword + '';
        const page = parseInt(req.query.page + '')
        if (page <= 0) {
            res.json({
                success: false,
                error: 'page number could not less than 0'
            })
        }
        try {
            const activities: DTO.SearchActivity[] = await this.searchService.getSearch(keyword, page);
            res.json({
                success: true,
                activities
            });
            return
        } catch (error) {
            res.status(400).json({
                success: false,
                error: errorToString(error)
            })
        }
        return;
    }

    getHomePage = async (req: Request, res: Response) => {
        try {
            const homepageInfo: DTO.HomepageInfo = await this.searchService.getHomePage();
            res.json({
                success: true,
                homepageInfo
            })
            return
        } catch (error) {
            res.status(400).json({
                success: false,
                error: errorToString(error)
            })
        }
        return
    }


}
