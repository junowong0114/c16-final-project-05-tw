import { Request, Response } from "express";
import { DTO, JWTPayload } from "shared";
import { errorToString } from "../helpers/format";
import { OrderService } from "../services/orderService";


export class OrderController {
  constructor(private orderService: OrderService) {};

  loadOrder = async (req: Request, res: Response) => {
    const payload = req.jwtPayload as JWTPayload
    const userId = payload.id

    try {
      const items: DTO.Booking[] = await this.orderService.loadOrder(userId);

      res.json({
        success: true,
        items
      });
      return;
    } catch (error) {
      res.status(400).json({
        success: false,
        error: errorToString(error),
      });
    }
    return;
  }
}