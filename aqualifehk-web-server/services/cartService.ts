import { Knex } from 'knex';
import { DTO, DB, BookingStatus, bookingPackage } from 'shared';

export class CartService {
  constructor(private knex: Knex) { };

  async loadCart(user_id: number): Promise<DTO.Booking[]> {
    let knex: Knex = this.knex;

    type BookingActivityQueryResult = DB.Booking & DB.Activity & { image_urls: string[] }

    let bookingsQuery: Knex.QueryBuilder<BookingActivityQueryResult, BookingActivityQueryResult[]> = knex('bookings')
      .select(knex.raw(`*, bookings.id, activities.name as activity_name, json_agg(activity_images.url) as image_urls`))
      .join("activities", "bookings.activity_id", "activities.id")
      .leftOuterJoin("activity_images", "activity_images.activity_id", "activities.id")
      .groupBy("bookings.id", "bookings.id", "activities.name", "activities.id", "activity_images.id")
      .where({ user_id, status: BookingStatus.inCart.toString() })

    let bookingsPackagesQuery: Knex.QueryBuilder<DB.BookingPackage, DB.BookingPackage[]> = knex('bookings_packages')
      .select('*')
      .from('bookings_packages')
      .join('bookings', 'bookings.id', 'bookings_packages.booking_id')
      .join('time_sections', 'time_sections.id', 'bookings_packages.time_section_id')
      .where({ user_id, status: BookingStatus.inCart.toString() })

    let packageQuery: Knex.QueryBuilder<DB.Package, DB.Package[]> = knex('packages')
      .select('*', "packages.id")
      .from('packages')
      .innerJoin(
        'bookings_packages',
        'bookings_packages.package_id',
        'packages.id',
      )
      .innerJoin('bookings', 'bookings.id', 'bookings_packages.booking_id')
      .where({ user_id })

    let [bookingList, bookingPackageList, packageList] = await Promise.all([
      bookingsQuery,
      bookingsPackagesQuery,
      packageQuery
    ])

    let bookingDict = Object.fromEntries(
      bookingList.map(row => {
        if (!row.id) throw new Error('cartService: Booking ID not found in query result.')
        let booking: DTO.Booking = {
          id: row.id,
          userId: row.user_id,
          activityName: row.activity_name,
          image_urls: row.image_urls,
          address: row.address,
          latLong: row.latlong,
          activityId: row.activity_id,
          unix_time: row.activity_date,
          totalPrice: row.total_price,
          packages: [],
          status: row.status,
          userRequirement: row.user_requirements,
        }
        return [row.id, booking]
      }),
    )

    let packageDict = Object.fromEntries(
      packageList.map(row => {
        if (!row.id) throw new Error('cartService: Package ID not found in query result.')
        return [row.id, row]
      })
    )

    bookingPackageList.forEach(row => {
      if (!bookingDict[row.booking_id]) throw new Error(`cartService: Booking ID ${row.booking_id} does not exist`)
      let booking = bookingDict[row.booking_id]
      let package_ = packageDict[row.package_id]

      if (!row.id) throw new Error('cartService: PackageBooking ID not found in query result.')
      booking.packages.push({
        packageId: row.package_id,
        packageName: package_.name,
        unitPrice: row.unit_price,
        quantity: row.quantity,
        timeSectionName: row.section,
        timeSectionId: row.time_section_id
      })
    })


    const bookings: DTO.Booking[] = [];
    for (let id in bookingDict) {
      bookings.push(bookingDict[id])
    }

    return bookings;
  }

  async deleteItem(user_id: number, item_id: number): Promise<void> {
    const knex = this.knex

    await knex
      .del()
      .from(knex.raw('bookings_packages USING bookings'))
      .where({
        'bookings_packages.booking_id': item_id,
        'bookings.user_id': user_id
      })

    await knex('bookings')
      .where({
        id: item_id,
        user_id
      })
      .del()

    return
  }

  async postCart(userId: number,
    activityId: number,
    activityDate: number,
    bookingDate: number,
    totalPrice: number,
    packages: bookingPackage[]): Promise<any> {

    const bookingId = await this.knex.raw(`
        INSERT INTO bookings (
          user_id,
          activity_id,
          activity_date,
          booking_date,
          total_price,
          status,
          user_requirements
          )
        VALUES (?,?,?,?,?,?,?)
        RETURNING id;
    `, [userId, activityId, Math.floor(new Date(activityDate).setHours(8, 0, 0, 0) / 1000), Math.floor(bookingDate / 1000), totalPrice, BookingStatus.inCart, null])

    packages.map(async (eachPackage) => {
      await this.knex.raw(`
            INSERT INTO bookings_packages (
              booking_id,
              package_id,
              time_section_id,
              quantity,
              unit_price
          )
              VALUES
              (?,?,?,?,?);
        `, [bookingId.rows[0].id, eachPackage.packageId, eachPackage.timeSectionId, eachPackage.quantity, eachPackage.unitPrice])
    })
  }


  async updateCart(userId: number, bookingId: number, activityDate: number, packages: bookingPackage[]): Promise<any> {
    await this.knex.raw(`
        UPDATE bookings
        SET activity_date = ?
        WHERE id = ?
        AND user_id = ?; 
    `, [Math.floor(new Date(activityDate).setHours(8, 0, 0, 0) / 1000), bookingId, userId]);

    packages.map(async (eachPackage: bookingPackage) => {
      await this.knex.raw(`
          UPDATE bookings_packages
          SET time_section_id = ?,
          quantity = ?,
          unit_price = ?
          WHERE booking_id = ?
          AND package_id = ?; 
      `, [eachPackage.timeSectionId, eachPackage.quantity, eachPackage.unitPrice, bookingId, eachPackage.packageId]);

      await this.knex.raw(`
        UPDATE bookings
        SET total_price = ?
        WHERE id = ?
        AND user_id = ?; 
    `, [eachPackage.quantity * eachPackage.unitPrice, bookingId, userId]);
    })
  }

  async getSingleCart(userId: number, bookingId: number) {
    const singleCartQuery = await this.knex.raw(`
    SELECT bookings.activity_id, bookings.activity_date, bookings_packages.id, bookings_packages.time_section_id, bookings_packages.quantity, bookings_packages.unit_price
    FROM bookings
    INNER JOIN bookings_packages
    ON bookings.id = bookings_packages.booking_id
    WHERE bookings.id = ?
    AND bookings.user_id = ?
    ORDER BY bookings_packages.id ASC;
    `, [bookingId, userId])

    if (singleCartQuery.rows.length === 0) throw new Error(`Booking with id ${bookingId} does not exist.`)

    const returnObject: any = {
      activityId: singleCartQuery.rows[0].activity_id,
      activityDate: singleCartQuery.rows[0].activity_date,
      packages: []
    }


    singleCartQuery.rows.map((row: any) => {
      returnObject.packages.push({
        packageId: row.id,
        timeSectionId: row.time_section_id,
        quantity: row.quantity,
        unitPrice: row.unit_price
      })
    })

    return returnObject
  }
}