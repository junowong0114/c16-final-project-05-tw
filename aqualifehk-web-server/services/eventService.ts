import { Knex } from 'knex';
import { DTO } from 'shared';
import { v4 as uuidv4 } from 'uuid';

export class EventService {
  constructor(private knex: Knex) { }

  createEvent = async (eventInfo: DTO.EventDetail, owner_id: number) => {
    let knex = this.knex

    // check if user id exists
    const ownerId = (await knex.select('id').from('users').where("id", owner_id).first()).id
    if (!ownerId) throw new Error(`user with id ${owner_id} is not found.`)

    // prepare uuid
    const uuid = uuidv4();
    // insert into events
    const insertEventQuery = knex('events')
      .insert({
        owner_id: ownerId,
        name: eventInfo.event_name,
        unix_confirmed_date: eventInfo.unix_confirmed_date,
        invite_code: uuid
      })
      .returning('id')
    const eventId = (await insertEventQuery)[0]
    if (!eventId) throw new Error('Error during event creation.')

    // insert into events_activities
    const activitiesIdQueries = eventInfo.activities.map(a => knex('activities').select('id').where('id', a.activity_id).first())
    const idRes = await Promise.all(activitiesIdQueries)

    const activitiesToInsert = idRes.map(idRow => {
      return {
        event_id: eventId,
        activity_id: idRow.id,
      }
    })
    const insertEventActivitiesQuery = knex('events_activities')
      .insert(activitiesToInsert)

    // insert into events_times
    const timesToInsert = eventInfo.times.map(t => {
      const startTime = Math.floor(t.start_time_unix / 1000)
      const endTime = t.end_time_unix ? Math.floor(t.end_time_unix / 1000) : null
      return {
        event_id: eventId,
        start_time_unix: startTime,
        end_time_unix: endTime,
      }
    })
    const insertEventTimesQuery = knex('events_times')
      .insert(timesToInsert)

    await Promise.all([insertEventActivitiesQuery, insertEventTimesQuery])

    return
  }

  deleteEvent = async (eventId: number, ownerId: number) => {
    const knex = this.knex
    const eventIdQuery = knex.select('id')
      .from('events')
      .where('id', eventId)
      .first()
    const isOwnerQuery = knex('events')
      .select('*')
      .where('owner_id', ownerId)
      .first()
    const [eventIdRes, isOwner] = await Promise.all([eventIdQuery, isOwnerQuery])
    if (!eventIdRes) throw new Error(`event with id ${eventId} is not found`)
    const event_id = eventIdRes.id
    if (!isOwner) throw new Error(`User with id ${ownerId} is no an owner of event ${eventId}`)
    
    // remove activity votes
    await knex.raw(`
      DELETE FROM events_activities_votes
      USING events_activities
      WHERE events_activities_votes.events_activities_id = events_activities.id
      AND events_activities.event_id = ?
    `, [event_id])

    // remove times votes
    await knex.raw(`
      DELETE FROM events_times_votes
      USING events_times
      WHERE events_times_votes.events_times_id = events_times.id
      AND events_times.event_id = ?
    `, [event_id])

    // remove activities
    await knex.raw(`
      DELETE FROM events_activities
      WHERE events_activities.event_id = ?
    `, [event_id])

    // remove times
    await knex.raw(`
      DELETE FROM events_times
      WHERE events_times.event_id = ?
    `, [event_id])

    await knex.raw(`
      DELETE FROM events_users
      WHERE event_id = ?
    `, [event_id])

    // remove event
    await knex.raw(`
      DELETE FROM events
      WHERE events.id = ?
    `, [event_id])

    return
  }

  // getEvent = async (user_id: number): Promise<DTO.EventDetail[]> => {
  getEvent = async (user_id: number): Promise<void> => {
    let knex = this.knex

    const userId = (await knex.select('id').from('users').where('id', user_id).first()).id
    if (!userId) throw new Error(`User with id ${user_id} is not found`)

    const events = (await knex.raw(`
      WITH owner_info AS (
        SELECT events.id as event_id,
          users.id as owner_id,
          jsonb_build_object(
            'first_name', users.first_name,
            'last_name', users.last_name
          ) as owner_name
        FROM users
        INNER JOIN events ON events.owner_id = users.id
      ),
      participant_info AS (
        SELECT events_users.event_id as event_id,
        jsonb_agg(events_users.user_id) as participant_ids,
        jsonb_agg(json_build_object(
          'first_name', users.first_name,
          'last_name', users.last_name
        )) as participant_names
        FROM events_users
        LEFT OUTER JOIN users ON users.id = events_users.user_id
        GROUP BY event_id
      )
      SELECT DISTINCT ON (events.id)
        events.id as event_id, 
        events.name as event_name,
        events.invite_code as invite_code,
        owner_info.owner_id as owner_id,
        owner_info.owner_name as owner_name,
        participant_info.participant_ids as participant_ids,
        participant_info.participant_names as participant_names,
        unix_confirmed_date
      FROM events
      LEFT OUTER JOIN events_users ON events_users.event_id = events.id
      INNER JOIN users 
      ON users.id = events.owner_id OR users.id = events_users.user_id
      INNER JOIN owner_info ON owner_info.event_id = events.id
      LEFT OUTER JOIN participant_info ON participant_info.event_id = events.id
      WHERE events.owner_id = ?
      OR events_users.user_id = ?
      GROUP BY events.id, events.name, unix_confirmed_date, owner_info.owner_id, events.invite_code
        , owner_info.owner_name, participant_info.participant_ids, participant_info.participant_names
    `, [user_id, user_id])).rows
    if (!events) throw new Error(`Error during fetching event for user ${user_id} from database`)

    const activitiesQueries = []
    const timesQueries = []
    for (let _e of events) {
      let eventId = _e.event_id
      activitiesQueries.push(
        knex.raw(`
        WITH tmp AS (
          WITH images AS ( 
            SELECT activity_id, jsonb_agg(url) as urls
              FROM activity_images
              GROUP BY activity_id
              ORDER BY activity_id
          )
          SELECT events.id as event_id,
            events.owner_id as owner_id,
            events_activities.id AS activity_vote_id,
            events_activities.activity_id,
            activities.name AS activity_name,
            activities.address AS activity_address,
            jsonb_agg(events_activities_votes.voter_id) AS voter_ids,
            COUNT(events_activities_votes.voter_id) AS vote_count,
            jsonb_agg(json_build_object(
              'first_name', users.first_name,
              'last_name', users.last_name)) AS voter_names,
            events.unix_confirmed_date,
            images.urls AS thumbnail_urls
          FROM events
          LEFT OUTER JOIN events_activities ON events_activities.event_id = events.id
          LEFT OUTER JOIN events_activities_votes
          ON events_activities_votes.events_activities_id = events_activities.id
          LEFT OUTER JOIN activities ON activities.id = events_activities.activity_id
          LEFT OUTER JOIN users ON users.id = events_activities_votes.voter_id
          FULL OUTER JOIN events_users ON events_users.user_id = users.id
          LEFT OUTER JOIN images ON images.activity_id = activities.id
          WHERE events.id = ?
          GROUP BY events.id, events_activities.activity_id, events_activities.id, activities.name,
            activities.address, images.urls, events.unix_confirmed_date, owner_id
        )
        SELECT DISTINCT ON (tmp.activity_vote_id)
          json_build_object(
          'activity_vote_id', tmp.activity_vote_id,
          'activity_id', tmp.activity_id,
          'activity_name', tmp.activity_name,
          'activity_address', tmp.activity_address,
          'voter_ids', tmp.voter_ids,
          'voter_names', tmp.voter_names,
          'vote_count', tmp.vote_count,
          'unix_confirmed_date', tmp.unix_confirmed_date,
          'thumbnail_urls', tmp.thumbnail_urls) as activities_row
        FROM tmp
        LEFT OUTER JOIN events_users ON events_users.event_id = tmp.event_id
        WHERE events_users.user_id = ? OR tmp.owner_id = ? AND tmp.event_id = ?
        `, [eventId, userId, userId, eventId])
      )

      timesQueries.push(
        knex.raw(`
        WITH tmp AS (
          SELECT events.id AS event_id,
            events.owner_id AS owner_id,
            events_times.id As time_vote_id,
            events_times.start_time_unix,
            events_times.end_time_unix,
            jsonb_agg(events_times_votes.voter_id) AS voter_ids,
            COUNT(events_times_votes.voter_id) AS vote_count,
            jsonb_agg(json_build_object(
              'first_name', users.first_name,
              'last_name', users.last_name)) AS voter_names
          FROM events
          LEFT OUTER JOIN events_times ON events_times.event_id = events.id
          LEFT OUTER JOIN events_times_votes ON events_times_votes.events_times_id = events_times.id
          LEFT OUTER JOIN users ON users.id = events_times_votes.voter_id
          WHERE events.id = ?
          GROUP BY events_times.id, events.id, events.owner_id, events
        )
        SELECT DISTINCT ON (tmp.time_vote_id)
          json_build_object(
          'time_vote_id', tmp.time_vote_id,
          'start_time_unix', tmp.start_time_unix,
          'end_time_unix', tmp.end_time_unix,
          'voter_ids', tmp.voter_ids,
          'voter_names', tmp.voter_names,
          'vote_count', tmp.vote_count) AS times_row
        FROM tmp
        LEFT OUTER JOIN events_users ON events_users.event_id = tmp.event_id
        WHERE events_users.user_id = ? OR tmp.owner_id = ? AND tmp.event_id = ?
        GROUP BY tmp.event_id, tmp.time_vote_id, tmp.start_time_unix, tmp.end_time_unix,
          tmp.voter_ids, tmp.voter_names, tmp.vote_count
        `, [eventId, userId, userId, eventId])
      )
    }
    const activitiesQueryResult = (await Promise.all(activitiesQueries)).map(_act => _act.rows)
    const timesQueryResult = (await Promise.all(timesQueries)).map(_time => _time.rows)

    return events.map((_e: any, i: number) => {
      return {
        ..._e,
        activities: activitiesQueryResult[i].map((row: any) => row.activities_row),
        times: timesQueryResult[i].map((row: any) => row.times_row)
      }
    })
  }

  insertParticipant = async (user_id: number, invite_code: string) => {
    let knex = this.knex

    const userId = (await knex.select('id').from('users').where('id', user_id).first()).id
    if (!userId) throw new Error(`User with id ${user_id} is not found`)
    
    const eventIdQuery = knex.select('id')
      .from('events')
      .where('invite_code', invite_code)
      .first()
    const isExistingUserQuery = knex('events_users')
      .select('*')
      .join('events', 'events.id', 'events_users.event_id')
      .where('user_id', user_id)
      .andWhere('invite_code', invite_code)
      .first()
    const isOwnerQuery = knex('events')
      .select('*')
      .join('events_users', 'events.id', 'events_users.event_id')
      .where('owner_id', user_id)
      .first()
    const [eventIdRes, isExisting, isOwner] = await Promise.all([eventIdQuery, isExistingUserQuery, isOwnerQuery])
    
    if (!eventIdRes) throw new Error(`event with invite code ${invite_code} is not found`)
    const eventId = eventIdRes.id
    if (isExisting || isOwner) throw new Error(`User with id ${user_id} is already a participant/owner of event ${eventId}`)

    await knex('events_users')
      .insert({
        event_id: eventId,
        user_id: userId,
      })

    return
  }

  insertActivityVote = async (event_id: number, event_activity_ids: number[], user_id: number) => {
    if (event_activity_ids.length === 0) throw new Error('no activity is selected')

    let knex = this.knex

    // check if event id, event_activity_id, and user_id exists
    const eventIdQuery = knex('events').select('id').where('id', event_id).first()
    const eventActivityIdQueries = []
    for (let _act_id of event_activity_ids) {
      eventActivityIdQueries.push(knex().
        select('id')
        .from('events_activities')
        .where('id', _act_id)
        .first()
      )
    }
    const userIdQuery = knex('users').select('id').where('id', user_id).first()
    const [
      eventIdRow,
      eventActivityIdRows,
      userIdRow
    ] = await Promise.all([eventIdQuery, eventActivityIdQueries, userIdQuery])

    if (!eventIdRow.id) throw new Error(`event with id ${event_id} does not exist`)
    if (!eventActivityIdRows || eventActivityIdRows.some(row => row === undefined)) {
      throw new Error(`Corrupted activity votes with incorrect id`)
    }
    if (!userIdRow.id) throw new Error(`user with id ${user_id} does not exist`)

    // check if vote is duplicate
    const isDuplicatedQuery = []
    for (let _act_id of event_activity_ids) {
      isDuplicatedQuery.push(knex('events_activities_votes')
        .select('id')
        .where('voter_id', userIdRow.id)
        .where('events_activities_id', _act_id)
        .first())
    }
    const isDuplicatedResult = await Promise.all(isDuplicatedQuery)
    if (isDuplicatedResult.some(row => row)) throw new Error('Duplicated vote found')

    // TODO: check if userId is in events_users, throw error if not
    const rowsToInsert = []
    for (let eventActivityId of eventActivityIdRows) {
      rowsToInsert.push({
        voter_id: userIdRow.id,
        events_activities_id: eventActivityId
      })
    }

    await knex('events_activities_votes').insert(rowsToInsert)
    return
  }

  insertTimeVote = async (event_id: number, event_time_ids: number[], user_id: number) => {
    if (event_time_ids.length === 0) throw new Error('no time is selected')

    let knex = this.knex

    // check if event id, event_time_id, and user_id exists
    const eventIdQuery = knex('events').select('id').where('id', event_id).first()
    const eventTimeIdQueries = []
    for (let _time_id of event_time_ids) {
      eventTimeIdQueries.push(knex().
        select('id')
        .from('events_times')
        .where('id', _time_id)
        .first()
      )
    }
    const userIdQuery = knex('users').select('id').where('id', user_id).first()
    const [
      eventIdRow,
      eventTimeIdRows,
      userIdRow
    ] = await Promise.all([eventIdQuery, eventTimeIdQueries, userIdQuery])

    if (!eventIdRow.id) throw new Error(`event with id ${event_id} does not exist`)
    if (!eventTimeIdRows || eventTimeIdRows.some(row => row === undefined)) {
      throw new Error(`Corrupted time votes with incorrect id`)
    }
    if (!userIdRow.id) throw new Error(`user with id ${user_id} does not exist`)

    // check if vote is duplicate
    const isDuplicatedQuery = []
    for (let _act_id of event_time_ids) {
      isDuplicatedQuery.push(knex('events_times_votes')
        .select('id')
        .where('voter_id', userIdRow.id)
        .where('events_times_id', _act_id)
        .first())
    }
    const isDuplicatedResult = await Promise.all(isDuplicatedQuery)
    if (isDuplicatedResult.some(row => row)) throw new Error('Duplicated vote found')

    // TODO: check if userId is in events_users, throw error if not
    const rowsToInsert = []
    for (let eventTimeId of eventTimeIdRows) {
      rowsToInsert.push({
        voter_id: userIdRow.id,
        events_times_id: eventTimeId
      })
    }

    await knex('events_times_votes').insert(rowsToInsert)
    return
  }
}