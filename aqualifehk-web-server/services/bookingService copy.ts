import { Knex } from 'knex';
import { SqlTypeFile } from 'gen-sql-type'

const sqlTypeFile = SqlTypeFile.withPrefix(__filename)

const ActivityQuerySql = sqlTypeFile.wrapSql('ActivityQuery', `
    WITH images AS ( 
      SELECT activity_id, json_agg(url) as activity_images
        FROM activity_images
        GROUP BY activity_id
        ),
    reviews AS (
      SELECT activity_id, AVG(reviews.stars) as rating, SUM(reviews.id) as review_count
        FROM reviews 
        INNER JOIN activities ON activities.id = reviews.activity_id
        GROUP BY reviews.activity_id
        )
    SELECT activities.id, shop_id, name as activity_name , address, latlong, category_id, rating, review_count, activity_images
      FROM activities
      LEFT JOIN images ON activities.id = images.activity_id
      LEFT JOIN reviews ON reviews.activity_id = activities.id
      WHERE activities.id = ?
      ORDER BY activities.id;
  `)

export class BookingService {
  constructor(private knex: Knex) {};

  async getActivityInfo(activity_id: number): Promise<any> {
    let knex: Knex = this.knex

    let activitiesQuery = knex.raw(ActivityQuerySql, activity_id)

    let packagesQuery = knex.raw(`
      WITH section_tmp AS (
        SELECT package_id, json_build_object('id', id, 'name', section) as sections
        FROM time_sections
        GROUP BY package_id, id, section
        ORDER BY package_id
        ),
        prices_tmp AS ( 
          WITH tmp AS (
            SELECT * FROM prices ORDER BY package_id, effective_date DESC
          ) 
          SELECT package_id, jsonb_agg(json_build_object('base_price', base_price, 
            'weekend_price', weekend_price,
            'unix_effective_date', effective_date,
            'unix_expiration_date', expiration_date)) as price
          FROM tmp
          GROUP BY package_id
        ) SELECT packages.id, packages.name, inclusion, eligibility, capacity, 
          json_agg(section_tmp.sections) as time_sections,
          prices_tmp.price as prices
        FROM packages
        INNER JOIN section_tmp ON packages.id = section_tmp.package_id
        INNER JOIN prices_tmp ON packages.id = prices_tmp.package_id
        WHERE packages.activity_id = ?
        GROUP BY packages.id, packages.name, inclusion, eligibility, capacity, prices_tmp.price
    `, activity_id)

    const [activityInfo, packagesInfo] = await Promise.all([
      activitiesQuery,
      packagesQuery,
    ])

    const returnValue = {
      ...activityInfo.rows[0],
      packages: packagesInfo.rows.length > 0 ? packagesInfo.rows : undefined
    }

    return returnValue
  }

  async getPackageAvailability(package_id: number, start_date_unix: string, quantity: number) {
    return (await this.knex.raw(`
      WITH tmp AS (
        SELECT * 
        FROM time_section_availability
        ORDER BY "date" ASC, time_section_id ASC
      ) SELECT "date" as unix_date, jsonb_object_agg(time_section_id, remain) as remaining
      FROM tmp
      WHERE package_id = ? AND "date" >= ?
      GROUP BY "date", package_id
      ORDER BY "date" ASC
      LIMIT ?
    `, [package_id, start_date_unix, quantity])).rows
  }
}

