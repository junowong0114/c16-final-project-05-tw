import { Knex } from 'knex';
import { getBookings } from '../helpers/knexHelpers';

export class OrderDetailsService {
  constructor(private knex: Knex) { };

  async loadOrder(user_id: number, order_id: number): Promise<any[]> {
    let orders = await getBookings(user_id, this.knex, false)
    let filtered = orders.filter((order) => {
      return order.id === order_id
    })
    return filtered
  }

  async loadOrderShop(user_id: number, order_id: number) {
    let shop = await this.knex.select('shops.name', 'shops.phone', 'shops.email')
      .from('bookings')
      .join('activities', 'bookings.activity_id', '=', 'activities.id')
      .join('shops', 'activities.shop_id', '=', 'shops.id')
      .where('bookings.id', order_id)
      .andWhere('bookings.user_id', user_id)
    return shop
  }
}