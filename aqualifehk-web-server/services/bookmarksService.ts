import { Knex } from 'knex';


export class BookmarksService {
    constructor(private knex: Knex) { };

    async addBookmark(userId: number, activityId: number) {

        await this.knex.raw(`
            INSERT INTO bookmarks(
                user_id,
                activity_id
            )
            VALUES (?,?)
        `, [userId, activityId])
    }

    async deleteBookmark(userId: number, bookmarkId: number) {
        await this.knex.raw(`
            DELETE FROM bookmarks
            WHERE user_id = ?
            AND id = ?
        `, [userId, bookmarkId])
    }

    async getBookmark(userId: number) {
        const bookmarksQuery = await this.knex.raw(`
        SELECT bookmarks.id AS bookmarksId, activities.id AS activityId, activities.name AS activityName, activities.address, shops.name AS shopName, categories.name AS category ,reviews
        FROM bookmarks
        INNER JOIN activities
        ON bookmarks.activity_id = activities.id
        INNER JOIN shops
        ON activities.shop_id = shops.id
        INNER JOIN categories
        ON activities.category_id = categories.id
        LEFT JOIN (
            SELECT AVG (reviews.stars) AS reviews, activity_id
            FROM reviews
            GROUP BY activity_id
            ) averageReviews
        ON activities.id = averageReviews.activity_id
        WHERE bookmarks.user_id = ?;
        `, [userId])

        let returnValue:any = []
        bookmarksQuery.rows.map((row:any) => {
            returnValue.push({
                "bookmarksId": row.bookmarksid,
                "activityId": row.activityid,
                "activityName": row.activityname,
                "address": row.address,
                "shopName": row.shopname,
                "category": row.category,
                "reviews": row.reviews
            })
        })
        return returnValue
    }
}