export type ActivityQueryParameters = {
  activity_id: any,
}
export type ActivityQueryRow = {
  id: any,
  shop_id: any,
  activity_name: any,
  address: any,
  latlong: any,
  category_id: any,
  rating: any,
  review_count: any,
  activity_images: any,
}

export type PackagesQueryParameters = {
  query_unix_date: any,
  activity_id: any,
}
export type PackagesQueryRow = {
  id: any,
  name: any,
  inclusion: any,
  eligibility: any,
  capacity: any,
  time_sections: any,
  prices: any,
}
