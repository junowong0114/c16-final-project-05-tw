import { bookingPackage } from 'shared';

import { BookingStatus } from 'shared';
import { Knex } from 'knex';
import { SqlTypeFile } from 'gen-sql-type';

const sqlTypeFile = SqlTypeFile.withPrefix(__filename)

const ActivityQuerySql = sqlTypeFile.wrapSql('ActivityQuery', `
    WITH images AS ( 
      SELECT activity_id, json_agg(url) as activity_images
        FROM activity_images
        GROUP BY activity_id
        ),
    reviews AS (
      SELECT activity_id, AVG(reviews.stars) as rating, COUNT(reviews.id) as review_count
        FROM reviews 
        INNER JOIN activities ON activities.id = reviews.activity_id
        GROUP BY reviews.activity_id
        )
    SELECT activities.id, shop_id, name as activity_name , address, latlong, category_id, rating, review_count, activity_images
      FROM activities
      LEFT JOIN images ON activities.id = images.activity_id
      LEFT JOIN reviews ON reviews.activity_id = activities.id
      WHERE activities.id = :activity_id
      ORDER BY activities.id;
  `)

const packagesQuerySql = sqlTypeFile.wrapSql('PackagesQuery', `
WITH section_tmp AS (
  SELECT package_id, json_build_object('id', id, 'name', section) as sections
  FROM time_sections
  GROUP BY package_id, id, section
  ORDER BY id ASC
  ),
  prices_tmp AS ( 
    WITH tmp AS (
      SELECT * FROM prices
      WHERE "effective_date" <= :query_unix_date
      ORDER BY package_id, effective_date DESC
    ) 
    SELECT package_id, jsonb_agg(json_build_object('base_price', base_price, 
      'weekend_price', weekend_price,
      'unix_effective_date', effective_date,
      'unix_expiration_date', expiration_date)) as price
    FROM tmp
    GROUP BY package_id
  ) SELECT packages.id, packages.name, inclusion, eligibility, capacity, 
    json_agg(section_tmp.sections) as time_sections,
    prices_tmp.price as prices
  FROM packages
  INNER JOIN section_tmp ON packages.id = section_tmp.package_id
  INNER JOIN prices_tmp ON packages.id = prices_tmp.package_id
  WHERE packages.activity_id = :activity_id
  GROUP BY packages.id, packages.name, inclusion, eligibility, capacity, prices_tmp.price
  ORDER BY packages.id
`)

export class BookingService {
  constructor(private knex: Knex) { };

  async getActivityInfo(activity_id: number): Promise<any> {
    let knex: Knex = this.knex
    const query_unix_date = Math.floor((new Date()).getTime() / 1000)

    let activitiesQuery = knex.raw(ActivityQuerySql, { activity_id })

    let packagesQuery = knex.raw(packagesQuerySql, { query_unix_date, activity_id })

    const [activityInfo, packagesInfo] = await Promise.all([
      activitiesQuery,
      packagesQuery,
    ])


    const returnValue = {
      ...activityInfo.rows[0],
      packages: packagesInfo.rows.length > 0 ? packagesInfo.rows : undefined
    }

    return returnValue
  }

  async getPackageAvailability(package_id: number, start_date_unix: number, quantity: number) {
    return (await this.knex.raw(`
      WITH tmp AS (
        SELECT * 
        FROM time_section_availability
        ORDER BY "date" ASC, time_section_id ASC
      ) SELECT "date" as unix_date, capacity, jsonb_object_agg(time_section_id, remain) as remaining
      FROM tmp
      WHERE package_id = ? AND "date" >= ?
      GROUP BY "date", package_id, capacity
      ORDER BY "date" ASC
      LIMIT ?
    `, [package_id, start_date_unix, quantity])).rows
  }

  async postBooking(userId: number,
    activityId: number,
    activityDate: number,
    bookingDate: number,
    userRequirements: string,
    packages: bookingPackage[]): Promise<any> {

    const _activityUnixDate = new Date(activityDate).setHours(8, 0, 0, 0)
    const _activityDate = new Date(_activityUnixDate)
    const fiveMin_ms = 5 * 60 * 1000;
    const txn = await this.knex.transaction();
    try {
      if (!activityDate) {
        throw Error('activityDate is missing from request body!(bookingService:postBooking)')
      }

      if (!packages || packages.length === 0) {
        throw new Error('Packages are not found in POST /book request')
      }

      // build queries
      const isWeekend = _activityDate.getDay() === 0 || _activityDate.getDay() === 6
      const pricesQuery = []
      const remainQueries = []
      for (let _pack of packages) {
        pricesQuery.push(
          await txn.select(this.knex.raw(`${isWeekend ? 'weekend_price' : 'base_price'} as price`))
            .from('prices')
            .where('package_id', _pack.packageId)
            .andWhere('effective_date', '<=', Math.floor((bookingDate - fiveMin_ms) / 1000))
            .orderBy('effective_date', "desc")
            .limit(1)
            .first()
        )

        remainQueries.push(
          await txn
            .select('remain')
            .from('time_section_availability')
            .andWhere('time_section_id', _pack.timeSectionId)
            .andWhere('date', Math.floor(_activityUnixDate / 1000))
            .first()
        )
      }

      // promise.all the queries
      const updatedPrices = await Promise.all(pricesQuery)
      if (!updatedPrices || updatedPrices.length === 0 || updatedPrices.some(row => !row)) {
        throw new Error('Error during query of latest prices')
      }


      const remainResult = await Promise.all(remainQueries)
      if (!remainQueries || remainQueries.length === 0 || remainQueries.some(row => !row)) {
        throw new Error('Error during query of packages availabilities')
      }

      // process the query results
      let totalPrice: number = 0;
      for (let index in packages) {
        const _pack = packages[index]

        // update prices
        const updatedUnitPrice = updatedPrices[index].price
        totalPrice += updatedUnitPrice * _pack.quantity
        _pack.unitPrice = updatedUnitPrice

        // check availabilities
        const remain = remainResult[index]
        if (remain > _pack.quantity) throw new Error('Booking failed: insufficient quantity in store')
      }

      // actual insertion into bookings + bookings_packages
      const bookingId = (await txn('bookings')
        .insert({
          user_id: userId,
          activity_id: activityId,
          activity_date: Math.floor(_activityUnixDate / 1000),
          booking_date: Math.floor(bookingDate / 1000),
          total_price: totalPrice,
          status: BookingStatus.confirmed.toString(),
          user_requirements: userRequirements,
        })
        .returning('id'))[0]

      // insert into bookings_packages and update remaining
      let insertQueries = []
      let updateRemainQueries = []
      for (let index in packages) {
        const _pack = packages[index]

        insertQueries.push(
          txn('bookings_packages')
            .insert({
              booking_id: bookingId,
              package_id: _pack.packageId,
              time_section_id: _pack.timeSectionId,
              quantity: _pack.quantity,
              unit_price: _pack.unitPrice,
            })
        )

        const updatedRemain = remainResult[index].remain - _pack.quantity
        updateRemainQueries.push(
          txn('time_section_availability')
            .update({
              remain: updatedRemain,
            })
            .where('package_id', _pack.packageId)
            .andWhere('time_section_id', _pack.timeSectionId)
            .andWhere('date', Math.floor(_activityUnixDate / 1000))
        )
      }
      await Promise.all(insertQueries)
      await Promise.all(updateRemainQueries)

      await txn.commit();
    } catch (e) {
      await txn.rollback();
      throw e;
    }
  }

  async postBookingFromCart(bookingId: number,
    userRequirements: string,
    bookingDate: number): Promise<any> {
    const fiveMin_ms = 5 * 60 * 1000;
    if (!userRequirements) {
      userRequirements = ''
    }
    const txn = await this.knex.transaction();

    try {

      const packages = await txn.select('package_id')
        .from('bookings_packages')
        .where('booking_id', bookingId)


      if (packages.length < 1) {
        throw Error(`The Booking of Id:${bookingId} is missing! (bookingService:postBookingFromCart)`)
      }

      const activityDateQuery = await txn.select('activity_date', 'activity_id')
        .from('bookings')
        .where('id', bookingId)

      if (activityDateQuery.length < 1) {
        throw Error(`The activity date of booking Id:${bookingId} is missing! (bookingService:postBookingFromCart)`)
      }

      const activityDate = activityDateQuery[0].activity_date * 1000;



      const weekDayOrWeekEnd = (new Date(activityDate).getDay() == 0 || new Date(activityDate).getDay() == 6) ? 'weekend_price' : 'base_price';

      let totalPrice: number = 0;
      // let packageWithUpdatedPrice: any[] = []
      const pricesQuery = []

      for (let eachPackage of packages) {
        pricesQuery.push(
          txn.raw(`
            SELECT bookings_packages.package_id AS packageId, bookings_packages.time_section_id AS timeSectionId, prices.${weekDayOrWeekEnd} AS prices, bookings_packages.quantity
            FROM prices
            INNER JOIN bookings_packages
            ON prices.package_id = bookings_packages.package_id
            WHERE prices.package_id = ?
            AND prices.effective_date <= ?
            ORDER BY prices.effective_date DESC
            LIMIT 1;
            `, [eachPackage.package_id, Math.floor((bookingDate - fiveMin_ms) / 1000)])
        )
      }

      const updatedPricesRows = await Promise.all(pricesQuery)
      if (!updatedPricesRows || updatedPricesRows.length === 0 || updatedPricesRows.some(row => !row)) {
        throw new Error('Error during query of latest prices')
      }
      const updatedPricesPackages: any = []
      for (let updatedPricesRow of updatedPricesRows) {
        updatedPricesPackages.push(updatedPricesRow.rows[0])
      }
      updatedPricesPackages.map((updatedPrice: any) => {
        totalPrice = totalPrice + ((updatedPrice.prices * updatedPrice.quantity))
      })


      // //check availability of time section
      const availabilityOfPackages: boolean[] = []

      const remainQuery: any = []
      updatedPricesPackages.map(async (eachPackage: any) => {
        remainQuery.push(
          txn.select('remain', 'capacity', 'package_id', 'time_section_id')
            .from('time_section_availability')
            .where('package_id', eachPackage.packageid)
            .andWhere('time_section_id', eachPackage.timesectionid)
            .andWhere('date', Math.floor(new Date(activityDateQuery[0].activity_date*1000).setHours(8, 0, 0, 0)/1000))
        )
      })
      const remainRows: any = await Promise.all(remainQuery)

      if (remainRows.some((row: any) => {
        return row[0] === undefined
      })) {
        throw Error('Missing One or more package data from time_section_availability table ')
      }

      if (remainRows.length !== updatedPricesPackages.length) {
        throw Error('Missing One or more package data from time_section_availability table ')
      }

      remainRows.map((row: any) => {
        let availabilityOfPackage = row[0].remain > updatedPricesPackages[remainRows.indexOf(row)].quantity ? true : false;
        availabilityOfPackages.push(availabilityOfPackage)
      })




      if (availabilityOfPackages.includes(false)) {
        return
      }

      await txn.raw(`
          UPDATE bookings 
          SET status = ?,
          total_price = ?,
          user_requirements = ?,
          booking_date = ?
          WHERE id = ?
      `, [BookingStatus.confirmed.toString(), totalPrice, userRequirements, Math.floor(bookingDate / 1000), bookingId]);




      updatedPricesPackages.map(async (packageInfo: any) => {
        let remainNumber = remainRows[updatedPricesPackages.indexOf(packageInfo)][0].remain - packageInfo.quantity
        await txn('time_section_availability')
          .where('package_id', packageInfo.packageid)
          .andWhere('time_section_id', packageInfo.timesectionid)
          .andWhere('date', activityDate / 1000)
          .update('remain', remainNumber)


        await txn.raw(`
        UPDATE bookings_packages
        SET unit_price = ?
        WHERE booking_id = ?
        AND package_id = ?
        AND time_section_id = ?; 
    `, [packageInfo.prices, bookingId, packageInfo.packageid, packageInfo.timesectionid])
        await txn.commit();
      })
    } catch (e) {
      await txn.rollback();
      return e;
    }
  }

}


