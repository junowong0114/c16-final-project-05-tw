import { Knex } from "knex";
import { comparePassword, hashPassword } from "../helpers/hash";
import { JWTPayload, registerUser } from 'shared';
import { jwtConfig } from '../helpers/jwt';
import jwtSimple from 'jwt-simple';

export class UserService {
  constructor(public knex: Knex) { };

  async createTokenForNonRegistered() {
    let [user_id] = await this.knex('users').insert({ username: 'unknown', type: '3' }).returning('id');
    let payload: JWTPayload = {
      id: user_id,
      isRegistered: false
    }
    let token = jwtSimple.encode(payload, jwtConfig.SECRET)
    return token
  }

  async createTokenWithPassword(email: string, password: string) {
    let row = await this.knex
      .select('id', 'password_hash')
      .from('users')
      .where({ email: email })
      .first();

    if (!row) {
      throw new Error('Wrong username')
    }
    if (!(await comparePassword(password, row.password_hash))) {
      throw new Error('Wrong username or password')
    }

    let payload: JWTPayload = {
      id: row.id,
      email: email,
      isRegistered: true
    }
    let token = jwtSimple.encode(payload, jwtConfig.SECRET)
    return token;
  }

  async getUserInfo(userId: number) {
    let row = await this.knex
      .select(this.knex.raw(`id as userId, username as userName, gender, email, phone, type, url, first_name as firstName, last_name as lastName`))
      .from('users')
      .where('id', userId)
      .first()

    if (!row) {
      throw new Error(`No user with id ${userId} exists`)
    }

    for (let key in row) {
      if (!row[key]) row[key] = ""
    }

    return row
  }

  async registerUser(user: registerUser, id: number) {
    const checkEmailReturn = await this.knex('users')
      .select('*')
      .where('email', user.email)
      .first();
    if (checkEmailReturn) {
      return {
        success: false,
        error: 'email already exist'
      }
    }

    const password = await hashPassword(user.password);
    const row = await this.knex('users')
      .update({
        email: user.email,
        type: 2,
        phone: user.phone,
        password_hash: password,
        gender: user.gender
      })
      .where({ id: id })
      .returning(['id', 'email'])

    const returning = row[0]
    let payload: JWTPayload = {
      id: returning.id,
      email: returning.email,
      isRegistered: true
    }
    let token = jwtSimple.encode(payload, jwtConfig.SECRET)
    return { success: true, token };
  }

}