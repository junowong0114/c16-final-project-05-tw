import { Knex } from 'knex';
import { getBookings } from '../helpers/knexHelpers';

export class OrderService {
  constructor(private knex: Knex) {};

  async loadOrder(user_id: number): Promise<any[]> {
    return getBookings(user_id, this.knex, false)
  }
}