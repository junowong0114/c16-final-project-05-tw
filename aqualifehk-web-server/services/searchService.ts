import { Knex } from "knex";

export class SearchService {
    constructor(private knex: Knex) { };

    async getSearch(keyWord: string, page: number) {
        const limit = 5;
        const offset = (page - 1) * limit;
        keyWord = '%' + keyWord + '%'
        return (
            await this.knex.raw(
                `select DISTINCT activities.id as id,shop_id, activities.name as name, latlong, categories.name as category,
                    (SELECT url from activity_images WHERE activity_id = activities.id ORDER BY url LIMIT 1) as image_url,
                    (SELECT round(avg(stars)::numeric,1) from reviews WHERE activity_id = activities.id GROUP BY activity_id) AS rating,
                    (SELECT count(COMMENT) FROM reviews WHERE activity_id = activities.id GROUP BY activity_id) AS review_count
                    from activities
                    left join packages on activities.id = packages.activity_id 
                    left join categories on activities.category_id = categories.id 
                    where activities.name ilike ?
                    or activities.address ilike ?
                    or categories.name ilike ?
                    or packages.name ilike ?
                    or packages.inclusion ilike ?
                    or packages.description ilike ?
                    ORDER BY rating DESC NULLS LAST
                    LIMIT ? OFFSET ?
            `, [keyWord, keyWord, keyWord, keyWord, keyWord, keyWord, limit, offset])).rows
    }




    async getHomePage() {
        // const number = 5;
        // const ratingLimit = 4;

        //  main sql for recommendations
        // const recommendations = await this.knex.raw(
        //     `
        //     WITH temp as(
        //         SELECT activity_id, round(avg(stars)::numeric,1) as rating from reviews
        //             GROUP by activity_id)
        //         select activities.id as id, activities.name as name, address, latlong, categories.name as category,rating,
        //             (SELECT url from activity_images WHERE activity_id = activities.id ORDER BY url LIMIT 1) as image_url,
        //             (SELECT count(COMMENT) FROM reviews WHERE activity_id = activities.id GROUP BY activity_id) AS review_count
        //             from activities
        //             left join categories on activities.category_id = categories.id 
        //             LEFT JOIN temp on activities.id = temp.activity_id
        //             ORDER BY 
        //                 CASE 
        //             WHEN rating >= ? THEN rating
        //             END DESC NULLS LAST,review_count DESC NULLS LAST
        //             LIMIT ?
        //     `, [ratingLimit, number]
        // )


        // recommendations for demo
        const recommendations = await this.knex.raw(
            `
            WITH temp as(
                SELECT activity_id, round(avg(stars)::numeric,1) as rating from reviews
                    GROUP by activity_id)
                select activities.id as id, activities.name as name, address, latlong, categories.name as category,rating,
                    (SELECT url from activity_images WHERE activity_id = activities.id ORDER BY url LIMIT 1) as image_url,
                    (SELECT count(COMMENT) FROM reviews WHERE activity_id = activities.id GROUP BY activity_id) AS review_count
                    from activities
                    left join categories on activities.category_id = categories.id 
                    LEFT JOIN temp on activities.id = temp.activity_id
                    where activities.id < 6
                    order by activities.id desc
            `
        )



        // main sql
        // const whatsNew = await this.knex.raw(
        //     `
        //     select DISTINCT activities.id as id, activities.name as name, address, latlong, categories.name as category,
        //         activities.created_at,activities.updated_at,
        //         (SELECT url from activity_images WHERE activity_id = activities.id ORDER BY url LIMIT 1) as image_url,
        //         (SELECT round(avg(stars)::numeric,1) from reviews WHERE activity_id = activities.id GROUP BY activity_id) AS rating,
        //         (SELECT count(COMMENT) FROM reviews WHERE activity_id = activities.id GROUP BY activity_id) AS review_count
        //         from activities
        //         left join categories on activities.category_id = categories.id 
        //         where activities.id >= 6
        //         order BY activities.created_at DESC,activities.updated_at DESC
        //         LIMIT ?
        //     `, [number]
        // )
        
        // sql for demo
        const whatsNew = await this.knex.raw(
            `
            WITH temp as(
                SELECT activity_id, round(avg(stars)::numeric,1) as rating from reviews
                    GROUP by activity_id)
                select activities.id as id, activities.name as name, address, latlong, categories.name as category,rating,
                    (SELECT url from activity_images WHERE activity_id = activities.id ORDER BY url LIMIT 1) as image_url,
                    (SELECT count(COMMENT) FROM reviews WHERE activity_id = activities.id GROUP BY activity_id) AS review_count
                    from activities
                    left join categories on activities.category_id = categories.id 
                    LEFT JOIN temp on activities.id = temp.activity_id
                    where activities.id BETWEEN 6 AND 10
                    order by activities.id             
            `
        )


        return {
            recommendations: recommendations.rows,
            whatsNew: whatsNew.rows
        }

    }
}



