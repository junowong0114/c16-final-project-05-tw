import Knex from 'knex';
import dotenv from 'dotenv';


dotenv.config();

const knexConfigs = require('./knexfile');

const environment = process.env.NODE_ENV;
if (!environment) {
  throw new Error('missing NODE_ENV in process.env');
}

const knexConfig = knexConfigs[environment];
export const knex = Knex(knexConfig);
