export interface User {
  id: number,
  username: string,
  password: string,
  gender: string,
  phone: string,
  email: string,
  facebook: string,
  google: string,
  // type:string
}