import { Knex } from "knex";
import Chance from "chance";
import { insertSeed, randomHKphone } from "../helpers/knexHelpers";
import { UserType } from "shared";

let chance = new Chance();

export async function seed(knex: Knex): Promise<void> {
  // Inserts seed entries
  const retailers = await knex.select('id').from('users').where({ type: UserType.retailer });

  const shopsSeed = [
    {
      user_id: retailers[0].id,
      name: 'Sai Kung kayak',
      phone: randomHKphone(3),
      email: chance.email(),
      address: '743 Tai Mong Tsai Rd, Sai Kung',
    },
    {
      user_id: retailers[0].id,
      name: 'Hong Kong Kiteboarding School',
      phone: randomHKphone(5),
      email: chance.email(),
      address: 'Hong Kong Kiteboarding School',
    },
    {
      user_id: retailers[1].id,
      name: 'AQUA TERRA Performance - personal training',
      phone: randomHKphone(9),
      email: chance.email(),
      address: '14 & 14A, Stanley Main St, Stanley',
    },
    {
      user_id: retailers[1].id,
      name: 'Blue Sky Sports Club',
      phone: randomHKphone(3),
      email: chance.email(),
      address: 'Blue Sky Sports Club Tai Mong Tsai Road Sai Kung Hong Kong',
    }
  ];

  await insertSeed(knex, 'shops', shopsSeed);
};
