import { Knex } from "knex";
import Chance from "chance";
import { hashPassword } from "../helpers/hash";
import { insertSeed, randomHKphone } from '../helpers/knexHelpers';
import { UserType } from "shared";

export async function seed(knex: Knex): Promise<void> {
  let chance = new Chance();

  await insertSeed(knex, 'users', [
    {
      username: 'admin',
      email: 'admin@gmail.com',
      password_hash: await hashPassword("admin"),
      phone: randomHKphone(2),
      type: UserType.admin,
      gender: "Male"
    },
    {
      username: 'retailer',
      email: 'retailer@gmail.com',
      password_hash: await hashPassword("retailer"),
      phone: randomHKphone(3),
      type: UserType.retailer,
      gender: "Male"
    },
    {
      username: 'seller',
      email: 'seller@gmail.com',
      password_hash: await hashPassword("retailer"),
      phone: randomHKphone(5),
      type: UserType.retailer,
      gender: "Male"
    },
    {
      username: 'user',
      email: 'user@gmail.com',
      first_name: 'Juno',
      last_name: 'Wong',
      password_hash: await hashPassword('user'),
      phone: randomHKphone(9),
      type: UserType.user,
      gender: "Male"
    },
    {
      username: 'user2',
      email: 'user2@gmail.com',
      first_name: 'Stephen',
      last_name: 'Hui',
      password_hash: await hashPassword('user'),
      phone: randomHKphone(9),
      type: UserType.user,
      gender: "Male"
    },
    {
      username: 'user3',
      email: 'user3@gmail.com',
      first_name: 'Happy',
      last_name: 'Sew',
      password_hash: await hashPassword('user'),
      phone: randomHKphone(9),
      type: UserType.user,
      gender: "Male"
    },
    {
      username: 'user4',
      email: 'user4@gmail.com',
      first_name: 'Wilson',
      last_name: 'Ho',
      password_hash: await hashPassword('user'),
      phone: randomHKphone(9),
      type: UserType.user,
      gender: "Male"
    },
    {
      username: 'user5',
      email: 'user5@gmail.com',
      first_name: 'Gordon',
      last_name: 'Lau',
      password_hash: await hashPassword('user'),
      phone: randomHKphone(9),
      type: UserType.user,
      gender: "Male"
    },
    {
      username: 'user6',
      email: 'user6@gmail.com',
      first_name: 'Beeno',
      last_name: 'Tung',
      password_hash: await hashPassword('user'),
      phone: randomHKphone(9),
      type: UserType.user,
      gender: "Male"
    },
    {
      username: chance.name(),
      email: chance.email(),
      password_hash: "user",
      phone: randomHKphone(3),
      type: UserType.user,
      gender: "Male"
    },
    {
      username: chance.name(),
      email: chance.email(),
      password_hash: "user",
      phone: randomHKphone(3),
      type: UserType.user,
      gender: chance.gender()
    },
    {
      username: chance.name(),
      email: chance.email(),
      password_hash: "user",
      phone: randomHKphone(5),
      type: UserType.user,
      gender: chance.gender()
    },
    {
      username: chance.name(),
      email: chance.email(),
      password_hash: "user",
      phone: randomHKphone(9),
      type: UserType.user,
      gender: chance.gender()
    },
    {
      username: chance.name(),
      email: chance.email(),
      password_hash: "user",
      phone: randomHKphone(3),
      type: UserType.user,
      gender: chance.gender()
    },
    {
      username: 'user3',
      email: 'happy@gmail.com',
      first_name: 'Happy',
      last_name: 'Sew',
      password_hash: await hashPassword('user'),
      phone: randomHKphone(9),
      type: UserType.user,
      gender: "Male"
    },
    {
      username: 'user3',
      email: 'happy1@gmail.com',
      first_name: 'Happy',
      last_name: 'Sew',
      password_hash: await hashPassword('user'),
      phone: randomHKphone(9),
      type: UserType.user,
      gender: "Male"
    },
    {
      username: 'user3',
      email: 'happy2@gmail.com',
      first_name: 'Happy',
      last_name: 'Sew',
      password_hash: await hashPassword('user'),
      phone: randomHKphone(9),
      type: UserType.user,
      gender: "Male"
    },
    {
      username: 'user3',
      email: 'happy3@gmail.com',
      first_name: 'Happy',
      last_name: 'Sew',
      password_hash: await hashPassword('user'),
      phone: randomHKphone(9),
      type: UserType.user,
      gender: "Male"
    },
    {
      username: 'user3',
      email: 'happy4@gmail.com',
      first_name: 'Happy',
      last_name: 'Sew',
      password_hash: await hashPassword('user'),
      phone: randomHKphone(9),
      type: UserType.user,
      gender: "Male"
    },
    {
      username: 'user3',
      email: 'happy5@gmail.com',
      first_name: 'Happy',
      last_name: 'Sew',
      password_hash: await hashPassword('user'),
      phone: randomHKphone(9),
      type: UserType.user,
      gender: "Male"
    },
    {
      username: 'user',
      email: 'juno@gmail.com',
      first_name: 'Juno',
      last_name: 'Wong',
      password_hash: await hashPassword('user'),
      phone: randomHKphone(9),
      type: UserType.user,
      gender: "Male"
    },
    {
      username: 'user',
      email: 'juno1@gmail.com',
      first_name: 'Juno',
      last_name: 'Wong',
      password_hash: await hashPassword('user'),
      phone: randomHKphone(9),
      type: UserType.user,
      gender: "Male"
    },
    {
      username: 'user',
      email: 'juno2@gmail.com',
      first_name: 'Juno',
      last_name: 'Wong',
      password_hash: await hashPassword('user'),
      phone: randomHKphone(9),
      type: UserType.user,
      gender: "Male"
    },
    {
      username: 'user',
      email: 'juno3@gmail.com',
      first_name: 'Juno',
      last_name: 'Wong',
      password_hash: await hashPassword('user'),
      phone: randomHKphone(9),
      type: UserType.user,
      gender: "Male"
    },
    {
      username: 'user',
      email: 'juno4@gmail.com',
      first_name: 'Juno',
      last_name: 'Wong',
      password_hash: await hashPassword('user'),
      phone: randomHKphone(9),
      type: UserType.user,
      gender: "Male"
    },
    {
      username: 'user',
      email: 'juno5@gmail.com',
      first_name: 'Juno',
      last_name: 'Wong',
      password_hash: await hashPassword('user'),
      phone: randomHKphone(9),
      type: UserType.user,
      gender: "Male"
    },
    {
      username: 'user2',
      email: 'stephen@gmail.com',
      first_name: 'Stephen',
      last_name: 'Hui',
      password_hash: await hashPassword('user'),
      phone: randomHKphone(9),
      type: UserType.user,
      gender: "Male"
    },
    {
      username: 'user2',
      email: 'stephen1@gmail.com',
      first_name: 'Stephen',
      last_name: 'Hui',
      password_hash: await hashPassword('user'),
      phone: randomHKphone(9),
      type: UserType.user,
      gender: "Male"
    },
    {
      username: 'user2',
      email: 'stephen2@gmail.com',
      first_name: 'Stephen',
      last_name: 'Hui',
      password_hash: await hashPassword('user'),
      phone: randomHKphone(9),
      type: UserType.user,
      gender: "Male"
    },
    {
      username: 'user2',
      email: 'stephen3@gmail.com',
      first_name: 'Stephen',
      last_name: 'Hui',
      password_hash: await hashPassword('user'),
      phone: randomHKphone(9),
      type: UserType.user,
      gender: "Male"
    },
    {
      username: 'user2',
      email: 'stephen4@gmail.com',
      first_name: 'Stephen',
      last_name: 'Hui',
      password_hash: await hashPassword('user'),
      phone: randomHKphone(9),
      type: UserType.user,
      gender: "Male"
    },
    {
      username: 'user2',
      email: 'stephen5@gmail.com',
      first_name: 'Stephen',
      last_name: 'Hui',
      password_hash: await hashPassword('user'),
      phone: randomHKphone(9),
      type: UserType.user,
      gender: "Male"
    },

  ]);
};

