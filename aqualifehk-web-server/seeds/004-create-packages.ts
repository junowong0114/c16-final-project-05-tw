import { Knex } from "knex";
import Chance from "chance";
import { insertSeed } from "../helpers/knexHelpers";
import { jsUnixToPostgresUnix } from "../helpers/format";
let chance = new Chance();


export async function seed(knex: Knex): Promise<void> {
  // Inserts seed entries
  const activitiesID = await knex.select('id').from('activities');

  const packagesSeed = [
    {
      activity_id: activitiesID[0].id,
      name: 'kayak for 1',
      inclusion: 'kayak for 1',
      eligibility: 'age 12 or above',
      description: chance.paragraph({ sentences: 4 }),
      capacity: 10
    },
    {
      activity_id: activitiesID[0].id,
      name: 'kayak for 2',
      inclusion: 'kayak for 2',
      eligibility: 'age 12 or above',
      description: chance.paragraph({ sentences: 4 }),
      capacity: 10
    },
    {
      activity_id: activitiesID[1].id,
      name: 'Surfing for 1',
      inclusion: 'Surfing for 1',
      eligibility: 'age 12 or above',
      description: "Hong Kong Surfing, Sai Kung Surfing",
      capacity: 10
    },
    {
      activity_id: activitiesID[1].id,
      name: 'Surfing for 2',
      inclusion: 'Surfing for 2',
      eligibility: 'age 12 or above',
      description: chance.paragraph({ sentences: 4 }),
      capacity: 10
    },
    {
      activity_id: activitiesID[2].id,
      name: 'Kiteboarding for 1',
      inclusion: 'Kiteboarding for 1',
      eligibility: 'age 12 or above',
      description: chance.paragraph({ sentences: 4 }),
      capacity: 10
    },
    {
      activity_id: activitiesID[2].id,
      name: 'Kiteboarding for 2',
      inclusion: 'Kiteboarding for 2',
      eligibility: 'age 12 or above',
      description: chance.paragraph({ sentences: 4 }),
      capacity: 10
    },
    {
      activity_id: activitiesID[3].id,
      name: 'Surfing for 1',
      inclusion: 'Surfing for 1',
      eligibility: 'age 12 or above',
      description: "Hong Kong Surfing, Sai Kung Surfing",
      capacity: 3
    },
    {
      activity_id: activitiesID[3].id,
      name: 'Surfing for 2',
      inclusion: 'Surfing for 2',
      eligibility: 'age 12 or above',
      description: chance.paragraph({ sentences: 4 }),
      capacity: 3
    },
    {
      activity_id: activitiesID[4].id,
      name: 'Kayak for 1',
      inclusion: 'Surfing for 1',
      eligibility: 'age 12 or above',
      description: chance.paragraph({ sentences: 4 }),
      capacity: 3
    },
    {
      activity_id: activitiesID[4].id,
      name: 'Kayak for 2',
      inclusion: 'Surfing for 2',
      eligibility: 'age 12 or above',
      description: chance.paragraph({ sentences: 4 }),
      capacity: 3
    },
    {
      activity_id: activitiesID[5].id,
      name: 'Angling',
      inclusion: 'Everything for angling',
      eligibility: 'age 12 or above',
      description: chance.paragraph({ sentences: 4 }),
      capacity: 3
    },

    {
      activity_id: activitiesID[6].id,
      name: 'Swimming for 2',
      inclusion: 'A coach',
      eligibility: 'age 12 or above',
      description: chance.paragraph({ sentences: 4 }),
      capacity: 3
    },

    {
      activity_id: activitiesID[7].id,
      name: 'Rafting (1-2)',
      inclusion: 'A coach',
      eligibility: 'age 12 or above',
      description: chance.paragraph({ sentences: 4 }),
      capacity: 5
    },

    {
      activity_id: activitiesID[7].id,
      name: 'Rafting (3-4)',
      inclusion: 'A coach',
      eligibility: 'age 12 or above',
      description: chance.paragraph({ sentences: 4 }),
      capacity: 5
    },
    {
      activity_id: activitiesID[8].id,
      name: 'SUP on Sun Moon Lake(3- 4)',
      inclusion: 'A coach',
      eligibility: 'age 12 or above',
      description: chance.paragraph({ sentences: 4 }),
      capacity: 16
    },
    {
      activity_id: activitiesID[8].id,
      name: 'SUP on Sun Moon Lake(1-2)',
      inclusion: 'A coach',
      eligibility: 'age 12 or above',
      description: chance.paragraph({ sentences: 4 }),
      capacity: 10
    },
    { activity_id: activitiesID[9].id, name: 'Hsiukuluan River Rafting(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 22 },
    { activity_id: activitiesID[9].id, name: 'Hsiukuluan River Rafting(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 2 },
    { activity_id: activitiesID[10].id, name: 'Qingshui Cliff Stand Up Paddle Boarding Experience in Hualien(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 14 },
    { activity_id: activitiesID[10].id, name: 'Qingshui Cliff Stand Up Paddle Boarding Experience in Hualien(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 14 },
    { activity_id: activitiesID[11].id, name: 'Mawuku River SUP Experience in Taitung(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 20 },
    { activity_id: activitiesID[11].id, name: 'Mawuku River SUP Experience in Taitung(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 21 },
    { activity_id: activitiesID[12].id, name: 'Surfing for 1', inclusion: 'A coach', eligibility: 'age 12 or above', description: "Hong Kong Surfing, Sai Kung Surfing" , capacity: 11 },
    { activity_id: activitiesID[12].id, name: 'Surfing for 2', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 19 },
    { activity_id: activitiesID[13].id, name: 'Penghu Underwater Mailbox Snorkeling Experience(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 21 },
    { activity_id: activitiesID[13].id, name: 'Penghu Underwater Mailbox Snorkeling Experience(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 16 },
    { activity_id: activitiesID[14].id, name: 'River Rafting Experience in Yilan(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 23 },
    { activity_id: activitiesID[14].id, name: 'River Rafting Experience in Yilan(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 27 },
    { activity_id: activitiesID[15].id, name: 'Discover Diving in Green Island(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 16 },
    { activity_id: activitiesID[15].id, name: 'Discover Diving in Green Island(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 27 },
    { activity_id: activitiesID[16].id, name: 'Scuba Dive with Green Turtles at Liuqiu Island(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 29 },
    { activity_id: activitiesID[16].id, name: 'Scuba Dive with Green Turtles at Liuqiu Island(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 25 },
    { activity_id: activitiesID[17].id, name: 'Kenting Shore Diving for Beginners(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 14 },
    { activity_id: activitiesID[17].id, name: 'Kenting Shore Diving for Beginners(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 18 },
    { activity_id: activitiesID[18].id, name: 'Sunset Semi-Submarine Experience in Lambai Island(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 19 },
    { activity_id: activitiesID[18].id, name: 'Sunset Semi-Submarine Experience in Lambai Island(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 6 },
    { activity_id: activitiesID[19].id, name: 'Nighttime Shrimp Picking with Dinner Experience in Hualien(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 2 },
    { activity_id: activitiesID[19].id, name: 'Nighttime Shrimp Picking with Dinner Experience in Hualien(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 26 },
    { activity_id: activitiesID[20].id, name: 'Stand Up Paddleboarding at Shuang River from Fulong Beach(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 2 },
    { activity_id: activitiesID[20].id, name: 'Stand Up Paddleboarding at Shuang River from Fulong Beach(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 7 },
    { activity_id: activitiesID[21].id, name: 'Discover Scuba Diving in Lanyu(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 12 },
    { activity_id: activitiesID[21].id, name: 'Discover Scuba Diving in Lanyu(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 14 },
    { activity_id: activitiesID[22].id, name: 'Kayaking and Snorkeling Experience at Liuqiu(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 2 },
    { activity_id: activitiesID[22].id, name: 'Kayaking and Snorkeling Experience at Liuqiu(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 21 },
    { activity_id: activitiesID[23].id, name: 'Fishing Experience in Kinmen(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 9 },
    { activity_id: activitiesID[23].id, name: 'Fishing Experience in Kinmen(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 20 },
    { activity_id: activitiesID[24].id, name: 'Golden Beach Water Sports(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 12 },
    { activity_id: activitiesID[24].id, name: 'Golden Beach Water Sports(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 15 },
    { activity_id: activitiesID[25].id, name: 'South Bay Watersports Package(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 13 },
    { activity_id: activitiesID[25].id, name: 'South Bay Watersports Package(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 11 },
    { activity_id: activitiesID[26].id, name: 'Xiao Liu Qiu Kayaking or Transparent Kayaking Experience in Liuqiu(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 4 },
    { activity_id: activitiesID[26].id, name: 'Xiao Liu Qiu Kayaking or Transparent Kayaking Experience in Liuqiu(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 13 },
    { activity_id: activitiesID[27].id, name: 'Waterskiing Experience(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 2 },
    { activity_id: activitiesID[27].id, name: 'Waterskiing Experience(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 27 },
    { activity_id: activitiesID[28].id, name: 'Kayaking Experience in Lambai Island(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 11 },
    { activity_id: activitiesID[28].id, name: 'Kayaking Experience in Lambai Island(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 11 },
    { activity_id: activitiesID[29].id, name: 'Penghu Sailing or Sunset Dinner Experience(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 22 },
    { activity_id: activitiesID[29].id, name: 'Penghu Sailing or Sunset Dinner Experience(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 26 },
    { activity_id: activitiesID[30].id, name: 'Surfing for 1', inclusion: 'A coach', eligibility: 'age 12 or above', description: "Hong Kong Surfing, Sai Kung Surfing" , capacity: 11 },
    { activity_id: activitiesID[30].id, name: 'Surfing for 2', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 19 },
    { activity_id: activitiesID[31].id, name: 'Nagannu Island Swimming Experience from Okinawa(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 22 },
    { activity_id: activitiesID[31].id, name: 'Nagannu Island Swimming Experience from Okinawa(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 15 },
    { activity_id: activitiesID[32].id, name: 'Onna Village Blue Cave Scuba and Snorkeling Experience(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 23 },
    { activity_id: activitiesID[32].id, name: 'Onna Village Blue Cave Scuba and Snorkeling Experience(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 7 },
    { activity_id: activitiesID[33].id, name: 'Onna Village Blue Cave Snorkel & Scuba + SUP & Parasailing Experience(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 7 },
    { activity_id: activitiesID[33].id, name: 'Onna Village Blue Cave Snorkel & Scuba + SUP & Parasailing Experience(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 26 },
    { activity_id: activitiesID[34].id, name: 'Half-Day Fishing Experience in Naha with Pick up & Drop off Service(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 14 },
    { activity_id: activitiesID[34].id, name: 'Half-Day Fishing Experience in Naha with Pick up & Drop off Service(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 13 },
    { activity_id: activitiesID[35].id, name: 'Onna Village Marine Walk(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 19 },
    { activity_id: activitiesID[35].id, name: 'Onna Village Marine Walk(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 29 },
    { activity_id: activitiesID[36].id, name: 'Night Mangrove Kayak Tour in Hijya River(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 24 },
    { activity_id: activitiesID[36].id, name: 'Night Mangrove Kayak Tour in Hijya River(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 10 },
    { activity_id: activitiesID[37].id, name: 'Stalactite Cave Exploring Experience with SUP or Canoeing in Miyako Islan(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 19 },
    { activity_id: activitiesID[37].id, name: 'Stalactite Cave Exploring Experience with SUP or Canoeing in Miyako Islan(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 3 },
    { activity_id: activitiesID[38].id, name: 'Natural Beach and Snorkeling Experience in Okinawa(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 26 },
    { activity_id: activitiesID[38].id, name: 'Natural Beach and Snorkeling Experience in Okinawa(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 8 },
    { activity_id: activitiesID[39].id, name: 'SUP or Canoe at Mangrove Forest(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 23 },
    { activity_id: activitiesID[39].id, name: 'SUP or Canoe at Mangrove Forest(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 9 },
    { activity_id: activitiesID[39].id, name: 'categoryId(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 7 },
    { activity_id: activitiesID[39].id, name: 'categoryId(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 18 },
    { activity_id: activitiesID[40].id, name: 'The Secrets of a Fish Farm with Seafood Bucket(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 3 },
    { activity_id: activitiesID[40].id, name: 'The Secrets of a Fish Farm with Seafood Bucket(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 2 },
    { activity_id: activitiesID[41].id, name: '12 Hours Private Fishing Experience with Room at Hulu Langat Fishing Resort(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 7 },
    { activity_id: activitiesID[41].id, name: '12 Hours Private Fishing Experience with Room at Hulu Langat Fishing Resort(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 11 },
    { activity_id: activitiesID[42].id, name: 'Bo Sang Fishing Park(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 29 },
    { activity_id: activitiesID[42].id, name: 'Bo Sang Fishing Park(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 20 },
    { activity_id: activitiesID[43].id, name: 'Fishing Charter on Lake Mapourika(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 19 },
    { activity_id: activitiesID[43].id, name: 'Fishing Charter on Lake Mapourika(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 7 },
    { activity_id: activitiesID[44].id, name: 'Traditional Fishing Trip in Galle(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 23 },
    { activity_id: activitiesID[44].id, name: 'Traditional Fishing Trip in Galle(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 21 },
    { activity_id: activitiesID[45].id, name: 'Fishing Experience at Temenggor Lake in Perak(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 7 },
    { activity_id: activitiesID[45].id, name: 'Fishing Experience at Temenggor Lake in Perak(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 27 },
    { activity_id: activitiesID[46].id, name: 'Big Game Fishing Adventure Chiang Mai(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 15 },
    { activity_id: activitiesID[46].id, name: 'Big Game Fishing Adventure Chiang Mai(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 13 },
    { activity_id: activitiesID[47].id, name: 'Kelongs, Fish Farming and Pulau Ubin Adventure(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 22 },
    { activity_id: activitiesID[47].id, name: 'Kelongs, Fish Farming and Pulau Ubin Adventure(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 5 },
    { activity_id: activitiesID[48].id, name: 'Ratee Petra Fishing Experience from Hua Hin(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 6 },
    { activity_id: activitiesID[48].id, name: 'Ratee Petra Fishing Experience from Hua Hin(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 9 },
    { activity_id: activitiesID[49].id, name: 'Reef Fishing from Broome(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 7 },
    { activity_id: activitiesID[49].id, name: 'Reef Fishing from Broome(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 26 },
    { activity_id: activitiesID[50].id, name: 'Pink Boat Snorkeling and Fishing Experience at Gaya Island(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 16 },
    { activity_id: activitiesID[50].id, name: 'Pink Boat Snorkeling and Fishing Experience at Gaya Island(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 22 },
    { activity_id: activitiesID[51].id, name: 'Fat Fish in Johor Bahru(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 25 },
    { activity_id: activitiesID[51].id, name: 'Fat Fish in Johor Bahru(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 14 },
    { activity_id: activitiesID[52].id, name: 'Chase the Toman Fishing Experience in Royal Belum State Park(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 21 },
    { activity_id: activitiesID[52].id, name: 'Chase the Toman Fishing Experience in Royal Belum State Park(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 2 },
    { activity_id: activitiesID[53].id, name: 'Private Fishing by Siam Sea Cret Cruises in Hua Hin(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 5 },
    { activity_id: activitiesID[53].id, name: 'Private Fishing by Siam Sea Cret Cruises in Hua Hin(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 22 },
    { activity_id: activitiesID[54].id, name: 'Agriculture and Fish Farming Sustainability Tour at Jalan Lekar(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 11 },
    { activity_id: activitiesID[54].id, name: 'Agriculture and Fish Farming Sustainability Tour at Jalan Lekar(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 26 },
    { activity_id: activitiesID[55].id, name: 'Kualoa Ranch Ocean Voyage and Fish Pond Experience(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 22 },
    { activity_id: activitiesID[55].id, name: 'Kualoa Ranch Ocean Voyage and Fish Pond Experience(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 5 },
    { activity_id: activitiesID[56].id, name: 'Boat Fishing Tour from Ishigaki Island(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 18 },
    { activity_id: activitiesID[56].id, name: 'Boat Fishing Tour from Ishigaki Island(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 13 },
    { activity_id: activitiesID[57].id, name: 'Night Squid Fishing and Sunset Private Day Tour by Private Longtail Boat from Krabi(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 9 },
    { activity_id: activitiesID[57].id, name: 'Night Squid Fishing and Sunset Private Day Tour by Private Longtail Boat from Krabi(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 11 },
    { activity_id: activitiesID[58].id, name: 'Fishing & Rice Paddy Fields Tour from Hoi An(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 16 },
    { activity_id: activitiesID[58].id, name: 'Fishing & Rice Paddy Fields Tour from Hoi An(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 27 },
    { activity_id: activitiesID[59].id, name: 'Phu Quoc Sunset and Squid Fishing Tour at North of Phu Quoc Island(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 15 },
    { activity_id: activitiesID[59].id, name: 'Phu Quoc Sunset and Squid Fishing Tour at North of Phu Quoc Island(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 8 },
    { activity_id: activitiesID[60].id, name: 'Banana Boat Ride in Boracay(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 23 },
    { activity_id: activitiesID[60].id, name: 'Banana Boat Ride in Boracay(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 11 },
    { activity_id: activitiesID[61].id, name: 'Bali Banana Boat Water Sport Packages(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 5 },
    { activity_id: activitiesID[61].id, name: 'Bali Banana Boat Water Sport Packages(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 5 },
    { activity_id: activitiesID[62].id, name: 'South Bay Watersports Package(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 24 },
    { activity_id: activitiesID[62].id, name: 'South Bay Watersports Package(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 29 },
    { activity_id: activitiesID[63].id, name: 'Banana Boat Ride and Clear Kayak Experience in Coron(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 21 },
    { activity_id: activitiesID[63].id, name: 'Banana Boat Ride and Clear Kayak Experience in Coron(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 1 },
    { activity_id: activitiesID[64].id, name: 'Blue Cave and Banana Boat Combo Activity(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 1 },
    { activity_id: activitiesID[64].id, name: 'Blue Cave and Banana Boat Combo Activity(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 27 },
    { activity_id: activitiesID[65].id, name: 'Maiton Island Speed Boat by Love Andaman(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 12 },
    { activity_id: activitiesID[65].id, name: 'Maiton Island Speed Boat by Love Andaman(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 2 },
    { activity_id: activitiesID[66].id, name: 'Banana Boat or Jet Ski Experience in Nipah Beach Lombok(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 16 },
    { activity_id: activitiesID[66].id, name: 'Banana Boat or Jet Ski Experience in Nipah Beach Lombok(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 10 },
    { activity_id: activitiesID[67].id, name: 'Speedboat to Koh Larn and Water Activity with Manta Marina(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 19 },
    { activity_id: activitiesID[67].id, name: 'Speedboat to Koh Larn and Water Activity with Manta Marina(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 7 },
    { activity_id: activitiesID[68].id, name: 'Water Sports at Toya Devasya Natural Hot Spring(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 18 },
    { activity_id: activitiesID[68].id, name: 'Water Sports at Toya Devasya Natural Hot Spring(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 1 },
    { activity_id: activitiesID[69].id, name: 'Water Activities at Cenang Beach in Langkawi(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 7 },
    { activity_id: activitiesID[69].id, name: 'Water Activities at Cenang Beach in Langkawi(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 9 },
    { activity_id: activitiesID[70].id, name: 'Pattaya to Koh Larn Half Day Tour(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 11 },
    { activity_id: activitiesID[70].id, name: 'Pattaya to Koh Larn Half Day Tour(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 16 },
    { activity_id: activitiesID[71].id, name: 'Water Activities in Desaru(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 10 },
    { activity_id: activitiesID[71].id, name: 'Water Activities in Desaru(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 10 },
    { activity_id: activitiesID[72].id, name: 'Nusa Dua Water Sport Adventure(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 19 },
    { activity_id: activitiesID[72].id, name: 'Nusa Dua Water Sport Adventure(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 22 },
    { activity_id: activitiesID[73].id, name: 'Tanjung Benoa Watersports Experience in Bali(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 10 },
    { activity_id: activitiesID[73].id, name: 'Tanjung Benoa Watersports Experience in Bali(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 11 },
    { activity_id: activitiesID[74].id, name: 'Nong Nooch Tropical Garden and Koh Larn Day Tour from Pattaya(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 6 },
    { activity_id: activitiesID[74].id, name: 'Nong Nooch Tropical Garden and Koh Larn Day Tour from Pattaya(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 19 },
    { activity_id: activitiesID[75].id, name: 'Sapi and Manukan Islands Tour with Water Sport Activities(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 5 },
    { activity_id: activitiesID[75].id, name: 'Sapi and Manukan Islands Tour with Water Sport Activities(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 19 },
    { activity_id: activitiesID[76].id, name: 'Bali Underwater Scooter(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 8 },
    { activity_id: activitiesID[76].id, name: 'Bali Underwater Scooter(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 23 },
    { activity_id: activitiesID[77].id, name: 'Tanjung Benoa Water Activities in Bali(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 15 },
    { activity_id: activitiesID[77].id, name: 'Tanjung Benoa Water Activities in Bali(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 14 },
    { activity_id: activitiesID[78].id, name: 'Tanjung Benoa Watersports in Bali by PMA(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 18 },
    { activity_id: activitiesID[78].id, name: 'Tanjung Benoa Watersports in Bali by PMA(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 10 },
    { activity_id: activitiesID[79].id, name: 'Koh Hey Banana Beach Speedboat or Catamaran Day Tour(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 10 },
    { activity_id: activitiesID[79].id, name: 'Koh Hey Banana Beach Speedboat or Catamaran Day Tour(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 5 },
    { activity_id: activitiesID[80].id, name: '5-in-1 Water Sports Package in Baga Beach(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 13 },
    { activity_id: activitiesID[80].id, name: '5-in-1 Water Sports Package in Baga Beach(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 10 },
    { activity_id: activitiesID[81].id, name: 'Yambuna Safari Self-Guided Canoeing Experience(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 11 },
    { activity_id: activitiesID[81].id, name: 'Yambuna Safari Self-Guided Canoeing Experience(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 6 },
    { activity_id: activitiesID[82].id, name: 'Noosa Everglades Eco Safari Cruise and Canoe(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 10 },
    { activity_id: activitiesID[82].id, name: 'Noosa Everglades Eco Safari Cruise and Canoe(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 10 },
    { activity_id: activitiesID[83].id, name: 'BIG Kanu Glow Worm Tour in Tauranga(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 28 },
    { activity_id: activitiesID[83].id, name: 'BIG Kanu Glow Worm Tour in Tauranga(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 2 },
    { activity_id: activitiesID[84].id, name: 'Kitulgala Adventure Day Tour from Kandy(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 18 },
    { activity_id: activitiesID[84].id, name: 'Kitulgala Adventure Day Tour from Kandy(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 23 },
    { activity_id: activitiesID[85].id, name: 'Kangaroo Valley Canoes, Cool Climate Wines and Canapes Tour(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 3 },
    { activity_id: activitiesID[85].id, name: 'Kangaroo Valley Canoes, Cool Climate Wines and Canapes Tour(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 13 },
    { activity_id: activitiesID[86].id, name: 'Ishigaki Island Private Activity(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 20 },
    { activity_id: activitiesID[86].id, name: 'Ishigaki Island Private Activity(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 28 },
    { activity_id: activitiesID[87].id, name: 'Iriomote Island Snorkeling at Coral Island��Magrove activity(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 28 },
    { activity_id: activitiesID[87].id, name: 'Iriomote Island Snorkeling at Coral Island��Magrove activity(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 26 },
    { activity_id: activitiesID[88].id, name: 'Kitulgala Adventure Day Tour From Colombo(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 4 },
    { activity_id: activitiesID[88].id, name: 'Kitulgala Adventure Day Tour From Colombo(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 11 },
    { activity_id: activitiesID[89].id, name: 'Khao Sok Discovery Day Tour from Phuket(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 7 },
    { activity_id: activitiesID[89].id, name: 'Khao Sok Discovery Day Tour from Phuket(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 27 },
    { activity_id: activitiesID[90].id, name: 'Three Water Sports Challenge in Lambai Island(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 18 },
    { activity_id: activitiesID[90].id, name: 'Three Water Sports Challenge in Lambai Island(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 11 },
    { activity_id: activitiesID[91].id, name: 'Classic Bangkok Full Day Tour by AK Travel(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 17 },
    { activity_id: activitiesID[91].id, name: 'Classic Bangkok Full Day Tour by AK Travel(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 19 },
    { activity_id: activitiesID[92].id, name: 'Luxury Yacht Experience at Lambai Island(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 18 },
    { activity_id: activitiesID[92].id, name: 'Luxury Yacht Experience at Lambai Island(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 12 },
    { activity_id: activitiesID[93].id, name: 'Sea Horizon Resort Glamping Suite with Breakfast Staycation in Johor(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 28 },
    { activity_id: activitiesID[93].id, name: 'Sea Horizon Resort Glamping Suite with Breakfast Staycation in Johor(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 22 },
    { activity_id: activitiesID[94].id, name: 'Ikenotaira Familyland 1Day Free Pass in Nagano(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 9 },
    { activity_id: activitiesID[94].id, name: 'Ikenotaira Familyland 1Day Free Pass in Nagano(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 10 },
    { activity_id: activitiesID[95].id, name: 'Koh Rin, Koh Ped, and Koh Kham Islands Day Tour from Pattaya by Luxury Yacht(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 11 },
    { activity_id: activitiesID[95].id, name: 'Koh Rin, Koh Ped, and Koh Kham Islands Day Tour from Pattaya by Luxury Yacht(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 25 },
    { activity_id: activitiesID[96].id, name: 'Nha Trang Island Hopping Private Day Tour(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 18 },
    { activity_id: activitiesID[96].id, name: 'Nha Trang Island Hopping Private Day Tour(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 18 },
    { activity_id: activitiesID[97].id, name: 'Margaret River Wine & Scenic Discovery Tour(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 16 },
    { activity_id: activitiesID[97].id, name: 'Margaret River Wine & Scenic Discovery Tour(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 28 },
    { activity_id: activitiesID[98].id, name: 'Kualoa Ranch Secret Island Beach Experience(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 26 },
    { activity_id: activitiesID[98].id, name: 'Kualoa Ranch Secret Island Beach Experience(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 23 },
    { activity_id: activitiesID[99].id, name: '3D2N Gili Trawangan Honeymoon in Lombok(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 25 },
    { activity_id: activitiesID[99].id, name: '3D2N Gili Trawangan Honeymoon in Lombok(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 28 },
    { activity_id: activitiesID[100].id, name: 'Essential Sea Kayaking Skills Course(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 10 },
    { activity_id: activitiesID[100].id, name: 'Essential Sea Kayaking Skills Course(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 11 },
    { activity_id: activitiesID[101].id, name: 'Jetpack/Jetlev Flyer Experience in Goa(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 21 },
    { activity_id: activitiesID[101].id, name: 'Jetpack/Jetlev Flyer Experience in Goa(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 19 },
    { activity_id: activitiesID[102].id, name: 'Flyboarding Experience at Marina Putrajaya(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 25 },
    { activity_id: activitiesID[102].id, name: 'Flyboarding Experience at Marina Putrajaya(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 10 },
    { activity_id: activitiesID[103].id, name: 'Okinawa Exciting Flyboard Experience(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 25 },
    { activity_id: activitiesID[103].id, name: 'Okinawa Exciting Flyboard Experience(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 20 },
    { activity_id: activitiesID[104].id, name: 'Flyboard Extreme in Gold Coast(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 11 },
    { activity_id: activitiesID[104].id, name: 'Flyboard Extreme in Gold Coast(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 8 },
    { activity_id: activitiesID[105].id, name: 'Gold Coast Water Sports Combo(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 17 },
    { activity_id: activitiesID[105].id, name: 'Gold Coast Water Sports Combo(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 17 },
    { activity_id: activitiesID[106].id, name: 'Boracay Jet Ski Experience(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 12 },
    { activity_id: activitiesID[106].id, name: 'Boracay Jet Ski Experience(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 19 },
    { activity_id: activitiesID[107].id, name: 'Beginner Jet Ski Fun Time Experience in Langkawi(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 3 },
    { activity_id: activitiesID[107].id, name: 'Beginner Jet Ski Fun Time Experience in Langkawi(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 15 },
    { activity_id: activitiesID[108].id, name: 'Jet Ski Experience in Dubai(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 9 },
    { activity_id: activitiesID[108].id, name: 'Jet Ski Experience in Dubai(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 10 },
    { activity_id: activitiesID[109].id, name: 'Jet Ski Experience at Desaru Coast(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 1 },
    { activity_id: activitiesID[109].id, name: 'Jet Ski Experience at Desaru Coast(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 21 },
    { activity_id: activitiesID[110].id, name: 'Jet Ski Experience in Coron(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 2 },
    { activity_id: activitiesID[110].id, name: 'Jet Ski Experience in Coron(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 20 },
    { activity_id: activitiesID[111].id, name: 'Jet Ski Safari in Airlie Beach(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 2 },
    { activity_id: activitiesID[111].id, name: 'Jet Ski Safari in Airlie Beach(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 28 },
    { activity_id: activitiesID[112].id, name: 'Jet Ski Experience at Airlie Beach in the Whitsundays(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 11 },
    { activity_id: activitiesID[112].id, name: 'Jet Ski Experience at Airlie Beach in the Whitsundays(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 28 },
    { activity_id: activitiesID[113].id, name: 'Jet Boating and Jet Skiing Experience in Gold Coast(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 4 },
    { activity_id: activitiesID[113].id, name: 'Jet Boating and Jet Skiing Experience in Gold Coast(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 27 },
    { activity_id: activitiesID[114].id, name: 'Jet Ski on the Hawkesbury River Sydney(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 19 },
    { activity_id: activitiesID[114].id, name: 'Jet Ski on the Hawkesbury River Sydney(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 20 },
    { activity_id: activitiesID[115].id, name: 'Pulau Dayang Bunting and Langkawi Islands Jet Ski Tour by Mega Water Sports(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 6 },
    { activity_id: activitiesID[115].id, name: 'Pulau Dayang Bunting and Langkawi Islands Jet Ski Tour by Mega Water Sports(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 17 },
    { activity_id: activitiesID[116].id, name: 'Fun Jet Ski and Water Sports Activities at Tanjung Benoa Beach Bali(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 23 },
    { activity_id: activitiesID[116].id, name: 'Fun Jet Ski and Water Sports Activities at Tanjung Benoa Beach Bali(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 7 },
    { activity_id: activitiesID[117].id, name: 'Jet Ski Island Safari in Gold Coast(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 21 },
    { activity_id: activitiesID[117].id, name: 'Jet Ski Island Safari in Gold Coast(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 5 },
    { activity_id: activitiesID[118].id, name: 'Jet Ski Experience in Bentota(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 10 },
    { activity_id: activitiesID[118].id, name: 'Jet Ski Experience in Bentota(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 13 },
    { activity_id: activitiesID[119].id, name: 'Gold Coast Jet Ski Safari Experience(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 29 },
    { activity_id: activitiesID[119].id, name: 'Gold Coast Jet Ski Safari Experience(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 11 },
    { activity_id: activitiesID[120].id, name: 'Jet Ski Fun Island Hopping Tour in Langkawi by Mega Water Sports(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 29 },
    { activity_id: activitiesID[120].id, name: 'Jet Ski Fun Island Hopping Tour in Langkawi by Mega Water Sports(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 17 },
    { activity_id: activitiesID[121].id, name: 'Magical Islands Jet Ski Tour with Paradise 101 in Langkawi(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 1 },
    { activity_id: activitiesID[121].id, name: 'Magical Islands Jet Ski Tour with Paradise 101 in Langkawi(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 22 },
    { activity_id: activitiesID[122].id, name: 'Jet Ski Thrilling Experience with Paradise 101 in Langkawi(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 14 },
    { activity_id: activitiesID[122].id, name: 'Jet Ski Thrilling Experience with Paradise 101 in Langkawi(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 15 },
    { activity_id: activitiesID[123].id, name: 'Langkawi Island Hopping Jet Ski Tour(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 4 },
    { activity_id: activitiesID[123].id, name: 'Langkawi Island Hopping Jet Ski Tour(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 1 },
    { activity_id: activitiesID[124].id, name: 'Jet Ski and Parasailing Experience at Batu Feringghi Beach in Penang(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 12 },
    { activity_id: activitiesID[124].id, name: 'Jet Ski and Parasailing Experience at Batu Feringghi Beach in Penang(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 9 },
    { activity_id: activitiesID[125].id, name: 'Water Sports Package at Tanjung Benoa(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 15 },
    { activity_id: activitiesID[125].id, name: 'Water Sports Package at Tanjung Benoa(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 11 },
    { activity_id: activitiesID[126].id, name: 'Gold Coast Jet Boat and Jet Ski Safari Experience(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 15 },
    { activity_id: activitiesID[126].id, name: 'Gold Coast Jet Boat and Jet Ski Safari Experience(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 25 },
    { activity_id: activitiesID[127].id, name: 'Tai O Sea Kayaking(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 7 },
    { activity_id: activitiesID[127].id, name: 'Tai O Sea Kayaking(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 29 },
    { activity_id: activitiesID[128].id, name: 'Night Paddle at Cheung Chau Island(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 6 },
    { activity_id: activitiesID[128].id, name: 'Night Paddle at Cheung Chau Island(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 11 },
    { activity_id: activitiesID[129].id, name: 'Nature Kayaking(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 5 },
    { activity_id: activitiesID[129].id, name: 'Nature Kayaking(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 9 },
    { activity_id: activitiesID[130].id, name: 'Kayaking Mangrove Exploration Tour(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 11 },
    { activity_id: activitiesID[130].id, name: 'Kayaking Mangrove Exploration Tour(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 3 },
    { activity_id: activitiesID[131].id, name: 'Kayak, Stand Up Paddleboard, Banana Boat and Donut at Ola Beach Club(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 2 },
    { activity_id: activitiesID[131].id, name: 'Kayak, Stand Up Paddleboard, Banana Boat and Donut at Ola Beach Club(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 28 },
    { activity_id: activitiesID[132].id, name: 'Enjoy a Northern Kayaking Tour(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 25 },
    { activity_id: activitiesID[132].id, name: 'Enjoy a Northern Kayaking Tour(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 5 },
    { activity_id: activitiesID[133].id, name: 'Taupo Kayaking Adventure(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 24 },
    { activity_id: activitiesID[133].id, name: 'Taupo Kayaking Adventure(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 14 },
    { activity_id: activitiesID[134].id, name: 'Kayaking at Fulong Beach(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 15 },
    { activity_id: activitiesID[134].id, name: 'Kayaking at Fulong Beach(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 18 },
    { activity_id: activitiesID[135].id, name: 'Kayaking Coastal Beach Clean Up Tour(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 9 },
    { activity_id: activitiesID[135].id, name: 'Kayaking Coastal Beach Clean Up Tour(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 22 },
    { activity_id: activitiesID[136].id, name: 'Kayaking Experience in Goa(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 27 },
    { activity_id: activitiesID[136].id, name: 'Kayaking Experience in Goa(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 23 },
    { activity_id: activitiesID[137].id, name: 'Kayaking Experience in Mandwa(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 3 },
    { activity_id: activitiesID[137].id, name: 'Kayaking Experience in Mandwa(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 2 },
    { activity_id: activitiesID[138].id, name: 'Guided Kayaking Tour in Brisbane(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 7 },
    { activity_id: activitiesID[138].id, name: 'Guided Kayaking Tour in Brisbane(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 4 },
    { activity_id: activitiesID[139].id, name: 'Seal Kayaking Adventure in Kaikoura(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 17 },
    { activity_id: activitiesID[139].id, name: 'Seal Kayaking Adventure in Kaikoura(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 2 },
    { activity_id: activitiesID[140].id, name: 'Rainforest Kayaking Adventure in Sarawak(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 19 },
    { activity_id: activitiesID[140].id, name: 'Rainforest Kayaking Adventure in Sarawak(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 22 },
    { activity_id: activitiesID[141].id, name: 'Kiteboarding experience at Hua Hin by KBA - KiteBoarding Asia(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 1 },
    { activity_id: activitiesID[141].id, name: 'Kiteboarding experience at Hua Hin by KBA - KiteBoarding Asia(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 29 },
    { activity_id: activitiesID[142].id, name: 'Boracay Parasailing(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 8 },
    { activity_id: activitiesID[142].id, name: 'Boracay Parasailing(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 28 },
    { activity_id: activitiesID[143].id, name: 'Parasailing Experience from Naha or North of Okinawa(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 11 },
    { activity_id: activitiesID[143].id, name: 'Parasailing Experience from Naha or North of Okinawa(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 10 },
    { activity_id: activitiesID[144].id, name: 'Parasailing in Gold Coast(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 3 },
    { activity_id: activitiesID[144].id, name: 'Parasailing in Gold Coast(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 29 },
    { activity_id: activitiesID[145].id, name: 'Bali Parasailing Adventure(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 9 },
    { activity_id: activitiesID[145].id, name: 'Bali Parasailing Adventure(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 25 },
    { activity_id: activitiesID[146].id, name: 'Parasailing Experience in Okinawa (Nago Departure)(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 14 },
    { activity_id: activitiesID[146].id, name: 'Parasailing Experience in Okinawa (Nago Departure)(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 13 },
    { activity_id: activitiesID[147].id, name: 'Parasailing and Snorkeling Experience in Sesoko Island(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 6 },
    { activity_id: activitiesID[147].id, name: 'Parasailing and Snorkeling Experience in Sesoko Island(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 12 },
    { activity_id: activitiesID[148].id, name: 'Parasailing Experience in Coron(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 6 },
    { activity_id: activitiesID[148].id, name: 'Parasailing Experience in Coron(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 2 },
    { activity_id: activitiesID[149].id, name: 'Gold Coast Parasailing Experience(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 14 },
    { activity_id: activitiesID[149].id, name: 'Gold Coast Parasailing Experience(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 22 },
    { activity_id: activitiesID[150].id, name: 'Onna Village Blue Cave Snorkel & Scuba + SUP & Parasailing Experience (Hotel Pick Up)(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 24 },
    { activity_id: activitiesID[150].id, name: 'Onna Village Blue Cave Snorkel & Scuba + SUP & Parasailing Experience (Hotel Pick Up)(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 29 },
    { activity_id: activitiesID[151].id, name: 'Chibishi Island Snorkeling and Optional Parasailing Experience from Naha(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 14 },
    { activity_id: activitiesID[151].id, name: 'Chibishi Island Snorkeling and Optional Parasailing Experience from Naha(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 9 },
    { activity_id: activitiesID[152].id, name: 'Active 3 Marine Sports & Parasailing Set Experience in Okinawa(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 3 },
    { activity_id: activitiesID[152].id, name: 'Active 3 Marine Sports & Parasailing Set Experience in Okinawa(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 18 },
    { activity_id: activitiesID[153].id, name: 'Parasailing and Snorkeling Tour in Okinawa Depart From Nago(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 14 },
    { activity_id: activitiesID[153].id, name: 'Parasailing and Snorkeling Tour in Okinawa Depart From Nago(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 21 },
    { activity_id: activitiesID[154].id, name: 'Jet Boating and Parasailing Experience in Gold Coast(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 16 },
    { activity_id: activitiesID[154].id, name: 'Jet Boating and Parasailing Experience in Gold Coast(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 16 },
    { activity_id: activitiesID[155].id, name: 'Koh Larn Island (Coral Island) Day Tour from Bangkok with Snorkeling and Water Sliding Experience(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 10 },
    { activity_id: activitiesID[155].id, name: 'Koh Larn Island (Coral Island) Day Tour from Bangkok with Snorkeling and Water Sliding Experience(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 8 },
    { activity_id: activitiesID[156].id, name: 'Boracay Helmet Dive Experience(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 20 },
    { activity_id: activitiesID[156].id, name: 'Boracay Helmet Dive Experience(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 28 },
    { activity_id: activitiesID[157].id, name: 'Boracay Tour Package(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 15 },
    { activity_id: activitiesID[157].id, name: 'Boracay Tour Package(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 10 },
    { activity_id: activitiesID[158].id, name: 'River Rafting Experience in Yilan(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 25 },
    { activity_id: activitiesID[158].id, name: 'River Rafting Experience in Yilan(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 25 },
    { activity_id: activitiesID[159].id, name: 'Ayung White Water Rafting(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 7 },
    { activity_id: activitiesID[159].id, name: 'Ayung White Water Rafting(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 8 },
    { activity_id: activitiesID[160].id, name: 'Trisuli River Rafting(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 12 },
    { activity_id: activitiesID[160].id, name: 'Trisuli River Rafting(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 7 },
    { activity_id: activitiesID[161].id, name: 'Rafting in Hanmer Springs(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 26 },
    { activity_id: activitiesID[161].id, name: 'Rafting in Hanmer Springs(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 6 },
    { activity_id: activitiesID[162].id, name: 'White Water Rafting Adventure in Kuala Kubu Bharu(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 29 },
    { activity_id: activitiesID[162].id, name: 'White Water Rafting Adventure in Kuala Kubu Bharu(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 19 },
    { activity_id: activitiesID[163].id, name: 'White Water Rafting in Kolad(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 20 },
    { activity_id: activitiesID[163].id, name: 'White Water Rafting in Kolad(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 25 },
    { activity_id: activitiesID[164].id, name: 'Upper Seti River Rafting(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 18 },
    { activity_id: activitiesID[164].id, name: 'Upper Seti River Rafting(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 6 },
    { activity_id: activitiesID[165].id, name: 'Ruakuri Black Water Rafting - Black Labyrinth(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 1 },
    { activity_id: activitiesID[165].id, name: 'Ruakuri Black Water Rafting - Black Labyrinth(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 5 },
    { activity_id: activitiesID[166].id, name: 'Kampar White Water Rafting Adventure(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 8 },
    { activity_id: activitiesID[166].id, name: 'Kampar White Water Rafting Adventure(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 13 },
    { activity_id: activitiesID[167].id, name: 'River Rafting Experience near Manali(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 28 },
    { activity_id: activitiesID[167].id, name: 'River Rafting Experience near Manali(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 7 },
    { activity_id: activitiesID[168].id, name: 'White Water Rafting with Caving Experience in Gopeng(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 20 },
    { activity_id: activitiesID[168].id, name: 'White Water Rafting with Caving Experience in Gopeng(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 25 },
    { activity_id: activitiesID[169].id, name: 'White Water Rafting Experience in Sungkai River(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 28 },
    { activity_id: activitiesID[169].id, name: 'White Water Rafting Experience in Sungkai River(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 26 },
    { activity_id: activitiesID[170].id, name: 'Rafting Experience in Lombok(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 24 },
    { activity_id: activitiesID[170].id, name: 'Rafting Experience in Lombok(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 7 },
    { activity_id: activitiesID[171].id, name: 'Kaituna River Rafting(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 8 },
    { activity_id: activitiesID[171].id, name: 'Kaituna River Rafting(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 10 },
    { activity_id: activitiesID[172].id, name: 'Telaga Waja White Water Rafting in Bali by BTR(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 15 },
    { activity_id: activitiesID[172].id, name: 'Telaga Waja White Water Rafting in Bali by BTR(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 8 },
    { activity_id: activitiesID[173].id, name: 'Balis Best Rafting and Spa Combo Experience(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 10 },
    { activity_id: activitiesID[173].id, name: 'Balis Best Rafting and Spa Combo Experience(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 22 },
    { activity_id: activitiesID[174].id, name: 'Ruakuri Cave Black Water Rafting - Black Abyss(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 20 },
    { activity_id: activitiesID[174].id, name: 'Ruakuri Cave Black Water Rafting - Black Abyss(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 20 },
    { activity_id: activitiesID[175].id, name: 'Ayung River Rafting and Cycling Combo Experience(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 22 },
    { activity_id: activitiesID[175].id, name: 'Ayung River Rafting and Cycling Combo Experience(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 29 },
    { activity_id: activitiesID[176].id, name: 'White Water Rafting Adventure in Slim River(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 29 },
    { activity_id: activitiesID[176].id, name: 'White Water Rafting Adventure in Slim River(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 13 },
    { activity_id: activitiesID[177].id, name: 'Half-Day Barron River Rafting Experience from Cairns or Port Douglas(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 25 },
    { activity_id: activitiesID[177].id, name: 'Half-Day Barron River Rafting Experience from Cairns or Port Douglas(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 22 },
    { activity_id: activitiesID[178].id, name: 'White Water Rafting Experience in Kuala Kubu Bharu(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 14 },
    { activity_id: activitiesID[178].id, name: 'White Water Rafting Experience in Kuala Kubu Bharu(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 9 },
    { activity_id: activitiesID[179].id, name: 'Sai Kung Beach Kayaking and Snorkeling in Hong Kong(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 6 },
    { activity_id: activitiesID[179].id, name: 'Sai Kung Beach Kayaking and Snorkeling in Hong Kong(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 8 },
    { activity_id: activitiesID[180].id, name: 'Island Snorkeling(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 28 },
    { activity_id: activitiesID[180].id, name: 'Island Snorkeling(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 28 },
    { activity_id: activitiesID[181].id, name: 'Onna Village Blue Cave Scuba and Snorkeling Experience(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 15 },
    { activity_id: activitiesID[181].id, name: 'Onna Village Blue Cave Scuba and Snorkeling Experience(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 11 },
    { activity_id: activitiesID[182].id, name: 'Pulau Kentut Private Snorkeling Experience in Langkawi(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 7 },
    { activity_id: activitiesID[182].id, name: 'Pulau Kentut Private Snorkeling Experience in Langkawi(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 12 },
    { activity_id: activitiesID[183].id, name: 'Snorkeling Experience in Nusa Penida(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 27 },
    { activity_id: activitiesID[183].id, name: 'Snorkeling Experience in Nusa Penida(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 19 },
    { activity_id: activitiesID[184].id, name: 'Nusa Lembongan and Manta Bay Snorkeling Experience(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 14 },
    { activity_id: activitiesID[184].id, name: 'Nusa Lembongan and Manta Bay Snorkeling Experience(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 8 },
    { activity_id: activitiesID[185].id, name: 'Snorkeling Experience and Island Exploration in the Southern Island(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 27 },
    { activity_id: activitiesID[185].id, name: 'Snorkeling Experience and Island Exploration in the Southern Island(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 18 },
    { activity_id: activitiesID[186].id, name: 'Snorkeling with Sea Turtles at Mirissa(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 1 },
    { activity_id: activitiesID[186].id, name: 'Snorkeling with Sea Turtles at Mirissa(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 4 },
    { activity_id: activitiesID[187].id, name: 'Berry Snorkeling Experience in Nago City(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 18 },
    { activity_id: activitiesID[187].id, name: 'Berry Snorkeling Experience in Nago City(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 1 },
    { activity_id: activitiesID[188].id, name: 'Coral Snorkeling in Miyako Island(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 10 },
    { activity_id: activitiesID[188].id, name: 'Coral Snorkeling in Miyako Island(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 17 },
    { activity_id: activitiesID[189].id, name: 'Semporna Island Snorkeling Experience(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 1 },
    { activity_id: activitiesID[189].id, name: 'Semporna Island Snorkeling Experience(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 14 },
    { activity_id: activitiesID[190].id, name: 'Snorkel Experience in Whitsundays(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 5 },
    { activity_id: activitiesID[190].id, name: 'Snorkel Experience in Whitsundays(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 14 },
    { activity_id: activitiesID[191].id, name: 'Snorkeling and Diving Experience on Ishigaki Island(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 7 },
    { activity_id: activitiesID[191].id, name: 'Snorkeling and Diving Experience on Ishigaki Island(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 10 },
    { activity_id: activitiesID[192].id, name: 'Coral Island Snorkelling Experience in Trincomalee(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 1 },
    { activity_id: activitiesID[192].id, name: 'Coral Island Snorkelling Experience in Trincomalee(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 18 },
    { activity_id: activitiesID[193].id, name: 'Half Day Wavebreak Island Snorkeling(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 1 },
    { activity_id: activitiesID[193].id, name: 'Half Day Wavebreak Island Snorkeling(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 23 },
    { activity_id: activitiesID[194].id, name: 'Sea Turtle Snorkeling in Miyako Island(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 5 },
    { activity_id: activitiesID[194].id, name: 'Sea Turtle Snorkeling in Miyako Island(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 9 },
    { activity_id: activitiesID[195].id, name: 'Lovina Dolphin Watching and Snorkeling in Bali(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 12 },
    { activity_id: activitiesID[195].id, name: 'Lovina Dolphin Watching and Snorkeling in Bali(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 27 },
    { activity_id: activitiesID[196].id, name: 'Cairns Dreamtime Dive & Snorkel Experience(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 24 },
    { activity_id: activitiesID[196].id, name: 'Cairns Dreamtime Dive & Snorkel Experience(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 14 },
    { activity_id: activitiesID[197].id, name: 'Pulau Payar Snorkeling & Diving Adventures from Langkawi(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 21 },
    { activity_id: activitiesID[197].id, name: 'Pulau Payar Snorkeling & Diving Adventures from Langkawi(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 3 },
    { activity_id: activitiesID[198].id, name: 'Snorkeling Experience in Amed(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 10 },
    { activity_id: activitiesID[198].id, name: 'Snorkeling Experience in Amed(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 5 },
    { activity_id: activitiesID[199].id, name: '3 Points Snorkeling Experience from Male(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 27 },
    { activity_id: activitiesID[199].id, name: '3 Points Snorkeling Experience from Male(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 23 },
    { activity_id: activitiesID[200].id, name: 'Kenting Shore Diving for Beginners(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 5 },
    { activity_id: activitiesID[200].id, name: 'Kenting Shore Diving for Beginners(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 16 },
    { activity_id: activitiesID[201].id, name: 'Discover Scuba Diving by WaterColors(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 2 },
    { activity_id: activitiesID[201].id, name: 'Discover Scuba Diving by WaterColors(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 29 },
    { activity_id: activitiesID[202].id, name: 'Scuba Dive with Green Turtles at Liuqiu Island (For Beginners)(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 23 },
    { activity_id: activitiesID[202].id, name: 'Scuba Dive with Green Turtles at Liuqiu Island (For Beginners)(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 16 },
    { activity_id: activitiesID[203].id, name: 'Scuba Diving At Pulau Hantu in Singapore(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 2 },
    { activity_id: activitiesID[203].id, name: 'Scuba Diving At Pulau Hantu in Singapore(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 11 },
    { activity_id: activitiesID[204].id, name: 'Discover Scuba Diving in Bohol(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 13 },
    { activity_id: activitiesID[204].id, name: 'Discover Scuba Diving in Bohol(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 1 },
    { activity_id: activitiesID[205].id, name: 'Discover Scuba Dive in Maldives(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 28 },
    { activity_id: activitiesID[205].id, name: 'Discover Scuba Dive in Maldives(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 26 },
    { activity_id: activitiesID[206].id, name: 'Discover Scuba Diving in Boracay(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 25 },
    { activity_id: activitiesID[206].id, name: 'Discover Scuba Diving in Boracay(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 26 },
    { activity_id: activitiesID[207].id, name: 'USS Liberty Shipwreck Scuba Dive at Tulamben(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 9 },
    { activity_id: activitiesID[207].id, name: 'USS Liberty Shipwreck Scuba Dive at Tulamben(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 2 },
    { activity_id: activitiesID[208].id, name: 'Scuba Diving Experience in Havelock Island(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 11 },
    { activity_id: activitiesID[208].id, name: 'Scuba Diving Experience in Havelock Island(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 15 },
    { activity_id: activitiesID[209].id, name: 'Scuba Diving Lessons in Boracay (For Kids)(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 17 },
    { activity_id: activitiesID[209].id, name: 'Scuba Diving Lessons in Boracay (For Kids)(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 17 },
    { activity_id: activitiesID[210].id, name: 'Koh Lipe Full Day Scuba Diving(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 20 },
    { activity_id: activitiesID[210].id, name: 'Koh Lipe Full Day Scuba Diving(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 21 },
    { activity_id: activitiesID[211].id, name: 'Kenting Surfing Lessons(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 11 },
    { activity_id: activitiesID[211].id, name: 'Kenting Surfing Lessons(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 24 },
    { activity_id: activitiesID[212].id, name: 'Surfing for 1', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 16 },
    { activity_id: activitiesID[212].id, name: 'Surfing for 2', inclusion: 'A coach', eligibility: 'age 12 or above', description: "Hong Kong Surfing, Sai Kung Surfing",capacity: 23  },
    { activity_id: activitiesID[213].id, name: 'Surfing for 1', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 10 },
    { activity_id: activitiesID[213].id, name: 'Surfing for 2', inclusion: 'A coach', eligibility: 'age 12 or above', description: "Hong Kong Surfing, Sai Kung Surfing", capacity: 5 },
    { activity_id: activitiesID[214].id, name: 'Surfing for 1', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 12 },
    { activity_id: activitiesID[214].id, name: 'Surfing for 2', inclusion: 'A coach', eligibility: 'age 12 or above', description: "Hong Kong Surfing, Sai Kung Surfing", capacity: 16 },
    { activity_id: activitiesID[215].id, name: 'Surfing for 1', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 21 },
    { activity_id: activitiesID[215].id, name: 'Surfing for 2', inclusion: 'A coach', eligibility: 'age 12 or above', description: "Hong Kong Surfing, Sai Kung Surfing", capacity: 24 },
    { activity_id: activitiesID[216].id, name: 'Surfing for 1', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 23 },
    { activity_id: activitiesID[216].id, name: 'Surfing for 2', inclusion: 'A coach', eligibility: 'age 12 or above', description: "Hong Kong Surfing, Sai Kung Surfing", capacity: 3 },
    { activity_id: activitiesID[217].id, name: 'Surfing for 1', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 5 },
    { activity_id: activitiesID[217].id, name: 'Surfing for 2', inclusion: 'A coach', eligibility: 'age 12 or above', description: "Hong Kong Surfing, Sai Kung Surfing", capacity: 2 },
    { activity_id: activitiesID[218].id, name: 'Surfing for 1', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 10 },
    { activity_id: activitiesID[218].id, name: 'Surfing for 2', inclusion: 'A coach', eligibility: 'age 12 or above', description: "Hong Kong Surfing, Sai Kung Surfing", capacity: 15 },
    { activity_id: activitiesID[219].id, name: 'Surf Lesson(for 1)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 22 },
    { activity_id: activitiesID[219].id, name: 'Surf Lesson(for 2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: "Hong Kong Surfing, Sai Kung Surfing", capacity: 24 },
    { activity_id: activitiesID[220].id, name: 'Surfing for 1 ', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 24 },
    { activity_id: activitiesID[220].id, name: 'Surfing for 2 ', inclusion: 'A coach', eligibility: 'age 12 or above', description: "Hong Kong Surfing, Sai Kung Surfing", capacity: 11 },
    { activity_id: activitiesID[221].id, name: 'Surfing Lesson for 1', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 27 },
    { activity_id: activitiesID[221].id, name: 'Surfing Lesson for 2', inclusion: 'A coach', eligibility: 'age 12 or above', description: "Hong Kong Surfing, Sai Kung Surfing", capacity: 29 },
    { activity_id: activitiesID[222].id, name: 'Surfing for 1', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 28 },
    { activity_id: activitiesID[222].id, name: 'Surfing for 2', inclusion: 'A coach', eligibility: 'age 12 or above', description: "Hong Kong Surfing, Sai Kung Surfing", capacity: 1 },
    { activity_id: activitiesID[223].id, name: 'Novotel Century Hong Kong Staycation Package(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 22 },
    { activity_id: activitiesID[223].id, name: 'Novotel Century Hong Kong Staycation Package(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 29 },
    { activity_id: activitiesID[224].id, name: 'Crowne Plaza Hong Kong Kowloon East Staycation Package(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 17 },
    { activity_id: activitiesID[224].id, name: 'Crowne Plaza Hong Kong Kowloon East Staycation Package(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 12 },
    { activity_id: activitiesID[225].id, name: 'Akaroa Swimming with Dolphin(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 27 },
    { activity_id: activitiesID[225].id, name: 'Akaroa Swimming with Dolphin(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 19 },
    { activity_id: activitiesID[226].id, name: 'Swimming Package at Heated Infinity Pool in Swiss-Belresort Dago Heritage(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 10 },
    { activity_id: activitiesID[226].id, name: 'Swimming Package at Heated Infinity Pool in Swiss-Belresort Dago Heritage(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 25 },
    { activity_id: activitiesID[227].id, name: 'Bishop Lei International House - Staycation Package(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 15 },
    { activity_id: activitiesID[227].id, name: 'Bishop Lei International House - Staycation Package(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 10 },
    { activity_id: activitiesID[228].id, name: 'Kubang Badak Mangrove River Join-In Cruise with Swimming(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 27 },
    { activity_id: activitiesID[228].id, name: 'Kubang Badak Mangrove River Join-In Cruise with Swimming(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 10 },
    { activity_id: activitiesID[229].id, name: 'Swim with Sea Lions from Port Lincoln(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 14 },
    { activity_id: activitiesID[229].id, name: 'Swim with Sea Lions from Port Lincoln(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 4 },
    { activity_id: activitiesID[230].id, name: 'Dolphin, Seal Swim and Sightseeing Experience in Port Phillip Bay(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 5 },
    { activity_id: activitiesID[230].id, name: 'Dolphin, Seal Swim and Sightseeing Experience in Port Phillip Bay(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 17 },
    { activity_id: activitiesID[231].id, name: 'Dolphin & Seal Swim on the Mornington Peninsula(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 7 },
    { activity_id: activitiesID[231].id, name: 'Dolphin & Seal Swim on the Mornington Peninsula(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 24 },
    { activity_id: activitiesID[232].id, name: 'Aquaspin Water Cycling in Singapore(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 19 },
    { activity_id: activitiesID[232].id, name: 'Aquaspin Water Cycling in Singapore(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 24 },
    { activity_id: activitiesID[233].id, name: 'Windsurfing Experience in Goa(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 3 },
    { activity_id: activitiesID[233].id, name: 'Windsurfing Experience in Goa(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 21 },
    { activity_id: activitiesID[234].id, name: 'Windsurfer Resort(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 12 },
    { activity_id: activitiesID[234].id, name: 'Windsurfer Resort(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 10 },
    { activity_id: activitiesID[235].id, name: 'Wind Surfing and Sailing Catamaran Experience in Desaru(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 5 },
    { activity_id: activitiesID[235].id, name: 'Wind Surfing and Sailing Catamaran Experience in Desaru(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 15 },
    { activity_id: activitiesID[236].id, name: 'Stand Up Paddle Boarding Course for Beginners in Hong Kong(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 28 },
    { activity_id: activitiesID[236].id, name: 'Stand Up Paddle Boarding Course for Beginners in Hong Kong(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 12 },
    { activity_id: activitiesID[237].id, name: 'Stand Up Paddle Boarding Experience in Hoi An(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 5 },
    { activity_id: activitiesID[237].id, name: 'Stand Up Paddle Boarding Experience in Hoi An(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 14 },
    { activity_id: activitiesID[238].id, name: 'Stand Up Paddle Boarding at the Bitan Scenic Area in Taipei(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 25 },
    { activity_id: activitiesID[238].id, name: 'Stand Up Paddle Boarding at the Bitan Scenic Area in Taipei(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 3 },
    { activity_id: activitiesID[239].id, name: 'Kayak and Stand Up Paddleboarding Experience in Desaru(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 2 },
    { activity_id: activitiesID[239].id, name: 'Kayak and Stand Up Paddleboarding Experience in Desaru(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 2 },
    { activity_id: activitiesID[240].id, name: 'Stand Up Paddle Boarding Experience in Lambai Island(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 4 },
    { activity_id: activitiesID[240].id, name: 'Stand Up Paddle Boarding Experience in Lambai Island(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 22 },
    { activity_id: activitiesID[241].id, name: 'Manly Stand Up Paddle Board Lesson(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 17 },
    { activity_id: activitiesID[241].id, name: 'Manly Stand Up Paddle Board Lesson(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 8 },
    { activity_id: activitiesID[242].id, name: 'Stand Up Paddle Boarding Experience in Gopeng(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 2 },
    { activity_id: activitiesID[242].id, name: 'Stand Up Paddle Boarding Experience in Gopeng(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 26 },
    { activity_id: activitiesID[243].id, name: 'Sunset Stand Up Paddle Boarding Experience in Kota Kinabalu(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 19 },
    { activity_id: activitiesID[243].id, name: 'Sunset Stand Up Paddle Boarding Experience in Kota Kinabalu(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 3 },
    { activity_id: activitiesID[244].id, name: 'Sunset Stand Up Paddle Boarding Near Blue Cave of Okinawa(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 12 },
    { activity_id: activitiesID[244].id, name: 'Sunset Stand Up Paddle Boarding Near Blue Cave of Okinawa(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 19 },
    { activity_id: activitiesID[245].id, name: 'Stand Up Paddle Board Lesson and Guided Snorkeling Experience in Guam(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 4 },
    { activity_id: activitiesID[245].id, name: 'Stand Up Paddle Board Lesson and Guided Snorkeling Experience in Guam(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 8 },
    { activity_id: activitiesID[246].id, name: 'Surfboard, Kayak, Stand Up Paddle Board Rental in Da Nang(3-4)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 14 },
    { activity_id: activitiesID[246].id, name: 'Surfboard, Kayak, Stand Up Paddle Board Rental in Da Nang(1-2)', inclusion: 'A coach', eligibility: 'age 12 or above', description: chance.paragraph({ sentences: 4 }), capacity: 1 },



  ];
  const package_ids = await insertSeed(knex, 'packages', packagesSeed, 'id');

  let pricesSeed = []
  let timeSectionsSeed = []
  for (let i = 0; i < package_ids.length; i++) {
    pricesSeed.push({
      package_id: package_ids[i],
      effective_date: jsUnixToPostgresUnix(new Date('2021-6-4').getTime()),
      base_price: 20000,
      weekend_price: 30000
    })
    timeSectionsSeed.push({
      package_id: package_ids[i],
      section: '10.00am'
    })
    timeSectionsSeed.push({
      package_id: package_ids[i],
      section: '13.00pm'
    })
    timeSectionsSeed.push({
      package_id: package_ids[i],
      section: '15.00pm'
    })
  }
  pricesSeed.push({
    package_id: package_ids[0],
    effective_date: jsUnixToPostgresUnix(new Date('2021-8-9').getTime()),
    base_price: 10000,
    weekend_price: 15000
  })
  pricesSeed.push({
    package_id: package_ids[0],
    effective_date: jsUnixToPostgresUnix((new Date('2021-12-26')).getTime()),
    base_price: 30000,
    weekend_price: 40000
  })

  pricesSeed.push({
    package_id: package_ids[14],
    effective_date: 1612483200,
    base_price: 7000,
    weekend_price: 7200
  })

  pricesSeed.push({
    package_id: package_ids[15],
    effective_date: 1633046400,
    base_price: 7000,
    weekend_price: 7200
  })
  pricesSeed.push({ package_id: package_ids[16], effective_date: 1630627200, base_price: 1000, weekend_price: 1200 })
  pricesSeed.push({ package_id: package_ids[17], effective_date: 1628467200, base_price: 1000, weekend_price: 1200 })
  pricesSeed.push({ package_id: package_ids[18], effective_date: 1609891200, base_price: 9000, weekend_price: 9200 })
  pricesSeed.push({ package_id: package_ids[19], effective_date: 1610064000, base_price: 9000, weekend_price: 9200 })
  pricesSeed.push({ package_id: package_ids[20], effective_date: 1609545600, base_price: 1000, weekend_price: 1200 })
  pricesSeed.push({ package_id: package_ids[21], effective_date: 1617667200, base_price: 1000, weekend_price: 1200 })
  pricesSeed.push({ package_id: package_ids[22], effective_date: 1620604800, base_price: 9000, weekend_price: 9200 })
  pricesSeed.push({ package_id: package_ids[23], effective_date: 1633305600, base_price: 9000, weekend_price: 9200 })
  pricesSeed.push({ package_id: package_ids[24], effective_date: 1617753600, base_price: 7000, weekend_price: 7200 })
  pricesSeed.push({ package_id: package_ids[25], effective_date: 1620432000, base_price: 7000, weekend_price: 7200 })
  pricesSeed.push({ package_id: package_ids[26], effective_date: 1612310400, base_price: 2000, weekend_price: 2200 })
  pricesSeed.push({ package_id: package_ids[27], effective_date: 1622505600, base_price: 2000, weekend_price: 2200 })
  pricesSeed.push({ package_id: package_ids[28], effective_date: 1620604800, base_price: 8000, weekend_price: 8200 })
  pricesSeed.push({ package_id: package_ids[29], effective_date: 1617753600, base_price: 8000, weekend_price: 8200 })
  pricesSeed.push({ package_id: package_ids[30], effective_date: 1617926400, base_price: 2000, weekend_price: 3000 })
  pricesSeed.push({ package_id: package_ids[31], effective_date: 1620604800, base_price: 2000, weekend_price: 3000 })
  pricesSeed.push({ package_id: package_ids[32], effective_date: 1620432000, base_price: 7000, weekend_price: 7200 })
  pricesSeed.push({ package_id: package_ids[33], effective_date: 1615248000, base_price: 7000, weekend_price: 7200 })
  pricesSeed.push({ package_id: package_ids[34], effective_date: 1628294400, base_price: 5000, weekend_price: 5200 })
  pricesSeed.push({ package_id: package_ids[35], effective_date: 1630972800, base_price: 5000, weekend_price: 5200 })
  pricesSeed.push({ package_id: package_ids[36], effective_date: 1620518400, base_price: 9000, weekend_price: 9200 })
  pricesSeed.push({ package_id: package_ids[37], effective_date: 1625184000, base_price: 9000, weekend_price: 9200 })
  pricesSeed.push({ package_id: package_ids[38], effective_date: 1633478400, base_price: 5000, weekend_price: 5200 })
  pricesSeed.push({ package_id: package_ids[39], effective_date: 1609977600, base_price: 5000, weekend_price: 5200 })
  pricesSeed.push({ package_id: package_ids[40], effective_date: 1622764800, base_price: 4000, weekend_price: 4200 })
  pricesSeed.push({ package_id: package_ids[41], effective_date: 1625356800, base_price: 4000, weekend_price: 4200 })
  pricesSeed.push({ package_id: package_ids[42], effective_date: 1623110400, base_price: 1000, weekend_price: 2000 })
  pricesSeed.push({ package_id: package_ids[43], effective_date: 1609718400, base_price: 1000, weekend_price: 2000 })
  pricesSeed.push({ package_id: package_ids[44], effective_date: 1622764800, base_price: 1000, weekend_price: 2000 })
  pricesSeed.push({ package_id: package_ids[45], effective_date: 1617840000, base_price: 1000, weekend_price: 2000 })
  pricesSeed.push({ package_id: package_ids[46], effective_date: 1633046400, base_price: 1000, weekend_price: 2000 })
  pricesSeed.push({ package_id: package_ids[47], effective_date: 1625184000, base_price: 1000, weekend_price: 2000 })
  pricesSeed.push({ package_id: package_ids[48], effective_date: 1630540800, base_price: 9000, weekend_price: 9200 })
  pricesSeed.push({ package_id: package_ids[49], effective_date: 1617408000, base_price: 9000, weekend_price: 9200 })
  pricesSeed.push({ package_id: package_ids[50], effective_date: 1630540800, base_price: 1000, weekend_price: 1200 })
  pricesSeed.push({ package_id: package_ids[51], effective_date: 1625184000, base_price: 1000, weekend_price: 1200 })
  pricesSeed.push({ package_id: package_ids[52], effective_date: 1615161600, base_price: 4000, weekend_price: 4200 })
  pricesSeed.push({ package_id: package_ids[53], effective_date: 1620000000, base_price: 4000, weekend_price: 4200 })
  pricesSeed.push({ package_id: package_ids[54], effective_date: 1622764800, base_price: 1000, weekend_price: 1200 })
  pricesSeed.push({ package_id: package_ids[55], effective_date: 1617408000, base_price: 1000, weekend_price: 1200 })
  pricesSeed.push({ package_id: package_ids[56], effective_date: 1622851200, base_price: 1000, weekend_price: 1200 })
  pricesSeed.push({ package_id: package_ids[57], effective_date: 1631232000, base_price: 1000, weekend_price: 1200 })
  pricesSeed.push({ package_id: package_ids[58], effective_date: 1612310400, base_price: 5000, weekend_price: 5200 })
  pricesSeed.push({ package_id: package_ids[59], effective_date: 1620172800, base_price: 5000, weekend_price: 5200 })
  pricesSeed.push({ package_id: package_ids[60], effective_date: 1609804800, base_price: 6000, weekend_price: 6200 })
  pricesSeed.push({ package_id: package_ids[61], effective_date: 1620086400, base_price: 6000, weekend_price: 6200 })
  pricesSeed.push({ package_id: package_ids[62], effective_date: 1628208000, base_price: 3000, weekend_price: 3200 })
  pricesSeed.push({ package_id: package_ids[63], effective_date: 1612396800, base_price: 3000, weekend_price: 3200 })
  pricesSeed.push({ package_id: package_ids[64], effective_date: 1622764800, base_price: 5000, weekend_price: 5200 })
  pricesSeed.push({ package_id: package_ids[65], effective_date: 1615161600, base_price: 5000, weekend_price: 5200 })
  pricesSeed.push({ package_id: package_ids[66], effective_date: 1620518400, base_price: 2000, weekend_price: 2200 })
  pricesSeed.push({ package_id: package_ids[67], effective_date: 1633046400, base_price: 2000, weekend_price: 2200 })
  pricesSeed.push({ package_id: package_ids[68], effective_date: 1630800000, base_price: 3000, weekend_price: 3200 })
  pricesSeed.push({ package_id: package_ids[69], effective_date: 1615334400, base_price: 3000, weekend_price: 3200 })
  pricesSeed.push({ package_id: package_ids[70], effective_date: 1625616000, base_price: 1000, weekend_price: 1200 })
  pricesSeed.push({ package_id: package_ids[71], effective_date: 1620432000, base_price: 1000, weekend_price: 1200 })
  pricesSeed.push({ package_id: package_ids[72], effective_date: 1617321600, base_price: 2000, weekend_price: 2200 })
  pricesSeed.push({ package_id: package_ids[73], effective_date: 1630627200, base_price: 2000, weekend_price: 2200 })
  pricesSeed.push({ package_id: package_ids[74], effective_date: 1617753600, base_price: 7000, weekend_price: 7200 })
  pricesSeed.push({ package_id: package_ids[75], effective_date: 1623196800, base_price: 7000, weekend_price: 7200 })
  pricesSeed.push({ package_id: package_ids[76], effective_date: 1614902400, base_price: 8000, weekend_price: 8200 })
  pricesSeed.push({ package_id: package_ids[77], effective_date: 1628380800, base_price: 8000, weekend_price: 8200 })
  pricesSeed.push({ package_id: package_ids[76], effective_date: 1612137600, base_price: 9000, weekend_price: 9200 })
  pricesSeed.push({ package_id: package_ids[77], effective_date: 1631232000, base_price: 9000, weekend_price: 9200 })
  pricesSeed.push({ package_id: package_ids[78], effective_date: 1622851200, base_price: 5000, weekend_price: 5200 })
  pricesSeed.push({ package_id: package_ids[79], effective_date: 1630540800, base_price: 5000, weekend_price: 5200 })
  pricesSeed.push({ package_id: package_ids[80], effective_date: 1617321600, base_price: 5000, weekend_price: 5200 })
  pricesSeed.push({ package_id: package_ids[81], effective_date: 1618012800, base_price: 5000, weekend_price: 5200 })
  pricesSeed.push({ package_id: package_ids[82], effective_date: 1610150400, base_price: 3000, weekend_price: 3200 })
  pricesSeed.push({ package_id: package_ids[83], effective_date: 1625788800, base_price: 3000, weekend_price: 3200 })
  pricesSeed.push({ package_id: package_ids[84], effective_date: 1630972800, base_price: 1000, weekend_price: 1200 })
  pricesSeed.push({ package_id: package_ids[85], effective_date: 1620259200, base_price: 1000, weekend_price: 1200 })
  pricesSeed.push({ package_id: package_ids[86], effective_date: 1620259200, base_price: 9000, weekend_price: 9200 })
  pricesSeed.push({ package_id: package_ids[87], effective_date: 1622678400, base_price: 9000, weekend_price: 9200 })
  pricesSeed.push({ package_id: package_ids[88], effective_date: 1633824000, base_price: 5000, weekend_price: 5200 })
  pricesSeed.push({ package_id: package_ids[89], effective_date: 1614902400, base_price: 5000, weekend_price: 5200 })
  pricesSeed.push({ package_id: package_ids[90], effective_date: 1620518400, base_price: 2000, weekend_price: 2200 })
  pricesSeed.push({ package_id: package_ids[91], effective_date: 1622764800, base_price: 2000, weekend_price: 2200 })
  pricesSeed.push({ package_id: package_ids[92], effective_date: 1627862400, base_price: 1000, weekend_price: 1200 })
  pricesSeed.push({ package_id: package_ids[93], effective_date: 1609718400, base_price: 1000, weekend_price: 1200 })
  pricesSeed.push({ package_id: package_ids[94], effective_date: 1625270400, base_price: 3000, weekend_price: 3200 })
  pricesSeed.push({ package_id: package_ids[95], effective_date: 1633564800, base_price: 3000, weekend_price: 3200 })
  pricesSeed.push({ package_id: package_ids[96], effective_date: 1625356800, base_price: 3000, weekend_price: 3200 })
  pricesSeed.push({ package_id: package_ids[97], effective_date: 1615334400, base_price: 3000, weekend_price: 3200 })
  pricesSeed.push({ package_id: package_ids[98], effective_date: 1633392000, base_price: 7000, weekend_price: 7200 })
  pricesSeed.push({ package_id: package_ids[99], effective_date: 1633132800, base_price: 7000, weekend_price: 7200 })
  pricesSeed.push({ package_id: package_ids[100], effective_date: 1633392000, base_price: 6000, weekend_price: 6200 })
  pricesSeed.push({ package_id: package_ids[101], effective_date: 1612224000, base_price: 6000, weekend_price: 6200 })
  pricesSeed.push({ package_id: package_ids[102], effective_date: 1612828800, base_price: 3000, weekend_price: 3200 })
  pricesSeed.push({ package_id: package_ids[103], effective_date: 1628294400, base_price: 3000, weekend_price: 3200 })
  pricesSeed.push({ package_id: package_ids[104], effective_date: 1614556800, base_price: 3000, weekend_price: 3200 })
  pricesSeed.push({ package_id: package_ids[105], effective_date: 1633046400, base_price: 3000, weekend_price: 3200 })
  pricesSeed.push({ package_id: package_ids[106], effective_date: 1633392000, base_price: 9000, weekend_price: 9200 })
  pricesSeed.push({ package_id: package_ids[107], effective_date: 1619913600, base_price: 9000, weekend_price: 9200 })
  pricesSeed.push({ package_id: package_ids[108], effective_date: 1631232000, base_price: 8000, weekend_price: 8200 })
  pricesSeed.push({ package_id: package_ids[109], effective_date: 1615075200, base_price: 8000, weekend_price: 8200 })
  pricesSeed.push({ package_id: package_ids[110], effective_date: 1615248000, base_price: 4000, weekend_price: 4200 })
  pricesSeed.push({ package_id: package_ids[111], effective_date: 1619913600, base_price: 4000, weekend_price: 4200 })
  pricesSeed.push({ package_id: package_ids[112], effective_date: 1622937600, base_price: 9000, weekend_price: 9200 })
  pricesSeed.push({ package_id: package_ids[113], effective_date: 1620604800, base_price: 9000, weekend_price: 9200 })
  pricesSeed.push({ package_id: package_ids[114], effective_date: 1625702400, base_price: 9000, weekend_price: 9200 })
  pricesSeed.push({ package_id: package_ids[115], effective_date: 1612310400, base_price: 9000, weekend_price: 9200 })
  pricesSeed.push({ package_id: package_ids[116], effective_date: 1619827200, base_price: 3000, weekend_price: 3200 })
  pricesSeed.push({ package_id: package_ids[117], effective_date: 1625616000, base_price: 3000, weekend_price: 3200 })
  pricesSeed.push({ package_id: package_ids[118], effective_date: 1630800000, base_price: 9000, weekend_price: 9200 })
  pricesSeed.push({ package_id: package_ids[119], effective_date: 1633392000, base_price: 9000, weekend_price: 9200 })
  pricesSeed.push({ package_id: package_ids[120], effective_date: 1633564800, base_price: 5000, weekend_price: 5200 })
  pricesSeed.push({ package_id: package_ids[121], effective_date: 1620345600, base_price: 5000, weekend_price: 5200 })
  pricesSeed.push({ package_id: package_ids[122], effective_date: 1620086400, base_price: 1000, weekend_price: 1200 })
  pricesSeed.push({ package_id: package_ids[123], effective_date: 1628035200, base_price: 1000, weekend_price: 1200 })
  pricesSeed.push({ package_id: package_ids[124], effective_date: 1628553600, base_price: 4000, weekend_price: 4200 })
  pricesSeed.push({ package_id: package_ids[125], effective_date: 1612396800, base_price: 4000, weekend_price: 4200 })
  pricesSeed.push({ package_id: package_ids[126], effective_date: 1612828800, base_price: 2000, weekend_price: 2200 })
  pricesSeed.push({ package_id: package_ids[127], effective_date: 1612656000, base_price: 2000, weekend_price: 2200 })
  pricesSeed.push({ package_id: package_ids[128], effective_date: 1609804800, base_price: 4000, weekend_price: 4200 })
  pricesSeed.push({ package_id: package_ids[129], effective_date: 1614902400, base_price: 4000, weekend_price: 4200 })
  pricesSeed.push({ package_id: package_ids[130], effective_date: 1609632000, base_price: 7000, weekend_price: 7200 })
  pricesSeed.push({ package_id: package_ids[131], effective_date: 1612396800, base_price: 7000, weekend_price: 7200 })
  pricesSeed.push({ package_id: package_ids[132], effective_date: 1615075200, base_price: 7000, weekend_price: 7200 })
  pricesSeed.push({ package_id: package_ids[133], effective_date: 1625184000, base_price: 7000, weekend_price: 7200 })
  pricesSeed.push({ package_id: package_ids[134], effective_date: 1623196800, base_price: 3000, weekend_price: 3200 })
  pricesSeed.push({ package_id: package_ids[135], effective_date: 1620172800, base_price: 3000, weekend_price: 3200 })
  pricesSeed.push({ package_id: package_ids[136], effective_date: 1628121600, base_price: 1000, weekend_price: 1200 })
  pricesSeed.push({ package_id: package_ids[137], effective_date: 1630540800, base_price: 1000, weekend_price: 1200 })
  pricesSeed.push({ package_id: package_ids[138], effective_date: 1622678400, base_price: 1000, weekend_price: 1200 })
  pricesSeed.push({ package_id: package_ids[139], effective_date: 1617926400, base_price: 1000, weekend_price: 1200 })
  pricesSeed.push({ package_id: package_ids[140], effective_date: 1631145600, base_price: 1000, weekend_price: 1200 })
  pricesSeed.push({ package_id: package_ids[141], effective_date: 1625702400, base_price: 1000, weekend_price: 1200 })
  pricesSeed.push({ package_id: package_ids[142], effective_date: 1631059200, base_price: 1000, weekend_price: 1200 })
  pricesSeed.push({ package_id: package_ids[143], effective_date: 1612137600, base_price: 1000, weekend_price: 1200 })
  pricesSeed.push({ package_id: package_ids[144], effective_date: 1633651200, base_price: 1000, weekend_price: 1200 })
  pricesSeed.push({ package_id: package_ids[145], effective_date: 1627862400, base_price: 1000, weekend_price: 1200 })
  pricesSeed.push({ package_id: package_ids[146], effective_date: 1622764800, base_price: 7000, weekend_price: 7200 })
  pricesSeed.push({ package_id: package_ids[147], effective_date: 1620518400, base_price: 7000, weekend_price: 7200 })
  pricesSeed.push({ package_id: package_ids[148], effective_date: 1610064000, base_price: 3000, weekend_price: 3200 })
  pricesSeed.push({ package_id: package_ids[149], effective_date: 1614556800, base_price: 3000, weekend_price: 3200 })
  pricesSeed.push({ package_id: package_ids[150], effective_date: 1614556800, base_price: 5000, weekend_price: 5200 })
  pricesSeed.push({ package_id: package_ids[151], effective_date: 1620432000, base_price: 5000, weekend_price: 5200 })
  pricesSeed.push({ package_id: package_ids[152], effective_date: 1614643200, base_price: 8000, weekend_price: 8200 })
  pricesSeed.push({ package_id: package_ids[153], effective_date: 1609459200, base_price: 8000, weekend_price: 8200 })
  pricesSeed.push({ package_id: package_ids[154], effective_date: 1614902400, base_price: 4000, weekend_price: 4200 })
  pricesSeed.push({ package_id: package_ids[155], effective_date: 1620518400, base_price: 4000, weekend_price: 4200 })
  pricesSeed.push({ package_id: package_ids[156], effective_date: 1612915200, base_price: 3000, weekend_price: 3200 })
  pricesSeed.push({ package_id: package_ids[157], effective_date: 1625184000, base_price: 3000, weekend_price: 3200 })
  pricesSeed.push({ package_id: package_ids[158], effective_date: 1620172800, base_price: 6000, weekend_price: 6200 })
  pricesSeed.push({ package_id: package_ids[159], effective_date: 1619827200, base_price: 6000, weekend_price: 6200 })
  pricesSeed.push({ package_id: package_ids[160], effective_date: 1620518400, base_price: 2000, weekend_price: 2200 })
  pricesSeed.push({ package_id: package_ids[161], effective_date: 1609718400, base_price: 2000, weekend_price: 2200 })
  pricesSeed.push({ package_id: package_ids[162], effective_date: 1614643200, base_price: 2000, weekend_price: 2200 })
  pricesSeed.push({ package_id: package_ids[163], effective_date: 1609545600, base_price: 2000, weekend_price: 2200 })
  pricesSeed.push({ package_id: package_ids[164], effective_date: 1625529600, base_price: 1000, weekend_price: 1200 })
  pricesSeed.push({ package_id: package_ids[165], effective_date: 1612310400, base_price: 1000, weekend_price: 1200 })
  pricesSeed.push({ package_id: package_ids[166], effective_date: 1615075200, base_price: 3000, weekend_price: 3200 })
  pricesSeed.push({ package_id: package_ids[167], effective_date: 1622505600, base_price: 3000, weekend_price: 3200 })
  pricesSeed.push({ package_id: package_ids[168], effective_date: 1628294400, base_price: 2000, weekend_price: 2200 })
  pricesSeed.push({ package_id: package_ids[169], effective_date: 1612483200, base_price: 2000, weekend_price: 2200 })
  pricesSeed.push({ package_id: package_ids[170], effective_date: 1609632000, base_price: 1000, weekend_price: 1200 })
  pricesSeed.push({ package_id: package_ids[171], effective_date: 1609545600, base_price: 1000, weekend_price: 1200 })
  pricesSeed.push({ package_id: package_ids[172], effective_date: 1633132800, base_price: 5000, weekend_price: 5200 })
  pricesSeed.push({ package_id: package_ids[173], effective_date: 1617408000, base_price: 5000, weekend_price: 5200 })
  pricesSeed.push({ package_id: package_ids[174], effective_date: 1609545600, base_price: 7000, weekend_price: 7200 })
  pricesSeed.push({ package_id: package_ids[175], effective_date: 1609891200, base_price: 7000, weekend_price: 7200 })
  pricesSeed.push({ package_id: package_ids[176], effective_date: 1630540800, base_price: 9000, weekend_price: 9200 })
  pricesSeed.push({ package_id: package_ids[177], effective_date: 1617408000, base_price: 9000, weekend_price: 9200 })
  pricesSeed.push({ package_id: package_ids[178], effective_date: 1615334400, base_price: 7000, weekend_price: 7200 })
  pricesSeed.push({ package_id: package_ids[179], effective_date: 1633132800, base_price: 7000, weekend_price: 7200 })
  pricesSeed.push({ package_id: package_ids[180], effective_date: 1623283200, base_price: 9000, weekend_price: 9200 })
  pricesSeed.push({ package_id: package_ids[181], effective_date: 1625270400, base_price: 9000, weekend_price: 9200 })
  pricesSeed.push({ package_id: package_ids[182], effective_date: 1623196800, base_price: 9000, weekend_price: 9200 })
  pricesSeed.push({ package_id: package_ids[183], effective_date: 1625616000, base_price: 9000, weekend_price: 9200 })
  pricesSeed.push({ package_id: package_ids[184], effective_date: 1625270400, base_price: 1000, weekend_price: 1200 })
  pricesSeed.push({ package_id: package_ids[185], effective_date: 1610150400, base_price: 1000, weekend_price: 1200 })
  pricesSeed.push({ package_id: package_ids[186], effective_date: 1617926400, base_price: 8000, weekend_price: 8200 })
  pricesSeed.push({ package_id: package_ids[187], effective_date: 1614988800, base_price: 8000, weekend_price: 8200 })
  pricesSeed.push({ package_id: package_ids[188], effective_date: 1625875200, base_price: 2000, weekend_price: 2200 })
  pricesSeed.push({ package_id: package_ids[189], effective_date: 1617235200, base_price: 2000, weekend_price: 2200 })
  pricesSeed.push({ package_id: package_ids[190], effective_date: 1623024000, base_price: 4000, weekend_price: 4200 })
  pricesSeed.push({ package_id: package_ids[191], effective_date: 1609459200, base_price: 4000, weekend_price: 4200 })
  pricesSeed.push({ package_id: package_ids[192], effective_date: 1628467200, base_price: 7000, weekend_price: 7200 })
  pricesSeed.push({ package_id: package_ids[193], effective_date: 1628380800, base_price: 7000, weekend_price: 7200 })
  pricesSeed.push({ package_id: package_ids[194], effective_date: 1615248000, base_price: 9000, weekend_price: 9200 })
  pricesSeed.push({ package_id: package_ids[195], effective_date: 1614556800, base_price: 9000, weekend_price: 9200 })
  pricesSeed.push({ package_id: package_ids[196], effective_date: 1623024000, base_price: 3000, weekend_price: 3200 })
  pricesSeed.push({ package_id: package_ids[197], effective_date: 1623110400, base_price: 3000, weekend_price: 3200 })
  pricesSeed.push({ package_id: package_ids[198], effective_date: 1609804800, base_price: 3000, weekend_price: 3200 })
  pricesSeed.push({ package_id: package_ids[199], effective_date: 1612915200, base_price: 3000, weekend_price: 3200 })
  pricesSeed.push({ package_id: package_ids[200], effective_date: 1627862400, base_price: 3000, weekend_price: 3200 })
  pricesSeed.push({ package_id: package_ids[201], effective_date: 1625702400, base_price: 3000, weekend_price: 3200 })
  pricesSeed.push({ package_id: package_ids[202], effective_date: 1628553600, base_price: 3000, weekend_price: 3200 })
  pricesSeed.push({ package_id: package_ids[203], effective_date: 1625184000, base_price: 3000, weekend_price: 3200 })
  pricesSeed.push({ package_id: package_ids[204], effective_date: 1625788800, base_price: 5000, weekend_price: 5200 })
  pricesSeed.push({ package_id: package_ids[205], effective_date: 1622764800, base_price: 5000, weekend_price: 5200 })
  pricesSeed.push({ package_id: package_ids[206], effective_date: 1620000000, base_price: 5000, weekend_price: 5200 })
  pricesSeed.push({ package_id: package_ids[207], effective_date: 1609977600, base_price: 5000, weekend_price: 5200 })
  pricesSeed.push({ package_id: package_ids[208], effective_date: 1612742400, base_price: 5000, weekend_price: 5200 })
  pricesSeed.push({ package_id: package_ids[209], effective_date: 1633737600, base_price: 5000, weekend_price: 5200 })
  pricesSeed.push({ package_id: package_ids[210], effective_date: 1620172800, base_price: 1000, weekend_price: 1200 })
  pricesSeed.push({ package_id: package_ids[211], effective_date: 1610150400, base_price: 1000, weekend_price: 1200 })
  pricesSeed.push({ package_id: package_ids[212], effective_date: 1620172800, base_price: 6000, weekend_price: 6200 })
  pricesSeed.push({ package_id: package_ids[213], effective_date: 1633651200, base_price: 6000, weekend_price: 6200 })
  pricesSeed.push({ package_id: package_ids[214], effective_date: 1617235200, base_price: 3000, weekend_price: 3200 })
  pricesSeed.push({ package_id: package_ids[215], effective_date: 1618012800, base_price: 3000, weekend_price: 3200 })
  pricesSeed.push({ package_id: package_ids[216], effective_date: 1630886400, base_price: 2000, weekend_price: 2200 })
  pricesSeed.push({ package_id: package_ids[217], effective_date: 1633737600, base_price: 2000, weekend_price: 2200 })
  pricesSeed.push({ package_id: package_ids[218], effective_date: 1615161600, base_price: 3000, weekend_price: 3200 })
  pricesSeed.push({ package_id: package_ids[219], effective_date: 1623283200, base_price: 3000, weekend_price: 3200 })
  pricesSeed.push({ package_id: package_ids[220], effective_date: 1630627200, base_price: 4000, weekend_price: 4200 })
  pricesSeed.push({ package_id: package_ids[221], effective_date: 1612569600, base_price: 4000, weekend_price: 4200 })
  pricesSeed.push({ package_id: package_ids[222], effective_date: 1619913600, base_price: 7000, weekend_price: 7200 })
  pricesSeed.push({ package_id: package_ids[223], effective_date: 1609891200, base_price: 7000, weekend_price: 7200 })
  pricesSeed.push({ package_id: package_ids[224], effective_date: 1609804800, base_price: 3000, weekend_price: 3200 })
  pricesSeed.push({ package_id: package_ids[225], effective_date: 1617926400, base_price: 3000, weekend_price: 3200 })
  pricesSeed.push({ package_id: package_ids[226], effective_date: 1612137600, base_price: 1000, weekend_price: 1200 })
  pricesSeed.push({ package_id: package_ids[227], effective_date: 1622851200, base_price: 1000, weekend_price: 1200 })
  pricesSeed.push({ package_id: package_ids[228], effective_date: 1633737600, base_price: 3000, weekend_price: 3200 })
  pricesSeed.push({ package_id: package_ids[229], effective_date: 1628467200, base_price: 3000, weekend_price: 3200 })
  pricesSeed.push({ package_id: package_ids[230], effective_date: 1609977600, base_price: 7000, weekend_price: 7200 })
  pricesSeed.push({ package_id: package_ids[231], effective_date: 1628294400, base_price: 7000, weekend_price: 7200 })
  pricesSeed.push({ package_id: package_ids[232], effective_date: 1633219200, base_price: 8000, weekend_price: 8200 })
  pricesSeed.push({ package_id: package_ids[233], effective_date: 1615161600, base_price: 8000, weekend_price: 8200 })
  pricesSeed.push({ package_id: package_ids[234], effective_date: 1612137600, base_price: 4000, weekend_price: 4200 })
  pricesSeed.push({ package_id: package_ids[235], effective_date: 1627948800, base_price: 4000, weekend_price: 4200 })
  pricesSeed.push({ package_id: package_ids[236], effective_date: 1617321600, base_price: 4000, weekend_price: 4200 })
  pricesSeed.push({ package_id: package_ids[237], effective_date: 1615075200, base_price: 4000, weekend_price: 4200 })
  pricesSeed.push({ package_id: package_ids[238], effective_date: 1627776000, base_price: 8000, weekend_price: 8200 })
  pricesSeed.push({ package_id: package_ids[239], effective_date: 1633478400, base_price: 8000, weekend_price: 8200 })
  pricesSeed.push({ package_id: package_ids[240], effective_date: 1628467200, base_price: 2000, weekend_price: 2200 })
  pricesSeed.push({ package_id: package_ids[241], effective_date: 1633046400, base_price: 2000, weekend_price: 2200 })
  pricesSeed.push({ package_id: package_ids[242], effective_date: 1619913600, base_price: 3000, weekend_price: 3200 })
  pricesSeed.push({ package_id: package_ids[243], effective_date: 1609632000, base_price: 3000, weekend_price: 3200 })
  pricesSeed.push({ package_id: package_ids[244], effective_date: 1630454400, base_price: 1000, weekend_price: 1200 })
  pricesSeed.push({ package_id: package_ids[245], effective_date: 1620432000, base_price: 1000, weekend_price: 1200 })
  pricesSeed.push({ package_id: package_ids[246], effective_date: 1620345600, base_price: 3000, weekend_price: 3200 })
  pricesSeed.push({ package_id: package_ids[247], effective_date: 1614988800, base_price: 3000, weekend_price: 3200 })
  pricesSeed.push({ package_id: package_ids[248], effective_date: 1612224000, base_price: 3000, weekend_price: 3200 })
  pricesSeed.push({ package_id: package_ids[249], effective_date: 1615248000, base_price: 3000, weekend_price: 3200 })
  pricesSeed.push({ package_id: package_ids[250], effective_date: 1617235200, base_price: 6000, weekend_price: 6200 })
  pricesSeed.push({ package_id: package_ids[251], effective_date: 1617408000, base_price: 6000, weekend_price: 6200 })
  pricesSeed.push({ package_id: package_ids[252], effective_date: 1612828800, base_price: 3000, weekend_price: 3200 })
  pricesSeed.push({ package_id: package_ids[253], effective_date: 1633132800, base_price: 3000, weekend_price: 3200 })
  pricesSeed.push({ package_id: package_ids[254], effective_date: 1630454400, base_price: 5000, weekend_price: 5200 })
  pricesSeed.push({ package_id: package_ids[255], effective_date: 1610236800, base_price: 5000, weekend_price: 5200 })
  pricesSeed.push({ package_id: package_ids[256], effective_date: 1609459200, base_price: 5000, weekend_price: 5200 })
  pricesSeed.push({ package_id: package_ids[257], effective_date: 1622937600, base_price: 5000, weekend_price: 5200 })
  pricesSeed.push({ package_id: package_ids[258], effective_date: 1617840000, base_price: 4000, weekend_price: 4200 })
  pricesSeed.push({ package_id: package_ids[259], effective_date: 1625702400, base_price: 4000, weekend_price: 4200 })
  pricesSeed.push({ package_id: package_ids[260], effective_date: 1633046400, base_price: 3000, weekend_price: 3200 })
  pricesSeed.push({ package_id: package_ids[261], effective_date: 1625270400, base_price: 3000, weekend_price: 3200 })
  pricesSeed.push({ package_id: package_ids[262], effective_date: 1627862400, base_price: 6000, weekend_price: 6200 })
  pricesSeed.push({ package_id: package_ids[263], effective_date: 1614816000, base_price: 6000, weekend_price: 6200 })
  pricesSeed.push({ package_id: package_ids[264], effective_date: 1612137600, base_price: 3000, weekend_price: 3200 })
  pricesSeed.push({ package_id: package_ids[265], effective_date: 1628467200, base_price: 3000, weekend_price: 3200 })
  pricesSeed.push({ package_id: package_ids[266], effective_date: 1612137600, base_price: 3000, weekend_price: 3200 })
  pricesSeed.push({ package_id: package_ids[267], effective_date: 1609632000, base_price: 3000, weekend_price: 3200 })
  pricesSeed.push({ package_id: package_ids[268], effective_date: 1620259200, base_price: 3000, weekend_price: 3200 })
  pricesSeed.push({ package_id: package_ids[269], effective_date: 1614816000, base_price: 3000, weekend_price: 3200 })
  pricesSeed.push({ package_id: package_ids[270], effective_date: 1614556800, base_price: 2000, weekend_price: 2200 })
  pricesSeed.push({ package_id: package_ids[271], effective_date: 1609804800, base_price: 2000, weekend_price: 2200 })
  pricesSeed.push({ package_id: package_ids[272], effective_date: 1633132800, base_price: 4000, weekend_price: 4200 })
  pricesSeed.push({ package_id: package_ids[273], effective_date: 1628553600, base_price: 4000, weekend_price: 4200 })
  pricesSeed.push({ package_id: package_ids[274], effective_date: 1622764800, base_price: 3000, weekend_price: 3200 })
  pricesSeed.push({ package_id: package_ids[275], effective_date: 1617494400, base_price: 3000, weekend_price: 3200 })
  pricesSeed.push({ package_id: package_ids[276], effective_date: 1618012800, base_price: 7000, weekend_price: 7200 })
  pricesSeed.push({ package_id: package_ids[277], effective_date: 1625529600, base_price: 7000, weekend_price: 7200 })
  pricesSeed.push({ package_id: package_ids[278], effective_date: 1612742400, base_price: 2000, weekend_price: 2200 })
  pricesSeed.push({ package_id: package_ids[279], effective_date: 1628467200, base_price: 2000, weekend_price: 2200 })
  pricesSeed.push({ package_id: package_ids[280], effective_date: 1614816000, base_price: 2000, weekend_price: 2200 })
  pricesSeed.push({ package_id: package_ids[281], effective_date: 1610064000, base_price: 2000, weekend_price: 2200 })
  pricesSeed.push({ package_id: package_ids[282], effective_date: 1612742400, base_price: 1000, weekend_price: 1200 })
  pricesSeed.push({ package_id: package_ids[283], effective_date: 1631232000, base_price: 1000, weekend_price: 1200 })
  pricesSeed.push({ package_id: package_ids[284], effective_date: 1620000000, base_price: 2000, weekend_price: 2200 })
  pricesSeed.push({ package_id: package_ids[285], effective_date: 1631145600, base_price: 2000, weekend_price: 2200 })
  pricesSeed.push({ package_id: package_ids[286], effective_date: 1622851200, base_price: 1000, weekend_price: 1200 })
  pricesSeed.push({ package_id: package_ids[287], effective_date: 1614556800, base_price: 1000, weekend_price: 1200 })
  pricesSeed.push({ package_id: package_ids[288], effective_date: 1614556800, base_price: 5000, weekend_price: 5200 })
  pricesSeed.push({ package_id: package_ids[289], effective_date: 1620172800, base_price: 5000, weekend_price: 5200 })
  pricesSeed.push({ package_id: package_ids[290], effective_date: 1623024000, base_price: 7000, weekend_price: 7200 })
  pricesSeed.push({ package_id: package_ids[291], effective_date: 1622592000, base_price: 7000, weekend_price: 7200 })
  pricesSeed.push({ package_id: package_ids[292], effective_date: 1628294400, base_price: 2000, weekend_price: 2200 })
  pricesSeed.push({ package_id: package_ids[293], effective_date: 1628035200, base_price: 2000, weekend_price: 2200 })
  pricesSeed.push({ package_id: package_ids[294], effective_date: 1625875200, base_price: 8000, weekend_price: 8200 })
  pricesSeed.push({ package_id: package_ids[295], effective_date: 1625356800, base_price: 8000, weekend_price: 8200 })
  pricesSeed.push({ package_id: package_ids[296], effective_date: 1633737600, base_price: 3000, weekend_price: 3200 })
  pricesSeed.push({ package_id: package_ids[297], effective_date: 1620259200, base_price: 3000, weekend_price: 3200 })
  pricesSeed.push({ package_id: package_ids[298], effective_date: 1617408000, base_price: 4000, weekend_price: 4200 })
  pricesSeed.push({ package_id: package_ids[299], effective_date: 1610150400, base_price: 4000, weekend_price: 4200 })
  pricesSeed.push({ package_id: package_ids[300], effective_date: 1615161600, base_price: 6000, weekend_price: 6200 })
  pricesSeed.push({ package_id: package_ids[301], effective_date: 1614902400, base_price: 6000, weekend_price: 6200 })
  pricesSeed.push({ package_id: package_ids[302], effective_date: 1628035200, base_price: 3000, weekend_price: 3200 })
  pricesSeed.push({ package_id: package_ids[303], effective_date: 1615334400, base_price: 3000, weekend_price: 3200 })
  pricesSeed.push({ package_id: package_ids[304], effective_date: 1614556800, base_price: 2000, weekend_price: 2200 })
  pricesSeed.push({ package_id: package_ids[305], effective_date: 1633564800, base_price: 2000, weekend_price: 2200 })
  pricesSeed.push({ package_id: package_ids[306], effective_date: 1633651200, base_price: 4000, weekend_price: 4200 })
  pricesSeed.push({ package_id: package_ids[307], effective_date: 1628467200, base_price: 4000, weekend_price: 4200 })
  pricesSeed.push({ package_id: package_ids[308], effective_date: 1612224000, base_price: 6000, weekend_price: 6200 })
  pricesSeed.push({ package_id: package_ids[309], effective_date: 1633651200, base_price: 6000, weekend_price: 6200 })
  pricesSeed.push({ package_id: package_ids[310], effective_date: 1628467200, base_price: 5000, weekend_price: 5200 })
  pricesSeed.push({ package_id: package_ids[311], effective_date: 1614816000, base_price: 5000, weekend_price: 5200 })
  pricesSeed.push({ package_id: package_ids[312], effective_date: 1619913600, base_price: 4000, weekend_price: 4200 })
  pricesSeed.push({ package_id: package_ids[313], effective_date: 1612828800, base_price: 4000, weekend_price: 4200 })
  pricesSeed.push({ package_id: package_ids[314], effective_date: 1633564800, base_price: 9000, weekend_price: 9200 })
  pricesSeed.push({ package_id: package_ids[315], effective_date: 1622505600, base_price: 9000, weekend_price: 9200 })
  pricesSeed.push({ package_id: package_ids[316], effective_date: 1609632000, base_price: 10000, weekend_price: 10200 })
  pricesSeed.push({ package_id: package_ids[317], effective_date: 1633392000, base_price: 10000, weekend_price: 10200 })
  pricesSeed.push({ package_id: package_ids[318], effective_date: 1612310400, base_price: 10000, weekend_price: 10200 })
  pricesSeed.push({ package_id: package_ids[319], effective_date: 1625788800, base_price: 10000, weekend_price: 10200 })
  pricesSeed.push({ package_id: package_ids[320], effective_date: 1614988800, base_price: 1000, weekend_price: 1200 })
  pricesSeed.push({ package_id: package_ids[321], effective_date: 1622851200, base_price: 1000, weekend_price: 1200 })
  pricesSeed.push({ package_id: package_ids[322], effective_date: 1614643200, base_price: 7000, weekend_price: 7200 })
  pricesSeed.push({ package_id: package_ids[323], effective_date: 1620432000, base_price: 7000, weekend_price: 7200 })
  pricesSeed.push({ package_id: package_ids[324], effective_date: 1633305600, base_price: 3000, weekend_price: 3200 })
  pricesSeed.push({ package_id: package_ids[325], effective_date: 1633478400, base_price: 3000, weekend_price: 3200 })
  pricesSeed.push({ package_id: package_ids[326], effective_date: 1612483200, base_price: 7000, weekend_price: 7200 })
  pricesSeed.push({ package_id: package_ids[327], effective_date: 1612656000, base_price: 7000, weekend_price: 7200 })
  pricesSeed.push({ package_id: package_ids[328], effective_date: 1625875200, base_price: 8000, weekend_price: 8200 })
  pricesSeed.push({ package_id: package_ids[329], effective_date: 1612137600, base_price: 8000, weekend_price: 8200 })
  pricesSeed.push({ package_id: package_ids[330], effective_date: 1609632000, base_price: 4000, weekend_price: 4200 })
  pricesSeed.push({ package_id: package_ids[331], effective_date: 1609891200, base_price: 4000, weekend_price: 4200 })
  pricesSeed.push({ package_id: package_ids[332], effective_date: 1617580800, base_price: 10000, weekend_price: 10200 })
  pricesSeed.push({ package_id: package_ids[333], effective_date: 1630713600, base_price: 10000, weekend_price: 10200 })
  pricesSeed.push({ package_id: package_ids[334], effective_date: 1617753600, base_price: 8000, weekend_price: 8200 })
  pricesSeed.push({ package_id: package_ids[335], effective_date: 1628553600, base_price: 8000, weekend_price: 8200 })
  pricesSeed.push({ package_id: package_ids[336], effective_date: 1617321600, base_price: 10000, weekend_price: 10200 })
  pricesSeed.push({ package_id: package_ids[337], effective_date: 1633824000, base_price: 10000, weekend_price: 10200 })
  pricesSeed.push({ package_id: package_ids[338], effective_date: 1614902400, base_price: 5000, weekend_price: 5200 })
  pricesSeed.push({ package_id: package_ids[339], effective_date: 1617753600, base_price: 5000, weekend_price: 5200 })
  pricesSeed.push({ package_id: package_ids[340], effective_date: 1630800000, base_price: 4000, weekend_price: 4200 })
  pricesSeed.push({ package_id: package_ids[341], effective_date: 1617235200, base_price: 4000, weekend_price: 4200 })
  pricesSeed.push({ package_id: package_ids[342], effective_date: 1623110400, base_price: 2000, weekend_price: 2200 })
  pricesSeed.push({ package_id: package_ids[343], effective_date: 1618012800, base_price: 2000, weekend_price: 2200 })
  pricesSeed.push({ package_id: package_ids[344], effective_date: 1633478400, base_price: 6000, weekend_price: 6200 })
  pricesSeed.push({ package_id: package_ids[345], effective_date: 1633132800, base_price: 6000, weekend_price: 6200 })
  pricesSeed.push({ package_id: package_ids[346], effective_date: 1614556800, base_price: 2000, weekend_price: 2200 })
  pricesSeed.push({ package_id: package_ids[347], effective_date: 1609804800, base_price: 2000, weekend_price: 2200 })
  pricesSeed.push({ package_id: package_ids[348], effective_date: 1622764800, base_price: 4000, weekend_price: 4200 })
  pricesSeed.push({ package_id: package_ids[349], effective_date: 1612396800, base_price: 4000, weekend_price: 4200 })
  pricesSeed.push({ package_id: package_ids[350], effective_date: 1627862400, base_price: 7000, weekend_price: 7200 })
  pricesSeed.push({ package_id: package_ids[351], effective_date: 1620432000, base_price: 7000, weekend_price: 7200 })
  pricesSeed.push({ package_id: package_ids[352], effective_date: 1609545600, base_price: 7000, weekend_price: 7200 })
  pricesSeed.push({ package_id: package_ids[353], effective_date: 1620432000, base_price: 7000, weekend_price: 7200 })
  pricesSeed.push({ package_id: package_ids[354], effective_date: 1633219200, base_price: 4000, weekend_price: 4200 })
  pricesSeed.push({ package_id: package_ids[355], effective_date: 1633651200, base_price: 4000, weekend_price: 4200 })
  pricesSeed.push({ package_id: package_ids[356], effective_date: 1622851200, base_price: 6000, weekend_price: 6200 })
  pricesSeed.push({ package_id: package_ids[357], effective_date: 1617408000, base_price: 6000, weekend_price: 6200 })
  pricesSeed.push({ package_id: package_ids[358], effective_date: 1630800000, base_price: 5000, weekend_price: 5200 })
  pricesSeed.push({ package_id: package_ids[359], effective_date: 1628035200, base_price: 5000, weekend_price: 5200 })
  pricesSeed.push({ package_id: package_ids[360], effective_date: 1630800000, base_price: 10000, weekend_price: 10200 })
  pricesSeed.push({ package_id: package_ids[361], effective_date: 1612569600, base_price: 10000, weekend_price: 10200 })
  pricesSeed.push({ package_id: package_ids[362], effective_date: 1620000000, base_price: 2000, weekend_price: 2200 })
  pricesSeed.push({ package_id: package_ids[363], effective_date: 1615334400, base_price: 2000, weekend_price: 2200 })
  pricesSeed.push({ package_id: package_ids[364], effective_date: 1612742400, base_price: 2000, weekend_price: 2200 })
  pricesSeed.push({ package_id: package_ids[365], effective_date: 1628121600, base_price: 2000, weekend_price: 2200 })
  pricesSeed.push({ package_id: package_ids[366], effective_date: 1617753600, base_price: 5000, weekend_price: 5200 })
  pricesSeed.push({ package_id: package_ids[367], effective_date: 1610064000, base_price: 5000, weekend_price: 5200 })
  pricesSeed.push({ package_id: package_ids[368], effective_date: 1623024000, base_price: 7000, weekend_price: 7200 })
  pricesSeed.push({ package_id: package_ids[369], effective_date: 1618012800, base_price: 7000, weekend_price: 7200 })
  pricesSeed.push({ package_id: package_ids[370], effective_date: 1623196800, base_price: 9000, weekend_price: 9200 })
  pricesSeed.push({ package_id: package_ids[371], effective_date: 1612742400, base_price: 9000, weekend_price: 9200 })
  pricesSeed.push({ package_id: package_ids[372], effective_date: 1617667200, base_price: 1000, weekend_price: 1200 })
  pricesSeed.push({ package_id: package_ids[373], effective_date: 1630886400, base_price: 1000, weekend_price: 1200 })
  pricesSeed.push({ package_id: package_ids[374], effective_date: 1620000000, base_price: 9000, weekend_price: 9200 })
  pricesSeed.push({ package_id: package_ids[375], effective_date: 1612137600, base_price: 9000, weekend_price: 9200 })
  pricesSeed.push({ package_id: package_ids[376], effective_date: 1612742400, base_price: 5000, weekend_price: 5200 })
  pricesSeed.push({ package_id: package_ids[377], effective_date: 1630540800, base_price: 5000, weekend_price: 5200 })
  pricesSeed.push({ package_id: package_ids[378], effective_date: 1609891200, base_price: 1000, weekend_price: 1200 })
  pricesSeed.push({ package_id: package_ids[379], effective_date: 1623196800, base_price: 1000, weekend_price: 1200 })
  pricesSeed.push({ package_id: package_ids[380], effective_date: 1630713600, base_price: 4000, weekend_price: 4200 })
  pricesSeed.push({ package_id: package_ids[381], effective_date: 1612396800, base_price: 4000, weekend_price: 4200 })
  pricesSeed.push({ package_id: package_ids[382], effective_date: 1612224000, base_price: 4000, weekend_price: 4200 })
  pricesSeed.push({ package_id: package_ids[383], effective_date: 1620345600, base_price: 4000, weekend_price: 4200 })
  pricesSeed.push({ package_id: package_ids[384], effective_date: 1625788800, base_price: 6000, weekend_price: 6200 })
  pricesSeed.push({ package_id: package_ids[385], effective_date: 1622851200, base_price: 6000, weekend_price: 6200 })
  pricesSeed.push({ package_id: package_ids[386], effective_date: 1614988800, base_price: 8000, weekend_price: 8200 })
  pricesSeed.push({ package_id: package_ids[387], effective_date: 1614902400, base_price: 8000, weekend_price: 8200 })
  pricesSeed.push({ package_id: package_ids[388], effective_date: 1630627200, base_price: 6000, weekend_price: 6200 })
  pricesSeed.push({ package_id: package_ids[389], effective_date: 1625616000, base_price: 6000, weekend_price: 6200 })
  pricesSeed.push({ package_id: package_ids[390], effective_date: 1609804800, base_price: 8000, weekend_price: 8200 })
  pricesSeed.push({ package_id: package_ids[391], effective_date: 1627948800, base_price: 8000, weekend_price: 8200 })
  pricesSeed.push({ package_id: package_ids[392], effective_date: 1619913600, base_price: 10000, weekend_price: 10200 })
  pricesSeed.push({ package_id: package_ids[393], effective_date: 1625356800, base_price: 10000, weekend_price: 10200 })
  pricesSeed.push({ package_id: package_ids[394], effective_date: 1622764800, base_price: 9000, weekend_price: 9200 })
  pricesSeed.push({ package_id: package_ids[395], effective_date: 1612137600, base_price: 9000, weekend_price: 9200 })
  pricesSeed.push({ package_id: package_ids[396], effective_date: 1633132800, base_price: 10000, weekend_price: 10200 })
  pricesSeed.push({ package_id: package_ids[397], effective_date: 1612310400, base_price: 10000, weekend_price: 10200 })
  pricesSeed.push({ package_id: package_ids[398], effective_date: 1627776000, base_price: 6000, weekend_price: 6200 })
  pricesSeed.push({ package_id: package_ids[399], effective_date: 1622937600, base_price: 6000, weekend_price: 6200 })
  pricesSeed.push({ package_id: package_ids[400], effective_date: 1619913600, base_price: 3000, weekend_price: 3200 })
  pricesSeed.push({ package_id: package_ids[401], effective_date: 1622505600, base_price: 3000, weekend_price: 3200 })
  pricesSeed.push({ package_id: package_ids[402], effective_date: 1617235200, base_price: 9000, weekend_price: 9200 })
  pricesSeed.push({ package_id: package_ids[403], effective_date: 1617235200, base_price: 9000, weekend_price: 9200 })
  pricesSeed.push({ package_id: package_ids[404], effective_date: 1628208000, base_price: 1000, weekend_price: 1200 })
  pricesSeed.push({ package_id: package_ids[405], effective_date: 1609804800, base_price: 1000, weekend_price: 1200 })
  pricesSeed.push({ package_id: package_ids[406], effective_date: 1628035200, base_price: 1000, weekend_price: 1200 })
  pricesSeed.push({ package_id: package_ids[407], effective_date: 1631145600, base_price: 1000, weekend_price: 1200 })
  pricesSeed.push({ package_id: package_ids[408], effective_date: 1630454400, base_price: 8000, weekend_price: 8200 })
  pricesSeed.push({ package_id: package_ids[409], effective_date: 1612742400, base_price: 8000, weekend_price: 8200 })
  pricesSeed.push({ package_id: package_ids[410], effective_date: 1609632000, base_price: 3000, weekend_price: 3200 })
  pricesSeed.push({ package_id: package_ids[411], effective_date: 1617753600, base_price: 3000, weekend_price: 3200 })
  pricesSeed.push({ package_id: package_ids[412], effective_date: 1630540800, base_price: 9000, weekend_price: 9200 })
  pricesSeed.push({ package_id: package_ids[413], effective_date: 1628121600, base_price: 9000, weekend_price: 9200 })
  pricesSeed.push({ package_id: package_ids[414], effective_date: 1628553600, base_price: 2000, weekend_price: 2200 })
  pricesSeed.push({ package_id: package_ids[415], effective_date: 1612483200, base_price: 2000, weekend_price: 2200 })
  pricesSeed.push({ package_id: package_ids[416], effective_date: 1609718400, base_price: 5000, weekend_price: 5200 })
  pricesSeed.push({ package_id: package_ids[417], effective_date: 1609977600, base_price: 5000, weekend_price: 5200 })
  pricesSeed.push({ package_id: package_ids[418], effective_date: 1614816000, base_price: 8000, weekend_price: 8200 })
  pricesSeed.push({ package_id: package_ids[419], effective_date: 1622937600, base_price: 8000, weekend_price: 8200 })
  pricesSeed.push({ package_id: package_ids[420], effective_date: 1617321600, base_price: 2000, weekend_price: 2200 })
  pricesSeed.push({ package_id: package_ids[421], effective_date: 1614816000, base_price: 2000, weekend_price: 2200 })
  pricesSeed.push({ package_id: package_ids[422], effective_date: 1633219200, base_price: 2000, weekend_price: 2200 })
  pricesSeed.push({ package_id: package_ids[423], effective_date: 1620172800, base_price: 2000, weekend_price: 2200 })
  pricesSeed.push({ package_id: package_ids[424], effective_date: 1627862400, base_price: 7000, weekend_price: 7200 })
  pricesSeed.push({ package_id: package_ids[425], effective_date: 1623110400, base_price: 7000, weekend_price: 7200 })
  pricesSeed.push({ package_id: package_ids[426], effective_date: 1620345600, base_price: 7000, weekend_price: 7200 })
  pricesSeed.push({ package_id: package_ids[427], effective_date: 1625788800, base_price: 7000, weekend_price: 7200 })
  pricesSeed.push({ package_id: package_ids[428], effective_date: 1612396800, base_price: 4000, weekend_price: 4200 })
  pricesSeed.push({ package_id: package_ids[429], effective_date: 1628208000, base_price: 4000, weekend_price: 4200 })
  pricesSeed.push({ package_id: package_ids[430], effective_date: 1619913600, base_price: 8000, weekend_price: 8200 })
  pricesSeed.push({ package_id: package_ids[431], effective_date: 1633564800, base_price: 8000, weekend_price: 8200 })
  pricesSeed.push({ package_id: package_ids[432], effective_date: 1618012800, base_price: 9000, weekend_price: 9200 })
  pricesSeed.push({ package_id: package_ids[433], effective_date: 1617235200, base_price: 9000, weekend_price: 9200 })
  pricesSeed.push({ package_id: package_ids[434], effective_date: 1623283200, base_price: 8000, weekend_price: 8200 })
  pricesSeed.push({ package_id: package_ids[435], effective_date: 1627948800, base_price: 8000, weekend_price: 8200 })
  pricesSeed.push({ package_id: package_ids[436], effective_date: 1622764800, base_price: 6000, weekend_price: 6200 })
  pricesSeed.push({ package_id: package_ids[437], effective_date: 1617667200, base_price: 6000, weekend_price: 6200 })
  pricesSeed.push({ package_id: package_ids[438], effective_date: 1623196800, base_price: 4000, weekend_price: 4200 })
  pricesSeed.push({ package_id: package_ids[439], effective_date: 1612224000, base_price: 4000, weekend_price: 4200 })
  pricesSeed.push({ package_id: package_ids[440], effective_date: 1614816000, base_price: 1000, weekend_price: 1200 })
  pricesSeed.push({ package_id: package_ids[441], effective_date: 1612396800, base_price: 1000, weekend_price: 1200 })
  pricesSeed.push({ package_id: package_ids[442], effective_date: 1612828800, base_price: 10000, weekend_price: 10200 })
  pricesSeed.push({ package_id: package_ids[443], effective_date: 1619913600, base_price: 10000, weekend_price: 10200 })
  pricesSeed.push({ package_id: package_ids[444], effective_date: 1617926400, base_price: 4000, weekend_price: 4200 })
  pricesSeed.push({ package_id: package_ids[445], effective_date: 1630540800, base_price: 4000, weekend_price: 4200 })
  pricesSeed.push({ package_id: package_ids[446], effective_date: 1622678400, base_price: 4000, weekend_price: 4200 })
  pricesSeed.push({ package_id: package_ids[447], effective_date: 1609459200, base_price: 4000, weekend_price: 4200 })
  pricesSeed.push({ package_id: package_ids[448], effective_date: 1631145600, base_price: 10000, weekend_price: 10200 })
  pricesSeed.push({ package_id: package_ids[449], effective_date: 1617494400, base_price: 10000, weekend_price: 10200 })
  pricesSeed.push({ package_id: package_ids[450], effective_date: 1625529600, base_price: 8000, weekend_price: 8200 })
  pricesSeed.push({ package_id: package_ids[451], effective_date: 1620432000, base_price: 8000, weekend_price: 8200 })
  pricesSeed.push({ package_id: package_ids[452], effective_date: 1623110400, base_price: 4000, weekend_price: 4200 })
  pricesSeed.push({ package_id: package_ids[453], effective_date: 1625270400, base_price: 4000, weekend_price: 4200 })
  pricesSeed.push({ package_id: package_ids[454], effective_date: 1612915200, base_price: 7000, weekend_price: 7200 })
  pricesSeed.push({ package_id: package_ids[455], effective_date: 1609545600, base_price: 7000, weekend_price: 7200 })
  pricesSeed.push({ package_id: package_ids[456], effective_date: 1620172800, base_price: 3000, weekend_price: 3200 })
  pricesSeed.push({ package_id: package_ids[457], effective_date: 1612742400, base_price: 3000, weekend_price: 3200 })
  pricesSeed.push({ package_id: package_ids[458], effective_date: 1631232000, base_price: 2000, weekend_price: 2200 })
  pricesSeed.push({ package_id: package_ids[459], effective_date: 1617926400, base_price: 2000, weekend_price: 2200 })
  pricesSeed.push({ package_id: package_ids[460], effective_date: 1617235200, base_price: 7000, weekend_price: 7200 })
  pricesSeed.push({ package_id: package_ids[461], effective_date: 1620172800, base_price: 7000, weekend_price: 7200 })
  pricesSeed.push({ package_id: package_ids[462], effective_date: 1622592000, base_price: 7000, weekend_price: 7200 })
  pricesSeed.push({ package_id: package_ids[463], effective_date: 1609977600, base_price: 7000, weekend_price: 7200 })
  pricesSeed.push({ package_id: package_ids[464], effective_date: 1618012800, base_price: 6000, weekend_price: 6200 })
  pricesSeed.push({ package_id: package_ids[465], effective_date: 1622678400, base_price: 6000, weekend_price: 6200 })
  pricesSeed.push({ package_id: package_ids[466], effective_date: 1622505600, base_price: 5000, weekend_price: 5200 })
  pricesSeed.push({ package_id: package_ids[467], effective_date: 1633564800, base_price: 5000, weekend_price: 5200 })
  pricesSeed.push({ package_id: package_ids[468], effective_date: 1625788800, base_price: 6000, weekend_price: 6200 })
  pricesSeed.push({ package_id: package_ids[469], effective_date: 1622592000, base_price: 6000, weekend_price: 6200 })
  pricesSeed.push({ package_id: package_ids[470], effective_date: 1612310400, base_price: 1000, weekend_price: 1200 })
  pricesSeed.push({ package_id: package_ids[471], effective_date: 1612310400, base_price: 1000, weekend_price: 1200 })
  pricesSeed.push({ package_id: package_ids[472], effective_date: 1620518400, base_price: 6000, weekend_price: 6200 })
  pricesSeed.push({ package_id: package_ids[473], effective_date: 1630627200, base_price: 6000, weekend_price: 6200 })
  pricesSeed.push({ package_id: package_ids[474], effective_date: 1612396800, base_price: 10000, weekend_price: 10200 })
  pricesSeed.push({ package_id: package_ids[475], effective_date: 1630540800, base_price: 10000, weekend_price: 10200 })
  pricesSeed.push({ package_id: package_ids[476], effective_date: 1625270400, base_price: 5000, weekend_price: 5200 })
  pricesSeed.push({ package_id: package_ids[477], effective_date: 1625616000, base_price: 5000, weekend_price: 5200 })
  pricesSeed.push({ package_id: package_ids[478], effective_date: 1617667200, base_price: 2000, weekend_price: 2200 })
  pricesSeed.push({ package_id: package_ids[479], effective_date: 1625356800, base_price: 2000, weekend_price: 2200 })
  pricesSeed.push({ package_id: package_ids[480], effective_date: 1627862400, base_price: 10000, weekend_price: 10200 })
  pricesSeed.push({ package_id: package_ids[481], effective_date: 1622678400, base_price: 10000, weekend_price: 10200 })
  pricesSeed.push({ package_id: package_ids[482], effective_date: 1609718400, base_price: 7000, weekend_price: 7200 })
  pricesSeed.push({ package_id: package_ids[483], effective_date: 1633737600, base_price: 7000, weekend_price: 7200 })
  pricesSeed.push({ package_id: package_ids[484], effective_date: 1633824000, base_price: 10000, weekend_price: 10200 })
  pricesSeed.push({ package_id: package_ids[485], effective_date: 1630972800, base_price: 10000, weekend_price: 10200 })
  pricesSeed.push({ package_id: package_ids[486], effective_date: 1633046400, base_price: 7000, weekend_price: 7200 })
  pricesSeed.push({ package_id: package_ids[487], effective_date: 1627948800, base_price: 7000, weekend_price: 7200 })
  pricesSeed.push({ package_id: package_ids[488], effective_date: 1622851200, base_price: 6000, weekend_price: 6200 })
  pricesSeed.push({ package_id: package_ids[489], effective_date: 1625529600, base_price: 6000, weekend_price: 6200 })
  pricesSeed.push({ package_id: package_ids[490], effective_date: 1618012800, base_price: 7000, weekend_price: 7200 })
  pricesSeed.push({ package_id: package_ids[491], effective_date: 1633305600, base_price: 7000, weekend_price: 7200 })





  await insertSeed(knex, 'prices', pricesSeed);
  await insertSeed(knex, 'time_sections', timeSectionsSeed);
};
