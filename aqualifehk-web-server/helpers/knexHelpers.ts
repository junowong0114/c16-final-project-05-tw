import { Knex } from "knex";
import { DB, BookingStatus, DTO } from "shared";

export async function insertSeed(knex: Knex, table: string, rows: object[], returning: string | string[] = '') {
  let row = await knex(table).count('* as count').first();

  if (row && row.count > 0) {
    console.log(`Table ${table} already has data inside.`);
    return await knex.select('id').from(table);
  }

  const returnedData = await knex(table).insert(rows).returning(returning);
  return returnedData;
}

export function randomHKphone(beginNum: number) {
  let phone = beginNum.toString()
  for (let i = 0; i < 7; i++) {
    phone += Math.floor(Math.random() * 10)
  }
  return phone;
}

export async function getBookings(user_id: number, knex:Knex, inCart:boolean){


  let bookingStatus;
  if(inCart){
    bookingStatus = BookingStatus.inCart.toString()
  }else{
    bookingStatus = BookingStatus.confirmed.toString(),BookingStatus.pending.toString(),BookingStatus.canceled.toString()
  }

  let bookingsQuery: any = knex('bookings')
    .select(knex.raw(`*, bookings.id, address, activities.name as activity_name`))
    .join("activities", "bookings.activity_id", "activities.id")
    .innerJoin('activity_images', 'activities.id', 'activity_images.activity_id')
    .where({ user_id, status: bookingStatus })

  let bookingsPackagesQuery: Knex.QueryBuilder<DB.BookingPackage, DB.BookingPackage[]> = knex('bookings_packages')
    .select('*')
    .from('bookings_packages')
    .join('bookings', 'bookings.id', 'bookings_packages.booking_id')
    .innerJoin('time_sections', 'time_sections.id', 'bookings_packages.time_section_id')
    .where({ user_id, status: bookingStatus })

  let packageQuery: any = knex('packages')
    .select('*', "packages.id")
    .from('packages')
    .innerJoin(
      'bookings_packages',
      'bookings_packages.package_id',
      'packages.id',
    )
    .innerJoin('bookings', 'bookings.id', 'bookings_packages.booking_id')
    .where({ user_id })

  let [bookingList, bookingPackageList, packageList] = await Promise.all([
    bookingsQuery,
    bookingsPackagesQuery,
    packageQuery
  ])


  let bookingDict = Object.fromEntries(
    bookingList.map(function(row:any) {
      if (!row.id) throw new Error('cartService: Booking ID not found in query result.')
      let booking: any = {
        id: row.id,
        userId: row.user_id,
        activityName: row.activity_name,
        address: row.address,
        latLong: row.latlong,
        unix_time: row.activity_date,
        totalPrice: row.total_price,
        packages: [],
        status: row.status,
        userRequirement: row.user_requirements,
        image: row.url
      }
      return [row.id, booking]
    }),
  )
  
  let packageDict = Object.fromEntries(
    packageList.map(function (row: any) {
        if (!row.id)
          throw new Error('cartService: Package ID not found in query result.');
        return [row.id, row];
      })
  )
  
  bookingPackageList.forEach(row => {
    if (!bookingDict[row.booking_id]) throw new Error(`cartService: Booking ID ${row.booking_id} does not exist`)
    let booking = bookingDict[row.booking_id]
    let package_ = packageDict[row.package_id]

    if (!row.id) throw new Error('cartService: PackageBooking ID not found in query result.')
    booking.packages.push({
      id: row.package_id,
      name: package_.name,
      unit_price: row.unit_price,
      quantity: row.quantity,
      timeSection: row.section,
      timeSectionId: row.time_section_id,
    })
  })

  
  const bookings: DTO.Booking[] = [];
  for (let id in bookingDict) {
    bookings.push(bookingDict[id])
  }
  return bookings;
}