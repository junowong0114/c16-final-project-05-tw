export function errorToString(error: unknown): string {
  return (error as Error).toString()
}

export function jsUnixToPostgresUnix(jsUnix: number) {
  const date = new Date(jsUnix)
  return Math.floor(date.setHours(8, 0, 0, 0) / 1000)
}