import { Request, Response, NextFunction } from 'express';
import { Bearer } from 'permit';
import jwtSimple from 'jwt-simple';
import { jwtConfig } from './jwt';
import { JWTPayload } from './types';
import { errorToString } from './format';

const permit = new Bearer({
  query: "access_token"
});

function retrieveToken(req: Request) {
  let token: string
  try {
    token = permit.check(req);
  } catch (error) { 
    throw new Error('failed to decode Bearer token in req')
  }

  if (!token) {
    throw new Error('missing Bearer token in req')
  }
  
  return token
}

function retrievePayload(token: string) {
  let payload: JWTPayload;
  try {
    payload = jwtSimple.decode(token, jwtConfig.SECRET);
  } catch (error) {
    throw new Error('failed to decode JWT token in req')
  }

  return payload
}

export function requireJWT(req: Request, res: Response, next: NextFunction) {
  let token: string
  let payload: JWTPayload

  try {
    token = retrieveToken(req)
    payload = retrievePayload(token)
  } catch (error) {
    res.status(401).json({
      success: false,
      error: errorToString(error)
    })
    return
  }

  req.jwtPayload = payload;
  next();
}

// TODO
export function requireLogin(req: Request, res: Response, next: NextFunction) {
  let token: string
  let payload: JWTPayload

  try {
    token = retrieveToken(req)
    payload = retrievePayload(token)
  } catch (error) {
    res.status(401).json({
      success: false,
      error: errorToString(error)
    })
    return
  }

  // TODO query UserService to check if payload.email exits + if user type is "user" 

  req.jwtPayload = payload;
  next()
}