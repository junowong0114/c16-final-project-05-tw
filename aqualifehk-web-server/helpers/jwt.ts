let JWT_SECRET = process.env.JWT_SECRET

if (!JWT_SECRET) {
  throw new Error('missing JWT_SECRET in process.env')
}

export let jwtConfig = {
  SECRET: JWT_SECRET,
}
