import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable('time_section_availability', (table) => {
        table.increments();
        table.integer('activity_id').unsigned();
        table.foreign('activity_id').references('id').inTable('activities')
        table.integer('package_id').unsigned();
        table.foreign('package_id').references('id').inTable('packages')
        table.integer('time_section_id').unsigned();
        table.foreign('time_section_id').references('id').inTable('time_sections')
        table.integer('date')
        table.integer('capacity')
        table.integer('remain')
        table.timestamps(false,true)
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists('time_section_availability');
}

