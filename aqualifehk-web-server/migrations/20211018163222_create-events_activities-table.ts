import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable('events_activities', (table) => {
        table.increments();
        table.integer('event_id').unsigned();
        table.foreign('event_id').references('id').inTable('events')
        table.integer('activity_id').unsigned();
        table.foreign('activity_id').references('id').inTable('activities')
        table.timestamps(false,true)
})
}

export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists('events_activities');
}

