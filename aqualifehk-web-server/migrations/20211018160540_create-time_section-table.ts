import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable('time_sections', (table) => {
        table.increments();
        table.string('section').notNullable();
        table.integer('package_id').unsigned()
        table.foreign('package_id').references('id').inTable('packages')
        table.timestamps(false,true)
})
}

export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists('time_sections');
}

