import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable('bookings_packages', (table) => {
        table.increments();
        table.integer('booking_id').unsigned()
        table.foreign('booking_id').references('id').inTable('bookings')
        table.integer('package_id').unsigned()
        table.foreign('package_id').references('id').inTable('packages')
        table.integer('time_section_id').unsigned()
        table.foreign('time_section_id').references('id').inTable('time_sections')
        table.integer('unit_price').unsigned().notNullable()
        table.integer('quantity')
        table.timestamps(false,true)
    })
}

export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists('bookings_packages');
}

