import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable('reviews', (table) => {
        table.increments();
        table.integer('user_id').unsigned()
        table.foreign('user_id').references('id').inTable('users')
        table.integer('activity_id').unsigned()
        table.foreign('activity_id').references('id').inTable('activities')
        table.integer('stars').notNullable();
        table.text('comment')
        table.timestamps(false,true)
})
}

export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists('reviews');
}

