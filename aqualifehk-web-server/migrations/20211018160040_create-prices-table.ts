import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable('prices', (table) => {
        table.increments();
        table.integer('package_id').unsigned();
        table.foreign('package_id').references('id').inTable('packages');
        table.integer('effective_date')
        table.integer('expiration_date')
        table.integer('base_price')
        table.integer('weekend_price')
        table.timestamps(false,true)
})
}

export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists('prices');
}

