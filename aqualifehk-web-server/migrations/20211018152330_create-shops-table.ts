import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable('shops', (table) => {
        table.increments();
        table.integer('user_id').unsigned();
        table.foreign('user_id').references('id').inTable('users')
        table.string('name').notNullable();
        table.string('phone');
        table.string('email');
        table.text('address');
        table.timestamps(false,true);
    })
}

export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists('shops');
}

