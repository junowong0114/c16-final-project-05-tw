import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable('categories', (table) => {
        table.increments();
        table.string('name').notNullable();
        table.timestamps(false,true)
})
}

export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists('categories');
}

