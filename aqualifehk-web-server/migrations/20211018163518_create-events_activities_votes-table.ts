import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable('events_activities_votes', (table) => {
        table.increments();
        table.integer('voter_id').unsigned();
        table.foreign('voter_id').references('id').inTable('users')
        table.integer('events_activities_id').unsigned();
        table.foreign('events_activities_id').references('id').inTable('events_activities')
        table.timestamps(false,true)
})
}

export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists('events_activities_votes');
}

