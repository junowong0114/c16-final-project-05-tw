import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable('activities', (table) => {
        table.increments();
        table.integer('shop_id').unsigned();
        table.foreign('shop_id').references('id').inTable('shops')
        table.integer('category_id').unsigned();
        table.foreign('category_id').references('id').inTable('categories')
        table.string('name').notNullable();
        table.text('address')
        table.string('latlong')
        table.timestamps(false,true)
})
}

export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists('activities');
}

