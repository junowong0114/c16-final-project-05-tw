import { Knex } from "knex";
import { BOOKING_STATUSES } from "shared";


export async function up(knex: Knex): Promise<void> {
  await knex.schema.createTable('bookings', (table) => {
    table.increments();
    table.integer('user_id').unsigned();
    table.foreign('user_id').references('id').inTable('users')
    table.integer('activity_id').unsigned();
    table.foreign('activity_id').references('id').inTable('activities')
    table.enum('status', BOOKING_STATUSES, { useNative: true, enumName: 'booking_status'})
    table.integer('activity_date').notNullable();
    table.integer('booking_date');
    table.integer('total_price').notNullable();
    table.text('user_requirements')
    table.timestamps(false, true);
  })
}


export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTableIfExists('bookings');
  await knex.raw('DROP TYPE IF EXISTS booking_status')
}

