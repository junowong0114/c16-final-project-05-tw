import { Knex } from "knex";
import { USER_TYPES } from "shared";

export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable('users', (table) => {
        table.increments();
        table.string('username')
        table.string('first_name')
        table.string('last_name')
        table.string('password_hash')
        table.string('gender')      
        table.string('phone')
        table.string('email')
        table.string('facebook')
        table.string('google')
        table.text('url')
        table.enum('type', USER_TYPES, { useNative: true, enumName: 'user_type' })
        table.timestamps(false, true)
    });
}

export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists('users')
    await knex.raw('DROP TYPE IF EXISTS user_type')
}