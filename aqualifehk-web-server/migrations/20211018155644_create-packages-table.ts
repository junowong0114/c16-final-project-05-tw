import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable('packages', (table) => {
        table.increments();
        table.integer('activity_id').unsigned();
        table.foreign('activity_id').references('id').inTable('activities');
        table.string('name').notNullable();
        table.text('inclusion')
        table.text('eligibility')
        table.text('description')
        table.integer('capacity')
        table.timestamps(false,true)
})
}

export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists('packages');
}

