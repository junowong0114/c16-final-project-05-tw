import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable('events_times', (table) => {
        table.increments();
        table.integer('event_id').unsigned();
        table.foreign('event_id').references('id').inTable('events')
        table.integer('start_time_unix')
        table.integer('end_time_unix')
        table.timestamps(false,true)
})
}

export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists('events_times');
}

