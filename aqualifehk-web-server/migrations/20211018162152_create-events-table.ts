import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable('events', (table) => {
        table.increments()
        table.string('name')
        table.integer('owner_id').unsigned()
        table.foreign('owner_id').references('id').inTable('users')
        table.integer('unix_confirmed_date')
        table.string('invite_code')
        table.timestamps(false, true)
    })

    await knex.schema.createTable('events_users', (table) => {
      table.increments()
      table.integer('event_id').unsigned()
      table.foreign('event_id').references('id').inTable('events')
      table.integer('user_id').unsigned()
      table.foreign('user_id').references('id').inTable('users')
      table.timestamps(false, true)
    })
}

export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists('events_users')
    await knex.schema.dropTableIfExists('events');
}

