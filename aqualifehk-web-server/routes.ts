import express from 'express';
import { requireJWT } from './helpers/guard';
import { bookingController, 
  bookmarksController, 
  cartController, 
  orderController, 
  orderDetailsController, 
  searchController, 
  userController, 
  eventController,
} from './server';

export const routes = express.Router();

routes.post('/loginPassword', userController.createTokenWithPassword);
routes.get('/getNonRegisteredToken', userController.createTokenForNonRegistered);
routes.get('/userInfo', requireJWT, userController.getUserInfo)
routes.post('/registerUser',requireJWT,userController.registerUser)

routes.get('/activityInfo/:id', bookingController.getActivityInfo)
routes.get('/packageAvailability',bookingController.getPackageAvailability)
routes.post('/booking', requireJWT, bookingController.postBooking);
routes.post('/bookingFromCart', requireJWT, bookingController.postBookingFromCart);

routes.get('/order', requireJWT, orderController.loadOrder);
routes.get('/orderDetails/:id', requireJWT, orderDetailsController.loadOrderDetails);


routes.get('/cart/all', cartController.loadCart);
routes.delete('/cart/:id', requireJWT, cartController.deleteItem);
routes.get('/cart', requireJWT, cartController.loadCart);
routes.post('/cart', requireJWT, cartController.addItem);
routes.get('/bookingOptions/:bookingId', requireJWT, cartController.getSingleItem);
routes.put('/cart', requireJWT, cartController.updateCartItem);

routes.post('/bookmarks', requireJWT, bookmarksController.addBookmark);
routes.delete('/bookmarks', requireJWT, bookmarksController.deleteBookmark);
routes.get('/bookmarks', requireJWT, bookmarksController.getBookmark);


routes.get('/homepageInfo', searchController.getHomePage);
routes.get('/search/?', searchController.getSearch);

routes.get('/event', requireJWT, eventController.getEvent)
routes.post('/event', requireJWT, eventController.createEvent)
routes.delete('/event', requireJWT, eventController.deleteEvent)
routes.put('/event/insert_participant', requireJWT, eventController.insertParticipant)
routes.post('/event/activity_vote', requireJWT, eventController.insertActivityVote)
routes.post('/event/time_vote', requireJWT, eventController.insertTimeVote)

//routes.post('/createUser)
/*
  body: {
    email: 'user@gmail.com
  }

  **Controller
  if (body.email) ==> call UserService.createUserWithEmail
  else call UserService.createToken
*/
