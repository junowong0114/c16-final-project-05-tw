# AqualifeHK

#### Video Demo:  
https://www.youtube.com/watch?v=eEJuTJhvkSM&ab_channel=JunoWong
#### Description:  
AquaLifeHK is a mobile app designed for water sports enthusiasts to discover and book water sports in Hong Kong. The built-in event organizer allows users to create and share events with friends.  
The app is built with React, Express, and PostgreSQL. The main development language is TypeScript.  
This project is made in collaboration of my two coding boot-camp classmates [Happy](https://gitlab.com/jasonsew.js) and [Stephen](https://gitlab.com/huilt1232). They didn't enroll to cs50 (and I think they should!).


## Steps to follow during deployment to AWS
### 1. Clone into AWS EC2. Make sure EC2 public key is saved in gitlab.
```cd ~```<br>
```git clone git@gitlab.com:junowong0114/c16-final-project-05-tw.git```  

### 2. Fill up .env files
- ~/c16-final-project-05-tw/.env
    ```
    # info for launching web & pg server
    POSTGRES_DB=
    POSTGRES_USER=
    POSTGRES_PASSWORD=
    ```
- ~/c16-final-project-05-tw/aqualifehk-react/.env
    ```
    PUBLIC_URL=
    NODE_ENV=
    ```
- ~/c16-final-project-05-tw/aqualifehk-web-server/.env
    ```
    # docker container port
    PORT=
    
    # node environment
    NODE_ENV=production
    
    # developement and test pg info
    DB_NAME=
    DB_USERNAME=
    DB_PASSWORD=
    
    # production pg info
    POSTGRES_DB=
    POSTGRES_USER=
    POSTGRES_PASSWORD=
    # postgres server container name
    POSTGRES_HOST=
    ```
    
### 3. Launch docker for web & pg server
```cd ~/c16-final-project-05-tw```  
```./scripts/dev-docker.sh```