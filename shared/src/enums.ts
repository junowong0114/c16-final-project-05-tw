import { enumToArray } from "./helpers";

export enum BookingStatus {
  "confirmed" = 1,
  "inCart",
  "pending",
  "canceled",
}
export const BOOKING_STATUSES = enumToArray(BookingStatus);

export enum ActivityCategory {
  'invalid' = 0,
  'Angling',
  'Banana Boat',
  'Flyboarding',
  'Jet Ski',
  'Kayaking',
  'Kite surfing',
  'Parasailing',
  'Rafting',
  'Snorkeling', 
  'Scuba diving',
  'Surfing',
  'Swimming',
  'Waterskiing',
  'Water polo',
  'Windsurfing',
  'Stand up pedal boarding',
  'Wake boarding',
  'Wake surfing'
}
export const ACTIVITY_CATEGORIES = enumToArray(ActivityCategory);

export enum UserType {
  'admin' = 0,
  'retailer',
  'user',
  'nonRegistered',
}
export const USER_TYPES = enumToArray(UserType);