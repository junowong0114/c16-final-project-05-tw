export type Enum<E> = Record<keyof E, number | string> & { [k: number]: string };

export function enumToArray<E extends Enum<E>>(e: E): string[] {
  return (
    Object.entries(e)
      .filter(value => (typeof value[1]) === "string")
      .map(value => value[0])
  )
}