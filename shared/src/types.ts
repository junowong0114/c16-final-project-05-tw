import { ActivityCategory, BookingStatus } from './enums'

export type JWTPayload = {
  id: number
  email?: string
  isRegistered: boolean
}

export namespace DB {
  export type Booking = {
    id?: number,
    user_id: number,
    activity_id: number,
    activity_date: number,
    booking_date: number,
    total_price: number,
    status: BookingStatus,
    user_requirements: string,
    created_at?: ReturnType<typeof Date.prototype.toISOString>,
    updated_at?: ReturnType<typeof Date.prototype.toISOString>,
  }

  export type BookingPackage = {
    id?: number,
    booking_id: number,
    package_id: number,
    section: string,
    time_section_id: number,
    quantity: number,
    unit_price: number,
  }

  export type Package = {
    id?: number,
    activity_id: number,
    name: string,
    inclusion: string,
    eligibility: string,
    capacity: number,
    description: string,
    created_at?: ReturnType<typeof Date.prototype.toISOString>,
    updated_at?: ReturnType<typeof Date.prototype.toISOString>,
  }

  export type Activity = {
    id?: number,
    shop_id: number,
    category_id: number,
    activity_name: string,
    address: string,
    latlong: string,
    created_at?: ReturnType<typeof Date.prototype.toISOString>,
    updated_at?: ReturnType<typeof Date.prototype.toISOString>,
  }
}

export namespace DTO {
  export type Booking = {
    id: number,
    userId: number,
    activityName: string,
    activityId: number,
    image_urls: (string | undefined)[],
    address?: string,
    latLong: string,
    unix_time: number,
    totalPrice: number,
    packages: BookingPackage[]
    status: BookingStatus,
    userRequirement: string
  }

  export type BookingPackage = {
    packageId: number,
    packageName: string,
    unitPrice: number,
    quantity: number,
    timeSectionId: number,
    timeSectionName: string
  }

  export type SearchActivity = {
    id: number,
    shop_id: number,
    category: ActivityCategory
    name: string,
    location?: string,
    latlong: string,
    rating?: number,
    image_url?: string,
    review_count?: string,
  }


  export type HomepageInfo = {
    recommendations: SearchActivity[],
    whatsNew: SearchActivity[]
  }

  export type Activity = {
    id?: number,
    activity_name: string,
    category_id: ActivityCategory,
    rating: string,
    review_count: number,
    descriptions?: string[],
    activity_images: string[],
    address: string,
    latlong: string,
    shop_id: number,
    packages: Package[],
    created_at?: ReturnType<typeof Date.prototype.toISOString>,
    updated_at?: ReturnType<typeof Date.prototype.toISOString>,
  }

  export type Package = {
    id: number,
    name: string,
    inclusion: string,
    eligibility: string,
    capacity: number,
    time_sections: {
      id: number,
      name: string,
    }[],
    prices: {
      base_price: number,
      weekend_price: number,
      unix_effective_date: number,
      unix_expiration_date: number | null,
    }[]
  }

  export type Availability = {
    package_id: number,
    availability: {
      unix_date: number,
      capacity: number,
      remaining: Record<string, number>
    }[]
  }

  export type EventDetail = {
    event_id: number,
    event_name: string,
    invite_code: string,
    owner_id: number,
    owner_name: {
      first_name: string,
      last_name: string,
    },
    participant_ids: number[],
    participant_names: {
      first_name: string,
      last_name: string,
    }[]
    unix_confirmed_date: number,
    activities: {
      activity_vote_id: number,
      activity_id: number,
      activity_name: string,
      activity_address: string,
      voter_ids: number[],
      voter_names: {
        first_name: string,
        last_name: string,
      }[],
      vote_count: number,
      thumbnail_urls: string[],
    }[],
    times: {
      time_vote_id: number,
      start_time_unix: number,
      end_time_unix: number | null
      voter_ids: number[],
      voter_names: {
        first_name: string,
        last_name: string,
      }[],
      vote_count: number,
    }[],
  }
}

export type order = {
  bookingId: number
  activityName: string
  date: number
  price: number
  location: string
  packages: DTO.BookingPackage[]
  status: BookingStatus
  image: string
}

export type orderDetail = {
  activityName: string,
  status: BookingStatus,
  price: number,
  packageNames: string[],
  dateTime: number,
  timeSections: string[],
  bookingId: number,
  dateOfBook: number,
  bookingRequirement: string,
  quantity: number[],
  image: string,
  error?: string
}

export type shopDetail = {
  name:string,
  phone:string,
  email:string
}


export type bookingPackage = {
  packageId: number,
  timeSectionId: number,
  quantity: number,
  unitPrice: number
}

export type registerUser = {
  email:string,
  password:string,
  phone?:string,
  gender?:string
}

export let version = '2.0.0';