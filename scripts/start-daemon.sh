#!/bin/bash
set -e
set -o pipefail
set -x

docker stop aqualifehk-web-server || true
docker rm aqualifehk-web-server || true
docker run -it -d -p 8080:8080 --name=aqualifehk-web-server aqualifehk-web-image
sleep 2
docker ps | grep aqualifehk-web-server