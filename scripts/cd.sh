#!/bin/bash
set -e
set -o pipefail
set -x

scripts/build-image.sh
aws_scripts/02-upload-image.sh
aws_scripts/03-deploy-image.sh