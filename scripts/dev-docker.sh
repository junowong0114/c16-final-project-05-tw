#!/bin/bash
set -e
set -o pipefail
set -x

scripts/build-image.sh
scripts/start-service.sh