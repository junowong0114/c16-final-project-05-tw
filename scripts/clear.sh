#!/bin/bash
set -e
set -o pipefail
set -x

docker rm -f $(docker ps -a -q) || true
docker volume prune -f
docker-compose down --volumes