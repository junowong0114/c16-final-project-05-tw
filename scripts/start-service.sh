#!/bin/bash
set -e
set -o pipefail
set -x

docker-compose up
docker ps | awk '{print $2}'