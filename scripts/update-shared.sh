#!/bin/bash
set -e
set -o pipefail

cd shared
npm install
npm run build

cd ../aqualifehk-web-server
rm -rf lib
mkdir lib
cp -a ../shared ./lib/shared
npm install

cd ../aqualifehk-react
npm install

cd ..