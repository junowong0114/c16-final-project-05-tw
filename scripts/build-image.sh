#!/bin/bash
set -e
set -o pipefail
set -x

cd shared
# docker build -t aqualifehk-shared-image .
npm install
npm run build

cd ../aqualifehk-web-server
rm -rf lib
mkdir lib
cp -r ../shared ./lib/shared
npm install
npm run update-dev
docker build -t aqualifehk-web-image .

cd ../aqualifehk-react
npm install
